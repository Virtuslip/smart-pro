CREATE proc [KS_DDLControl].[xp_PrgPpGp_svod] (@nmode int=0, @GP int=6566, @PPGP int=7424, @Ver00 varchar(2) = '01', @Ver01 varchar(2) = '03', @OnlyChanges varchar(3) = 'Нет')
as

--exec [KS_DDLControl].[xp_PrgPpGp_svod] @nmode=0, @GP=6566, @PPGP=6773, @Ver00 ='01', @Ver01 = '03', @OnlyChanges = 'Нет'

    create table #temp_resultPPGPChanges
    (
        idppgp int
        , [ППГП] varchar(2000)
        , orderPPGP varchar(2)
        , action varchar(8000)
        , idContent int
        , [Content] varchar(8000)
        , [код_Content] varchar(50)
        , GpDBegin int
        , GpDEnd int
        , [S_gp] varchar(500)
        , [Source] varchar(100)
        , [ОЧГ13] numeric (20,2)
        , [ОЧГ14] numeric (20,2)
        , [ОЧГ15]   numeric (20,2)
        , [ОЧГ16] numeric (20,2)
        , [ОЧГ17] numeric (20,2)
        , [ОЧГ18] numeric (20,2)
        , [ОЧГ19] numeric (20,2)
        , [ОЧГ20] numeric (20,2)
        , [ОЧГ21] numeric (20,2)
        , [ОЧГ22] numeric (20,2)
        , [ОЧГ23] numeric (20,2)
        , [ОЧГ24] numeric (20,2)
        , [ОЧГ25] numeric (20,2)
        , [КодППГП] varchar(50)
        , [ПорядковыйНомерПЗППГП] varchar(10)
        --, [idППГП] varchar(20)
        , [ЕдиницаИзмерения] varchar(200)

        , idppgp_1 int
        , [ППГП_1] varchar(8000)
--			, orderPPGP_1 varchar(2)
--			, action_1 varchar(500)
        , idContent_1 int
        , [Content_1] varchar(8000)
        , [код_Content_1] varchar(20)
        , GpDBegin_1 int
        , GpDEnd_1 int
        , [S_gp_1] varchar(500)
        , [Source_1] varchar(100)
        , [ОЧГ13_1] numeric (20,2)
        , [ОЧГ14_1] numeric (20,2)
        , [ОЧГ15_1]   numeric (20,2)
        , [ОЧГ16_1] numeric (20,2)
        , [ОЧГ17_1] numeric (20,2)
        , [ОЧГ18_1] numeric (20,2)
        , [ОЧГ19_1] numeric (20,2)
        , [ОЧГ20_1] numeric (20,2)
        , [ОЧГ21_1] numeric (20,2)
        , [ОЧГ22_1] numeric (20,2)
        , [ОЧГ23_1] numeric (20,2)
        , [ОЧГ24_1] numeric (20,2)
        , [ОЧГ25_1] numeric (20,2)
        , [КодППГП_1] varchar(50)
        , [ПорядковыйНомерПЗППГП_1] varchar(10)
        --	, [idППГП_1] varchar(20)
        , [ЕдиницаИзмерения_1] varchar(200)


        , documentid int
        , Post varchar(max)

        , id int identity (1,1)
    )


    if @nmode <> -1
        begin
            set nocount on
            set transaction isolation level read uncommitted

            create table #ToTable
            (
                idppgp int
                , [ППГП] varchar(8000)
                , orderPPGP varchar(2)
                , action varchar(500)
                , idContent int
                , [Content] varchar(8000)
                , [код_Content] varchar(50)
                , GpDBegin int
                , GpDEnd int
                , ParamDate int
                , [S_gp] varchar(500)
                , [Source] varchar(100)
                , [ОЧГ13] numeric (20,2)
                , [ОЧГ14] numeric (20,2)
                , [ОЧГ15] numeric (20,2)
                , [ОЧГ16] numeric (20,2)
                , [ОЧГ17] numeric (20,2)
                , [ОЧГ18] numeric (20,2)
                , [ОЧГ19] numeric (20,2)
                , [ОЧГ20] numeric (20,2)
                , [ОЧГ21] numeric (20,2)
                , [ОЧГ22] numeric (20,2)
                , [ОЧГ23] numeric (20,2)
                , [ОЧГ24] numeric (20,2)
                , [ОЧГ25] numeric (20,2)
                , [КодППГП] varchar(50)
                , documentid int
                , Post varchar(max)
                , id int
                , [ПорядковыйНомерПЗППГП] varchar(10)
                --	, [idППГП] varchar(20)
                , [ЕдиницаИзмерения] varchar(200)
            )

            create table #Ver00
            (
                idppgp int
                , [ППГП] varchar(8000)
                , orderPPGP varchar(2)
                , action varchar(500)
                , idContent int
                , [Content] varchar(8000)
                , [код_Content] varchar(50)
                , GpDBegin int
                , GpDEnd int
                , ParamDate int
                , [S_gp] varchar(500)
                , [Source] varchar(100)
                , [ОЧГ13] numeric (20,2)
                , [ОЧГ14] numeric (20,2)
                , [ОЧГ15] numeric (20,2)
                , [ОЧГ16] numeric (20,2)
                , [ОЧГ17] numeric (20,2)
                , [ОЧГ18] numeric (20,2)
                , [ОЧГ19] numeric (20,2)
                , [ОЧГ20] numeric (20,2)
                , [ОЧГ21] numeric (20,2)
                , [ОЧГ22] numeric (20,2)
                , [ОЧГ23] numeric (20,2)
                , [ОЧГ24] numeric (20,2)
                , [ОЧГ25] numeric (20,2)
                , [КодППГП] varchar(50)
                , documentid int
                , Post varchar(max)
                , id int
                , [ПорядковыйНомерПЗППГП] varchar(10)
                --, [idППГП] varchar(20)
                , [ЕдиницаИзмерения] varchar(200)
            )

            exec [KS_DDLControl].[xp_PrgPpGp] @nmode=0, @GP=@GP, @PPGP=@PPGP, @Ver=@Ver00, @ToTable=1
            insert into #Ver00 select * from #ToTable
            delete from #Ver00 where idppgp is null


            delete from #ToTable
            --select * from #Ver00
            create table #Ver01
            (
                idppgp int
                , [ППГП] varchar(8000)
                , orderPPGP varchar(2)
                , action varchar(500)
                , idContent int
                , [Content] varchar(8000)
                , [код_Content] varchar(50)
                , GpDBegin int
                , GpDEnd int
                , ParamDate int
                , [S_gp] varchar(500)
                , [Source] varchar(100)
                , [ОЧГ13] numeric (20,2)
                , [ОЧГ14] numeric (20,2)
                , [ОЧГ15]   numeric (20,2)
                , [ОЧГ16] numeric (20,2)
                , [ОЧГ17] numeric (20,2)
                , [ОЧГ18] numeric (20,2)
                , [ОЧГ19] numeric (20,2)
                , [ОЧГ20] numeric (20,2)
                , [ОЧГ21] numeric (20,2)
                , [ОЧГ22] numeric (20,2)
                , [ОЧГ23] numeric (20,2)
                , [ОЧГ24] numeric (20,2)
                , [ОЧГ25] numeric (20,2)
                , [КодППГП] varchar(20)
                , documentid int
                , Post varchar(max)
                , id int
                , [ПорядковыйНомерПЗППГП] varchar(10)
                --	, [idППГП] varchar(20)
                , [ЕдиницаИзмерения] varchar(200)
            )


            exec [KS_DDLControl].[xp_PrgPpGp] @nmode=0, @GP=@GP, @PPGP=@PPGP, @Ver=@Ver01, @ToTable=1
            insert into #Ver01 select * from #ToTable
            delete from #Ver01 where idppgp is null
--select * from #Ver01
            insert into #temp_resultPPGPChanges
            select
--			  isnull(a.idppgp    , b.idppgp    )
--		    , isnull(a.[ППГП]    , b.[ППГП]    )

                a.idppgp
                 ,a.[ППГП]
                 , isnull(a.orderPPGP , b.orderPPGP )
                 , isnull(a.action    , b.action    )
--,a.orderPPGP
--,a.action
                 , a.idContent
                 , a.[Content]
                 , a.[код_Content]
                 , a.GpDBegin
                 , a.GpDEnd
                 , a.[S_gp]
                 , a.[Source]
                 , a.[ОЧГ13]
                 , a.[ОЧГ14]
                 , a.[ОЧГ15]
                 , a.[ОЧГ16]
                 , a.[ОЧГ17]
                 , a.[ОЧГ18]
                 , a.[ОЧГ19]
                 , a.[ОЧГ20]
                 , a.[ОЧГ21]
                 , a.[ОЧГ22]
                 , a.[ОЧГ23]
                 , a.[ОЧГ24]
                 , a.[ОЧГ25]
                 , a.[КодППГП]
                 , a.[ПорядковыйНомерПЗППГП]
                 --, a.[idППГП]
                 , a.[ЕдиницаИзмерения]

                 , b.idppgp
                 , b.[ППГП]
--            , b.orderPPGP
--            , b.action
                 , b.idContent
                 , b.[Content]
                 , b.[код_Content]
                 , b.GpDBegin
                 , b.GpDEnd
                 , b.[S_gp]
                 , b.[Source]
                 , b.[ОЧГ13]
                 , b.[ОЧГ14]
                 , b.[ОЧГ15]
                 , b.[ОЧГ16]
                 , b.[ОЧГ17]
                 , b.[ОЧГ18]
                 , b.[ОЧГ19]
                 , b.[ОЧГ20]
                 , b.[ОЧГ21]
                 , b.[ОЧГ22]
                 , b.[ОЧГ23]
                 , b.[ОЧГ24]
                 , b.[ОЧГ25]
                 , b.[КодППГП]
                 , b.[ПорядковыйНомерПЗППГП]
                 --, b.[idППГП]
                 , b.[ЕдиницаИзмерения]

                 , isnull(a.documentid,b.documentid)
                 , isnull(a.Post,b.Post)


            from
                #Ver00 a full join #Ver01 b on
                            isnull(a.orderPPGP    , '') = isnull(b.orderPPGP    ,'')
                        and isnull(a.action       , '') = isnull(b.action     ,'')
                        and isnull(a.[Content]    , '') = isnull(b.[Content]     ,'')
                        and isnull(a.[код_Content], '') = isnull(b.[код_Content] ,'')
                        --and isnull(a.GpDBegin    , 0) = isnull(b.GpDBegin   , 0)
                        --and isnull(a.GpDEnd      , 0) = isnull(b.GpDEnd     , 0)
                        --and isnull(a.[S_gp]      ,'') = isnull(b.[S_gp]     ,'')
                        and isnull(a.[Source]    ,'') = isnull(b.[Source]   ,'')
            --			and isnull(a.[ОЧГ13]     , 0) = isnull(b.[ОЧГ13]    , 0)
--			and isnull(a.[ОЧГ14]     , 0) = isnull(b.[ОЧГ14]    , 0)
--			and isnull(a.[ОЧГ15]       , 0) = isnull(b.[ОЧГ15]      , 0)
--			and isnull(a.[ОЧГ16]      , 0) = isnull(b.[ОЧГ16]     , 0)
--			and isnull(a.[ОЧГ17]      , 0) = isnull(b.[ОЧГ17]     , 0)
--			and isnull(a.[ОЧГ18]  , 0) != isnull(b.[ОЧГ18]  , 0)
--		    and isnull(a.[ОЧГ19]  , 0) != isnull(b.[ОЧГ19]  , 0)
--		    and isnull(a.[ОЧГ20]  , 0) != isnull(b.[ОЧГ20]  , 0)
--          and isnull(a.[ОЧГ21]  , 0) != isnull(b.[ОЧГ21]  , 0)



            delete from #temp_resultPPGPChanges where [код_Content] is null and [код_Content_1] is null and action in (select distinct action from #temp_resultPPGPChanges where [код_Content] is not null or [код_Content_1] is not null)

            delete from #temp_resultPPGPChanges where orderPPgp ='09' and [ОЧГ13]=0 and [ОЧГ14]=0 and [ОЧГ15]=0 and [ОЧГ16]=0 and [ОЧГ17]=0 and [ОЧГ13_1] is null


        end


    if @OnlyChanges = 'Да'
        begin
            select
                *
            from
                #temp_resultPPGPChanges
            where
               --idppgp is null
                    isnull([Код_Content],'') != isnull([Код_Content_1],'')
               or isnull([Content] ,'')  != isnull([Content_1]    ,'')
--		or isnull(GpDBegin , 0)  != isnull(GpDBegin_1 , 0)
--		or isnull(GpDEnd   , 0)  != isnull(GpDEnd_1   , 0)
--		or isnull([S_gp]   ,'')  != isnull([S_gp_1]   ,'')
--		or isnull([Source] ,'')  != isnull([Source_1] ,'')
               or isnull([ОЧГ13]  , 0)  != isnull([ОЧГ13_1]  , 0)
               or isnull([ОЧГ14]  , 0)  != isnull([ОЧГ14_1]  , 0)
               or isnull([ОЧГ15]  , 0)  != isnull([ОЧГ15_1]  , 0)
               or isnull([ОЧГ16]  , 0)  != isnull([ОЧГ16_1]  , 0)
               or isnull([ОЧГ17]  , 0)  != isnull([ОЧГ17_1]  , 0)
               or isnull([ОЧГ18]  , 0)  != isnull([ОЧГ18_1]  , 0)
               or isnull([ОЧГ19]  , 0)  != isnull([ОЧГ19_1]  , 0)
               or isnull([ОЧГ20]  , 0)  != isnull([ОЧГ20_1]  , 0)
               or isnull([ОЧГ21]  , 0)  != isnull([ОЧГ21_1]  , 0)
               or isnull([ОЧГ22]  , 0)  != isnull([ОЧГ22_1]  , 0)
               or isnull([ОЧГ23]  , 0)  != isnull([ОЧГ23_1]  , 0)
               or isnull([ОЧГ24]  , 0)  != isnull([ОЧГ24_1]  , 0)
               or isnull([ОЧГ25]  , 0)  != isnull([ОЧГ25_1]  , 0)
               or isnull([ПорядковыйНомерПЗППГП]  , 0)  != isnull([ПорядковыйНомерПЗППГП_1]  , 0)


        end else begin
        select * from #temp_resultPPGPChanges
    end