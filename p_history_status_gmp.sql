alter PROC [KS_DDLControl].[p_history_status_gmp] (
	@dateStart DateTime = '20190906',
	@dateEnd DateTime = '20190920',
	@nmode INT = 0
	)
AS

--������� ������ ���������
CREATE TABLE #status_bp 
(checked int, link INT, code varchar(100),  name VARCHAR (1000), is_sys int, is_button int, t_icon varchar(2000), t_orders int, set_comment int,action_name varchar (50) );
insert into #status_bp exec dbo.list @cObjCode='DICTIONARY_STEP_STATUS'

--��������� ���������� ���������
select 
	ID, 
	ID_UP, 
	AT_3287 as DateDoc,
	AT_3235.value('(/file/name)[1]', 'varchar(200)') as FileName,
	'1279_3268_3235_' + convert(varchar, ID) as FileID,
	CL_3292
into #docsUE
from KS_DDLControl.DS_1279_3268
where CL_3292 = 3

--��������� ���������� ��������
select 
	ID, 
	ID_UP, 
	AT_3287 as DateDoc,
	AT_3235.value('(/file/name)[1]', 'varchar(200)') as FileName,
	'1279_3268_3235_' + convert(varchar, ID) as FileID
into #docsUF
from KS_DDLControl.DS_1279_3268
where CL_3292 = 4


--������ ����������
select 
	gmp.ID,
	bp_hist.date_doc,
	bp_hist.bpms_object,
	status.name
into #status_end
from KS_DDLControl.DS_1279_3189 gmp
left join bpms_document_router_history bp_hist on gmp.ID = bp_hist.link_doc
left join #status_bp status on status.link = bp_hist.step_status
where bp_hist.bpms_object = 669 and bp_hist.date_doc >= @dateStart and bp_hist.date_doc <= @dateEnd

--select * from #status_end
--select * from bpms_document_router_history bp_hist


--����������� ���
select 
	isp.ID,
	ver.AT_3201 as Name
into #isp_gmp
from KS_DDLControl.CL_1280_3226 isp
left join KS_DDLControl.CL_1280_3231 ver on ver.ID_UP = isp.ID


--���
select 
	gmp.ID,
	gmp.AT_2537,
	gmp.AT_2845,
	gmp.AT_3303
into #GMP
from KS_DDLControl.DS_957_2475 gmp
where DOCUMENTID = 220

select 
	gmp.ID, 
	gmp.DS_3238,
	bp_hist.bpms_object,
	CONVERT (date,convert(char(8),gmp.AT_3190)) as [���� ���������], 
	gp.AT_2524 as [��� ���������],
	gpName.AT_2526 as [������������], 	 
	gmp_source.AT_2845 as [������],
	gmp_s.AT_3303 as [����������],
	bp_hist.date_doc as [���� ����������� � ��]
	,CONVERT (date,convert(char(8),docs.DateDoc)) as [���� ���������� ��]
	--,docs.DateDoc as [���� ���������� ��]
	,docs.FileName as [��� ����� ��]
	,docs.FileID as [ID ����� ��]
	,CONVERT (date,convert(char(8),docs_uf.DateDoc)) as [���� ���������� ��]
	--,docs_uf.DateDoc as [���� ���������� ��]
	,docs_uf.FileName as [��� ����� ��]
	,docs_uf.FileID as [ID ����� ��]
	,status_end.date_doc as [���� ����������]
	,status_end.name as [������ ����������]
	,isp.Name as [������������ �����������]
	,(datediff(day, bp_hist.date_doc, status_end.date_doc) - datediff(week, bp_hist.date_doc, status_end.date_doc)*2) as [������� ���]
	,(datediff(day, bp_hist.date_doc, CONVERT (date,convert(char(8),docs_uf.DateDoc))) - datediff(week, bp_hist.date_doc, CONVERT (date,convert(char(8),docs_uf.DateDoc)))*2)as [������� ��� c ��]
	,(datediff(day, bp_hist.date_doc, CONVERT (date,convert(char(8),docs.DateDoc))) - datediff(week, bp_hist.date_doc, CONVERT (date,convert(char(8),docs.DateDoc)))*2)as [������� ��� c ��]
	
from KS_DDLControl.DS_1279_3189 gmp
left join bpms_document_router_history bp_hist on gmp.ID = bp_hist.link_doc
left join #docsUE docs on docs.ID_UP = gmp.ID
left join #docsUF docs_uf on docs_uf.ID_UP = gmp.ID
left join #status_end status_end on status_end.ID = gmp.ID
left join KS_DDLControl.DS_1279_3270 gp_source on gmp.ID = gp_source.ID_UP
left join KS_DDLControl.CL_956_2470 gp on gp_source.CL_3236 = gp.ID
left join (select id_up, AT_2526, max(isnull(at_3398, 20000101)) as dateStart from ks_ddlcontrol.cl_956_3452 group by id_up, AT_2526) gpName on (gp.id = gpName.id_up and gpName.dateStart <= gmp.at_3190)
left join KS_DDLControl.DS_957_2475 gmp_source on gmp.DS_3238 = gmp_source.ID
left join #GMP gmp_s on gmp_s.AT_2845 = gmp_source.AT_2845 and gmp_s.AT_2537 = gpName.AT_2526
left join #isp_gmp isp on gmp.CL_3234 = isp.ID
where (bp_hist.bpms_object = 666) and  bp_hist.date_doc >= @dateStart and bp_hist.date_doc <= @dateEnd
order by gp.AT_2524