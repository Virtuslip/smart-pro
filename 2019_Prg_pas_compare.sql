alter proc [KS_DDLControl].[2019_Prg_pas_compare](@nmode int=0, @gp int=109368, @gp1 int =108942)
    as
    begin
        /*
        declare @gp int
                ,@gp1 int
        [KS_DDLControl].[2019_Prg_pas_compare] @gp=109726,@gp1=108459
        select @gp=109726
                ,@gp1=108459
        */
/*
select * from ks_ddlcontrol.ds_957_2475
where at_2537='Развитие транспортной системы Липецкой области '
		and at_2845
*/
        --id=109726
        if object_id('tempdb..#temp_result') is not null drop table #temp_result
        create table #temp_result
        (
            str_id        int,
            str_order     varchar(10),
            str_name      varchar(max),
            str_name_ei   varchar(100),
            str_part      varchar(10),
            content       varchar(max),
            content_metod varchar(max),
            content_src   varchar(max),
            descr         varchar(50),
            chapter       varchar(5)
        )

--------------------------------------------
        create table #psp
        (
            str_gp   varchar(30),
            str_name varchar(500),
            str_id   int,
            content  varchar(max)
        )

        insert into #psp exec [KS_DDLControl].[2019_Prg_pasport] @gp=@gp

        create table #psp1
        (
            str_gp   varchar(30),
            str_name varchar(500),
            str_id   int,
            content  varchar(max)
        )

        insert into #psp1 exec [KS_DDLControl].[2019_Prg_pasport] @gp=@gp1

        select --isnull(a.str_gp,b.str_gp)		 str_gp
            isnull(a.str_name, b.str_name)                                                str_name
             , isnull(a.str_id, b.str_id)                                                 str_id
             , isnull(a.content, b.content)                                               content
             , case
                   when a.content is null then 'del'
                   else case
                            when b.content is null then 'add'
                            else case when a.content != b.content then 'edit' end end end descr
             , '1'                                                                        chapter
        into #temp_pasport
        from #psp a
                 full join #psp1 b on a.str_id = b.str_id
             --and a.content=b.content
        where b.str_id is null
           or a.str_id is null
           or a.content != b.content

        ----------------------------------------------------------------------------
/*
declare @gp int
		,@gp1 int

select @gp=109726
		,@gp1=108459
		*/
        create table #texting1
        (
            str_order varchar(20),
            npa       varchar(500),
            part      int
        )

        insert into #texting1
            exec [KS_DDLControl].[2019_Prg_Texting] @gp=@gp

        create table #texting2
        (
            str_order varchar(20),
            npa       varchar(500),
            part      int
        )

        insert into #texting2
            exec [KS_DDLControl].[2019_Prg_Texting] @gp=@gp1

        select isnull(a.str_order, b.str_order)                                   str_order
             , isnull(a.npa, b.npa)                                               content
             , isnull(a.part, b.part)                                             str_part
             , case
                   when a.npa is null then 'del'
                   else case
                            when b.npa is null then 'add'
                            else case when a.npa != b.npa then 'edit' end end end descr
             , '2'                                                                chapter
        into #temp_text
        from #texting1 a
                 full join #texting2 b on a.str_order = b.str_order
            and a.part = b.part
        where b.part is null
           or a.part is null
           or b.str_order is null
           or a.str_order is null
           or a.npa != b.npa

        ----------------------------------------------------------------------------
/*
declare @gp int
		,@gp1 int

select @gp=109726
		,@gp1=108459
	*/
        create table #ind1
        (
            str_order     varchar(20),
            str_name      varchar(max), --индиктор
            str_name_ei   varchar(100), --ед. изм
            content_metod varchar(max), --метод
            content_src   varchar(max)  --источник
        )

        insert into #ind1
            exec [KS_DDLControl].[2019_Prg_Indikators] @gp=@gp

        create table #ind2
        (
            str_order     varchar(20),
            str_name      varchar(max) --индиктор
            ,
            str_name_ei   varchar(100) --ед. изм
            ,
            content_metod varchar(max) --метод
            ,
            content_src   varchar(max) --источник
        )

        insert into #ind2
            exec [KS_DDLControl].[2019_Prg_Indikators] @gp=@gp1


        select isnull(a.str_order, b.str_order)                  str_order
             , isnull(a.str_name, b.str_name)                    str_name
             , isnull(a.str_name_ei, b.str_name_ei)              str_name_ei
             , isnull(a.content_metod, b.content_metod)          content_metod
             , isnull(a.content_src, b.content_src)              content_src
             --	,isnull(a.part,b.part) 		str_id
             , case
                   when isnull(a.content_metod, '') + isnull(a.content_src, '') = '' then 'del'
                   else case
                            when isnull(b.content_metod, '') + isnull(b.content_src, '') = '' then 'add'
                            else case
                                     when isnull(a.content_metod, '') + isnull(a.content_src, '') !=
                                          isnull(b.content_metod, '') + isnull(b.content_src, '')
                                         then 'edit' end end end descr
             , '3'                                               chapter
        into #temp_ind
        from #ind1 a
                 full join #ind2 b on a.str_order = b.str_order
            and isnull(a.str_name_ei, '') = isnull(b.str_name_ei, '')
        where isnull(a.content_metod, '') + isnull(a.content_src, '') = ''
           or isnull(b.content_metod, '') + isnull(b.content_src, '') = ''
           or b.str_order is null
           or a.str_order is null
           or isnull(a.content_metod, '') + isnull(a.content_src, '') !=
              isnull(b.content_metod, '') + isnull(b.content_src, '')

        ---------------------

--select concat('asdfa','asdfad')
        if object_id('tempdb..#temp_pasport') is not null
            begin
                insert into #temp_result( str_id
                                        , str_name
                                        , content
                                        , descr
                                        , chapter)
                select str_id, str_name, content, descr, chapter
                from #temp_pasport
            end
        if object_id('tempdb..#temp_text') is not null
            begin
                insert into #temp_result( str_order
                                        , str_part
                                        , content
                                        , descr
                                        , chapter)
                select str_order, str_part, content, descr, chapter
                from #temp_text
            end
        if object_id('tempdb..#temp_ind') is not null
            begin
                insert into #temp_result( str_order
                                        , str_name
                                        , str_name_ei
                                        , content_metod
                                        , content_src
                                        , descr
                                        , chapter)
                select str_order, str_name, str_name_ei, content_metod, content_src, descr, chapter
                from #temp_ind
            end

        --     select * from #temp_pasport
--     select * from #temp_ind
--     select * from #temp_text
--     select * from #ind2
--     select * from #ind1
--     select * from #texting2
--     select * from #texting1
--     select * from #psp1
--     select * from #psp

        drop table #temp_pasport
        drop table #temp_ind
        drop table #temp_text
        drop table #ind2
        drop table #ind1
        drop table #texting2
        drop table #texting1
        drop table #psp1
        drop table #psp


        select * from #temp_result
    end