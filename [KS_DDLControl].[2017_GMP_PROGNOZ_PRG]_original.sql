CREATE proc [KS_DDLControl].[2017_GMP_PROGNOZ_PRG] (@nmode int=0)
as

create table #temp_result 
			( 
			 id int 
			, parentid int
			, code varchar(500)
			, naim varchar(max)
			, date_from varchar(20)
			, date_to varchar(20)
			, isp_id int
			, prg_id int
			, isp_naim varchar(2000)
			, typegp varchar(500)
			, keyprg int
			, sequencenumber varchar (10)
			, version  varchar (2)
			, parentidconstant int
			, compositeid int
			, status int
			, article varchar (10)
			, stkod varchar (10)
			)  

if @nmode <> -1 
begin
set nocount on
set transaction isolation level read uncommitted

-- ��� ��������� ������� �������
/*
declare @nmode int, @GP int, @Ver varchar(2), @Ved int, @Filtr varchar(1000) 
select  @nmode =0, @GP =6559, @Ver ='01', @Ved =12631, @Filtr ='___���___' 
*/


declare @GpDocID int
declare @aDateInt int
--select  @aDateInt = convert(varchar,getdate(),112)
select  @aDateInt = convert(varchar,dbo.getWorkDate(),112)
	------------
	--������� --
	------------
	Declare @user_type int
	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()

	Declare @GmpPermission int
	select 
		@GmpPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 956  --����������� ���

	Declare @VedPermission int
	select 
		@VedPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 864  --���������� ��������� 

	---------------------------------------------------------KS_DDLControl.CL_962_2518
	--������� ���������� �������� ����������� ������������ --
	---------------------------------------------------------
	declare @Cl table (id int, [������������] varchar(2000), [DBegin] int, [���] varchar(20))
	insert into @Cl
	select
		  a.id
		, isnull(AT_2641, AT_2526) as [��]
		, b.AT_2642 as [DBegin]
		, a.AT_2524
	from
        	KS_DDLControl.CL_956_2470 a
		left join (select * from KS_DDLControl.CL_956_2615 where AT_2642 <= @aDateInt) b on a.id = b.id_up
		where a.AT_2639<>06 
		--Where a.ID=6559
/*	
		---------------------------------------------------------
	--������� ���������� �������� ����������� ����������� ������� --
	---------------------------------------------------------
	declare @C2 table (id int, [������������] varchar(2000), [DBegin] int, [���] varchar(20))
	insert into @C2
	select
		  a.id
		, a.AT_2565 as [��]
		, a.AT_2835 as [DBegin]
		, a.AT_2564
	from
        	KS_DDLControl.CL_962_2518 a
*/			
--select * from @Cl
--select * from @C2
	select tab2.id, tab2.[������������] as [��], tab2.[���] into #sprGp from
	(select id, max(DBegin) as DBegin from @Cl group by id) tab1
	inner join 
	(select id, DBegin, [������������], [���] from @Cl) tab2
	on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)
--	UNION 
--	select id, [������������], [���] from @C2
--select * from #sprGp
	--�������� ��������� �� ������� ����������� ������
	if isnull(@GmpPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprGp where id not in (select CLID from KS_DDLControl.CLA_956 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

		-------------------------------------------
		--������� �������� ����������� ��������� --
		-------------------------------------------
		
	select a.id, a1.AT_1797, a1.AT_1796, a1.AT_1930
	into #sprVed 
	from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
	UNION
	select b.id, b1.AT_3201, b1.AT_3200, b1.AT_3202
	from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_1280_3226 b on b.CL_3232 = a.id
	left join KS_DDLControl.CL_1280_3231 b1 on b.id = b1.id_up
	
		--�������� ��������� �� ������� ����������� ������
		if isnull(@VedPermission,0) > 0 and @user_type != 4
		begin
			delete from #sprVed where id not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
		end 
        
        
     
        
		-------------------------------------------------
		-- ������� ���������� ���������� ��������� ��� --2646 - �������������

		-------------------------------------------------
		
--		select * from #sprGp
insert into #temp_result
select 	a.id 
        --,a.docUp_id

    ,case when a.type_m = 13 then a.docUp_id_m 
        	when a.doc_type_id = 221 then a.docUp_id_ppr	else a.docup_id end 
			  
		, gp.[���]
		, gp.[��]
		, dbo.int_to_date(a.date_start)
		, dbo.int_to_date(a.date_end)
		, a.Isp_id
		, a.prg_id 
		, ved.AT_1797
		,case a.type_m when 13 then '�������� �����������' else a.doc_type_name end
		,1
		,a.AT_2837 as sequencenumber
		,a.version
		,a.parentidconstant
		--,LTRIM(STR(prg_id,10)) + LTRIM(STR(parentidconstant,10))
		,case when parentidconstant is Null then prg_id else (LTRIM(STR(prg_id,10)) + LTRIM(STR(parentidconstant,10))) end
		,a.status 
		,a.AT_3388
		,a.AT_1801
		
		
from	
		v_gmp_doc_v v 
		inner join v_gmp_doc a on a.prg_id=v.ppgp_id   and a.version = v. version
		inner join #sprVed ved on a.Isp_id=ved.id
		inner join #sprGp gp on a.prg_id=gp.id
		
		
where a.doc_type_name in ('���������'
			,'������������'
			,'����'
			,'�����������'
			,'������'
			
		) 
			
			
			
			--and 
--			,'����������� �������') and 
--		and	a.id=23878 or a.docUp_id=23878 or a.docUp_id in 
--(select id from v_gmp_doc where docUp_id =23878) or
--a.docUp_id in (select id from v_gmp_doc where docUp_id in
--(select id from v_gmp_doc where docUp_id =23878)) or
--a.docUp_id in (select id from v_gmp_doc where docUp_id in
--(select id from v_gmp_doc where docUp_id in
--(select id from v_gmp_doc where docUp_id =23878)))

--group by
--a.id 
--		,a.type_m 
--		,a.docUp_id_m 
--		,a.docUp_id
--		,a.doc_type_id 
--		,a.docUp_id_ppr	
--		, gp.[���]
--		, gp.[��]
--		, a.date_start
--		, a.date_end
--		, a.Isp_id
--		, a.prg_id
--		, ved.AT_1797
--		,a.doc_type_name 
--		,a.AT_2837

order by case doc_type_name
			when '���������' then 1
			when '������������' then 4
			when '����'		then 2
			when '�����������'	then 5
			when '������'	then 3
			end
--case  when a.doc_type_id = 220 then 1	
--			   else isnull(a.docup_id,a.docUp_id_m)  end 	
end

--select * from #sprVed 
--select * from #sprGp
select * from #temp_result