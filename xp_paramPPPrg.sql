
alter proc [KS_DDLControl].[xp_ParamPPPrg] (@nmode int=0, @GP int = 14)
as

	
/*
declare @ParamDate datetime
set @ParamDate = getdate()


������ �������: exec [KS_DDLControl].[xp_ParamPPPrg] @nmode=0, @GP = 14
*/
	
create table #temp_result (
			  idppgp int
			, ppgp varchar(2000)
			)
	
	
if @nmode <> -1 
begin
	set nocount on
	set transaction isolation level read uncommitted	

	declare @aDateInt int
	select  @aDateInt = convert(varchar,Getdate(),112) 
	------------
	--������� --
	------------
	Declare @user_type int
	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()

	Declare @GmpPermission int
	select 
		@GmpPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 956  --����������� ���

	---------------------------------------------------------
	--������� ���������� �������� ����������� ������������ --
	---------------------------------------------------------
	declare @Cl table (id int, [����.���] varchar(100), [������������] varchar(2000), [DBegin] int)
	insert into @Cl
	select
		  a.id
		, AT_2524 as [����.���]
		, isnull(AT_2641, AT_2526) as [����]
		, b.AT_2642 as [DBegin]
	from
        	KS_DDLControl.CL_956_2470 a
		left join (select * from KS_DDLControl.CL_956_2615 where AT_2642 <= @aDateInt) b on a.id = b.id_up
		left join (select * from ks_ddlcontrol.cl_956_3452 where at_3398 <= @aDateInt) � on a.id = �.id_up 
	where
		 AT_2639 = '02'
--select * from @Cl
	select tab2.id, tab2.[����.���] as [����.���],  tab2.[������������] as [����] into #sprPpGp from
	(select id, max(DBegin) as DBegin from @Cl group by id) tab1
	inner join 
	(select id, DBegin, [������������], [����.���] from @Cl) tab2
	on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)
--select * from #sprPpGp
	--�������� ��������� �� ������� ����������� ������
	if isnull(@GmpPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprPpGp where id not in (select CLID from KS_DDLControl.CLA_956 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

	-------------------------------------------------
	-- ������� ���������� ���������� ��������� ��� --  and AT_2529 <= @aDateInt and (at_2646 is null or at_2646 not in (select id from KS_DDLControl.DS_957_2475 where AT_2529 <= @aDateInt))) a --����
	insert into #temp_result
	select distinct
		  d.id
		, a.AT_2837+'. '+d.[����]
	from
		(select * from KS_DDLControl.DS_957_2475 where documentid = 221 ) a 
			inner join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
		inner join KS_DDLControl.DS_957_2475 a1 on a1.id = b.DS_2654 and a1.CL_2640 = @GP 
		inner Join #sprPpGp  d on a.CL_2640 = d.id   		     --�_������������  
	where
	
		d.[����.���] is not null	
		
end
select * from #temp_result