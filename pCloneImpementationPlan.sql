alter proc [KS_DDLControl].[pCloneImpementationPlan](@GP int=6567, @Ver varchar(10)='11', @VerTo varchar(10)='16', @dateDocEvent varchar(20) = '24.06.2019', @dateTarget varchar(20) = '15.11.2019')
    as
    begin
        set nocount on
        set transaction isolation level read uncommitted
        declare
            @docIdCe int --Код документа контрольные события
        set @docIdCe = 337
        declare
            @UnitGMP uniqueidentifier
        set @UnitGMP = newid()

        /*declare
            @aDateInt int
        select @aDateInt = convert(varchar, dbo.getworkdate(), 112)*/

        declare
            @aDateInt varchar(8)
        SET @aDateInt =
                    SUBSTRING(@dateTarget, 7, 4) + SUBSTRING(@dateTarget, 4, 2) + SUBSTRING(@dateTarget, 1, 2)

        declare
            @dateEvent varchar(8)
        SET @dateEvent =
                    SUBSTRING(@dateDocEvent, 7, 4) + SUBSTRING(@dateDocEvent, 4, 2) + SUBSTRING(@dateDocEvent, 1, 2)

        begin try
            begin transaction
                --План реализации
                select p.*, newid() as new_id, gmp.CL_2640
                into #planSource
                from ks_ddlcontrol.ds_1279_3189 p
                         inner join ks_ddlcontrol.ds_1279_3270 gp on (gp.id_up = p.id)
                         inner join ks_ddlcontrol.ds_957_2475 gmp on (p.ds_3238 = gmp.id)
                where gmp.at_2845 = @Ver
                  and gp.cl_3236 = @GP
                  and p.at_3190 = @dateEvent

--                 Документ План реализации
                insert into ks_ddlcontrol.ds_1279_3189
                ( documentid
                , unit
                , guid
                , cl_3164
                , at_3190
                , at_3267
                , at_3268
                , at_3192
                , at_3215
                , at_3191
                , at_3193
                , at_3194
                , at_3195
                , at_3196
                , at_3197
                , at_3198
                , at_3230
                , cl_3233
                , cl_3234
                , ds_3238)
                select s.documentid
                     , @UnitGMP
                     , s.new_id
                     , 8
                     , @aDateInt -- Дата документа
                     , s.at_3267
                     , s.at_3268
                     , s.at_3192
                     , s.at_3215
                     , s.at_3191
                     , s.at_3193
                     , s.at_3194
                     , s.at_3195
                     , s.at_3196
                     , s.at_3197
                     , s.at_3198
                     , s.at_3230
                     , s.cl_3233
                     , s.cl_3234
                     , gmp_new.id
                from #planSource s
                         left join ks_ddlcontrol.ds_957_2475 gmp_new
                                    on (s.CL_2640 = gmp_new.CL_2640 and gmp_new.at_2845 = @VerTo)

                --Список вставленных записей
                select p.*
                into #inserted_plan
                from ks_ddlcontrol.ds_1279_3189 p
                         inner join #planSource s on s.new_id = p.guid

                --Расчет расходов
                insert into ks_ddlcontrol.ds_1279_3196( id_up
                                                      , unit
                                                      , guid
                                                      , cl_3176
                                                      , cl_3177
                                                      , cl_3178
                                                      , cl_3170
                                                      , cl_3171
                                                      , cl_3172
                                                      , cl_3173
                                                      , cl_3174
                                                      , cl_3175
                                                      , m_3182
                                                      , m_3179
                                                      , m_3180
                                                      , m_3181
                                                      , m_3183
                                                      , m_3184
                                                      , m_3185
                                                      , m_3186
                                                      , m_3187
                                                      , m_3188
                                                      , m_3189
                                                      , m_3214
                                                      , m_3285
                                                      , m_3286)
                select ip.id
                     , @UnitGMP
                     , newid()
                     , cc.cl_3176
                     , cc.cl_3177
                     , cc.cl_3178
                     , cc.cl_3170
                     , cc.cl_3171
                     , cc.cl_3172
                     , cc.cl_3173
                     , cc.cl_3174
                     , cc.cl_3175
                     , cc.m_3182
                     , cc.m_3179
                     , cc.m_3180
                     , cc.m_3181
                     , cc.m_3183
                     , cc.m_3184
                     , cc.m_3185
                     , cc.m_3186
                     , cc.m_3187
                     , cc.m_3188
                     , cc.m_3189
                     , cc.m_3214
                     , cc.m_3285
                     , cc.m_3286
                from ks_ddlcontrol.ds_1279_3196 cc
                         inner join #planSource ps on ps.id = cc.id_up
                         inner join #inserted_plan ip on ip.guid = ps.new_id

                --ГП
--                insert into ks_ddlcontrol.ds_1279_3270 ( id_up
--                                                        , unit
--                                                        , guid
--                                                         , cl_3236)
                select ip.id
                     , @UnitGMP
                     , newid()
                     , a.cl_3236
                from ks_ddlcontrol.ds_1279_3270 a
                         inner join #planSource ps on ps.id = a.id_up
                         inner join #inserted_plan ip on ip.guid = ps.new_id

                                 select * from #planSource
--                 select * from #inserted_plan
            --commit transaction
             rollback transaction
        end try
        begin catch
            declare
                @error_message varchar(4000), @error_severity int, @error_state int, @error_line int
            select @error_message = ERROR_MESSAGE(),
                   @error_severity = ERROR_SEVERITY(),
                   @error_state = ERROR_STATE(),
                   @error_line = ERROR_LINE()
            set @error_message = @error_message + '%s' + '%d'
            raiserror (@error_message, @error_severity, @error_state, N' Ошибка в строке: ', @error_line)
            rollback transaction
        end catch
    end