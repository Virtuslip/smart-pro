alter PROC [KS_DDLControl].[xp_GPpril7_work_new](@nmode INT = 0
    , @GP INT = 6572 --6570 --6559
    , @Ver VARCHAR(2) = '18'
    , @Data_n DateTime = '20190101'
    , @Data_ks DateTime = '20191231'
    , @ToTable int=0)
    AS
    --Пример запуска: exec [KS_DDLControl].[xp_GPpril7_work_new] @nmode=0, @GP=6574, @Ver = '02', @Data_n  = '20170504', @Data_ks  = '20170504'
--План работ:
--Выводить все КБК мероприятия - включить КБК в группу RDL контента
--Добавить в заголовок всех групп итоги по исполнителям(ГРБС)


    CREATE TABLE #temp_result_Gppril6
    (
        documentid         INT,
        NAME               VARCHAR(50),
        Doc                VARCHAR(2000),
        ДатаНачала         VARCHAR(50) --datetime
        ,
        ДатаОконч          VARCHAR(50),
        ДатаНаступления    VARCHAR(50) --1
        ,
        [KBK_Ved]          VARCHAR(3),
        [KBK_Div]          VARCHAR(4),
        [KBK_CS]           VARCHAR(10),
        [KBK_VR]           VARCHAR(3),
        [KBK_ITEM]         VARCHAR(3),
        [KBK_ADD_BK]       VARCHAR(10)
        --,[ОЧГ13] NUMERIC(20, 4)
        --,[ОЧГ14] NUMERIC(20, 4)
        --,[ОЧГ15] NUMERIC(20, 4)
        --,[ОЧГ16] NUMERIC(20, 4)
        --,[ОЧГ17] NUMERIC(20, 4)
        --,[ОЧГ18] NUMERIC(20, 4)
        --,[ОЧГ19] NUMERIC(20, 4)
        --,[ОЧГ20] NUMERIC(20, 4)
        --,[ОЧГ21] NUMERIC(20, 4)
        ,
        [Isp]              VARCHAR(2000),
        DocId              INT,
        Num                VARCHAR(50) --Номерв разрезе групп 01.01.01.02
        ,
        Post               VARCHAR(max),
        id                 INT identity (1, 1),
        [ОЧГ17фед]         NUMERIC(20, 4),
        [ОЧГ17обл]         NUMERIC(20, 4),
        [ОЧГ17итог]        NUMERIC(20, 4)
        --,[КС_показатель] VARCHAR(100)
        ,
        [КС_показатель]    NUMERIC(20, 2),
        ДатаДокумента      VARCHAR(50)
        --,[БР] NUMERIC(20, 2)
        ,
        [КС_ЕдИзмерения]   VARCHAR(50) --11.07
        ,
        [Порядковый номер] VARCHAR(50),
        serial             int,
        ПорядковыйНомерКС  VARCHAR(50)
        --,CL_3164 VARCHAR(100)
        ,
        Примечание         VARCHAR(2000),
        Файл               VARCHAR(100),
        ИмяФайла           VARCHAR(2000),
    )
    DECLARE
        @n varchar(50)
    DECLARE
        @nm varchar(50)
    DECLARE
        @nmb varchar(50)
    DECLARE
        @tmp varchar(50)
    DECLARE
        @tm integer
    DECLARE
        @cnt integer
    DECLARE
        @docmid integer
    DECLARE
        @last integer
    DECLARE
        @new integer
    DECLARE
        @rzn integer
    DECLARE
        @i integer
    DECLARE
        @tmpr varchar(50)
    set @cnt = 0
    set @new = 0
    set @last = 0
    set @docmid = 0
    IF @nmode <> - 1
        BEGIN
            SET NOCOUNT ON
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

            DECLARE
                @aDateInt INT
            --
--	--SELECT @aDateInt = convert(VARCHAR, getdate(), 112)
            select @aDateInt = convert(varchar, dbo.getWorkDate(), 112)

            ------------
            --Доступы --
            ------------
            DECLARE
                @user_type INT

            SELECT @user_type = user_type -- если user_type = 4, то это администратор, для него доступы проверять не надо
            FROM s_users
            WHERE loginame = suser_sname()

            DECLARE
                @GmpPermission INT

            SELECT @GmpPermission = AllowPermission --Признак назначать права
            FROM dbo.ENTITY
            WHERE ID = 956 --Справочники ГМП

            DECLARE
                @VedPermission INT

            SELECT @VedPermission = AllowPermission --Признак назначать права
            FROM dbo.ENTITY
            WHERE ID = 864
            --Справочник Ведомства

            ---------------------------------------------------------
            --Выборка актуальных значений справочника Госпрограммы --
            ---------------------------------------------------------
            DECLARE
                @Cl TABLE
                    (
                        id             INT,
                        [Наименование] VARCHAR(2000),
                        [DBegin]       INT,
                        [Код]          VARCHAR(50),
                        Post           VARCHAR(max)
                    )

            INSERT INTO @Cl
            SELECT a.id
                 --,isnull(AT_2641, AT_2526) AS [ГП]
                 , b.AT_2526 AS [ГП]
                 , b.at_3398 AS [DBegin]
                 , a.AT_2524
                 , a.AT_2924


            FROM KS_DDLControl.CL_956_2470 a
                     LEFT JOIN (
                SELECT *
                FROM ks_ddlcontrol.cl_956_3452 b
                WHERE at_3398 <= @aDateInt
            ) b ON a.id = b.id_up

            SELECT tab2.id
                 , tab2.[Наименование] AS [ГП]
                 , tab2.[Код]
                 , tab2.Post
            INTO #sprGp
            FROM (
                     SELECT id
                          , max(DBegin) AS DBegin
                     FROM @Cl
                     GROUP BY id
                 ) tab1
                     INNER JOIN (
                SELECT id
                     , DBegin
                     , [Наименование]
                     , [Код]
                     , Post
                FROM @Cl
            ) tab2 ON tab1.id = tab2.id
                AND isnull(tab1.DBegin, 0) = isnull(tab2.DBegin, 0)
            --select * from #sprGP

            --Удаление элементов на которые отсутствует доступ
            IF isnull(@GmpPermission, 0) > 0
                AND @user_type != 4
                BEGIN
                    DELETE
                    FROM #sprGp
                    WHERE id NOT IN (
                        SELECT CLID
                        FROM KS_DDLControl.CLA_956 CLA
                                 INNER JOIN dbo.USERS_GROUPS GRP ON CLA.usergroupid = GRP.[groups]
                        WHERE GRP.[users] = user_id(suser_sname())
                    )
                END

            --select * from @Cl

            -------------------------------------------
            --Выборка значений справочника ИсполнителиГМП --
            -------------------------------------------
            select a.*, a1.AT_3200, a1.AT_3201, a1.AT_3202
            into #sprVed1
            from KS_DDLControl.CL_1280_3226 a
                     left join KS_DDLControl.CL_1280_3231 a1 on a.id = a1.id_up


            --select * from    #sprVed1


            -------------------------------------------
            --Выборка значений справочника Ведомства --
            -------------------------------------------
            SELECT a.*
                 , a1.AT_1797
                 , a1.AT_1796
                 , a1.AT_1930
                 --	(select * from KS_DDLControl.CL_864_3145 a1  where a1.ID_UP=a.ID)
            INTO #sprVed
            FROM KS_DDLControl.CL_864_1710 a
                     LEFT JOIN KS_DDLControl.CL_864_3145 a1 ON a.id = a1.id_up

            --select * from    #sprVed

            /*--Удаление элементов на которые отсутствует доступ
            IF isnull(@VedPermission, 0) > 0
                AND @user_type != 4
            BEGIN
                DELETE
                FROM #sprVed
                WHERE id NOT IN (
                        SELECT CLID
                        FROM KS_DDLControl.CLA_864 CLA
                        INNER JOIN dbo.USERS_GROUPS GRP ON CLA.usergroupid = GRP.[groups]
                        WHERE GRP.[users] = user_id(suser_sname())
                        )
            END
        */
            CREATE TABLE #tab_sprGpOnDoc
            (
                [ИД_Документа] INT,
                [Код]          VARCHAR(50),
                [Наименование] VARCHAR(2000),
                [Тип1]         VARCHAR(10),
                [idГП]         INT
                --,[ver] VARCHAR(2)
            )

            EXEC [ks_ddlcontrol].[xp_sprGpOnDoc] @nmode = 0
                , @table_name_result = ''
                , @ToTable = 1

            --select * from #tab_sprGpOnDoc

            -------------------------------------------------
            -- Выборка актуальных документов источника ГМП --2646 - ИдНовойВерсии
            -------------------------------------------------
            SELECT a.documentid
                 , bn.[Наименование]                                  AS Doc
                 --,c.AT_1795 AS [IspCode]
                 --,c.AT_1797 AS [Isp]
                 , c.AT_3199                                          AS [IspCode]
                 , c.AT_3200                                          AS [Isp]
                 , a.id
                 , b.DS_2654                                          AS [id_up]
                 , a.CL_2640                                          AS IdSprGp
                 , a.at_2837                                          AS [Code]
                 , s.CL_2758                                          as [KS]

                 , (
                SELECT count(*)
                FROM KS_DDLControl.DS_957_2625 o1
                         INNER JOIN KS_DDLControl.DS_957_2475 a1 ON o1.id_up = a1.id
                WHERE o1.DS_2654 = a.id
                  AND a1.documentid IN (
                                        221, 226, 272
                    )
            )                                                         AS [CountDwn]
                 , (
                SELECT count(DISTINCT b1.CL_2556)
                FROM KS_DDLControl.DS_957_2625 a1 --ПпГП
                         LEFT JOIN KS_DDLControl.DS_957_2625 a2 ON a2.DS_2654 = a1.id_up --Мероприятия
                         LEFT JOIN KS_DDLControl.DS_957_2625 a3 ON a3.DS_2654 = a2.id_up --МероприятияДет
                         LEFT JOIN KS_DDLControl.DS_957_2625 a4 ON a4.DS_2654 = a3.id_up --МероприятияДетДет
                         LEFT JOIN KS_DDLControl.DS_957_2506 b1 ON b1.id_up IN (
                                                                                a1.id_up, a2.id_up, a3.id_up, a4.id_up
                    )
                WHERE a1.DS_2654 = a.id --ГП
                   OR a2.DS_2654 = a.id --ППГП
                   OR a3.DS_2654 = a.id --Мероприятие
                   OR a4.DS_2654 = a.id --МероприятиеДет
                   OR a4.id_up = a.id --МероприятиеДетДет
            )                                                         AS CountIsp
                 , a.[M_2879]                                         AS Post
                 , a.AT_2538                                          AS date_s ---CONVERT(DATETIME, a.AT_2538, 104) as date_s
                 , a.AT_2539                                          AS date_e
                 , (
                CASE
                    WHEN a.documentid IN (
                                          221, 272
                        )
                        THEN 0
                    ELSE a.documentid
                    END
                )                                                     AS [EntityObjectID]
                 , a.AT_2845
                 , a.CL_3253
                 --,sum(isnull(q.M_2932, 0.00)) / 1000 AS [q.M_2932]
                 , a.AT_2529
                 , s.AT_2760
                 , s.AT_2755.value('(/file/name)[1]', 'varchar(200)') as FileName
                 ,
                '957_2521_2755_' + convert(varchar, s.ID)             as FileID
                 , s.AT_3407
                 , s.AT_2759
                 , s.AT_3386
                 , s.AT_3288
                 , e.AT_2544
            INTO [#alldoc]

            FROM [ks_ddlControl].[GetDocGP](@GP, 0, @Ver) o
                     INNER JOIN KS_DDLControl.DS_957_2475 a ON o.id = a.id

                     LEFT JOIN #tab_sprGpOnDoc bn ON a.id = bn.[ИД_Документа]
                     LEFT JOIN KS_DDLControl.DS_957_2625 b ON a.id = b.id_up --вышестоящий документа
                     LEFT JOIN KS_DDLControl.DS_957_2521 s on a.id = s.id_up -- контрольные события
                     LEFT JOIN KS_DDLControl.CL_960_2491 e on e.id = s.CL_3260
                     LEFT JOIN #sprVed m ON a.CL_2533 = m.id --Макет_Ведомства
                     LEFT JOIN #sprVed1 c ON a.CL_3252 = c.id --М_ИсполнителиГМП
                     LEFT JOIN #sprGP d ON a.CL_2640 = d.id --М_ГОСПРОГРАММЫ
                 --LEFT join KS_DDLControl.DS_957_2506 q on a.id=q.id_up


            WHERE a.documentid IN (
                                   220, 221, 226, 272, 337
                )
            --select * from #alldoc
--	GROUP BY
--	a.documentid
--			,bn.[Наименование]
--
--		,c.AT_3199
--		,c.AT_3200
--		,a.id
--		,b.DS_2654
--		,a.CL_2640
--		,a.at_2837
--		,s.CL_2758
--		,a.[M_2879]
--		,a.AT_2538
--		,a.AT_2539
--		,a.AT_2845
--	,a.CL_3253
--	,q.M_2932


--
--select * from  [#alldoc]

            select

                 --KS_DDLControl.DS_1279_3196 a  KS_DDLControl.DS_1279_3189


                a.documentid
                 , a.Doc
                 , a.date_s
                 , a.date_e
                 , e1.AT_3267
                 , e1.AT_3268
                 , e1.AT_3190
                 , kbk6.AT_1795                  AS [KBK_Ved]
                 --,kbk1.AT_3199 AS [KBK_Ved]
                 , kbk2.AT_1798                  AS [KBK_Div]
                 , kbk3.AT_1801                  AS [KBK_CS]
                 , 'x'                           AS [KBK_VR]

                 -- /1000 30.08.2018
                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --	THEN e.M_3214
                 --ELSE 0
                 --END)  AS [ОЧГ13]
                 --,sum(CASE
                 --		--	WHEN s.at_1998 = 'областные'
                 --	THEN e.M_3179
                 --ELSE 0
                 --END)  AS [ОЧГ14]
                 --,sum(CASE
                 --	WHEN s.at_1998 = 'областные'
                 --	THEN e.M_3180
                 --	ELSE 0
                 --END)  AS [ОЧГ15]
                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --THEN e.M_3181
                 --	ELSE 0
                 --END)  AS [ОЧГ16]
                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --THEN e.M_3182
                 --ELSE 0
                 --END)  AS [ОЧГ17]

                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --THEN e.M_3183
                 ----ELSE 0
                 --END)  AS [ОЧГ18]
                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --	THEN e.M_3184
                 --	ELSE 0
                 --END)  AS [ОЧГ19]
                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --THEN e.M_3185
                 --ELSE 0
                 --END)  AS [ОЧГ20]
                 --,sum(CASE
                 --WHEN s.at_1998 = 'областные'
                 --THEN e.M_3188
                 --ELSE 0
                 --END)  AS [ОЧГ21]
                 --,isnull(kbk1.AT_1797, a.[Isp]) AS [Isp]
                 , isnull(kbk1.AT_3200, a.[Isp]) AS [Isp]
                 , a.id
                 , a.[id_up]
                 , a.IdSprGp
                 , s.at_1998                     AS [res]
                 , a.[Code]
                 , a.[CountDwn]
                 , a.[CountIsp]
                 , newid()                       AS serial
                 , a.[Post]
                 , sum(CASE

                           WHEN s.at_1998 = 'областные'
                               THEN e.M_3182
                           ELSE 0
                END)                             AS [ОЧГ17обл]

                 , sum(CASE
                           WHEN s.at_1998 = 'федеральные'
                               OR s.at_1998 = 'внебюджетные РФ'
                               THEN e.M_3182

                           ELSE 0
                END)                             AS [ОЧГ17фед]

                 , sum(isnull(e.M_3182, 0.00))   AS [ОЧГ17итог]


                 , a.AT_2845
                 , a.CL_3253
                 , docs.FileID
                 , docs.AT_3269
                 , docs.FileName
                 --,sum(isnull(q.M_2932, 0.00)) / 1000 AS [q.M_2932]

            INTO [#allSource]

            FROM [#alldoc] a
                     LEFT JOIN KS_DDLControl.DS_1279_3189 e1 ON e1.DS_3238 = a.id
                     LEFT JOIN KS_DDLControl.DS_1279_3196 e ON e.id_up = e1.id --Область_Ввода2
                     LEFT JOIN #sprVed kbk6 ON e.CL_3170 = kbk6.id --Макет_Ведомства
                     LEFT JOIN #sprVed1 kbk1 ON e.CL_3170 = kbk1.id --Макет_Ведомства
                     LEFT JOIN KS_DDLControl.CL_865_1714 kbk2 ON e.CL_3171 = kbk2.id --Макет_Подразделы
                     LEFT JOIN KS_DDLControl.CL_866_1718 kbk3 ON e.CL_3172 = kbk3.id --Макет_Целевые статьи
                     LEFT JOIN KS_DDLControl.CL_867_1722 kbk4 ON e.CL_3173 = kbk4.id --Макет_Виды расходов
                     LEFT JOIN KS_DDLControl.CL_870_1752 kbk5 ON e.CL_3175 = kbk5.id --Макет_ДопКласс
                     LEFT JOIN KS_DDLControl.CL_917_2326 shet ON e.CL_3178 = shet.id --Макет_Корреспонденты
                     LEFT JOIN KS_DDLControl.CL_894_1936 S ON e.CL_3176 = S.id --Макет_Источники финансирования
                     LEFT JOIN (select ID,
                                       ID_UP,
                                       AT_3235.value('(/file/name)[1]', 'varchar(200)') as FileName,
                                       AT_3269,
                                       '1279_3268_3235_' + convert(varchar, ID)         as FileID
                                from KS_DDLControl.DS_1279_3268
                                where CL_3292 = 5) docs ON e1.ID = docs.ID_UP
                 --left join KS_DDLControl.DS_957_2506 q on q.id_up=a.id
                 --LEFT JOIN KS_DDLControl.CL_1008_2633 m ON e1.CL_3164 = m.id --статус

            WHERE a.documentid IN (
                                   220, 221, 226, 272
                )
              and e1.AT_3190 IN (convert(varchar(10), @Data_n, 112))


            GROUP BY a.documentid
                   , a.id
                   , a.[Doc]
                   , a.date_s
                   , a.date_e
                   , e1.AT_3267
                   , e1.AT_3268
                   , e1.AT_3190
                   , kbk6.AT_1795
                   , kbk1.AT_3199
                   , kbk2.AT_1798
                   , kbk3.AT_1801
                   --, kbk4.AT_1804
                   --,isnull(kbk1.AT_1797, a.[Isp])
                   , isnull(kbk1.AT_3200, a.[Isp])
                   , a.[id_up]
                   , a.IdSprGp
                   , s.at_1998
                   , a.[Code]
                   , a.[CountDwn]
                   , a.[CountIsp]
                   , a.[Post]
                   , a.AT_2845
                   , docs.FileID
                   , docs.FileName
                   , docs.AT_3269
                   , a.CL_3253
            --,q.M_2932
--select * from KS_DDLControl.DS_1279_3268
            --select * from [#alldoc]
--	select * from [#allSource]
--select * from KS_DDLControl.DS_1279_3268
--select * from #allSource
            -----------------
            --Госпрограмма --
            -----------------
            DECLARE
                @Post VARCHAR(max)

            SELECT @Post = Post
            FROM #alldoc
            WHERE documentid = 220
              AND IdSprGp = @GP


            INSERT INTO #temp_result_Gppril6 ( documentid
                                             , NAME
                                             , Doc
                --,ДатаНачала
                --,ДатаОконч
                                             , [KBK_Ved]
                                             , [KBK_Div]
                                             , [KBK_CS]
                                             , [KBK_VR]
--		,[ОЧГ13]
--		,[ОЧГ14]
--		,[ОЧГ15]
--		,[ОЧГ16]
--		,[ОЧГ17]
--		,[ОЧГ18]
--		,[ОЧГ19]
--		,[ОЧГ20]
--		,[ОЧГ21]
                                             , [Isp]
                                             , DocId
                                             , [ОЧГ17фед]
                                             , [ОЧГ17обл]
                                             , [ОЧГ17итог])

            SELECT a1.documentid
                 , ''
                 , a1.Doc
                 --,''
                 --,''
                 , 'x'
                 , 'x'
                 , 'x'
                 , 'x'
                 --,sum(isnull(a2.[ОЧГ13], 0.00))
                 --,sum(isnull(a2.[ОЧГ14], 0.00))
                 --,sum(isnull(a2.[ОЧГ15], 0.00))
                 --,sum(isnull(a2.[ОЧГ16], 0.00))
                 --,sum(isnull(a2.[ОЧГ17], 0.00))
                 --,sum(isnull([ОЧГ18], 0.00))
                 --,sum(isnull([ОЧГ19], 0.00))
                 --,sum(isnull([ОЧГ20], 0.00))
                 --,sum(isnull([ОЧГ21], 0.00))
                 --,''
                 , 'Всего'
                 , a1.Id

                 , sum(isnull(a2.[ОЧГ17фед], 0.00))
                 , sum(isnull(a2.[ОЧГ17обл], 0.00))
                 , sum(isnull(a2.[ОЧГ17итог], 0.00))

            FROM #alldoc a1
                     CROSS JOIN #allSource a2
            WHERE a1.documentid = 220
              AND a1.IdSprGp = @GP

            GROUP BY a1.documentid
                   , a1.Doc
                   , a1.ID


            --------------------------
            -- Итоги в разрезе ГРБС --
            --------------------------
            INSERT INTO #temp_result_Gppril6 ( documentid
                --NAME
                --,Doc
                                             , [Isp]
--,[KBK_Ved]
--		,[ОЧГ13]
--		,[ОЧГ14]
--		,[ОЧГ15]
--		,[ОЧГ16]
--		,[ОЧГ17]
--		,[ОЧГ18]
--		,[ОЧГ19]
--		,[ОЧГ20]
--		,[ОЧГ21]
                                             , [ОЧГ17фед]
                                             , [ОЧГ17обл]
                                             , [ОЧГ17итог])

            SELECT 220
                 --a.NAME
                 --,b.Doc
                 , b.[Isp]
                 --,isnull(b.[KBK_Ved], a.[IspCode])
--		,sum(isnull([ОЧГ13], 0.00))
--		,sum(isnull([ОЧГ14], 0.00))
--		,sum(isnull([ОЧГ15], 0.00))
--		,sum(isnull([ОЧГ16], 0.00))
--		,sum(isnull([ОЧГ17], 0.00))
--		,sum(isnull([ОЧГ18], 0.00))
--		,sum(isnull([ОЧГ19], 0.00))
--		,sum(isnull([ОЧГ20], 0.00))
--		,sum(isnull([ОЧГ21], 0.00))
                 , sum(isnull([ОЧГ17фед], 0.00))
                 , sum(isnull([ОЧГ17обл], 0.00))
                 , sum(isnull([ОЧГ17итог], 0.00))

            FROM #alldoc a
                     INNER JOIN #allSource b ON a.id = b.id
            GROUP BY b.documentid
                   --,b.Doc
                   , b.[Isp]
                   --,isnull(b.[KBK_Ved], a.[IspCode])
                   --HAVING
                   --sum(isnull([ОЧГ13], 0.00)) + sum(isnull([ОЧГ14], 0.00)) + sum(isnull([ОЧГ15], 0.00)) + sum(isnull([ОЧГ16], 0.00)) + sum(isnull([ОЧГ17], 0.00)) + sum(isnull([ОЧГ18], 0.00)) + sum(isnull([ОЧГ19], 0.00)) + sum(isnull([ОЧГ20], 0.00)) + sum(isnull([ОЧГ21], 0.00)) > 0

            HAVING sum(isnull([ОЧГ17фед], 0.00)) + sum(isnull([ОЧГ17обл], 0.00)) + sum(isnull([ОЧГ17итог], 0.00)) > 0

            DECLARE
                @documentidPpGpPrevious INT
            DECLARE
                @NumOM INT
            DECLARE
                @documentidPpGp INT
            DECLARE
                @DocPpGp VARCHAR(2000)
            DECLARE
                @IspPpGp VARCHAR(2000)
            DECLARE
                @IdPpGP INT
            DECLARE
                @NumPpGP INT
            DECLARE
                @Num VARCHAR(100)
            DECLARE
                @CountDwnPpGP INT
            DECLARE
                @CountIspPpGP INT
            DECLARE
                @documentidM1 INT
            DECLARE
                @DocM1 VARCHAR(2000)
            DECLARE
                @IspM1 VARCHAR(2000)
            DECLARE
                @IdM1 INT
            DECLARE
                @NumM1 INT
            DECLARE
                @CountDwnM1 INT
            DECLARE
                @CountIspM1 INT
            DECLARE
                @documentidM2 INT
            DECLARE
                @DocM2 VARCHAR(2000)
            DECLARE
                @IspM2 VARCHAR(2000)
            DECLARE
                @IdM2 INT
            DECLARE
                @NumM2 INT
            DECLARE
                @CountDwnM2 INT
            DECLARE
                @CountIspM2 INT

            -----------------------
            -- Для Итогов по ОМ --
            -----------------------
            CREATE TABLE #t
            (
                id INT
            )

            DECLARE
                @IdOM INT

            DECLARE CurOM CURSOR
                FOR
                SELECT a.Id
                FROM #alldoc a
                         INNER JOIN #temp_result_Gppril6 b ON a.[id_up] = b.DocID
                WHERE a.documentid = 226

            OPEN CurOM

            FETCH NEXT
                FROM CurOM
                INTO @IdOM

            WHILE @@fetch_status = 0
            BEGIN
                INSERT INTO #t
                SELECT id
                FROM [ks_ddlControl].[GetDocID](@IdOM)

                FETCH NEXT
                    FROM CurOM
                    INTO @IdOM
            END

            CLOSE CurOM

            DEALLOCATE CurOM

            -------------------
            -- ППГП, ОМ, ПФЗ --
            -------------------
            DECLARE Cur1 CURSOR
                FOR
                SELECT a.documentid
                     , a.Doc
                     , a.Isp
                     , a.Id
                     , a.CountDwn
                     , a.CountIsp
                FROM #alldoc a
                         INNER JOIN #temp_result_Gppril6 b ON a.[id_up] = b.DocID
                ORDER BY a.[EntityObjectID]
                       , a.Code
                       , a.ID

            OPEN Cur1

            FETCH NEXT
                FROM Cur1
                INTO @documentidPpGP
                    ,@DocPpGP
                    ,@IspPpGP
                    ,@IdPpGP
                    ,@CountDwnPpGP
                    ,@CountIspPpGP

            WHILE @@fetch_status = 0
            BEGIN
                IF @documentidPpGP = 226
                    AND isnull(@documentidPpGpPrevious, 0) = @documentidPpGP
                    BEGIN
                        SET @NumOM = isnull(@NumOM, 0) + 1
                        SET @Num = convert(VARCHAR, @NumPpGP) + '.' + convert(VARCHAR, @NumOM)
                    END
                ELSE
                    IF @documentidPpGP = 226
                        BEGIN
                            SET @NumPpGP = isnull(@NumPpGP, 0) + 1
                            SET @NumOM = isnull(@NumOM, 0) + 1
                            SET @Num = convert(VARCHAR, @NumPpGP) + '.' + convert(VARCHAR, @NumOM)
                            --insert into #temp_result_Gppril6  (documentid, name, Doc, Num)
                            --select 0, 'Отдельные мероприятия', ': ', convert(varchar, @NumPpGP)
                            --------------------------
                            -- Итоги в разрезе ГРБС --
                            --------------------------
                            INSERT INTO #temp_result_Gppril6 ( documentid
                                                             , NAME
                                                             , Doc
                                                             , ДатаНачала
                                                             , ДатаОконч
                                                             , [KBK_Ved]
                                                             , [KBK_Div]
                                                             , [KBK_CS]
                                                             , [KBK_VR]
                                --,[ОЧГ13]
                                --,[ОЧГ14]
                                --,[ОЧГ15]
                                --,[ОЧГ16]
                                --,[ОЧГ17]
                                --,[ОЧГ18]
                                --,[ОЧГ19]
                                --,[ОЧГ20]
                                --,[ОЧГ21]
                                                             , [Isp]
                                                             , Num)
                            SELECT 0
                                 , 'Отдельные мероприятия'
                                 , ': '
                                 , ''
                                 , ''
                                 , isnull(b.KBK_Ved, a.IspCode)
                                 , 'x'
                                 , 'x'
                                 , 'x'
                                 --,sum(isnull([ОЧГ13], 0.00))
                                 --,sum(isnull([ОЧГ14], 0.00))
                                 --,sum(isnull([ОЧГ15], 0.00))
                                 --,sum(isnull([ОЧГ16], 0.00))
                                 --,sum(isnull([ОЧГ17], 0.00))
                                 --,sum(isnull([ОЧГ18], 0.00))
                                 --,sum(isnull([ОЧГ19], 0.00))
                                 --,sum(isnull([ОЧГ20], 0.00))
                                 --,sum(isnull([ОЧГ21], 0.00))
                                 , isnull(b.[Isp], a.[Isp])
                                 , @NumPpGP
                            FROM [#allDoc] a
                                     LEFT JOIN [#allSource] b ON a.id = b.id
                                AND b.res = 'краевые'
                                     INNER JOIN (
                                SELECT id
                                FROM #t
                            ) o ON a.id = o.id
                            GROUP BY isnull(b.[Isp], a.[Isp])
                                   , isnull(b.KBK_Ved, a.IspCode)
                        END
                    ELSE
                        BEGIN
                            SET @NumOM = 0
                            --SET @NumOM = isnull(@NumOM, 0) + 1
                            SET @NumPpGP = isnull(@NumPpGP, 0) + 1
                            SET @Num = convert(VARCHAR, @NumPpGP)
                        END
--DELETE FROM #temp_result_Gppril6 where documentid=221  and [ОЧГ17итог] = 0.0 and [ОЧГ17фед] = 0.0 and [ОЧГ17обл] = 0.0  AND [KBK_CS] = 'x'
                DELETE
                FROM #temp_result_Gppril6
                where [ОЧГ17итог] = 0
                  and [ОЧГ17фед] = 0
                  and [ОЧГ17обл] = 0
                  AND [KBK_CS] = 'x'


                IF isnull(@CountDwnPpGP, 0) = 0
                    BEGIN
                        --		SET @tmp=(select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				if @tmp like '%.%'
--				begin
--				if @tmp>9
--				begin
--				SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				if substring(@tmpr,4,1)='0'
--				begin
--				SET @tmpr=substring(@tmpr,1,3)+substring(@tmpr,5,1)
--				end
--				end
--				else
--				begin
--				SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				 if substring(@tmpr,1,1)='0'
--				 begin
--				 SET @tmpr =substring(@tmpr,2,4)
--				if substring(@tmpr,3,1)='0'
--				SET @tmpr =REPLACE(@tmpr, '0', '')
--				end
--				end
--				SET @N = @tmpr
--				end
--				else
--				begin
--				IF @TMP>9
--				SET @N = (convert(VARCHAR, @TMP))
--				ELSE
--				SET @N = (convert(VARCHAR,REPLACE(@tmp, 0, '')))
--				end
                        SET @tmpr = (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdPpGP)
                        if substring(@tmpr, 1, 1) = 0
                            SET @tmpr = REPLACE(@tmpr, '0', '')
                        SET @N = @TMPR
                        if (@docmid = @IdPpGP)
                            set @tm = @cnt
                        else
                            begin
                                if ((select count(*) FROM [#allSource] where id = @IdPpGP) > 0)
                                    begin
                                        set @docmid = @IdPpGP
                                        set @cnt = @cnt + 1
                                        set @tm = @cnt
                                    end
                            end
                        INSERT INTO #temp_result_Gppril6 ( documentid
                                                         , NAME
                                                         , Doc
                                                         , ДатаНачала
                                                         , ДатаОконч
                                                         , [KBK_Ved]
                                                         , [KBK_Div]
                                                         , [KBK_CS]
                                                         , [KBK_VR]
                            --,[ОЧГ13]
                            --,[ОЧГ14]
                            --,[ОЧГ15]
                            --,[ОЧГ16]
                            --,[ОЧГ17]
                            --,[ОЧГ18]
                            --,[ОЧГ19]
                            --,[ОЧГ20]
                            --,[ОЧГ21]
                                                         , [Isp]
                                                         , DocId
                                                         , Num
                                                         , [ОЧГ17фед]
                                                         , [ОЧГ17обл]
                                                         , [ОЧГ17итог]
                                                         , [Порядковый номер]
                                                         , serial)
                        SELECT a.documentid
                             , (
                            CASE
                                WHEN a.documentid = 221
                                    THEN 'Подпрограмма'
                                WHEN a.documentid = 226
                                    THEN 'Отдельное мероприятие'
                                ELSE 'Программа по ФЗ №'
                                END
                            )
                             , a.Doc
                             , a.date_s
                             , a.date_e
                             , isnull(b.KBK_Ved, a.IspCode)
                             , b.KBK_Div
                             , b.KBK_CS
                             , b.KBK_VR
                             --,sum(b.ОЧГ13)
                             --,sum(b.ОЧГ14)
                             --,sum(b.ОЧГ15)
                             --,sum(b.ОЧГ16)
                             --,sum(b.ОЧГ17)
                             --,sum(b.ОЧГ18)
                             --,sum(b.ОЧГ19)
                             --,sum(b.ОЧГ20)
                             --,sum(b.ОЧГ21)
                             , isnull(b.Isp, a.Isp)
                             , a.Id
                             , @Num
                             , sum(isnull([ОЧГ17фед], 0.00))
                             , sum(isnull([ОЧГ17обл], 0.00))
                             , sum(isnull([ОЧГ17итог], 0.00))
                             , @N
                             , @tm
                        FROM [#allDoc] a
                                 LEFT JOIN [#allSource] b ON a.id = b.id
                            AND b.res = 'краевые'
                        WHERE a.id = @IdPpGP
                        GROUP BY a.[EntityObjectID]
                               , a.Code
                               , a.documentid
                               , a.Doc
                               , a.date_s
                               , a.date_e
                               , isnull(b.KBK_Ved, a.IspCode)
                               , b.KBK_Div
                               , b.KBK_CS
                               , b.KBK_VR
                               , isnull(b.Isp, a.Isp)
                               , a.Id
                        ORDER BY a.[EntityObjectID]
                               , a.Code
                               , a.ID
                        --=iif(Previous(Fields!Num.Value)=Fields!Num.Value,'',Fields!Num.Value)
                        --=Code.Dwh.SetVariable("NN", Code.Dwh.GetVariable("NN")+1)
                    END
                ELSE
                    BEGIN
                        IF @CountIspPpGP > 1
                            BEGIN
                                --			SET @tmp=(select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				if @tmp like '%.%'
--				begin
--				if @tmp>9
--				begin
--				SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				if substring(@tmpr,4,1)='0'
--				begin
--				SET @tmpr=substring(@tmpr,1,3)+substring(@tmpr,5,1)
--				end
--				end
--				else
--				begin
--				SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				 if substring(@tmpr,1,1)='0'
--				 begin
--				 SET @tmpr =substring(@tmpr,2,4)
--				if substring(@tmpr,3,1)='0'
--				SET @tmpr =REPLACE(@tmpr, '0', '')
--				end
--				end
--				SET @N = @tmpr
--				end
--				else
--				begin
--				IF @TMP>9
--				SET @N = (convert(VARCHAR, @TMP))
--				ELSE
--				SET @N = (convert(VARCHAR,REPLACE(@tmp, 0, '')))
--				end
                                SET @tmpr = (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdPpGP)
                                if substring(@tmpr, 1, 1) = 0
                                    SET @tmpr = REPLACE(@tmpr, '0', '')
                                SET @N = @TMPR
                                if (@docmid = @IdPpGP)
                                    set @tm = @cnt
                                else
                                    begin
                                        set @docmid = @IdPpGP
                                        set @cnt = @cnt + 1
                                        set @tm = @cnt
                                    end
                                -----------
                                -- Всего --
                                -----------
                                INSERT INTO #temp_result_Gppril6 ( documentid
                                                                 , NAME
                                                                 , Doc
                                                                 , ДатаНачала
                                                                 , ДатаОконч
                                                                 , [KBK_Ved]
                                                                 , [KBK_Div]
                                                                 , [KBK_CS]
                                                                 , [KBK_VR]
                                    --,[ОЧГ13]
                                    --,[ОЧГ14]
                                    --,[ОЧГ15]
                                    --,[ОЧГ16]
                                    --,[ОЧГ17]
                                    --,[ОЧГ18]
                                    --,[ОЧГ19]
                                    --,[ОЧГ20]
                                    --,[ОЧГ21]
                                                                 , [Isp]
                                                                 , DocId
                                                                 , Num
                                                                 , [ОЧГ17фед]
                                                                 , [ОЧГ17обл]
                                                                 , [ОЧГ17итог]
                                                                 , [Порядковый номер]
                                                                 , serial)
                                SELECT @documentidPpGp
                                     , (
                                    CASE
                                        WHEN @documentidPpGp = 221
                                            THEN 'Подпрограмма'
                                        WHEN @documentidPpGp = 226
                                            THEN 'Отдельное мероприятие'
                                        ELSE 'Программа по ФЗ №'
                                        END
                                    )
                                     , @DocPpGp
                                     , ''
                                     , ''
                                     , 'x'
                                     , 'x'
                                     , 'x'
                                     , 'x'
                                     --,sum(isnull(b.[ОЧГ13], 0.00))
                                     --,sum(isnull(b.[ОЧГ14], 0.00))
                                     --,sum(isnull(b.[ОЧГ15], 0.00))
                                     --,sum(isnull(b.[ОЧГ16], 0.00))
                                     --,sum(isnull(b.[ОЧГ17], 0.00))
                                     --,sum(isnull(b.[ОЧГ18], 0.00))
                                     --,sum(isnull(b.[ОЧГ19], 0.00))
                                     --,sum(isnull(b.[ОЧГ20], 0.00))
                                     --,sum(isnull(b.[ОЧГ21], 0.00))
                                     , 'Всего'
                                     , @IdPpGp
                                     , @Num
                                     , sum(isnull([ОЧГ17фед], 0.00))
                                     , sum(isnull([ОЧГ17обл], 0.00))
                                     , sum(isnull([ОЧГ17итог], 0.00))
                                     , @N
                                     , @tm

                                FROM [#allDoc] a
                                         LEFT JOIN [#allSource] b ON a.id = b.id

                                         INNER JOIN (
                                    SELECT id
                                    FROM [ks_ddlControl].[GetDocID](@IdPpGP)

                                    UNION

                                    SELECT @IdPpGP
                                ) o ON a.id = o.id
                            END
                        --------------------------
                        -- Итоги в разрезе ГРБС --
                        --------------------------

--			SET @tmp=(select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				if @tmp like '%.%'
--				begin
--				if @tmp>9
--				begin
--				SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				if substring(@tmpr,4,1)='0'
--				begin
--				SET @tmpr=substring(@tmpr,1,3)+ substring(@tmpr,5,1)
--				end
--				end
--
--				else
--				begin
--				SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdPpGP)
--				 if substring(@tmpr,1,1)='0'
--				 begin
--				 SET @tmpr =substring(@tmpr,2,4)
--				if substring(@tmpr,3,1)='0'
--				SET @tmpr =REPLACE(@tmpr, '0', '')
--				end
--				end
--				SET @N = @tmpr
--				end
--				else
--				begin
--				IF @TMP>9
--				SET @N = (convert(varchar, @TMP))
--				ELSE
--				SET @N = (convert(varchar,REPLACE(@tmp, 0, '')))
--				end
                        SET @tmpr = (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdPpGP)
                        if substring(@tmpr, 1, 1) = 0
                            SET @tmpr = REPLACE(@tmpr, '0', '')
                        SET @N = @TMPR
                        if (@docmid = @IdPpGP)
                            set @tm = @cnt
                        else
                            begin
                                set @docmid = @IdPpGP
                                set @cnt = @cnt + 1
                                set @tm = @cnt
                            end
                        INSERT INTO #temp_result_Gppril6 ( documentid
                                                         , NAME
                                                         , Doc

                            --,ДатаНачала
                            --,ДатаОконч
                                                         , [KBK_Ved]
                                                         , [KBK_Div]
                                                         , [KBK_CS]
                                                         , [KBK_VR]
                            --,[ОЧГ13]
                            --,[ОЧГ14]
                            --,[ОЧГ15]
                            --,[ОЧГ16]
                            --,[ОЧГ17]
                            --,[ОЧГ18]
                            --,[ОЧГ19]
                            --,[ОЧГ20]
                            --,[ОЧГ21]
                                                         , [Isp]
                                                         , DocId
                                                         , Num
                                                         , [ОЧГ17фед]
                                                         , [ОЧГ17обл]
                                                         , [ОЧГ17итог]
                                                         , [Порядковый номер]
                                                         , serial)
                        SELECT @documentidPpGp
                             , (
                            CASE
                                WHEN @documentidPpGp = 221
                                    THEN 'Подпрограмма'
                                WHEN @documentidPpGp = 226
                                    THEN 'Отдельное мероприятие'
                                ELSE 'Программа по ФЗ №'
                                END
                            )
                             , @DocPpGp
                             --,a.date_s
                             --,a.date_e
                             , isnull(b.KBK_Ved, a.IspCode)
                             , 'x'
                             , 'x'
                             , 'x'
                             --,sum(isnull(b.[ОЧГ13], 0.00))
                             --,sum(isnull(b.[ОЧГ14], 0.00))
                             --,sum(isnull(b.[ОЧГ15], 0.00))
                             --,sum(isnull(b.[ОЧГ16], 0.00))
                             --,sum(isnull(b.[ОЧГ17], 0.00))
                             --,sum(isnull(b.[ОЧГ18], 0.00))
                             --,sum(isnull(b.[ОЧГ19], 0.00))
                             --,sum(isnull(b.[ОЧГ20], 0.00))
                             --,sum(isnull(b.[ОЧГ21], 0.00))
                             , isnull(b.[Isp], a.[Isp])
                             , @IdPpGp
                             , @Num
                             , sum(isnull([ОЧГ17фед], 0.00))
                             , sum(isnull([ОЧГ17обл], 0.00))
                             , sum(isnull([ОЧГ17итог], 0.00))
                             , @N
                             , @tm
                        FROM [#allDoc] a
                                 LEFT JOIN [#allSource] b ON a.id = b.id

                                 INNER JOIN (
                            SELECT id
                            FROM [ks_ddlControl].[GetDocID](@IdPpGP)

                            UNION

                            SELECT @IdPpGP
                        ) o ON a.id = o.id
                        GROUP BY isnull(b.[Isp], a.[Isp])
                               --,a.date_s
                               --,a.date_e
                               , isnull(b.KBK_Ved, a.IspCode)
                    END

                -----------------
                -- Мероприятие --
                -----------------
                SET @NumM1 = NULL
                DECLARE Cur2 CURSOR
                    FOR
                    SELECT a.documentid
                         , a.Doc
                         , a.Isp
                         , a.Id
                         , a.CountDwn
                         , a.CountIsp
                    FROM #alldoc a
                    WHERE a.[id_up] = @IdPpGP
                    ORDER BY a.code
                           , a.ID

                OPEN Cur2

                FETCH NEXT
                    FROM Cur2
                    INTO @documentidM1
                        ,@DocM1
                        ,@IspM1
                        ,@IdM1
                        ,@CountDwnM1
                        ,@CountIspM1

                WHILE @@fetch_status = 0
                BEGIN
                    SET @NumM1 = isnull(@NumM1, 0) + 1
                    SET @tmp =
                            (convert(VARCHAR, (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)))
                    if @tmp like '%.%'
                        begin
                            if LEN(@tmp) > 1
                                begin
                                    SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)
                                    if substring(@tmpr, 4, 1) = '0'
                                        begin
                                            SET @tmpr = substring(@tmpr, 1, 3) + substring(@tmpr, 5, 1)
                                        end
                                end
                            else
                                begin
                                    SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)
                                    if substring(@tmpr, 1, 1) = '0'
                                        begin
                                            SET @tmpr = substring(@tmpr, 2, 4)
                                            if substring(@tmpr, 3, 1) = '0'
                                                SET @tmpr = REPLACE(@tmpr, '0', '')
                                        end
                                end
                            SET @NM = @N + '.' + @tmpr
                        end
                    else
                        begin
                            IF LEN(@tmp) > 1
                                SET @NM = @N + '.' + (convert(VARCHAR, @TMP))
                            ELSE
                                SET @NM = @N + '.' + (convert(VARCHAR, REPLACE(@tmp, 0, '')))
                        end
                    IF isnull(@CountDwnM1, 0) = 0
                        BEGIN
                            if (@docmid = @IdM1)
                                set @tm = @cnt
                            else
                                begin
                                    if ((select count(*) FROM [#allSource] where id = @IdPpGP) > 0)
                                        begin
                                            set @docmid = @IdM1
                                            set @cnt = @cnt + 1
                                            set @tm = @cnt
                                        end
                                end
                            INSERT INTO #temp_result_Gppril6 ( documentid
                                                             , NAME
                                                             , Doc
                                --,ДатаНачала
                                --,ДатаОконч
                                                             , [KBK_Ved]
                                                             , [KBK_Div]
                                                             , [KBK_CS]
                                                             , [KBK_VR]
                                --,[ОЧГ13]
                                --,[ОЧГ14]
                                --,[ОЧГ15]
                                --,[ОЧГ16]
                                --,[ОЧГ17]
                                --,[ОЧГ18]
                                --,[ОЧГ19]
                                --,[ОЧГ20]
                                --,[ОЧГ21]
                                                             , [Isp]
                                                             , DocId
                                                             , Num
                                                             , [ОЧГ17фед]
                                                             , [ОЧГ17обл]
                                                             , [ОЧГ17итог]
                                                             , [Порядковый номер]
                                                             , serial)
                            SELECT @documentidM1
                                 , 'Основное мероприятие'
                                 , @DocM1
                                 --,b.date_s
                                 --,b.date_e
                                 , isnull(b.KBK_Ved, a.IspCode)
                                 , b.KBK_Div
                                 , b.KBK_CS
                                 , b.KBK_VR
                                 --,sum(isnull(b.[ОЧГ13], 0.00))
                                 --,sum(isnull(b.[ОЧГ14], 0.00))
                                 --,sum(isnull(b.[ОЧГ15], 0.00))
                                 --,sum(isnull(b.[ОЧГ16], 0.00))
                                 --,sum(isnull(b.[ОЧГ17], 0.00))
                                 --,sum(isnull(b.[ОЧГ18], 0.00))
                                 --,sum(isnull(b.[ОЧГ19], 0.00))
                                 --,sum(isnull(b.[ОЧГ20], 0.00))
                                 --,sum(isnull(b.[ОЧГ21], 0.00))
                                 , isnull(b.Isp, a.Isp)
                                 --,@IspM1
                                 , @IdM1
                                 , @Num + '.' + convert(VARCHAR, @NumM1)
                                 , sum(isnull([ОЧГ17фед], 0.00))
                                 , sum(isnull([ОЧГ17обл], 0.00))
                                 , sum(isnull([ОЧГ17итог], 0.00))
                                 , @nm
                                 , @tm
                            FROM [#allDoc] a
                                     LEFT JOIN [#allSource] b ON a.id = b.id
                            WHERE a.id = @IdM1
                            GROUP BY
                                   --b.date_s
                                   --,b.date_e
                                isnull(b.KBK_Ved, a.IspCode)
                                   , b.KBK_Div
                                   , b.KBK_CS
                                   , b.KBK_VR
                                   , isnull(b.Isp, a.Isp)
                        END
                    ELSE
                        BEGIN
                            IF @CountIspM1 > 1
                                BEGIN
                                    SET @tmp = (convert(VARCHAR,
                                                (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)))
                                    if @tmp like '%.%'
                                        begin
                                            if len(@tmp) > 1
                                                begin
                                                    SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)
                                                    if substring(@tmpr, 4, 1) = '0'
                                                        begin
                                                            SET @tmpr = substring(@tmpr, 1, 3) + substring(@tmpr, 5, 1)
                                                        end
                                                end
                                            else
                                                begin
                                                    SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)
                                                    if substring(@tmpr, 1, 1) = '0'
                                                        begin
                                                            SET @tmpr = substring(@tmpr, 2, 4)
                                                            if substring(@tmpr, 3, 1) = '0'
                                                                SET @tmpr = REPLACE(@tmpr, '0', '')
                                                        end
                                                end
                                            SET @NM = @N + '.' + @tmpr
                                        end
                                    else
                                        begin
                                            IF len(@tmp) > 1
                                                SET @NM = @N + '.' + (convert(VARCHAR, @TMP))
                                            ELSE
                                                SET @NM = @N + '.' + (convert(VARCHAR, REPLACE(@tmp, 0, '')))
                                        end
                                    if (@docmid = @IdM1)
                                        set @tm = @cnt
                                    else
                                        begin
                                            set @docmid = @IdM1
                                            set @cnt = @cnt + 1
                                            set @tm = @cnt
                                        end
                                    -----------
                                    -- Всего --
                                    -----------
                                    INSERT INTO #temp_result_Gppril6 ( documentid
                                                                     , NAME
                                                                     , Doc
                                                                     , ДатаНачала
                                                                     , ДатаОконч
                                                                     , [KBK_Ved]
                                                                     , [KBK_Div]
                                                                     , [KBK_CS]
                                                                     , [KBK_VR]
                                        --,[ОЧГ13]
                                        --,[ОЧГ14]
                                        --,[ОЧГ15]
                                        --,[ОЧГ16]
                                        --,[ОЧГ17]
                                        --,[ОЧГ18]
                                        --,[ОЧГ19]
                                        --,[ОЧГ20]
                                        --,[ОЧГ21]
                                                                     , [Isp]
                                                                     , DocId
                                                                     , Num
                                                                     , [ОЧГ17фед]
                                                                     , [ОЧГ17обл]
                                                                     , [ОЧГ17итог]
                                                                     , [Порядковый номер]
                                                                     , serial)
                                    SELECT @documentidM1
                                         , 'Основное мероприятие'
                                         , @DocM1
                                         , ''
                                         , ''
                                         , 'x'
                                         , 'x'
                                         , 'x'
                                         , 'x'
                                         --,sum(isnull(b.[ОЧГ13], 0.00))
                                         --,sum(isnull(b.[ОЧГ14], 0.00))
                                         --,sum(isnull(b.[ОЧГ15], 0.00))
                                         --,sum(isnull(b.[ОЧГ16], 0.00))
                                         --,sum(isnull(b.[ОЧГ17], 0.00))
                                         --,sum(isnull(b.[ОЧГ18], 0.00))
                                         --,sum(isnull(b.[ОЧГ19], 0.00))
                                         --,sum(isnull(b.[ОЧГ20], 0.00))
                                         --,sum(isnull(b.[ОЧГ21], 0.00))
                                         , 'Всего'
                                         , @IdM1
                                         , @Num + '.' + convert(VARCHAR, @NumM1)
                                         , sum(isnull([ОЧГ17фед], 0.00))
                                         , sum(isnull([ОЧГ17обл], 0.00))
                                         , sum(isnull([ОЧГ17итог], 0.00))
                                         --,(select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where DOCUMENTID=@documentidM1 and AT_2537 like @DocM1)
                                         , @Nm
                                         , @tm
                                    FROM [#allDoc] a
                                             LEFT JOIN [#allSource] b ON a.id = b.id
                                             INNER JOIN (
                                        SELECT id
                                        FROM [ks_ddlControl].[GetDocID](@IdM1)

                                        UNION

                                        SELECT @IdM1
                                    ) o ON a.id = o.id
                                END

                            --------------------------
                            -- Итоги в разрезе ГРБС --
                            --------------------------
                            SET @tmp = ((select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1))
                            --SET @tmpr=((select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdM1))
                            --SET @tm=substring(@tmpr,1,2)
                            if @tmp like '%.%'
                                begin
                                    if len(@tmp) > 1
                                        begin
                                            SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)
                                            if substring(@tmpr, 4, 1) = '0'
                                                begin
                                                    SET @tmpr = substring(@tmpr, 1, 3) + substring(@tmpr, 5, 1)
                                                end
                                        end
                                    else
                                        begin
                                            SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM1)
                                            if substring(@tmpr, 1, 1) = '0'
                                                begin
                                                    SET @tmpr = substring(@tmpr, 2, 4)
                                                    if substring(@tmpr, 3, 1) = '0'
                                                        SET @tmpr = REPLACE(@tmpr, '0', '')
                                                end
                                        end
                                    SET @NM = @N + '.' + @tmpr
                                end
                            else
                                begin
                                    IF len(@tmp) > 1
                                        SET @NM = @N + '.' + (convert(VARCHAR, @TMP))
                                    ELSE
                                        SET @NM = @N + '.' + (convert(VARCHAR, REPLACE(@tmp, 0, '')))
                                end
                            if (@docmid = @IdM1)
                                set @tm = @cnt
                            else
                                begin
                                    set @docmid = @IdM1
                                    set @cnt = @cnt + 1
                                    set @tm = @cnt
                                end
                            INSERT INTO #temp_result_Gppril6 ( documentid
                                                             , NAME
                                                             , Doc
                                --,ДатаНачала
                                --,ДатаОконч
                                                             , [KBK_Ved]
                                                             , [KBK_Div]
                                                             , [KBK_CS]
                                                             , [KBK_VR]
                                --,[ОЧГ13]
                                --,[ОЧГ14]
                                --,[ОЧГ15]
                                --,[ОЧГ16]
                                --,[ОЧГ17]
                                --,[ОЧГ18]
                                --,[ОЧГ19]
                                --,[ОЧГ20]
                                --,[ОЧГ21]
                                                             , [Isp]
                                                             , DocId
                                                             , Num
                                                             , [ОЧГ17фед]
                                                             , [ОЧГ17обл]
                                                             , [ОЧГ17итог]
                                                             , [Порядковый номер]
                                                             , serial)
                            SELECT @documentidM1
                                 , 'Основное мероприятие'
                                 , @DocM1
                                 --,a.date_s
                                 --,a.date_e
                                 , isnull(b.KBK_Ved, a.IspCode)
                                 , 'x'
                                 , 'x'
                                 , 'x'
                                 --,sum(isnull(b.[ОЧГ13], 0.00))
                                 --,sum(isnull(b.[ОЧГ14], 0.00))
                                 --,sum(isnull(b.[ОЧГ15], 0.00))
                                 --,sum(isnull(b.[ОЧГ16], 0.00))
                                 --,sum(isnull(b.[ОЧГ17], 0.00))
                                 --,sum(isnull(b.[ОЧГ18], 0.00))
                                 --,sum(isnull(b.[ОЧГ19], 0.00))
                                 --,sum(isnull(b.[ОЧГ20], 0.00))
                                 --,sum(isnull(b.[ОЧГ21], 0.00))
                                 , isnull(b.[Isp], a.[Isp])
                                 , @IdM1
                                 , @Num + '.' + convert(VARCHAR, @NumM1)
                                 , sum(isnull([ОЧГ17фед], 0.00))
                                 , sum(isnull([ОЧГ17обл], 0.00))
                                 , sum(isnull([ОЧГ17итог], 0.00))
                                 --,convert(VARCHAR,(select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id=@IdM1))
                                 , @Nm
                                 , @tm
                            FROM [#allDoc] a
                                     LEFT JOIN [#allSource] b ON a.id = b.id
                                     INNER JOIN (
                                SELECT id
                                FROM [ks_ddlControl].[GetDocID](@IdM1)

                                UNION

                                SELECT @IdM1
                            ) o ON a.id = o.id
                            GROUP BY isnull(b.[Isp], a.[Isp])
                                   --,a.date_s
                                   --,a.date_e
                                   , isnull(b.KBK_Ved, a.IspCode)
                        END

                    --------------------
                    -- МероприятиеДет --
                    --------------------
                    SET @NumM2 = NULL

                    DECLARE Cur3 CURSOR
                        FOR
                        SELECT a.documentid
                             , a.Doc
                             , a.Isp
                             , a.Id
                             , a.CountDwn
                             , a.CountIsp
                        FROM #alldoc a
                        WHERE a.[id_up] = @IdM1
                        ORDER BY a.code
                               , a.Id

                    OPEN Cur3

                    FETCH NEXT
                        FROM Cur3
                        INTO @documentidM2
                            ,@DocM2
                            ,@IspM2
                            ,@IdM2
                            ,@CountDwnM2
                            ,@CountIspM2

                    WHILE @@fetch_status = 0
                    BEGIN

                        SET @NumM2 = isnull(@NumM2, 0) + 1
                        SET @tmp = (convert(VARCHAR,
                                    (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)))
                        if @tmp like '%.%'
                            begin
                                if len(@tmp) > 1
                                    begin
                                        SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)
                                        if substring(@tmpr, 4, 1) = '0'
                                            begin
                                                SET @tmpr = substring(@tmpr, 1, 3) + substring(@tmpr, 5, 1)
                                            end
                                    end
                                else
                                    begin
                                        SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)
                                        if substring(@tmpr, 1, 1) = '0'
                                            begin
                                                SET @tmpr = substring(@tmpr, 2, 4)
                                                if substring(@tmpr, 3, 1) = '0'
                                                    SET @tmpr = REPLACE(@tmpr, '0', '')
                                            end
                                    end
                                SET @Nmb = @NM + '.' + @tmpr
                            end
                        else
                            begin
                                IF len(@tmp) > 1
                                    SET @NMB = @NM + '.' + (convert(VARCHAR, @TMP))
                                ELSE
                                    SET @NMB = @NM + '.' + (convert(VARCHAR, REPLACE(@tmp, 0, '')))
                            end
                        IF isnull(@CountDwnM2, 0) = 0
                            BEGIN
                                if (@docmid = @IdM2)
                                    set @tm = @cnt
                                else
                                    begin
                                        if ((select count(*) FROM [#allSource] where id = @IdM2) > 0)
                                            begin
                                                set @docmid = @IdM2
                                                set @cnt = @cnt + 1
                                                set @tm = @cnt
                                                --select @docmid
                                                --select @cnt
                                            end
                                    end
                                INSERT INTO #temp_result_Gppril6 ( documentid
                                                                 , NAME
                                                                 , Doc
                                                                 , ДатаНачала
                                                                 , ДатаОконч
                                                                 , KBK_Ved
                                                                 , [KBK_Div]
                                                                 , [KBK_CS]
                                                                 , [KBK_VR]
                                    --,[ОЧГ13]
                                    --,[ОЧГ14]
                                    --,[ОЧГ15]
                                    --,[ОЧГ16]
                                    --,[ОЧГ17]
                                    --,[ОЧГ18]
                                    --,[ОЧГ19]
                                    --,[ОЧГ20]
                                    --,[ОЧГ21]
                                                                 , [Isp]
                                                                 , DocId
                                                                 , Num
                                                                 , [ОЧГ17фед]
                                                                 , [ОЧГ17обл]
                                                                 , [ОЧГ17итог]
                                                                 , ДатаДокумента
                                                                 , [Порядковый номер]
                                                                 , serial
                                                                 , Примечание
                                                                 , Файл
                                                                 , ИмяФайла
--						,[БР]
--						,q.CL_2558
                                    -- ,CL_3164
                                )
                                SELECT @documentidM2
                                     , 'Мероприятие'
                                     , @DocM2
                                     --,a.date_s
                                     --,a.date_e
                                     , b.AT_3267
                                     , b.AT_3268
                                     , isnull(b.KBK_Ved, a.IspCode)
                                     , b.KBK_Div
                                     , b.KBK_CS
                                     , b.KBK_VR
                                     --,sum(isnull(b.[ОЧГ13], 0.00))
                                     --,sum(isnull(b.[ОЧГ14], 0.00))
                                     --,sum(isnull(b.[ОЧГ15], 0.00))
                                     --,sum(isnull(b.[ОЧГ16], 0.00))
                                     --,sum(isnull(b.[ОЧГ17], 0.00))
                                     --,sum(isnull(b.[ОЧГ18], 0.00))
                                     --,sum(isnull(b.[ОЧГ19], 0.00))
                                     --,sum(isnull(b.[ОЧГ20], 0.00))
                                     --,sum(isnull(b.[ОЧГ21], 0.00))
                                     , isnull(b.Isp, a.Isp)
                                     --,@IspM2
                                     , @IdM2
                                     , @Num + '.' + convert(VARCHAR, @NumM1) + '.' + convert(VARCHAR, @NumM2)
                                     , sum(isnull([ОЧГ17фед], 0.00))
                                     , sum(isnull([ОЧГ17обл], 0.00))
                                     , sum(isnull([ОЧГ17итог], 0.00))
                                     , convert(Varchar, b.AT_3190)
                                     , @nmb
                                     , @tm
                                     , isnull(b.AT_3269, a.AT_2760)
                                     , isnull(b.FileID, a.FileID)
                                     , isnull(b.FileName, a.FileName)
--						,sum(CASE
--				    WHEN q.at_1998 = 'федеральные'
--					  OR q.at_1998 = 'внебюджетные РФ'
--					THEN q.M_2932
--
--				ELSE 0
--				END) / 1000 AS [БР]
--
--						,sum(isnull(q.M_2932, 0.00)) / 1000 AS [q.M_2932]
--						,q.CL_2558
                                     --,b.CL_3164

                                FROM [#allDoc] a
                                         LEFT JOIN [#allSource] b ON a.id = b.id
--					left join KS_DDLControl.DS_957_2506 q on q.id_up=a.id
--					LEFT JOIN #sprVed m ON a.CL_2533 = m.id
                                WHERE a.id = @IdM2
                                GROUP BY

                                       -- a.date_s
                                       --,a.date_e

                                    b.AT_3267
                                       , b.AT_3268
                                       , isnull(b.KBK_Ved, a.IspCode)
                                       , b.KBK_Div
                                       , b.KBK_CS
                                       , isnull(b.AT_3269, a.AT_2760)
                                       , isnull(b.FileID, a.FileID)
                                       , isnull(b.FileName, a.FileName)
                                       , b.KBK_VR
                                       , isnull(b.Isp, a.Isp)
                                       , convert(Varchar, b.AT_3190)
--						,q.CL_2558


                            END
                        ELSE
                            BEGIN

                                IF @CountIspM2 > 1
                                    SET @tmp = (convert(VARCHAR,
                                                (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)))
                                if @tmp like '%.%'
                                    begin
                                        if len(@tmp) > 1
                                            begin
                                                SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)
                                                if substring(@tmpr, 4, 1) = '0'
                                                    begin
                                                        SET @tmpr = substring(@tmpr, 1, 3) + substring(@tmpr, 5, 1)
                                                    end
                                            end
                                        else
                                            begin
                                                SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)
                                                if substring(@tmpr, 1, 1) = '0'
                                                    begin
                                                        SET @tmpr = substring(@tmpr, 2, 4)
                                                        if substring(@tmpr, 3, 1) = '0'
                                                            SET @tmpr = REPLACE(@tmpr, '0', '')
                                                    end
                                            end
                                        SET @Nmb = @NM + '.' + @tmpr
                                    end
                                else
                                    begin
                                        IF len(@tmp) > 1
                                            SET @NMB = @NM + '.' + (convert(VARCHAR, @TMP))
                                        ELSE
                                            SET @NMB = @NM + '.' + (convert(VARCHAR, REPLACE(@tmp, 0, '')))
                                    end
                                BEGIN
                                    if (@docmid = @IdM2)
                                        set @tm = @cnt
                                    else
                                        begin
                                            set @docmid = @IdM2
                                            set @cnt = @cnt + 1
                                            set @tm = @cnt
                                        end
                                    -----------
                                    -- Всего --
                                    -----------
                                    INSERT INTO #temp_result_Gppril6 ( documentid
                                                                     , NAME
                                                                     , Doc
                                                                     , ДатаНачала
                                                                     , ДатаОконч
                                                                     , [KBK_Ved]
                                                                     , [KBK_Div]
                                                                     , [KBK_CS]
                                                                     , [KBK_VR]
                                        --,[ОЧГ13]
                                        --,[ОЧГ14]
                                        --,[ОЧГ15]
                                        --,[ОЧГ16]
                                        --,[ОЧГ17]
                                        --,[ОЧГ18]
                                        --,[ОЧГ19]
                                        --,[ОЧГ20]
                                        --,[ОЧГ21]
                                                                     , [Isp]
                                                                     , DocId
                                                                     , Num
                                                                     , [ОЧГ17фед]
                                                                     , [ОЧГ17обл]
                                                                     , [ОЧГ17итог]
                                                                     , [Порядковый номер]
                                                                     , serial)
                                    SELECT @documentidM2
                                         , 'Мероприятие'
                                         , @DocM2
                                         , ''
                                         , ''
                                         , 'x'
                                         , 'x'
                                         , 'x'
                                         , 'x'
                                         --,sum(isnull(b.[ОЧГ13], 0.00))
                                         --,sum(isnull(b.[ОЧГ14], 0.00))
                                         --,sum(isnull(b.[ОЧГ15], 0.00))
                                         --,sum(isnull(b.[ОЧГ16], 0.00))
                                         --,sum(isnull(b.[ОЧГ17], 0.00))
                                         --,sum(isnull(b.[ОЧГ18], 0.00))
                                         --,sum(isnull(b.[ОЧГ19], 0.00))
                                         --,sum(isnull(b.[ОЧГ20], 0.00))
                                         --,sum(isnull(b.[ОЧГ21], 0.00))
                                         , 'Всего'
                                         , @IdM2
                                         , @Num + '.' + convert(VARCHAR, @NumM1) + '.' + convert(VARCHAR, @NumM2)
                                         , sum(isnull([ОЧГ17фед], 0.00))
                                         , sum(isnull([ОЧГ17обл], 0.00))
                                         , sum(isnull([ОЧГ17итог], 0.00))
                                         , @nmb
                                         , @tm
                                    FROM [#allDoc] a
                                             LEFT JOIN [#allSource] b ON a.id = b.id
                                             INNER JOIN (
                                        SELECT id
                                        FROM [ks_ddlControl].[GetDocID](@IdM2)

                                        UNION

                                        SELECT @IdM2
                                    ) o ON a.id = o.id
                                END


                                --------------------------
                                -- Итоги в разрезе ГРБС --
                                --------------------------
                                SET @tmp = (convert(VARCHAR,
                                            (select top 1 AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)))
                                if @tmp like '%.%'
                                    begin
                                        if len(@tmp) > 1
                                            begin
                                                SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)
                                                if substring(@tmpr, 4, 1) = '0'
                                                    begin
                                                        SET @tmpr = substring(@tmpr, 1, 3) + substring(@tmpr, 5, 1)
                                                    end
                                            end
                                        else
                                            begin
                                                SET @tmpr = (select AT_2837 FROM KS_DDLControl.DS_957_2475 where id = @IdM2)
                                                if substring(@tmpr, 1, 1) = '0'
                                                    begin
                                                        SET @tmpr = substring(@tmpr, 2, 4)
                                                        if substring(@tmpr, 3, 1) = '0'
                                                            SET @tmpr = REPLACE(@tmpr, '0', '')
                                                    end
                                            end
                                        SET @Nmb = @NM + '.' + @tmpr
                                    end
                                else
                                    begin
                                        IF len(@tmp) > 1
                                            SET @NMB = @NM + '.' + (convert(VARCHAR, @TMP))
                                        ELSE
                                            SET @NMB = @NM + '.' + (convert(VARCHAR, REPLACE(@tmp, 0, '')))
                                    end
                                if (@docmid = @IdM2)
                                    set @tm = @cnt
                                else
                                    begin
                                        set @docmid = @IdM2
                                        set @cnt = @cnt + 1
                                        set @tm = @cnt
                                    end
                                INSERT INTO #temp_result_Gppril6 ( documentid
                                                                 , NAME
                                                                 , Doc
                                                                 , ДатаНачала
                                                                 , ДатаОконч
                                                                 , [KBK_Ved]
                                                                 , [KBK_Div]
                                                                 , [KBK_CS]
                                                                 , [KBK_VR]
                                    --,[ОЧГ13]
                                    --,[ОЧГ14]
                                    --,[ОЧГ15]
                                    --,[ОЧГ16]
                                    --,[ОЧГ17]
                                    --,[ОЧГ18]
                                    --,[ОЧГ19]
                                    --,[ОЧГ20]
                                    --,[ОЧГ21]
                                                                 , [Isp]
                                                                 , DocId
                                                                 , Num
                                                                 , [ОЧГ17фед]
                                                                 , [ОЧГ17обл]
                                                                 , [ОЧГ17итог]
                                                                 , [Порядковый номер]
                                                                 , serial)
                                SELECT @documentidM2
                                     , 'Мероприятие'
                                     , @DocM2
                                     , a.date_s
                                     , a.date_e
                                     , isnull(b.KBK_Ved, a.IspCode)
                                     , 'x'
                                     , 'x'
                                     , 'x'
                                     --,sum(isnull(b.[ОЧГ13], 0.00))
                                     --,sum(isnull(b.[ОЧГ14], 0.00))
                                     --,sum(isnull(b.[ОЧГ15], 0.00))
                                     --,sum(isnull(b.[ОЧГ16], 0.00))
                                     --,sum(isnull(b.[ОЧГ17], 0.00))
                                     --,sum(isnull(b.[ОЧГ18], 0.00))
                                     --,sum(isnull(b.[ОЧГ19], 0.00))
                                     --,sum(isnull(b.[ОЧГ20], 0.00))
                                     --,sum(isnull(b.[ОЧГ21], 0.00))
                                     , isnull(b.[Isp], a.[Isp])
                                     , @IdM2
                                     , @Num + '.' + convert(VARCHAR, @NumM1) + '.' + convert(VARCHAR, @NumM2)
                                     , sum(isnull([ОЧГ17фед], 0.00))
                                     , sum(isnull([ОЧГ17обл], 0.00))
                                     , sum(isnull([ОЧГ17итог], 0.00))
                                     , @nmb
                                     , @tm

                                FROM [#allDoc] a
                                         LEFT JOIN [#allSource] b ON a.id = b.id
                                         INNER JOIN (
                                    SELECT id
                                    FROM [ks_ddlControl].[GetDocID](@IdM2)

                                    UNION

                                    SELECT @IdM2
                                ) o ON a.id = o.id
                                GROUP BY isnull(b.[Isp], a.[Isp])
                                       , a.date_s
                                       , a.date_e
                                       , isnull(b.KBK_Ved, a.IspCode)
                            END

                        --select * from #allDoc

                        -----------------------
                        -- Контрольное событие --
                        -----------------------
                        --set @cnt=@cnt+1
                        --set @tm=@cnt
                        --select @NumM2
                        INSERT INTO #temp_result_Gppril6 ( documentid
                                                         , NAME
                                                         , Doc
--					,ДатаНачала
                                                         , ДатаНаступления
--					,[KBK_Ved]
--					,[KBK_Div]
--					,[KBK_CS]
--					,[KBK_VR]
--					,[ОЧГ13]
--					,[ОЧГ14]
--					,[ОЧГ15]
--					,[ОЧГ16]
--					,[ОЧГ17]
--					,[ОЧГ18]
--					,[ОЧГ19]
--					,[ОЧГ20]
--					,[ОЧГ21]
                                                         , [Isp]
                                                         , DocId
                                                         , Num
--					,[ОЧГ17фед]
--					,[ОЧГ17обл]
                                                         , [КС_показатель]
                                                         , [КС_ЕдИзмерения] --11.07
                                                         , [Порядковый номер]
                                                         , serial
                                                         , ПорядковыйНомерКС)
                        SELECT a.documentid
                             , 'Контрольное событие'
                             , b.AT_2565
--					,a.date_s
--					,a.date_e
                             , a.AT_2759
--		            ,isnull(b.KBK_Ved, a.IspCode)
--					,[KBK_Div]
--					,[KBK_CS]
--					,[KBK_VR]
--					,sum([ОЧГ13])
--					,sum([ОЧГ14])
--					,sum([ОЧГ15])
--					,sum([ОЧГ16])
--					,sum([ОЧГ17])
--					,sum([ОЧГ18])
--					,sum([ОЧГ19])
--					,sum([ОЧГ20])
--					,sum([ОЧГ21])
                             , a.[Isp]
                             , a.Id
                             , @Num + '.' + convert(VARCHAR, @NumM1) + '.' + convert(VARCHAR, @NumM2) + '.' +
                               convert(VARCHAR, dense_rank() OVER (
                                   PARTITION BY a.[id_up] ORDER BY a.ks
                                       ,a.id
                                   ))
--					,sum(isnull([ОЧГ17фед], 0.00))
--					,sum(isnull([ОЧГ17обл], 0.00))
                             , isnull(a.AT_3386, b.AT_2753)
                             , a.AT_2544 --11.07
                             , @nmb
                             --,@tm + dense_rank() OVER (PARTITION BY a.[id_up] ORDER BY a.ks,a.id)
                             , 0
                             , a.AT_3407
--
--				FROM [#allDoc] a
--
--   INNER JOIN KS_DDLControl.CL_962_2518 b ON a.KS = b.id
--                left JOIN KS_DDLControl.DS_957_2521 c ON b.id=c.CL_2758
--                left join KS_DDLControl.CL_960_2491 e on e.id=c.CL_3260  --11.07
--
--
--				WHERE
--				a.id_up = @IdM2 and
--				a.id = c.id_up -- связь с мероприятием, для вывода 1-ой даты КС
--				and a.AT_2529 =  (convert(varchar(10),@Data_ks,112))

                        FROM [#allDoc] a
                                 left JOIN KS_DDLControl.CL_962_2518 b ON b.id = a.KS


                        WHERE a.id_up = @IdM2
                          and a.AT_2529 = (convert(varchar(10), @Data_ks, 112))


--			GROUP BY
--a.documentid
--					,b.AT_2565
--					,a.date_s
--					,a.date_e

--					,isnull(b.KBK_Ved, a.IspCode)
--					,[KBK_Div]
--					,[KBK_CS]
--					,[KBK_VR]
--					,isnull(b.[Isp], a.[Isp])
--					,a.Id
--					,a.Code
--					,a.[id_up]
                        ORDER BY -- a.Code
                                 --a.id
                                 a.AT_2759

                        set @new = (SELECT count(*) FROM #temp_result_Gppril6 WHERE documentid = 337)
                        --set @cnt=@cnt+(@new-@last)
                        set @rzn = @new - @last
                        set @last = (SELECT count(*) FROM #temp_result_Gppril6 WHERE documentid = 337)
                        set @i = 0

                        if (@rzn <> 0)
                            begin
                                while (@i <> @rzn)
                                begin
                                    set @i = @i + 1
                                    UPDATE #temp_result_Gppril6
                                    SET serial=(@cnt + @i),
                                        [Порядковый номер]=(@NMB + '.' + (convert(VARCHAR, (@cnt + @i))))
                                    WHERE id = (select min(id)
                                                from #temp_result_Gppril6
                                                WHERE documentid = 337
                                                  and serial = 0)
                                end
                                set @cnt = @cnt + @rzn
                            end


                        --select * from [#allDoc]
--select * from #temp_result_Gppril6

                        FETCH NEXT
                            FROM Cur3
                            INTO @documentidM2
                                ,@DocM2
                                ,@IspM2
                                ,@IdM2
                                ,@CountDwnM2
                                ,@CountIspM2
                    END

                    CLOSE Cur3

                    DEALLOCATE Cur3

                    FETCH NEXT
                        FROM Cur2
                        INTO @documentidM1
                            ,@DocM1
                            ,@IspM1
                            ,@IdM1
                            ,@CountDwnM1
                            ,@CountIspM1
                END

                CLOSE Cur2

                DEALLOCATE Cur2

                SET @documentidPpGpPrevious = @documentidPpGP

                FETCH NEXT
                    FROM Cur1
                    INTO @documentidPpGP
                        ,@DocPpGP
                        ,@IspPpGP
                        ,@IdPpGP
                        ,@CountDwnPpGP
                        ,@CountIspPpGP
            END

            CLOSE Cur1

            DEALLOCATE Cur1

            UPDATE #temp_result_Gppril6
            SET Post = @Post

            DELETE
            FROM #temp_result_Gppril6
            WHERE num IN (
                SELECT num
                FROM #temp_result_Gppril6
                WHERE num IS NOT NULL
                  and documentid <> 337
                GROUP BY num
                HAVING count(*) > 1
            )
              --AND (
              --(isnull([ОЧГ13], 0.00) + isnull([ОЧГ14], 0.00) + isnull([ОЧГ15], 0.00) + isnull([ОЧГ16], 0.00) + isnull([ОЧГ17], 0.00) + isnull([ОЧГ18], 0.00) + isnull([ОЧГ19], 0.00) + isnull([ОЧГ20], 0.00) + isnull([ОЧГ21], 0.00) = 0)
              AND [KBK_Ved] IS NULL
            --)

        END

    --/*Удаляем пустые строки*/
--
    DELETE
    FROM #temp_result_Gppril6
    where [ОЧГ17итог] = 0.0 and [ОЧГ17фед] = 0.0 and [ОЧГ17обл] = 0.0 and KBK_CS IS NULL
       or [ОЧГ17итог] = 0.0 and [ОЧГ17фед] = 0.0 and [ОЧГ17обл] = 0.0 and KBK_CS = 'x'


--удаление Всего при одном мероприятии

    CREATE TABLE #temp
    (
        NAME    VARCHAR(50),
        Doc     VARCHAR(2000),
        [ОЧГ17] NUMERIC(20, 2),
        id      INT,
        kbk_ved VARCHAR(50),
        kbk_cs  VARCHAR(50),
        DocId   VARCHAR(50)

    ) ;

    INSERT INTO #temp
    select name
         , doc
         , [ОЧГ17итог]
         , min(id)
         , kbk_ved
         , kbk_cs
         , DocId

    FROM #temp_result_Gppril6
--where kbk_ved='x'
    GROUP BY [ОЧГ17итог]
           , name
           , doc
--,id
           , kbk_ved
           , kbk_cs
           , DocId

    HAVING COUNT([ОЧГ17итог]) > 1 ;


--select * from #temp

    DELETE
    FROM #temp_result_Gppril6
    where id in (select id from #temp)
    CREATE TABLE #temp2
    (
        id         INT,
        DocId      VARCHAR(50),
        kbk_cs     VARCHAR(50),
        Doc        VARCHAR(2000),
        [KBK_Div]  VARCHAR(4),
        documentid INT

    ) ;

    INSERT INTO #temp2
    select min(id)
         , DocId
         , kbk_cs
         , doc
         , [KBK_Div]
         , documentid

    FROM #temp_result_Gppril6
    where documentid <> 337
    GROUP BY DocId
           , kbk_cs
           , doc
           , [KBK_Div]
           , documentid

    HAVING COUNT(DocId) = 2 ;


--select * from #temp2

    DELETE
    FROM #temp_result_Gppril6
    where id in (select id from #temp2)
    drop TABLE #temp;
    drop TABLE #temp2;


    DECLARE
        @q integer
    DECLARE
        @w integer
    DECLARE
        @max integer
    set @max = (select max(serial)
                from #temp_result_Gppril6)
    set @q = 1
    set @w = 1
    while @q < @max + 1
    begin
        if (select count(*) from #temp_result_Gppril6 where serial = @q) > 0
            begin
                UPDATE #temp_result_Gppril6 SET serial = @w where serial = @q
                set @w = @w + 1
                set @q = @q + 1
            end
        else
            set @q = @q + 1
    end

    --Обновляем номер
    update #temp_result_Gppril6
    SET [Порядковый номер] = replace([Порядковый номер], '.0', '.')
    SELECT *
    FROM #temp_result_Gppril6
    ORDER BY id
    if @ToTable = 1
        begin
            insert into #t_GPpril select * from #temp_result_Gppril6
        end