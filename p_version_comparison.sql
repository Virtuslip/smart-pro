-- Сравнение версий
    alter proc [KS_DDLControl].[p_version_comparison](@nmode int = 0, @GP int = 6561, @Ver00 varchar(2) = '01',
    @Ver01 varchar(2) = '02', @OnlyChanges varchar(3) = 'Нет')
    as
    create table #temp_result
    (
        orderGP                   varchar(2),
        action                    varchar(500),
        idgp                      int,
        [ГП]                      varchar(2000),
        idppgp                    int,
        [ППГП]                    varchar(max),
        [ППГП_Код]                varchar(20),
        GpDBegin                  int,
        GpDEnd                    int,
        ParamDate                 int,
        [Source]                  varchar(100),
        [ОЧГ13]                   numeric(20, 2),
        [ОЧГ14]                   numeric(20, 2),
        [ОЧГ15]                   numeric(20, 2),
        [ОЧГ16]                   numeric(20, 2),
        [ОЧГ17]                   numeric(20, 2),
        [ОЧГ18]                   numeric(20, 2),
        [ОЧГ19]                   numeric(20, 2),
        [ОЧГ20]                   numeric(20, 2),
        [ОЧГ21]                   numeric(20, 2),
        [ОЧГ22]                   numeric(20, 2),
        [ОЧГ23]                   numeric(20, 2),
        [ОЧГ24]                   numeric(20, 2),
        [ОЧГ25]                   numeric(20, 2),
        [ЕдИзмерения]             varchar(500),
        [ПорядковыйНомерПЗППГП]   varchar(10),
        idgp_1                    int,
        [ГП_1]                    varchar(2000),
        idppgp_1                  int,
        [ППГП_1]                  varchar(max),
        [ППГП_Код_1]              varchar(20),
        GpDBegin_1                int,
        GpDEnd_1                  int,
        ParamDate_1               int,
        [Source_1]                varchar(100),
        [ОЧГ13_1]                 numeric(20, 2),
        [ОЧГ14_1]                 numeric(20, 2),
        [ОЧГ15_1]                 numeric(20, 2),
        [ОЧГ16_1]                 numeric(20, 2),
        [ОЧГ17_1]                 numeric(20, 2),
        [ОЧГ18_1]                 numeric(20, 2),
        [ОЧГ19_1]                 numeric(20, 2),
        [ОЧГ20_1]                 numeric(20, 2),
        [ОЧГ21_1]                 numeric(20, 2),
        [ОЧГ22_1]                 numeric(20, 2),
        [ОЧГ23_1]                 numeric(20, 2),
        [ОЧГ24_1]                 numeric(20, 2),
        [ОЧГ25_1]                 numeric(20, 2),
        [ЕдИзмерения_1]           varchar(500),
        [ПорядковыйНомерПЗППГП_1] varchar(10),
        Post                      varchar(max),
        documentid                int,
        id                        int identity (1,1)


    )
    begin
        set nocount on
        set transaction isolation level read uncommitted

        --Версия левой части
        create table #t_version_00
        (
            [idgp]                              int,
            [гп]                                varchar(max),
            [ordergp]                           varchar(max),
            [action]                            varchar(max),
            [idppgp]                            int,
            [ппгп]                              varchar(max),
            [ппгп_код]                          varchar(max),
            [gpdbegin]                          int,
            [gpdend]                            int,
            [paramdate]                         int,
            [source]                            varchar(max),
            [код_источника_финансирования]      varchar(max),
            [очг13]                             numeric(20, 4),
            [очг14]                             numeric(20, 4),
            [очг15]                             numeric(20, 4),
            [очг16]                             numeric(20, 4),
            [очг17]                             numeric(20, 4),
            [очг18]                             numeric(20, 4),
            [очг19]                             numeric(20, 4),
            [очг20]                             numeric(20, 4),
            [очг21]                             numeric(20, 4),
            [очг22]                             numeric(20, 4),
            [очг23]                             numeric(20, 4),
            [очг24]                             numeric(20, 4),
            [очг25]                             numeric(20, 4),
            [documentid]                        int,
            [post]                              varchar(max),
            [id]                                int,
            [едизмерения]                       varchar(max),
            [порядковыйномерпзппгп]             varchar(max),
            [наименование_показателя]           varchar(max),
            [методика_расчета]                  varchar(max),
            [методика_расчета_формула_описание] varchar(max),
            [источник_значения]                 varchar(max)
        )
        insert into #t_version_00 exec [ks_ddlcontrol].[sp_poly_hybrid_query_3329] @nmode=0, @Госпрограмма = @GP,
                                       @Версия = @Ver00,
                                       @Собрать_ожидаемые_результаты = 'Да'

        --Версия правой части
        create table #t_version_01
        (
            [idgp]                              int,
            [гп]                                varchar(max),
            [ordergp]                           varchar(max),
            [action]                            varchar(max),
            [idppgp]                            int,
            [ппгп]                              varchar(max),
            [ппгп_код]                          varchar(max),
            [gpdbegin]                          int,
            [gpdend]                            int,
            [paramdate]                         int,
            [source]                            varchar(max),
            [код_источника_финансирования]      varchar(max),
            [очг13]                             numeric(20, 4),
            [очг14]                             numeric(20, 4),
            [очг15]                             numeric(20, 4),
            [очг16]                             numeric(20, 4),
            [очг17]                             numeric(20, 4),
            [очг18]                             numeric(20, 4),
            [очг19]                             numeric(20, 4),
            [очг20]                             numeric(20, 4),
            [очг21]                             numeric(20, 4),
            [очг22]                             numeric(20, 4),
            [очг23]                             numeric(20, 4),
            [очг24]                             numeric(20, 4),
            [очг25]                             numeric(20, 4),
            [documentid]                        int,
            [post]                              varchar(max),
            [id]                                int,
            [едизмерения]                       varchar(max),
            [порядковыйномерпзппгп]             varchar(max),
            [наименование_показателя]           varchar(max),
            [методика_расчета]                  varchar(max),
            [методика_расчета_формула_описание] varchar(max),
            [источник_значения]                 varchar(max)
        )
        insert into #t_version_01 exec [ks_ddlcontrol].[sp_poly_hybrid_query_3329] @nmode=0, @Госпрограмма = @GP,
                                       @Версия = @Ver01,
                                       @Собрать_ожидаемые_результаты = 'Да'

        declare
            @gp_name   varchar(2000),
            @id        int,
            @gp_name_1 varchar(2000),
            @id_1      int

        select @gp_name = [гп], @id = [idgp] from #t_version_00
        select @gp_name_1 = [гп], @id_1 = [idgp] from #t_version_01

        insert into #temp_result
        select isnull(a.[ordergp], b.[ordergp]),
               isnull(a.[action], b.[action]),
               @id,
               @gp_name,
               a.[idppgp],
               a.[ппгп],
               a.[ппгп_код],
               a.[gpdbegin],
               a.[gpdend],
               a.[paramdate],
               a.[source],
               a.[очг13],
               a.[очг14],
               a.[очг15],
               a.[очг16],
               a.[очг17],
               a.[очг18],
               a.[очг19],
               a.[очг20],
               a.[очг21],
               a.[очг22],
               a.[очг23],
               a.[очг24],
               a.[очг25],
               a.[едизмерения],
               a.[порядковыйномерпзппгп],
               @id_1,
               @gp_name_1,
               b.[idppgp],
               b.[ппгп],
               b.[ппгп_код],
               b.[gpdbegin],
               b.[gpdend],
               b.[paramdate],
               b.[source],
               b.[очг13],
               b.[очг14],
               b.[очг15],
               b.[очг16],
               b.[очг17],
               b.[очг18],
               b.[очг19],
               b.[очг20],
               b.[очг21],
               b.[очг22],
               b.[очг23],
               b.[очг24],
               b.[очг25],
               b.[едизмерения],
               b.[порядковыйномерпзппгп],
               isnull(a.[post], b.[post]),
               isnull(a.[documentid], b.[documentid])
        from #t_version_00 a
                 full join #t_version_01 b on isnull(a.[ordergp], '') = isnull(b.[ordergp], '') and
                                              isnull(a.[action], '') = isnull(b.[action], '') and
                                              isnull(a.[ппгп], '') = isnull(b.[ппгп], '') and
                                              isnull(a.[ппгп_код], '') = isnull(b.[ппгп_код], '') and
                                              isnull(a.[source], '') = isnull(b.[source], '')

        delete
        from #temp_result
        where [ппгп_код] is null
          and [ппгп_код_1] is null
          and [action] in
              (select distinct [action] from #temp_result where [ппгп_код] is not null or [ппгп_код_1] is not null)

    end
    if @OnlyChanges = 'Да'
        begin
            select *
            from #temp_result
            where [ГП] != [ГП_1]
               or isnull([ППГП_Код], '') != isnull([ППГП_Код_1], '')
               or isnull([ППГП], '') != isnull([ППГП_1], '')
               or isnull([Source], '') != isnull([Source_1], '')
               or isnull([ОЧГ13], 0) != isnull([ОЧГ13_1], 0)
               or isnull([ОЧГ14], 0) != isnull([ОЧГ14_1], 0)
               or isnull([ОЧГ15], 0) != isnull([ОЧГ15_1], 0)
               or isnull([ОЧГ16], 0) != isnull([ОЧГ16_1], 0)
               or isnull([ОЧГ17], 0) != isnull([ОЧГ17_1], 0)
               or isnull([ОЧГ18], 0) != isnull([ОЧГ18_1], 0)
               or isnull([ОЧГ19], 0) != isnull([ОЧГ19_1], 0)
               or isnull([ОЧГ20], 0) != isnull([ОЧГ20_1], 0)
               or isnull([ОЧГ21], 0) != isnull([ОЧГ21_1], 0)
               or isnull([ОЧГ22], 0) != isnull([ОЧГ22_1], 0)
               or isnull([ОЧГ23], 0) != isnull([ОЧГ23_1], 0)
               or isnull([ОЧГ24], 0) != isnull([ОЧГ24_1], 0)
               or isnull([ОЧГ25], 0) != isnull([ОЧГ25_1], 0)
        end
    else
        begin
            select * from #temp_result
        end