alter PROC [KS_DDLControl].[p_history_status_gp](@GP varchar(max)='6562',
    @dateStart DateTime = '20191101',
    @dateEnd DateTime = '20191206',
    @nmode INT = 0)
    --@GP varchar(max)='6562@#$6821@#$6854',
    AS

    --Статусы бизнес процессов
    CREATE TABLE #status_bp
    (
        checked     int,
        link        INT,
        code        varchar(100),
        name        VARCHAR(1000),
        is_sys      int,
        is_button   int,
        t_icon      varchar(2000),
        t_orders    int,
        set_comment int,
        action_name varchar(50)
    );
    insert into #status_bp exec dbo.list @cObjCode='DICTIONARY_STEP_STATUS'

    --     select *
    --     from #status_bp
    declare
        @dateEndNextDay DateTime  = dateadd(day, 1, @dateEnd)

--Собираем ID подпрограммы
    set @GP = replace(@GP, '@#$', '|') + '|'
    declare
        @gpID table
              (
                  id int
              )
    declare
        @pos int = charindex('|', @GP)
    declare
        @id int
    while (@pos != 0)
    begin
        set @id = SUBSTRING(@GP, 1, @pos - 1)
        insert into @gpID (id) values (cast(@id as int))
        set @GP = SUBSTRING(@GP, @pos + 1, LEN(@GP))
        set @pos = CHARINDEX('|', @GP)
    end

--Документы управления экономики
    select ID,
           ID_UP,
           AT_3289                                          as DateDoc,
           AT_3270.value('(/file/name)[1]', 'varchar(200)') as FileName,
           '957_3310_3270_' + convert(varchar, ID)          as FileID
    into #docsUE
    from KS_DDLControl.DS_957_3310
    where CL_3293 = 3

--Документы управления финансов
    select ID,
           ID_UP,
           AT_3289                                          as DateDoc,
           AT_3270.value('(/file/name)[1]', 'varchar(200)') as FileName,
           '957_3310_3270_' + convert(varchar, ID)          as FileID
    into #docsUF
    from KS_DDLControl.DS_957_3310
    where CL_3293 = 4

--Документы правового управления
    select ID,
           ID_UP,
           AT_3289                                          as DateDoc,
           AT_3270.value('(/file/name)[1]', 'varchar(200)') as FileName,
           '957_3310_3270_' + convert(varchar, ID)          as FileID
    into #docsPU
    from KS_DDLControl.DS_957_3310
    where CL_3293 = 6
    --select * from #docsPU

--Статус завершения
    select gmp.ID,
           bp_hist.date_last_change as date_doc,
           status.name
    into #status_end
    from KS_DDLControl.DS_957_2475 gmp
             left join bpms_document_router_history bp_hist on gmp.ID = bp_hist.link_doc
             left join #status_bp status on status.link = bp_hist.step_status
             left join KS_DDLControl.CL_956_2470 gp on gmp.CL_2640 = gp.ID
    where (bp_hist.bpms_object = 575 or bp_hist.bpms_object = 704)
      and bp_hist.date_last_change >= @dateStart
      and bp_hist.date_last_change < @dateEndNextDay
      and gp.ID in (select * from @gpID)
    --     select * from KS_DDLControl.DS_957_2475 where id = 22323
--     select top 1000 *
--     from bpms_document_router_history bp_hist
--     where bp_hist.date_last_change >= @dateStart
--       and bp_hist.date_last_change <= @dateEnd
--
--     select *
--     from #status_end

--Исполнители ГМП
    select isp.ID,
           ver.AT_3201 as Name
    into #isp_gmp
    from KS_DDLControl.CL_1280_3226 isp
             left join KS_DDLControl.CL_1280_3231 ver on ver.ID_UP = isp.ID
    select gmp.ID
         ,
        CONVERT(date, convert(char(8), gmp.AT_2529))                   as [Дата документа]
         ,
        gp.AT_2524                                                     as [Код программы]
         ,
        gpName.AT_2526                                                 as [Наименование]
         ,
        gmp.AT_2845                                                    as [Версия]
         ,
        gmp.AT_3303                                                    as [Примечание]
         ,
        bp_hist.date_doc                                               as [Дата поступления в УЭ]
         , CONVERT(date, convert(char(8), docs.DateDoc))               as [Дата заключения УЭ]
         --,docs.DateDoc as [Дата заключения УЭ]
         , docs.FileName                                               as [Имя файла УЭ]
         , docs.FileID                                                 as [ID файла УЭ]
         , CONVERT(date, convert(char(8), docs_uf.DateDoc))            as [Дата заключения УФ]
         --,docs_uf.DateDoc as [Дата заключения УФ]
         , docs_uf.FileName                                            as [Имя файла УФ]
         , docs_uf.FileID                                              as [ID файла УФ]
         , CONVERT(date, convert(char(8), docs_pu.DateDoc))            as [Дата заключения ПУ]
         , docs_pu.FileName                                            as [Имя файла ПУ]
         , docs_pu.FileID                                              as [ID файла ПУ]
         , status_end.date_doc                                         as [Дата завершения]
         , status_end.name                                             as [Статус завершения]
         , isp.Name                                                    as [Ответсвенный исполнитель]
         , (datediff(day, bp_hist.date_doc, status_end.date_doc) -
            datediff(week, bp_hist.date_doc, status_end.date_doc) * 2) as [Разница дат]
         , (datediff(day, bp_hist.date_doc, CONVERT(date, convert(char(8), docs_uf.DateDoc))) -
            datediff(week, bp_hist.date_doc, CONVERT(date, convert(char(8), docs_uf.DateDoc))) *
            2)                                                         as [Разница дат c УФ]
         , (datediff(day, bp_hist.date_doc, CONVERT(date, convert(char(8), docs.DateDoc))) -
            datediff(week, bp_hist.date_doc, CONVERT(date, convert(char(8), docs.DateDoc))) *
            2)                                                         as [Разница дат c УЭ]
         , (datediff(day, bp_hist.date_doc, CONVERT(date, convert(char(8), docs_pu.DateDoc))) -
            datediff(week, bp_hist.date_doc, CONVERT(date, convert(char(8), docs_pu.DateDoc))) *
            2)                                                         as [Разница дат c ПУ]

    from KS_DDLControl.DS_957_2475 gmp
             left join bpms_document_router_history bp_hist on gmp.ID = bp_hist.link_doc
             left join #docsUE docs on docs.ID_UP = gmp.ID
             left join #docsUF docs_uf on docs_uf.ID_UP = gmp.ID
             left join #docsPU docs_pu on docs_pu.ID_UP = gmp.ID
             left join #status_end status_end on status_end.ID = gmp.ID
             left join KS_DDLControl.CL_956_2470 gp on gmp.CL_2640 = gp.ID
             left join (select id_up, AT_2526, max(isnull(at_3398, 20000101)) as dateStart
                        from ks_ddlcontrol.cl_956_3452
                        group by id_up, AT_2526) gpName on (gp.id = gpName.id_up)
             left join #isp_gmp isp on gmp.CL_3252 = isp.ID
    where (bp_hist.bpms_object = 573 or bp_hist.bpms_object = 691)
      and bp_hist.date_last_change >= @dateStart
      and bp_hist.date_last_change < @dateEndNextDay
      and gp.ID in (select * from @gpID)
    order by gp.AT_2524, gmp.AT_2845