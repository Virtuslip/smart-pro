

create proc [ks_ddlcontrol].[sp_poly_hybrid_query_3329]
    @nmode int=0,
    @table_name_result varchar(max)='',
    @cProject int = 0,
    @Госпрограмма int = '6561',
    @Версия varchar(2) = '11',
    @Собрать_ожидаемые_результаты varchar(max) = 'Да'
as
    set nocount on
    set transaction isolation level read uncommitted
declare @ErrorMessage nvarchar(4000)
declare @ErrorSeverity int
declare @ErrorState int
declare @sel varchar(max)
declare @temp_result VARCHAR(max)
select @temp_result = '[ks_ddlcontrol].[temp_result_' + convert(varchar(36), newid()) + ']'

    -- Выполнение подзапросов

-- Подзапрос Запрос_3312
DECLARE @table_Запрос_77774613 VARCHAR(max)
SELECT @table_Запрос_77774613 = '[ks_ddlcontrol].[Запрос_77774613_' + convert(varchar(36), newid()) + ']'
begin try
    declare @table_Запрос_77774613_sql varchar(max)
    select @table_Запрос_77774613_sql = '[ks_ddlcontrol].[Запрос_77774613_' + convert(varchar(36), newid()) + ']'

    EXEC [ks_ddlcontrol].[xp_prggp_2] @nmode = @nmode, @table_name_result = @table_Запрос_77774613_sql, @GP = @Госпрограмма, @Ver = @Версия, @Result = @Собрать_ожидаемые_результаты

    -- Дополнительная обработка для пользовательских запросов к БД (нестандартные имена полей)
    exec ('create table ' + @table_Запрос_77774613 + ' (
    [idgp] int null,
    [гп] varchar(max) null,
    [ordergp] varchar(max) null,
    [action] varchar(max) null,
    [idppgp] int null,
    [ппгп] varchar(max) null,
    [ппгп_код] varchar(max) null,
    [gpdbegin] int null,
    [gpdend] int null,
    [paramdate] int null,
    [source] varchar(max) null,
    [код_источника_финансирования] varchar(max) null,
    [очг13] numeric(20,4) null,
    [очг14] numeric(20,4) null,
    [очг15] numeric(20,4) null,
    [очг16] numeric(20,4) null,
    [очг17] numeric(20,4) null,
    [очг18] numeric(20,4) null,
    [очг19] numeric(20,4) null,
    [очг20] numeric(20,4) null,
    [очг21] numeric(20,4) null,
    [очг22] numeric(20,4) null,
    [очг23] numeric(20,4) null,
    [очг24] numeric(20,4) null,
    [очг25] numeric(20,4) null,
    [documentid] int null,
    [post] varchar(max) null,
    [id] int null,
    [едизмерения] varchar(max) null,
    [порядковыйномерпзппгп] varchar(max) null,
    [наименование_показателя] varchar(max) null,
    [методика_расчета] varchar(max) null,
    [методика_расчета_формула_описание] varchar(max) null,
    [источник_значения] varchar(max) null)')
    exec ('insert ' + @table_Запрос_77774613 + ' select [idgp], [гп], [ordergp], [action], [idppgp], [ппгп], [ппгп_код], [gpdbegin], [gpdend], [paramdate], [source], [код источника финансирования], [очг13], [очг14], [очг15], [очг16], [очг17], [очг18], [очг19], [очг20], [очг21], [очг22], [очг23], [очг24], [очг25], [documentid], [post], [id], [едизмерения], [порядковыйномерпзппгп], [наименование_показателя], [методика_расчета], [методика_расчета_формула_описание], [источник_значения] from ' + @table_Запрос_77774613_sql)
    exec ('drop table ' + @table_Запрос_77774613_sql)
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_77774613 + ''') IS NULL DROP TABLE ' + @table_Запрос_77774613
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[xp_prggp_2] (Запрос_3312): ' + ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3313
DECLARE @table_Запрос_32749635 VARCHAR(max)
SELECT @table_Запрос_32749635 = '[ks_ddlcontrol].[Запрос_32749635_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3313] @nmode = @nmode, @table_name_result = @table_Запрос_32749635, @Госпрограмма = @Госпрограмма, @Версия = @Версия
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_77774613 + ''') IS NULL DROP TABLE ' + @table_Запрос_77774613
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_32749635 + ''') IS NULL DROP TABLE ' + @table_Запрос_32749635
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3313] (Запрос_3313): ' + ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3314
DECLARE @table_Запрос_38410441 VARCHAR(max)
SELECT @table_Запрос_38410441 = '[ks_ddlcontrol].[Запрос_38410441_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3314] @nmode = @nmode, @table_name_result = @table_Запрос_38410441, @Госпрограмма = @Госпрограмма, @Версия = @Версия
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_77774613 + ''') IS NULL DROP TABLE ' + @table_Запрос_77774613
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_32749635 + ''') IS NULL DROP TABLE ' + @table_Запрос_32749635
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_38410441 + ''') IS NULL DROP TABLE ' + @table_Запрос_38410441
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3314] (Запрос_3314): ' + ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3311
DECLARE @table_Запрос_72779952 VARCHAR(max)
SELECT @table_Запрос_72779952 = '[ks_ddlcontrol].[Запрос_72779952_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3311] @nmode = @nmode, @table_name_result = @table_Запрос_72779952, @Версия = @Версия, @Госпрограмма = @Госпрограмма
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_77774613 + ''') IS NULL DROP TABLE ' + @table_Запрос_77774613
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_32749635 + ''') IS NULL DROP TABLE ' + @table_Запрос_32749635
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_38410441 + ''') IS NULL DROP TABLE ' + @table_Запрос_38410441
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_72779952 + ''') IS NULL DROP TABLE ' + @table_Запрос_72779952
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3311] (Запрос_3311): ' + ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Соединение результатов
SELECT @sel = 'SELECT *, 1 as [query_order] INTO ' + @temp_result + ' FROM ' + @table_Запрос_77774613 + '  UNION SELECT *, 2 as [query_order] FROM ' + @table_Запрос_32749635 + '  UNION SELECT *, 3 as [query_order] FROM ' + @table_Запрос_38410441 + '  UNION SELECT *, 4 as [query_order] FROM ' + @table_Запрос_72779952 + ' '
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_77774613 + ''') IS NULL DROP TABLE ' + @table_Запрос_77774613
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_32749635 + ''') IS NULL DROP TABLE ' + @table_Запрос_32749635
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_38410441 + ''') IS NULL DROP TABLE ' + @table_Запрос_38410441
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_72779952 + ''') IS NULL DROP TABLE ' + @table_Запрос_72779952
    EXEC (@sel)




-- Финальный процесс
    if @table_name_result <> ''
        begin
            select @sel = 'select   [idgp], [гп], [ordergp], [action], [idppgp], [ппгп], [ппгп_код], [gpdbegin], [gpdend], [paramdate], [source], [код_источника_финансирования], [очг13], [очг14], [очг15], [очг16], [очг17], [очг18], [очг19], [очг20], [очг21], [очг22], [очг23], [очг24], [очг25], [documentid], [post], [id], [едизмерения], [порядковыйномерпзппгп], [наименование_показателя], [методика_расчета], [методика_расчета_формула_описание], [источник_значения] into ' + @table_name_result + ' from ' + @temp_result + '   '
            exec (@sel)
        end
    else
        begin
            select @sel = 'select   [idgp], [гп], [ordergp], [action], [idppgp], [ппгп], [ппгп_код], [gpdbegin], [gpdend], [paramdate], [source], [код_источника_финансирования], [очг13], [очг14], [очг15], [очг16], [очг17], [очг18], [очг19], [очг20], [очг21], [очг22], [очг23], [очг24], [очг25], [documentid], [post], [id], [едизмерения], [порядковыйномерпзппгп], [наименование_показателя], [методика_расчета], [методика_расчета_формула_описание], [источник_значения] from ' + @temp_result + '   '
            exec (@sel)
        end

select @sel = 'IF NOT object_id(''' + @temp_result + ''') IS NULL DROP TABLE ' + @temp_result
    exec (@sel)

