alter proc [KS_DDLControl].[2017_GMP_PROGNOZ_MS] (@nmode int=0)
as

create table #temp_result 
			( 
			 id int 
			,parentid int
			,naim varchar(max)
--			,type_name varchar(max)
			,date_to varchar(20)
			,prg_id int
			,unit varchar(50)
			,planvalues varchar(max)
			,sequencenumber varchar (10)
			,version varchar (2)
			,direct varchar (1)
			,parentidconstant int
			,compositeid int
			,status int
			)  


if @nmode <> -1 
begin
set nocount on
set transaction isolation level read uncommitted

-- ��� ��������� ������� �������
/*
declare @nmode int, @GP int, @Ver varchar(2), @Ved int, @Filtr varchar(1000) 
select  @nmode =0, @GP =6559, @Ver ='01', @Ved =12631, @Filtr ='___���___' 
*/

declare @GpDocID int
declare @aDateInt int
--select  @aDateInt = convert(varchar,getdate(),112)
select  @aDateInt = convert(varchar,dbo.getWorkDate(),112)
	------------
	--������� --
	------------
	Declare @user_type int
	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()

	Declare @GmpPermission int
	select 
		@GmpPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 956  --����������� ���

	Declare @VedPermission int
	select 
		@VedPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 864  --���������� ��������� 

	---------------------------------------------------------
	--������� ���������� �������� ����������� ������������ --
	---------------------------------------------------------
	/*
	declare @Cl table (id int, [������������] varchar(2000), [DBegin] int, [���] varchar(20))
	insert into @Cl
	select
		  a.id
		, isnull(AT_2641, AT_2526) as [��]
		, b.AT_2642 as [DBegin]
		, a.AT_2524
	from
        KS_DDLControl.CL_956_2470 a
		left join (select * from KS_DDLControl.CL_956_2615 where AT_2642 <= @aDateInt) b on a.id = b.id_up
		


	select tab2.id, tab2.[������������] as [��], tab2.[���] into #sprGp from
	(select id, max(DBegin) as DBegin from @Cl group by id) tab1
	inner join 
	(select id, DBegin, [������������], [���] from @Cl) tab2
	on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)*/
	
	

	--���������� ������ �� ���� ���������
	declare @Cl table (id int, [������������] varchar(2000), [DBegin] int, [���] varchar(50), Post varchar(max))
	insert into @Cl
	select
		a.id
		, isnull(AT_2526,'') as [��]
		, isnull(b.at_3398, 20160101) as [DBegin]
		, a.AT_2524
		, a.AT_2924
	from	    
		KS_DDLControl.CL_956_2470 a
		left join  KS_DDLControl.DS_957_2475 e on a.id=e.cl_2640
		left join (select * from ks_ddlcontrol.cl_956_3452 where isnull(at_3398,20160101) <= @aDateInt) b on a.id=b.id_up
	
	select tab2.id, tab2.[������������] as [��], tab2.[���], tab2.Post into #sprGp from
	(select id, max(DBegin) as DBegin from @Cl group by id) tab1
	inner join 
	(select id, DBegin, [������������], [���], Post from @Cl) tab2
	on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)


	--�������� ��������� �� ������� ����������� ������
	if isnull(@GmpPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprGp where id not in (select CLID from KS_DDLControl.CLA_956 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

			-------------------------------------------
		--������� �������� ����������� ��������� --
		-------------------------------------------
/*	
	select a.*, a1.AT_1797, a1.AT_1796, a1.AT_1930
	into #sprVed 
	from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
*/	
	select a.id, a.AT_1795, a1.AT_1797, a1.AT_1796, a1.AT_1930
	into #sprVed 
	from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
	UNION
	select b.id, b.AT_3199, b1.AT_3201, b1.AT_3200, b1.AT_3202
	from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_1280_3226 b on b.CL_3232 = a.id
	left join KS_DDLControl.CL_1280_3231 b1 on b.id = b1.id_up
	
		--�������� ��������� �� ������� ����������� ������
		if isnull(@VedPermission,0) > 0 and @user_type != 4
		begin
			delete from #sprVed where id not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
		end 
        
		-------------------------------------------------
		-- ������� ���������� ���������� ��������� ��� --2646 - �������������
		-------------------------------------------------

select 	a.*
		,ved.AT_1795 IspCode
		,ved.AT_1797 IspNaim
		,gp.[��]	 prgNaim
		,gp.[���]    prgCode
	
into #gmp

--from	v_gmp_doc a
--		inner join #sprVed ved on a.Isp_id=ved.id
--		left join #sprGp gp on a.prg_id=gp.id
		
from	
		v_gmp_doc_ms v 
		
		inner join v_gmp_doc a on a.prg_id=v.id and v.status =1
				
		inner join #sprVed ved on a.Isp_id=ved.id  
		left join #sprGp gp on a.prg_id=gp.id		
		
		
	
--select * from 	#gmp

insert 	#temp_result
select 	prg.id 					docid
		,prg.docUp_id			docUpid
--      ,ks.id 					ksid
--		,prg.doc_type_name   type_name
		,sprks.AT_2565 ksName     ---isnull(sprks.AT_2565,sprmgr.AT_3219)	ksName
		,dbo.int_to_date(ks1.dateks)				ksDateto
		,prg_id as idconstant -- id �� ����������� ����������� �������
		,prg.AT_2544 as unit
		,prg.result as planvalues
		,prg.AT_2837 as sequencenumber
		,prg.version as version
		,ks1.direct 
	    ,prg.parentidconstant
		,LTRIM(STR(prg_id,10)) + LTRIM(STR(parentidconstant,10))
		,prg.status
		
		from #GMP prg
--  left join #gmp ks on prg.id=ks.docUp_id and ks.doc_type_name = '����������� �������'
	left join v_gmp_ks ks1 on prg.id=ks1.docid and prg.prg_id=ks1.ksid and prg.doc_type_name='����������� �������'
	left join KS_DDLControl.CL_962_2518 sprks on ks1.ksid=sprks.id
--	left join v_gmp_mgr ks2 on prg.id=ks2.docid and prg.doc_type_name='������������'
--	left join KS_DDLControl.CL_1283_3248 sprmgr on ks2.ksid=sprmgr.id		
--  where prg.doc_type_name ='�����������'

where prg.doc_type_name = '����������� �������'---, '������������')
end

--select * from #GMP

select * from #temp_result where isnull( naim,'')<>''