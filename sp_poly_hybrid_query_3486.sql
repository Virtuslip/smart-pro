CREATE proc [ks_ddlcontrol].[sp_poly_hybrid_query_3486] @nmode int=0,
                                                        @table_name_result varchar(max)='',
                                                        @cProject int = 0,
                                                        @Программа int = '6574',
                                                        @Версия varchar(2) = '22',
                                                        @Подпрограмма varchar(max) = '7242@#$7355@#$7375',
                                                        @Собрать_ожидаемые_результаты varchar(max) = 'Да',
                                                        @Дата_версионность varchar(max) = ''
as
    set nocount on
    set transaction isolation level read uncommitted
declare @ErrorMessage nvarchar(4000)
declare @ErrorSeverity int
declare @ErrorState int
declare @sel varchar(max)
declare @temp_result VARCHAR(max)
select @temp_result = '[ks_ddlcontrol].[temp_result_' + convert(varchar(36), newid()) + ']'

    -- Выполнение подзапросов

-- Подзапрос Запрос_3473
DECLARE @table_Запрос_60152496 VARCHAR(max)
SELECT @table_Запрос_60152496 = '[ks_ddlcontrol].[Запрос_60152496_' + convert(varchar(36), newid()) + ']'
begin try
    declare
        @table_Запрос_60152496_sql varchar(max)
    select @table_Запрос_60152496_sql = '[ks_ddlcontrol].[Запрос_60152496_' + convert(varchar(36), newid()) + ']'

    EXEC [ks_ddlcontrol].[xp_prgppgp_4] @nmode = @nmode, @table_name_result = @table_Запрос_60152496_sql,
         @GP = @Программа, @Ver = @Версия, @PPGP = @Подпрограмма, @Result = @Собрать_ожидаемые_результаты

    -- Дополнительная обработка для пользовательских запросов к БД (нестандартные имена полей)
    exec ('create table ' + @table_Запрос_60152496 + ' (
    [idppgp] int null,
    [ид_ппгп] int null,
    [ппгп] varchar(max) null,
    [orderppgp] varchar(max) null,
    [action] varchar(max) null,
    [idcontent] int null,
    [content] varchar(max) null,
    [код_content] varchar(max) null,
    [gpdbegin] int null,
    [gpdend] int null,
    [paramdate] int null,
    [s_gp] varchar(max) null,
    [source] varchar(max) null,
    [код_источника_финансирования] varchar(max) null,
    [очг13] numeric(20,4) null,
    [очг14] numeric(20,4) null,
    [очг15] numeric(20,4) null,
    [очг16] numeric(20,4) null,
    [очг17] numeric(20,4) null,
    [очг18] numeric(20,4) null,
    [очг19] numeric(20,4) null,
    [очг20] numeric(20,4) null,
    [очг21] numeric(20,4) null,
    [очг22] numeric(20,4) null,
    [очг23] numeric(20,4) null,
    [очг24] numeric(20,4) null,
    [очг25] numeric(20,4) null,
    [кодппгп] varchar(max) null,
    [documentid] int null,
    [post] varchar(max) null,
    [id] int null,
    [порядковыйномерпзппгп] varchar(max) null,
    [единицаизмерения] varchar(max) null,
    [показательзадачи] varchar(max) null,
    [методика_расчета_номер_строки] varchar(max) null,
    [методика_расчета_метод_расчета] varchar(max) null,
    [методика_расчета_формула_описание] varchar(max) null,
    [источник_значения] varchar(max) null)')
    exec ('insert ' + @table_Запрос_60152496 + ' select [idppgp], [ид_ппгп], [ппгп], [orderppgp], [action], [idcontent], [content], [код_content], [gpdbegin], [gpdend], [paramdate], [s_gp], [source], [код_источника_финансирования], [очг13], [очг14], [очг15], [очг16], [очг17], [очг18], [очг19], [очг20], [очг21], [очг22], [очг23], [очг24], [очг25], [кодппгп], [documentid], [post], [id], [порядковыйномерпзппгп], [единицаизмерения], [показательзадачи], [методика_расчета_номер_строки], [методика_расчета_метод_расчета], [методика_расчета_формула_описание], [источник_значения] from ' + @table_Запрос_60152496_sql)
    exec ('drop table ' + @table_Запрос_60152496_sql)
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60152496 + ''') IS NULL DROP TABLE ' + @table_Запрос_60152496
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[xp_prgppgp_4] (Запрос_3473): ' + ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3484
DECLARE @table_Запрос_59007941 VARCHAR(max)
SELECT @table_Запрос_59007941 = '[ks_ddlcontrol].[Запрос_59007941_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3484] @nmode = @nmode, @table_name_result = @table_Запрос_59007941,
         @Дата_версионность = @Дата_версионность, @Версия = @Версия, @Программа = @Программа,
         @Подпрограмма = @Подпрограмма
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60152496 + ''') IS NULL DROP TABLE ' + @table_Запрос_60152496
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_59007941 + ''') IS NULL DROP TABLE ' + @table_Запрос_59007941
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3484] (Запрос_3484): ' + ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3476
DECLARE @table_Запрос_60348482 VARCHAR(max)
SELECT @table_Запрос_60348482 = '[ks_ddlcontrol].[Запрос_60348482_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3476] @nmode = @nmode, @table_name_result = @table_Запрос_60348482,
         @Дата_версионность = @Дата_версионность, @Версия = @Версия, @Программа = @Программа,
         @Подпрограмма = @Подпрограмма
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60152496 + ''') IS NULL DROP TABLE ' + @table_Запрос_60152496
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_59007941 + ''') IS NULL DROP TABLE ' + @table_Запрос_59007941
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60348482 + ''') IS NULL DROP TABLE ' + @table_Запрос_60348482
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3476] (Запрос_3476): ' + ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3485
DECLARE @table_Запрос_58739027 VARCHAR(max)
SELECT @table_Запрос_58739027 = '[ks_ddlcontrol].[Запрос_58739027_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3485] @nmode = @nmode, @table_name_result = @table_Запрос_58739027,
         @Дата_версионность = @Дата_версионность, @Версия = @Версия, @Программа = @Программа,
         @Подпрограмма = @Подпрограмма
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60152496 + ''') IS NULL DROP TABLE ' + @table_Запрос_60152496
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_59007941 + ''') IS NULL DROP TABLE ' + @table_Запрос_59007941
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60348482 + ''') IS NULL DROP TABLE ' + @table_Запрос_60348482
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_58739027 + ''') IS NULL DROP TABLE ' + @table_Запрос_58739027
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3485] (Запрос_3485): ' + ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Подзапрос Запрос_3480
DECLARE @table_Запрос_1285778 VARCHAR(max)
SELECT @table_Запрос_1285778 = '[ks_ddlcontrol].[Запрос_1285778_' + convert(varchar(36), newid()) + ']'
begin try
    EXEC [ks_ddlcontrol].[sp_poly_hybrid_query_3480] @nmode = @nmode, @table_name_result = @table_Запрос_1285778,
         @Дата_версионность = @Дата_версионность, @Версия = @Версия, @Программа = @Программа,
         @Подпрограмма = @Подпрограмма
end try
begin catch
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60152496 + ''') IS NULL DROP TABLE ' + @table_Запрос_60152496
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_59007941 + ''') IS NULL DROP TABLE ' + @table_Запрос_59007941
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60348482 + ''') IS NULL DROP TABLE ' + @table_Запрос_60348482
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_58739027 + ''') IS NULL DROP TABLE ' + @table_Запрос_58739027
    EXEC (@sel)
    SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_1285778 + ''') IS NULL DROP TABLE ' + @table_Запрос_1285778
    EXEC (@sel)
    SELECT @ErrorMessage = '[ks_ddlcontrol].[sp_poly_hybrid_query_3480] (Запрос_3480): ' + ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE()
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
end catch

-- Соединение результатов
SELECT @sel = 'SELECT *, 1 as [query_order] INTO ' + @temp_result + ' FROM ' + @table_Запрос_60152496 +
              '  UNION SELECT *, 2 as [query_order] FROM ' + @table_Запрос_59007941 +
              '  UNION SELECT *, 3 as [query_order] FROM ' + @table_Запрос_60348482 +
              '  UNION SELECT *, 4 as [query_order] FROM ' + @table_Запрос_58739027 +
              '  UNION SELECT *, 5 as [query_order] FROM ' + @table_Запрос_1285778 + ' '
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60152496 + ''') IS NULL DROP TABLE ' + @table_Запрос_60152496
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_59007941 + ''') IS NULL DROP TABLE ' + @table_Запрос_59007941
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_60348482 + ''') IS NULL DROP TABLE ' + @table_Запрос_60348482
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_58739027 + ''') IS NULL DROP TABLE ' + @table_Запрос_58739027
    EXEC (@sel)
SELECT @sel = 'IF NOT object_id(''' + @table_Запрос_1285778 + ''') IS NULL DROP TABLE ' + @table_Запрос_1285778
    EXEC (@sel)

-- Финальный процесс
    if @table_name_result <> ''
        begin
            select @sel =
                   'select DISTINCT  [idppgp], [ид_ппгп], [ппгп], [orderppgp], [action], [idcontent], [content], [код_content], [gpdbegin], [gpdend], [paramdate], [s_gp], [source], [код_источника_финансирования], [очг13], [очг14], [очг15], [очг16], [очг17], [очг18], [очг19], [очг20], [очг21], [очг22], [очг23], [очг24], [очг25], [кодппгп], [documentid], [post], [id], [порядковыйномерпзппгп], [единицаизмерения], [показательзадачи], [методика_расчета_номер_строки], [методика_расчета_метод_расчета], [методика_расчета_формула_описание], [источник_значения] into ' +
                   @table_name_result + ' from ' + @temp_result + '   ORDER BY [idppgp] ASC, [orderppgp] ASC'
            exec (@sel)
        end
    else
        begin
            select @sel =
                   'select DISTINCT  [idppgp], [ид_ппгп], [ппгп], [orderppgp], [action], [idcontent], [content], [код_content], [gpdbegin], [gpdend], [paramdate], [s_gp], [source], [код_источника_финансирования], [очг13], [очг14], [очг15], [очг16], [очг17], [очг18], [очг19], [очг20], [очг21], [очг22], [очг23], [очг24], [очг25], [кодппгп], [documentid], [post], [id], [порядковыйномерпзппгп], [единицаизмерения], [показательзадачи], [методика_расчета_номер_строки], [методика_расчета_метод_расчета], [методика_расчета_формула_описание], [источник_значения] from ' +
                   @temp_result + '   ORDER BY [idppgp] ASC, [orderppgp] ASC'
            exec (@sel)
        end

select @sel = 'IF NOT object_id(''' + @temp_result + ''') IS NULL DROP TABLE ' + @temp_result
    exec (@sel)