alter proc [KS_DDLControl].[2019_Prg_Texting] (@nmode int=0, @GP int=109368)
as
begin
    /*declare @gp int
    select @gp=109726
    */
    declare @date int
    select @date=cast(convert(varchar,value,112) as int) from dbo.WorkDate()

    ;WITH
         gp AS(
             SELECT cast(null as int) parent_doc, a.id doc, 0 as lvl,cast('' as varchar(max)) pathstr
             from ks_ddlcontrol.ds_957_2475 a
                  --ks_ddlcontrol.ds_957_2625 b --on a.id=b.id_up
             where a.id=@gp
             UNION ALL
             SELECT  b1.ds_2654, b1.id_up,lvl+1 ,pathstr+'@#$'+(cast(gp.doc as varchar(max)))
             FROM gp gp
                      inner join ks_ddlcontrol.ds_957_2625 b1  on gp.doc=b1.ds_2654
             --left join ks_ddlcontrol.ds_957_2475 a1 on b1.id_up=a1.id
         )
         --insert #gp
         --SELECT  parent_doc,doc,documentid,lvl, pathstr,@bp pb  FROM gp  --order by lvl desc --where parent_doc is null
     SELECT  gp.parent_doc
          ,gp.doc
          ,gp.pathstr
          ,case a.documentid
               when 220 then 'Госпрограмма'
               when 221 then 'Подпрограмма'
               when 222 then 'Цель'
               when 226 then 'Мероприятие'
               when 229 then 'Задача'
               when 252 then 'Индикатор'
         end 				prg_type
          ,a.at_2537			prg_name
          ,isp.at_3199 		resp_kod
          ,isp_n.at_3201 		resp_name
     into #gp
     FROM gp gp
              inner join ks_ddlcontrol.ds_957_2475 a on gp.doc=a.id
              left join ks_ddlcontrol.cl_1280_3226 isp on a.cl_3252=isp.id
              left join ks_ddlcontrol.cl_1280_3231 isp_n on isp.id=isp_n.id_up
         and isnull(isp_n.at_3202,0)=(select max(isnull(isp_n1.at_3202,0)) from ks_ddlcontrol.cl_1280_3231 isp_n1 where isp_n1.id_up=isp_n.id_up and isnull(isp_n1.at_3202,0)<=@date)
    ---select * from #gp

/*
	create table #prg (str_gp varchar(30), str_name varchar(500), str_id int )
	insert into  #prg select 'Госпрограммы', 'Ответственный исполнитель государственной программы',1
	insert into  #prg select 'Госпрограммы', 'Соисполнители государственной программы',2
	insert into  #prg select 'Госпрограммы', 'Сроки и этапы реализации государственной программы',3
	insert into  #prg select 'Госпрограммы', 'Подпрограммы',4
	insert into  #prg select 'Госпрограммы', 'Цель государственной программы',5
	insert into  #prg select 'Госпрограммы', 'Индикаторы цели',6
	insert into  #prg select 'Госпрограммы', 'Задачи государственной программы',7
	insert into  #prg select 'Госпрограммы', 'Показатели задач',8
	insert into  #prg select 'Госпрограммы', 'Объемы финансирования за счет средств областного бюджета всего, в том числе по годам реализации государственной программы',9
	insert into  #prg select 'Госпрограммы', 'Ожидаемые результаты реализации государственной программы',10

		--order by lvl desc --where parent_doc is null
	select distinct resp_name content,1 str_id into #prg_resp from #gp where prg_type='Госпрограмма'

	select distinct resp_name content,2 str_id into #prg_soresp from #gp where prg_type!='Госпрограмма' and resp_name is not null

	select distinct (isnull(cast(at_2538/10000 as varchar(10)),'') + isnull('-'+cast(at_2539/10000 as varchar(10)),'')+'г.') as content,3 str_id
	into #prg_period
	from #gp  gp
		left join ks_ddlcontrol.ds_957_2475 a on gp.doc=a.id
	where gp.prg_type='Госпрограмма'
	--select * from #prg_period
	select distinct prg_name content,4 str_id into #ppgp from #gp where prg_type='Подпрограмма' and parent_doc=@gp

	select distinct prg_name content,5 str_id into #tgt from #gp where prg_type='Цель' and parent_doc=@gp

	select distinct gp1.prg_name content,6 str_id
	into #ind_tgt
	from #gp gp
		inner join #gp gp1 on gp.doc=gp1.parent_doc and gp1.prg_type='Индикатор'
	where gp.prg_type='Цель' and gp.parent_doc=@gp

	select distinct prg_name content,7 str_id
	into #task
	from #gp
	where prg_type='Задача' and parent_doc=@gp



	select distinct gp1.prg_name content,8 str_id
	into #ind_task
	from #gp gp
		inner join #gp gp1 on gp.doc=gp1.parent_doc and gp1.prg_type='Индикатор'
	where gp.prg_type='Задача' and gp.parent_doc=@gp
	----------------------------------------------ресурсное обеспечение

select pm.cl_2572	finid
		,spr_fin.at_1999 fin
		, sum(isnull(pm.M_2928,0.00)/1000) as [ОЧГ13]
		, sum(isnull(pm.M_2929,0.00)/1000) as [ОЧГ14]
		, sum(isnull(pm.M_2930,0.00)/1000) as [ОЧГ15]
		, sum(isnull(pm.M_2931,0.00)/1000) as [ОЧГ16]
		, sum(isnull(pm.M_2932,0.00)/1000) as [ОЧГ17]
		, sum(isnull(pm.M_2937,0.00)/1000) as [ОЧГ18]
		, sum(isnull(pm.M_2938,0.00)/1000) as [ОЧГ19]
		, sum(isnull(pm.M_2939,0.00)/1000) as [ОЧГ20]
		, sum(isnull(pm.M_3073,0.00)/1000) as [ОЧГ21]
		, sum(isnull(pm.M_3344,0.00)/1000) as [ОЧГ22]
		, sum(isnull(pm.M_3345,0.00)/1000) as [ОЧГ23]
		, sum(isnull(pm.M_3346,0.00)/1000) as [ОЧГ24]
		, sum(isnull(pm.M_3347,0.00)/1000) as [ОЧГ25]
		,sum(isnull(pm.M_2928,0.00)+isnull(pm.M_2929,0.00)+isnull(pm.M_2930,0.00)+isnull(pm.M_2931,0.00)
			+isnull(pm.M_2932,0.00)+isnull(pm.M_2937,0.00)+isnull(pm.M_2938,0.00)+isnull(pm.M_2939,0.00)
			+isnull(pm.M_3073,0.00)+isnull(pm.M_3344,0.00)+isnull(pm.M_3346,0.00)+isnull(pm.M_3347,0.00)) as summall
into #t
from #gp gp
	inner join ks_ddlcontrol.ds_957_2506 pm on gp.doc=pm.id_up
	inner join ks_ddlcontrol.cl_894_1936 spr_fin on pm.cl_2572=spr_fin.id
where gp.prg_type='мероприятие'

group by pm.cl_2572
		,spr_fin.at_1999

select

		    replace('Общий объем финансового обеспечения: '--+char(13)+char(10)
				+ char(13)+char(10)+ '2013 год - ' + cast(sum(a.[ОЧГ13]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2014 год - ' + cast(sum(a.[ОЧГ14]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2015 год - ' + cast(sum(a.[ОЧГ15]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2016 год - ' + cast(sum(a.[ОЧГ16]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2017 год - ' + cast(sum(a.[ОЧГ17]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2018 год - ' + cast(sum(a.[ОЧГ18]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2019 год - ' + cast(sum(a.[ОЧГ19]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2020 год - ' + cast(sum(a.[ОЧГ20]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2021 год - ' + cast(sum(a.[ОЧГ21]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2022 год - ' + cast(sum(a.[ОЧГ22]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2023 год - ' + cast(sum(a.[ОЧГ23]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2024 год - ' + cast(sum(a.[ОЧГ24]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2025 год - ' + cast(sum(a.[ОЧГ25]) as varchar)+ ' тыс. руб.;'

				--from #t  a  for xml path(''))--+char(13)+char(10)+
				+(select char(13)+char(10)+fin + ' - ' +
					char(13)+char(10)+ '2013 год - ' + cast(sum(c.[ОЧГ13]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2014 год - ' + cast(sum(c.[ОЧГ14]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2015 год - ' + cast(sum(c.[ОЧГ15]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2016 год - ' + cast(sum(c.[ОЧГ16]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2017 год - ' + cast(sum(c.[ОЧГ17]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2018 год - ' + cast(sum(c.[ОЧГ18]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2019 год - ' + cast(sum(c.[ОЧГ19]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2020 год - ' + cast(sum(c.[ОЧГ20]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2021 год - ' + cast(sum(c.[ОЧГ21]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2022 год - ' + cast(sum(c.[ОЧГ22]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2023 год - ' + cast(sum(c.[ОЧГ23]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2024 год - ' + cast(sum(c.[ОЧГ24]) as varchar)+ ' тыс. руб.;'
					+char(13)+char(10)+ '2025 год - ' + cast(sum(c.[ОЧГ25]) as varchar)+ ' тыс. руб.;'
					from #t c
				--	where 	c.[Источник финансирования]=b.[Источник финансирования]
					group by c.fin for xml path('')),'&#x0D;','') content
					,9 str_id
into #volume
from #t a

	----------------------------------------------------------------------

	select distinct at_2532 content,10 str_id

	into #rests
	from #gp gp
		inner join ks_ddlcontrol.ds_957_2478 a on gp.doc=a.id_up
	where gp.doc=@gp

	--select * from ks_ddlcontrol.ds_957_2478 where id_up=109726
	---------------------паспорт гп
	select a.*
			,case a.str_id
				when 1 then resp.content
				when 2 then soresp.content
				when 3 then period.content
				when 4 then ppgp.content
				when 5 then tgt.content
				when 6 then ind_tgt.content
				when 7 then task.content
				when 8 then ind_task.content
				when 9 then volume.content
				when 10 then rests.content
			end content
	into #pasport
	from #prg a
		left join #prg_resp resp on a.str_id=resp.str_id
		left join #prg_soresp soresp on a.str_id=soresp.str_id
		left join #prg_period period on a.str_id=period.str_id
		left join #ppgp ppgp on a.str_id=ppgp.str_id
		left join #tgt tgt on a.str_id=tgt.str_id
		left join #ind_tgt ind_tgt on a.str_id=ind_tgt.str_id
		left join #task task on a.str_id=task.str_id
		left join #ind_task ind_task on a.str_id=ind_task.str_id
		left join #volume volume on a.str_id=volume.str_id
		left join #rests rests on a.str_id=rests.str_id
		*/
-----------------------------------------------------------------
---------------Текстовая часть------------
    create table #chapter2([order] varchar(10),npa varchar(2000),part int)
    insert #chapter2
    select npa.at_3579 [order], spr_npa.at_3537 npa,1 part--+' от '+dbo.int_to_date(spr_npa.at_2630) npa
    from ks_ddlcontrol.ds_957_3626 npa
             inner join ks_ddlcontrol.cl_968_2614 spr_npa on npa.cl_3538=spr_npa.id
    where npa.id_up=@gp

    insert #chapter2
    select pr.at_3580 [order], spr_pr.at_3526 npa,2 part--+' от '+dbo.int_to_date(spr_npa.at_2630) npa
    from ks_ddlcontrol.ds_957_3629 pr
             inner join ks_ddlcontrol.cl_1394_3607 spr_pr on pr.cl_3540=spr_pr.id and spr_pr.cl_3530=2
    where pr.id_up=@gp

    --select * from #chapter2
-------------
-----Перечень индикаторов
-------------------------------------------
/*
select distinct doc.at_3338	[order]
				,gp.prg_name indikator
				,ei.at_2544 ei
				,stuff((select distinct char(13)+char(10)+ det.at_3541 from ks_ddlcontrol.ds_957_3631 det where det.id_up=gp.doc for xml path('')),1,7,'') [metod]
				--,det.at_3541 metod,
				,stuff((select distinct char(13)+char(10)+ spr.at_3526 from ks_ddlcontrol.ds_957_3629 det
								inner join ks_ddlcontrol.cl_1394_3607 spr on det.cl_3540=spr.id and spr.cl_3530=1
					where det.id_up=gp.doc for xml path('')),1,7,'') [source]
into #indikators
from #gp gp
	inner join ks_ddlcontrol.ds_957_2475 doc on gp.doc=doc.id
	left join ks_ddlcontrol.cl_960_2491 ei on doc.cl_2633=ei.id
where gp.prg_type='Индикатор' --and gp.parent_doc=@gp
*/
-------------------------------------------



/*


drop table #prg
drop table #prg_resp
drop table #prg_soresp
drop table #prg_period
drop table #ppgp
drop table #ind_tgt
drop table #task
drop table #ind_task
drop table #volume
drop table #rests
drop table #t


drop table #pasport
*/
    select * from #chapter2
    drop table #chapter2
    --drop table #indikators
/*
select
from
*/
end