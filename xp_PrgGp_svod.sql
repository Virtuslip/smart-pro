--exec [KS_DDLControl].[xp_PrgGp_svod] @nmode=0, @GP=6575, @Ver00 = '01', @Ver01 = '02', @OnlyChanges ='Да'


create proc [KS_DDLControl].[xp_PrgGp_svod](@nmode int=0, @GP int=72, @Ver00 varchar(2) = '01',
                                            @Ver01 varchar(2) = '02', @OnlyChanges varchar(3) = 'Да')
as
    create table #temp_result
    (

        orderGP                   varchar(2),
        action                    varchar(500),
        idgp                      int,
        [ГП]                      varchar(2000),
        idppgp                    int,
        [ППГП]                    varchar(max),
        [ППГП_Код]                varchar(20),
        GpDBegin                  int,
        GpDEnd                    int,
        ParamDate                 int
        --, [S_gp] varchar(500)
        ,
        [Source]                  varchar(100),
        [ОЧГ13]                   numeric(20, 2),
        [ОЧГ14]                   numeric(20, 2),
        [ОЧГ15]                   numeric(20, 2),
        [ОЧГ16]                   numeric(20, 2),
        [ОЧГ17]                   numeric(20, 2),
        [ОЧГ18]                   numeric(20, 2),
        [ОЧГ19]                   numeric(20, 2),
        [ОЧГ20]                   numeric(20, 2),
        [ОЧГ21]                   numeric(20, 2),
        [ОЧГ22]                   numeric(20, 2),
        [ОЧГ23]                   numeric(20, 2),
        [ОЧГ24]                   numeric(20, 2),
        [ОЧГ25]                   numeric(20, 2)
        --, documentid int
        ,
        [ЕдИзмерения]             varchar(500)
--			,status varchar(2000)
--			,AT_3255 varchar(2000)
--			,AT_3254 datetime
--			,AT_3272 datetime
--			,AT_3273 varchar(2000)
        ,
        [ПорядковыйНомерПЗППГП]   varchar(10),
        idgp_1                    int,
        [ГП_1]                    varchar(2000),
        idppgp_1                  int,
        [ППГП_1]                  varchar(max),
        [ППГП_Код_1]              varchar(20),
        GpDBegin_1                int,
        GpDEnd_1                  int,
        ParamDate_1               int
        --, [S_gp_1] varchar(500)
        ,
        [Source_1]                varchar(100),
        [ОЧГ13_1]                 numeric(20, 2),
        [ОЧГ14_1]                 numeric(20, 2),
        [ОЧГ15_1]                 numeric(20, 2),
        [ОЧГ16_1]                 numeric(20, 2),
        [ОЧГ17_1]                 numeric(20, 2),
        [ОЧГ18_1]                 numeric(20, 2),
        [ОЧГ19_1]                 numeric(20, 2),
        [ОЧГ20_1]                 numeric(20, 2),
        [ОЧГ21_1]                 numeric(20, 2),
        [ОЧГ22_1]                 numeric(20, 2),
        [ОЧГ23_1]                 numeric(20, 2),
        [ОЧГ24_1]                 numeric(20, 2),
        [ОЧГ25_1]                 numeric(20, 2)
        --, documentid_1 int
        ,
        [ЕдИзмерения_1]           varchar(500),
        [ПорядковыйНомерПЗППГП_1] varchar(10)

--			,status_1 varchar(2000)
--			,AT_3255_1 varchar(2000)
--			,AT_3254_1 datetime
--			,AT_3272_1 datetime
--			,AT_3273_1 varchar(2000)

        ,
        Post                      varchar(max),
        documentid                int,
        id                        int identity (1,1)


    )
    if @nmode <> -1
        begin
            set nocount on
            set transaction isolation level read uncommitted
-- Пример запуска:  exec [KS_DDLControl].[xp_PrgGp_svod] @nmode=0, @GP=56, @Ver00='01', @Ver01='02', @OnlyChanges = 'Да'

            create table #t_PrgGP
            (
                idgp                    int,
                [ГП]                    varchar(2000),
                orderGP                 varchar(2),
                action                  varchar(500),
                idppgp                  int,
                [ППГП]                  varchar(max),
                [ППГП_Код]              varchar(20),
                GpDBegin                int,
                GpDEnd                  int,
                ParamDate               int
                --, [S_gp] varchar(500)
                ,
                [Source]                varchar(100),
                [ОЧГ13]                 numeric(20, 2),
                [ОЧГ14]                 numeric(20, 2),
                [ОЧГ15]                 numeric(20, 2),
                [ОЧГ16]                 numeric(20, 2),
                [ОЧГ17]                 numeric(20, 2),
                [ОЧГ18]                 numeric(20, 2),
                [ОЧГ19]                 numeric(20, 2),
                [ОЧГ20]                 numeric(20, 2),
                [ОЧГ21]                 numeric(20, 2),
                [ОЧГ22]                 numeric(20, 2),
                [ОЧГ23]                 numeric(20, 2),
                [ОЧГ24]                 numeric(20, 2),
                [ОЧГ25]                 numeric(20, 2),
                documentid              int,
                Post                    varchar(max),
                id                      int,
                [ЕдИзмерения]           varchar(500),
                [ПорядковыйНомерПЗППГП] varchar(10)
--			, id int identity (1,1)
--			,status varchar(2000)
--			,AT_3255 varchar(2000)
--			,AT_3254 datetime
--			,AT_3272 datetime
--			,AT_3273 varchar(2000)

            )

            create table #Ver00
            (
                idgp                    int,
                [ГП]                    varchar(2000),
                orderGP                 varchar(2),
                action                  varchar(500),
                idppgp                  int,
                [ППГП]                  varchar(max),
                [ППГП_Код]              varchar(20),
                GpDBegin                int,
                GpDEnd                  int,
                ParamDate               int
                --, [S_gp] varchar(500)
                ,
                [Source]                varchar(100),
                [ОЧГ13]                 numeric(20, 2),
                [ОЧГ14]                 numeric(20, 2),
                [ОЧГ15]                 numeric(20, 2),
                [ОЧГ16]                 numeric(20, 2),
                [ОЧГ17]                 numeric(20, 2),
                [ОЧГ18]                 numeric(20, 2),
                [ОЧГ19]                 numeric(20, 2),
                [ОЧГ20]                 numeric(20, 2),
                [ОЧГ21]                 numeric(20, 2),
                [ОЧГ22]                 numeric(20, 2),
                [ОЧГ23]                 numeric(20, 2),
                [ОЧГ24]                 numeric(20, 2),
                [ОЧГ25]                 numeric(20, 2),
                documentid              int,
                Post                    varchar(max),
                id                      int,
                [ЕдИзмерения]           varchar(500),
                [ПорядковыйНомерПЗППГП] varchar(10)
--			,status varchar(2000)
--			,AT_3255 varchar(2000)
--			,AT_3254 datetime
--			,AT_3272 datetime
--			,AT_3273 varchar(2000)
            )
            exec [KS_DDLControl].[xp_PrgGp] @nmode=0, @GP=@GP, @Ver=@Ver00, @ToTable=1
            insert into #Ver00 select * from #t_PrgGP
            delete from #Ver00 where idgp is null

            delete from #t_PrgGP

            --	select * from #Ver00
--	select * from #t_PrgGP
            create table #Ver01
            (
                idgp                    int,
                [ГП]                    varchar(2000),
                orderGP                 varchar(2),
                action                  varchar(500),
                idppgp                  int,
                [ППГП]                  varchar(max),
                [ППГП_Код]              varchar(20),
                GpDBegin                int,
                GpDEnd                  int,
                ParamDate               int
                --, [S_gp] varchar(500)
                ,
                [Source]                varchar(100),
                [ОЧГ13]                 numeric(20, 2),
                [ОЧГ14]                 numeric(20, 2),
                [ОЧГ15]                 numeric(20, 2),
                [ОЧГ16]                 numeric(20, 2),
                [ОЧГ17]                 numeric(20, 2),
                [ОЧГ18]                 numeric(20, 2),
                [ОЧГ19]                 numeric(20, 2),
                [ОЧГ20]                 numeric(20, 2),
                [ОЧГ21]                 numeric(20, 2),
                [ОЧГ22]                 numeric(20, 2),
                [ОЧГ23]                 numeric(20, 2),
                [ОЧГ24]                 numeric(20, 2),
                [ОЧГ25]                 numeric(20, 2),
                documentid              int,
                Post                    varchar(max),
                id                      int,
                [ЕдИзмерения]           varchar(500),
                [ПорядковыйНомерПЗППГП] varchar(10)
--			,status varchar(2000)
--			,AT_3255 varchar(2000)
--			,AT_3254 datetime
--			,AT_3272 datetime
--			,AT_3273 varchar(2000)
            )
            exec [KS_DDLControl].[xp_PrgGp] @nmode=0, @GP=@GP, @Ver=@Ver01, @ToTable=1
            insert into #Ver01 select * from #t_PrgGP
            delete from #Ver01 where idgp is null
            --select * from #Ver01
--	select * from #t_PrgGP
            --------------------------------
            --Изменение в наименовании ГП --
            --------------------------------
            declare
                @gp_name varchar(2000), @id int
            declare
                @gp_name_1 varchar(2000), @id_1 int
            select @gp_name = [ГП], @id = idgp from #Ver00
            select @gp_name_1 = [ГП], @id_1 = idgp from #Ver01

--select * from #Ver01

            insert into #temp_result
            select isnull(a.orderGP, b.orderGP)
                 , isnull(a.action, b.action)

                 , @id
                 , @gp_name
                 , a.idppgp
                 , a.[ППГП]
                 , a.[ППГП_Код]
                 , a.GpDBegin
                 , a.GpDEnd
                 , a.ParamDate
                 --, a.[S_gp]
                 , a.[Source]
                 , a.[ОЧГ13]
                 , a.[ОЧГ14]
                 , a.[ОЧГ15]
                 , a.[ОЧГ16]
                 , a.[ОЧГ17]
                 , a.[ОЧГ18]
                 , a.[ОЧГ19]
                 , a.[ОЧГ20]
                 , a.[ОЧГ21]
                 , a.[ОЧГ22]
                 , a.[ОЧГ23]
                 , a.[ОЧГ24]
                 , a.[ОЧГ25]

                 , a.[ЕдИзмерения]
                 , a.[ПорядковыйНомерПЗППГП]

                 , @id_1
                 , @gp_name_1
                 , b.idppgp
                 , b.[ППГП]
                 , b.[ППГП_Код]
                 , b.GpDBegin
                 , b.GpDEnd
                 , b.ParamDate
                 --, b.[S_gp]
                 , b.[Source]
                 , b.[ОЧГ13]
                 , b.[ОЧГ14]
                 , b.[ОЧГ15]
                 , b.[ОЧГ16]
                 , b.[ОЧГ17]
                 , b.[ОЧГ18]
                 , b.[ОЧГ19]
                 , b.[ОЧГ20]
                 , b.[ОЧГ21]
                 , b.[ОЧГ22]
                 , b.[ОЧГ23]
                 , b.[ОЧГ24]
                 , b.[ОЧГ25]

                 , b.[ЕдИзмерения]
                 , b.[ПорядковыйНомерПЗППГП]

                 , isnull(a.Post, b.Post)
                 , isnull(a.documentid, b.documentid)


            from #Ver00 a
                     full join #Ver01 b on
                    isnull(a.orderGP, '') = isnull(b.orderGP, '')
                    and isnull(a.action, '') = isnull(b.action, '')
                    and isnull(a.[ППГП], '') = isnull(b.[ППГП], '')
                    and isnull(a.[ППГП_Код], '') = isnull(b.[ППГП_Код], '')
                    and isnull(a.[Source], '') = isnull(b.[Source], '')

            delete
            from #temp_result
            where [ППГП_Код] is null
              and [ППГП_Код_1] is null
              and action in
                  (select distinct action from #temp_result where [ППГП_Код] is not null or [ППГП_Код_1] is not null)
            --delete from #temp_result where ordergp ='11' and [ОЧГ13]=0 and [ОЧГ14]=0 and [ОЧГ15]=0 and [ОЧГ16]=0 and [ОЧГ17]=0 and [ОЧГ18]=0 and [ОЧГ19]=0 and [ОЧГ20]=0 and [ОЧГ16_1] is null

        end
    if @OnlyChanges = 'Да'
        begin
            select *
            from #temp_result
            where [ГП] != [ГП_1]
               or isnull([ППГП_Код], '') != isnull([ППГП_Код_1], '')
               or isnull([ППГП], '') != isnull([ППГП_1], '')
               or isnull([Source], '') != isnull([Source_1], '')
               or isnull([ОЧГ13], 0) != isnull([ОЧГ13_1], 0)
               or isnull([ОЧГ14], 0) != isnull([ОЧГ14_1], 0)
               or isnull([ОЧГ15], 0) != isnull([ОЧГ15_1], 0)
               or isnull([ОЧГ16], 0) != isnull([ОЧГ16_1], 0)
               or isnull([ОЧГ17], 0) != isnull([ОЧГ17_1], 0)
               or isnull([ОЧГ18], 0) != isnull([ОЧГ18_1], 0)
               or isnull([ОЧГ19], 0) != isnull([ОЧГ19_1], 0)
               or isnull([ОЧГ20], 0) != isnull([ОЧГ20_1], 0)
               or isnull([ОЧГ21], 0) != isnull([ОЧГ21_1], 0)
               or isnull([ОЧГ22], 0) != isnull([ОЧГ22_1], 0)
               or isnull([ОЧГ23], 0) != isnull([ОЧГ23_1], 0)
               or isnull([ОЧГ24], 0) != isnull([ОЧГ24_1], 0)
               or isnull([ОЧГ25], 0) != isnull([ОЧГ25_1], 0)


        end
    else
        begin
            select * from #temp_result
        end