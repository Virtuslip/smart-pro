alter proc [KS_DDLControl].[2019_Prg_pasport] (@nmode int=0, @GP int=109368)
    as
    begin
        /*declare @gp int
        select @gp=109726

        [KS_DDLControl].[2019_Prg_pasport] @gp=109726
        */
        declare
            @date int
        select @date = cast(convert(varchar, value, 112) as int) from dbo.WorkDate();
        WITH gp AS (
            SELECT cast(null as int) parent_doc, a.id doc, 0 as lvl, cast('' as varchar(max)) pathstr
            from ks_ddlcontrol.ds_957_2475 a
                 --ks_ddlcontrol.ds_957_2625 b --on a.id=b.id_up
            where a.id = @gp
            UNION ALL
            SELECT b1.ds_2654, b1.id_up, lvl + 1, pathstr + '@#$' + (cast(gp.doc as varchar(max)))
            FROM gp gp
                     inner join ks_ddlcontrol.ds_957_2625 b1 on gp.doc = b1.ds_2654
            --left join ks_ddlcontrol.ds_957_2475 a1 on b1.id_up=a1.id
        )
             --insert #gp
             --SELECT  parent_doc,doc,documentid,lvl, pathstr,@bp pb  FROM gp  --order by lvl desc --where parent_doc is null
        SELECT gp.parent_doc
             , gp.doc
             , gp.pathstr
             , case a.documentid
                   when 220 then 'Госпрограмма'
                   when 221 then 'Подпрограмма'
                   when 222 then 'Цель'
                   when 226 then 'Мероприятие'
                   when 229 then 'Задача'
                   when 252 then 'Индикатор'
            end              prg_type
             , a.at_2537     prg_name
             , isp.at_3199   resp_kod
             , isp_n.at_3201 resp_name
             , case left(a.at_2837, 1)
                   when '0' then substring(a.at_2837, 2, LEN(a.at_2837) - 1)
                   else a.at_2837
            end as           prg_order
        into #gp
        FROM gp gp
                 inner join ks_ddlcontrol.ds_957_2475 a on gp.doc = a.id
                 left join ks_ddlcontrol.cl_1280_3226 isp on a.cl_3252 = isp.id
                 left join ks_ddlcontrol.cl_1280_3231 isp_n on isp.id = isp_n.id_up
            and isnull(isp_n.at_3202, 0) = (select max(isnull(isp_n1.at_3202, 0)) from ks_ddlcontrol.cl_1280_3231 isp_n1 where isp_n1.id_up = isp_n.id_up and isnull(isp_n1.at_3202, 0) <= @date)
        select * from #gp
        create table #prg
        (
            str_gp   varchar(30),
            str_name varchar(500),
            str_id   int
        )
        insert into #prg select 'Госпрограммы', 'Ответственный исполнитель государственной программы', 1
        insert into #prg select 'Госпрограммы', 'Соисполнители государственной программы', 2
        insert into #prg select 'Госпрограммы', 'Сроки и этапы реализации государственной программы', 3
        insert into #prg select 'Госпрограммы', 'Подпрограммы', 4
        insert into #prg select 'Госпрограммы', 'Цель государственной программы', 5
        insert into #prg select 'Госпрограммы', 'Индикаторы цели', 6
        insert into #prg select 'Госпрограммы', 'Задачи государственной программы', 7
        insert into #prg select 'Госпрограммы', 'Показатели задач', 8
        insert into #prg select 'Госпрограммы', 'Параметры финансового обеспечения всего, в том числе по годам реализации государственной программы', 9
        insert into #prg select 'Госпрограммы', 'Ожидаемые результаты реализации государственной программы', 10

        --order by lvl desc --where parent_doc is null
        select distinct resp_name content, 1 str_id into #prg_resp from #gp where prg_type = 'Госпрограмма'

        select distinct resp_name content, 2 str_id into #prg_soresp from #gp where prg_type != 'Госпрограмма' and resp_name is not null

        ----------------------------------------------------------------
        -- Сроки и этапы реализации государственной программы
        ----------------------------------------------------------------

        create table #prg_period
        (
            content varchar(1000),
            str_id  int
        )
        --Собираем сроки
        insert into #prg_period (content, str_id)
        select distinct (isnull(cast(at_2538 / 10000 as varchar(10)), '') + isnull('-' + cast(at_2539 / 10000 as varchar(10)), '') + 'г.') as content, 3 str_id
        from #gp gp
                 left join ks_ddlcontrol.ds_957_2475 a on gp.doc = a.id
        where gp.prg_type = 'Госпрограмма'

        --Собираем этапы
        insert into #prg_period (content, str_id)
        select p.AT_3546, 3
        from #gp gp
                 inner join ks_ddlcontrol.ds_957_3639 p on p.id_up = gp.doc

        ----------------------------------------------------------------

--         select * from #prg_period

        ----------------------------------------------------------------
        -- Подпрограммы
        ----------------------------------------------------------------
        select *
        into #ppgp
        from (select distinct 'Подпрограмма ' + prg_order + '. «' + prg_name + '»' content, 4 str_id, prg_order from #gp where prg_type = 'Подпрограмма' and parent_doc = @gp) as tbl
        order by tbl.prg_order
        --select * from #ppgp
        ----------------------------------------------------------------

        select distinct prg_name content, 5 str_id into #tgt from #gp where prg_type = 'Цель' and parent_doc = @gp

        ----------------------------------------------------------------
        -- Индикаторы
        ----------------------------------------------------------------
        select distinct
            --case when gp.prg_type = 'задача' then 'Показатель' else gp1.prg_type end + ' ' + gp1.prg_order + ' ' + left(gp.prg_type, len(gp.prg_type) - 1) + 'и ' +
            gp1.prg_order + '. ' + gp1.prg_name + ', ' + ei.at_2544 content,
            6                                                       str_id
        into #ind_tgt
        from #gp gp
                 inner join #gp gp1 on gp.doc = gp1.parent_doc and gp1.prg_type = 'Индикатор'
                 inner join ks_ddlcontrol.ds_957_2475 gmp on gp1.doc = gmp.id
                 left join ks_ddlcontrol.cl_960_2491 ei on gmp.cl_2633 = ei.id

        where gp.prg_type = 'Цель'
          and gp.parent_doc = @gp

        --         select * from #ind_tgt
        ----------------------------------------------------------------
        select distinct prg_order + '. ' + prg_name content, 7 str_id
        into #task
        from #gp
        where prg_type = 'Задача'
          and parent_doc = @gp

        ----------------------------------------------------------------
        -- Показатели
        ----------------------------------------------------------------
        select *
        into #ind_task
        from (select distinct case when gp.prg_type = 'задача' then 'Показатель' else gp1.prg_type end + ' ' + gp1.prg_order + ' ' + left(gp.prg_type, len(gp.prg_type) - 1) + 'и ' +
                              +gp.prg_order + ' ' + gp1.prg_name content,
                              8                                  str_id,
                              gp.prg_order  as                   gp_prg_order,
                              gp1.prg_order as                   gp1_prg_order

              from #gp gp
                       inner join #gp gp1 on gp.doc = gp1.parent_doc and gp1.prg_type = 'Индикатор'
              where gp.prg_type = 'Задача'
                and gp.parent_doc = @gp) as tbl
        order by tbl.gp_prg_order, tbl.gp1_prg_order
        --         select * from #ind_task
        ----------------------------------------------------------------

        ----------------------------------------------ресурсное обеспечение

        select pm.cl_2572                                                                                                   finid
             , spr_fin.at_1999                                                                                              fin
             , spr_fin.at_1997                                                                                              fin_code
             , round(sum(isnull(pm.M_2928, 0.00)), 2)                                                                    as [ОЧГ13]
             , round(sum(isnull(pm.M_2929, 0.00)), 2)                                                                    as [ОЧГ14]
             , round(sum(isnull(pm.M_2930, 0.00)), 2)                                                                    as [ОЧГ15]
             , round(sum(isnull(pm.M_2931, 0.00)), 2)                                                                    as [ОЧГ16]
             , round(sum(isnull(pm.M_2932, 0.00)), 2)                                                                    as [ОЧГ17]
             , round(sum(isnull(pm.M_2937, 0.00)), 2)                                                                    as [ОЧГ18]
             , round(sum(isnull(pm.M_2938, 0.00)), 2)                                                                    as [ОЧГ19]
             , round(sum(isnull(pm.M_2939, 0.00)), 2)                                                                    as [ОЧГ20]
             , round(sum(isnull(pm.M_3073, 0.00)), 2)                                                                    as [ОЧГ21]
             , round(sum(isnull(pm.M_3344, 0.00)), 2)                                                                    as [ОЧГ22]
             , round(sum(isnull(pm.M_3345, 0.00)), 2)                                                                    as [ОЧГ23]
             , round(sum(isnull(pm.M_3346, 0.00)), 2)                                                                    as [ОЧГ24]
             , round(sum(isnull(pm.M_3347, 0.00)), 2)                                                                    as [ОЧГ25]
             , round(sum(isnull(pm.M_2928, 0.00) + isnull(pm.M_2929, 0.00) + isnull(pm.M_2930, 0.00) + isnull(pm.M_2931, 0.00)
            + isnull(pm.M_2932, 0.00) + isnull(pm.M_2937, 0.00) + isnull(pm.M_2938, 0.00) + isnull(pm.M_2939, 0.00)
            + isnull(pm.M_3073, 0.00) + isnull(pm.M_3344, 0.00) + isnull(pm.M_3346, 0.00) + isnull(pm.M_3347, 0.00)), 2) as summall
        into #t
        from #gp gp
                 inner join ks_ddlcontrol.ds_957_2506 pm on gp.doc = pm.id_up
                 inner join ks_ddlcontrol.cl_894_1936 spr_fin on pm.cl_2572 = spr_fin.id
        where gp.prg_type = 'мероприятие'

        group by pm.cl_2572
               , spr_fin.at_1999
               , spr_fin.at_1997
        --select * from #t

        /*declare
            @yearStartImplementation int --год начала реализации
            , @yearEndImplementation int --год окончания реализации
        select @yearStartImplementation = (select top 1 left(at_2538, 4) from #gp gp left join ks_ddlcontrol.ds_957_2475 a on gp.doc = a.id)
        select @yearEndImplementation = (select top 1 left(at_2539, 4) from #gp  left join ks_ddlcontrol.ds_957_2475 a on gp.doc = a.id)*/


        select replace('Общий объем финансового обеспечения: ' + ' - ' + replace(replace(convert(varchar, cast(sum(a.summall) as money), 1), ',', ' '), '.', ',') + ' руб., в том числе по годам:'
                           + isnull(char(13) + char(10) + '2013 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ13]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2014 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ14]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2015 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ15]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2016 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ16]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2017 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ17]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2018 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ18]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2019 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ19]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2020 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ20]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2021 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ21]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2022 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ22]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2023 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ23]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2024 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ24]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                           + isnull(char(13) + char(10) + '2025 год - ' + nullif(replace(replace(convert(varchar, cast(sum(a.[ОЧГ25]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')

                           --from #t  a  for xml path(''))--+char(13)+char(10)+
                           + (select char(13) + char(10) + fin + ' - ' + replace(replace(convert(varchar, cast(sum(c.summall) as money), 1), ',', ' '), '.', ',') + ' руб., в том числе по годам:'
                                         + isnull(char(13) + char(10) + '2013 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ13]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2014 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ14]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2015 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ15]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2016 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ16]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2017 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ17]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2018 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ18]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2019 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ19]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2020 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ20]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2021 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ21]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2022 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ22]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2023 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ23]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2024 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ24]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                                         + isnull(char(13) + char(10) + '2025 год - ' + nullif(replace(replace(convert(varchar, cast(sum(c.[ОЧГ25]) as money), 1), ',', ' '), '.', ','), '0,00') + ' руб.;', '')
                              from #t c
                                   --	where 	c.[Источник финансирования]=b.[Источник финансирования]

                              group by c.fin, c.fin_code
                              order by c.fin_code for xml path ('')), '&#x0D;', '') content
             , 9                                                                    str_id
        into #volume
        from #t a


        ----------------------------------------------------------------------

        select distinct at_2532 content, 10 str_id

        into #rests
        from #gp gp
                 inner join ks_ddlcontrol.ds_957_2478 a on gp.doc = a.id_up
        where gp.doc = @gp
        select * from #rests

        declare
            @textResult varchar(max)
        select
            --LOWER(LEFT(gmp.AT_2537, 1))+RIGHT(gmp.AT_2537,LEN(gmp.AT_2537)-1) as [наименование],
            LOWER(LEFT(gmp.AT_2537, 1)) + SUBSTRING(gmp.AT_2537, 2, LEN(gmp.AT_2537) - 1) as [наименование],
            --gmp.AT_2537 as [наименование],
            LEFT(gmp.AT_2538, 4)                                                          as [год_начала_реализации],
            LEFT(gmp.AT_2539, 4)                                                             as [год_окончания_реализации],
            [М_Госпрограммы].cl_3586                                                      as [признак_количественности],
            cast(cast(case (LEFT(gmp.AT_2538, 4))
                          when '2012' then p.m_2643
                          when '2013' then p.m_2628
                          when '2014' then p.m_2629
                          when '2015' then p.m_2630
                          when '2016' then p.m_2631
                          when '2017' then p.m_2632
                          when '2018' then p.m_2934
                          when '2019' then p.m_2935
                          when '2020' then p.m_2936
                          when '2021' then p.m_3072
                          when '2022' then p.m_3339
                          when '2023' then p.m_3340
                          when '2024' then p.m_3341
                          when '2025' then p.m_3343
                          ELSE '0'
                end as float) as varchar)                                                 as [показатель_начала_реализации],
            --r.[едизмерения]                                                               as [ед_измерения],
            gmp.at_3275                                                                   as [показатель_направленности],
            case gmp.at_3275
                when '1' then 'увеличится'
                when '0' then 'снизится'
                end                                                                       as [направленность_текст],
            cast(cast(case (LEFT(r.AT_2539, 4))
                          when '2012' then p.m_2643
                          when '2013' then p.m_2628
                          when '2014' then p.m_2629
                          when '2015' then p.m_2630
                          when '2016' then p.m_2631
                          when '2017' then p.m_2632
                          when '2018' then p.m_2934
                          when '2019' then p.m_2935
                          when '2020' then p.m_2936
                          when '2021' then p.m_3072
                          when '2022' then p.m_3339
                          when '2023' then p.m_3340
                          when '2024' then p.m_3341
                          when '2025' then p.m_3343
                          ELSE '0'
                end as float) as varchar)                                                 as [показатель_окончания_реализации],
            cast(cast(p.m_3577 as float) as varchar)                                      as [показатель_итого],
            gmp.at_2837                                                                   as [порядковый_номер],
            r.ordergp
        into #expectedResult
        from #gp r
                 left join ks_ddlcontrol.ds_957_2475 gmp on (gmp.id = r.doc)
                 left join ks_ddlcontrol.cl_956_2470 [М_Госпрограммы] on ([М_Госпрограммы].id = gmp.cl_2640)
                 left join ks_ddlcontrol.ds_957_2478 p on (p.id_up = r.doc)
        where r.prg_type = 'Индикатор'

        select * from #expectedResult
        --select * from ks_ddlcontrol.ds_957_2478 where id_up=109726
        ---------------------паспорт гп


        select a.*
             , replace(stuff(case a.str_id
                                 when 1 then (select char(13) + char(10) + resp.content from #prg_resp resp where a.str_id = resp.str_id for xml path (''))
                                 when 2 then (select char(13) + char(10) + soresp.content from #prg_soresp soresp where a.str_id = soresp.str_id for xml path (''))
                                 when 3 then (select char(13) + char(10) + period.content from #prg_period period where a.str_id = period.str_id for xml path (''))
                                 when 4 then (select char(13) + char(10) + ppgp.content from #ppgp ppgp where a.str_id = ppgp.str_id for xml path (''))
                                 when 5 then (select char(13) + char(10) + tgt.content from #tgt tgt where a.str_id = tgt.str_id for xml path (''))
                                 when 6 then (select char(13) + char(10) + ind_tgt.content from #ind_tgt ind_tgt where a.str_id = ind_tgt.str_id for xml path (''))
                                 when 7 then (select char(13) + char(10) + task.content from #task task where a.str_id = task.str_id for xml path (''))
                                 when 8 then (select char(13) + char(10) + ind_task.content from #ind_task ind_task where a.str_id = ind_task.str_id for xml path (''))
                                 when 9 then (select char(13) + char(10) + volume.content from #volume volume where a.str_id = volume.str_id for xml path (''))
                                 when 10 then (select char(13) + char(10) + rests.content from #rests rests where a.str_id = rests.str_id for xml path (''))
                                 end, 1, 7, ''), '&#x0D;', '') content
        into #pasport
        from #prg a
        /*
            left join #prg_resp resp on a.str_id=resp.str_id
            left join #prg_soresp soresp on a.str_id=soresp.str_id
            left join #prg_period period on a.str_id=period.str_id
            left join #ppgp ppgp on a.str_id=ppgp.str_id
            left join #tgt tgt on a.str_id=tgt.str_id
            left join #ind_tgt ind_tgt on a.str_id=ind_tgt.str_id
            left join #task task on a.str_id=task.str_id
            left join #ind_task ind_task on a.str_id=ind_task.str_id
            left join #volume volume on a.str_id=volume.str_id
            left join #rests rests on a.str_id=rests.str_id
        */
-----------------------------------------------------------------
/*
---------------Текстовая часть------------
create table #chapter2([order] varchar(10),npa varchar(2000),part int)
insert #chapter2
select npa.at_3579 [order], spr_npa.at_3537 npa,1 part--+' от '+dbo.int_to_date(spr_npa.at_2630) npa
from ks_ddlcontrol.ds_957_3626 npa
	inner join ks_ddlcontrol.cl_968_2614 spr_npa on npa.cl_3538=spr_npa.id
where npa.id_up=@gp

insert #chapter2
select pr.at_3580 [order], spr_pr.at_3526 npa,2 part--+' от '+dbo.int_to_date(spr_npa.at_2630) npa
from ks_ddlcontrol.ds_957_3629 pr
	inner join ks_ddlcontrol.cl_1394_3607 spr_pr on pr.cl_3540=spr_pr.id and spr_pr.cl_3530=2
where pr.id_up=@gp

--select * from #chapter2
-------------
-----Перечень индикаторов
-------------------------------------------

select distinct doc.at_3338	[order]
				,gp.prg_name indikator
				,ei.at_2544 ei
				,stuff((select distinct char(13)+char(10)+ det.at_3541 from ks_ddlcontrol.ds_957_3631 det where det.id_up=gp.doc for xml path('')),1,7,'') [metod]
				--,det.at_3541 metod,
				,stuff((select distinct char(13)+char(10)+ spr.at_3526 from ks_ddlcontrol.ds_957_3629 det
								inner join ks_ddlcontrol.cl_1394_3607 spr on det.cl_3540=spr.id and spr.cl_3530=1
					where det.id_up=gp.doc for xml path('')),1,7,'') [source]
into #indikators
from #gp gp
	inner join ks_ddlcontrol.ds_957_2475 doc on gp.doc=doc.id
	left join ks_ddlcontrol.cl_960_2491 ei on doc.cl_2633=ei.id
where gp.prg_type='Индикатор' --and gp.parent_doc=@gp
-------------------------------------------
*/


        drop table #prg
        drop table #prg_resp
        drop table #prg_soresp
        drop table #prg_period
        drop table #ppgp
        drop table #ind_tgt
        drop table #task
        drop table #ind_task
        drop table #volume
        drop table #rests
        drop table #t

        select * from #pasport
        drop table #pasport
        --drop table #chapter2
--drop table #indikators
/*
select
from
*/
    end