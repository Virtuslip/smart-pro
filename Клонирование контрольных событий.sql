--select * from KS_DDLControl.DS_957_2475 where id = 86683

declare @Unit uniqueidentifier
set @Unit = 'c956df22-1ac2-4911-9cdf-cb966fffc372'

--Получаем контрольные события источника
select
	a.*
	,newid() as new_guid
	,@Unit as new_unit
	,b.DS_2654   --Вышестоящий документ
	,upDoc.cl_2640 as [ID мероприятия]
into #gmp_source
from
	KS_DDLControl.DS_957_2475 a  
	left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
	left join KS_DDLControl.DS_957_2475 upDoc on upDoc.id = b.DS_2654
where
	a.AT_2845='12'
	and a.documentid = 337
	and a.cl_3258 = 6567
	
	
--Генерируем новые guid
select id, newid() as new_guid into #gmp_source_id from #gmp_source

--Получаем контрольные события получателя
select
 distinct
	b.DS_2654   --Вышестоящий документ
	,upDoc.cl_2640 as  [ID мероприятия]
into #gmp_target
from
	KS_DDLControl.DS_957_2475 a  
	left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
	left join KS_DDLControl.DS_957_2475 upDoc on upDoc.id = b.DS_2654
where
	a.AT_2845='10'
	and a.documentid = 337
	and a.cl_3258 = 6567
/*
	BEGIN TRANSACTION
        INSERT INTO Users(ID, Name, Age)
        VALUES(1, 'Bob', 24)
        
        DELETE FROM Users WHERE Name = 'Todd'
   ROLLBACK TRANSACTION*/

--Получаем контрольные события источника	
select source.*, target.DS_2654  from #gmp_source source
	left join #gmp_target target on target.[ID мероприятия] = source.[ID мероприятия]
select * from #gmp_target
select * from #gmp_source_id