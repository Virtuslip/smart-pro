CREATE proc [KS_DDLControl].[xp_GPpril2] (@nmode int=0, @GP int=14, @Ved int=12631, @Ver varchar(2) = '99')--[KS_DDLControl].[xp_GPpril2]  @Ved int=12631,
as

--������ �������: exec [KS_DDLControl].[xp_GPpril2] @nmode=0, @GP=16, @Ved=132, @Ver = '01'
	
create table #temp_result_Gppril2 (
			res varchar(500)
			, documentid int
			, name varchar(50)
			, Doc varchar(2000)
			, DocOriginal varchar(2000)

			, [KBK_Ved] varchar(6)
			, [���13] numeric (20,4)
			, [���14] numeric (20,4)
			, [���15] numeric (20,4)
			, [���16] numeric (20,4)
			, [���17] numeric (20,4)
			, [���18] numeric (20,4)
			, [���19] numeric (20,4)
			, [���20] numeric (20,4)
			, [���21] numeric (20,4)
			, [���22] numeric (20,4)
			, [���23] numeric (20,4)
			, [���24] numeric (20,4)
			, [���25] numeric (20,4)
--			, [���26] numeric (20,2)

			, [Isp] varchar(2000)
			, DocId int
			, Num varchar(50)     --������ ������� ����� 01.01.01.02
			, Post varchar(max)
			, id int identity (1,1)
			)
	
	
if @nmode <> -1 
begin
	set nocount on
	set transaction isolation level read uncommitted
	
	declare @aDateInt int
	select  @aDateInt = convert(varchar,dbo.getworkdate(),112)
	------------
	--������� --
	------------
	Declare @user_type int
	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()

	Declare @GmpPermission int
	select 
		@GmpPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 956  --����������� ���

	Declare @VedPermission int
	select 
		@VedPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 864  --���������� ��������� 

	---------------------------------------------------------
	--������� ���������� �������� ����������� ������������ --
	---------------------------------------------------------
	declare @Cl table (id int, [������������] varchar(2000), [DBegin] int, [���] varchar(20), Post varchar(max))
	insert into @Cl
	select
		  a.id
		, isnull(AT_2641, AT_2526) as [��]
		, b.AT_2642 as [DBegin]
		, a.AT_2524
		, a.AT_2924
	from
        	KS_DDLControl.CL_956_2470 a
		left join (select * from KS_DDLControl.CL_956_2615 where AT_2642 <= @aDateInt) b on a.id = b.id_up

	select tab2.id, tab2.[������������] as [��], tab2.[���], tab2.Post into #sprGp from
	(select id, max(DBegin) as DBegin from @Cl group by id) tab1
	inner join 
	(select id, DBegin, [������������], [���], Post from @Cl) tab2
	on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)

	--�������� ��������� �� ������� ����������� ������
	if isnull(@GmpPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprGp where id not in (select CLID from KS_DDLControl.CLA_956 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

-------------------------------------------
	--������� �������� ����������� ��������� --
	-------------------------------------------
	select a.*, a1.AT_1797, a1.AT_1796, a1.AT_1930
	into #sprVed 
	from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
	--�������� ��������� �� ������� ����������� ������
	if isnull(@VedPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprVed where id not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

	create table #tab_sprGpOnDoc 
			( 
			  [��_���������] int
			, [���] varchar(50)
			, [������������] varchar(2000)
			, [���1] varchar(10)
			, [id��] int
			)  
	exec [ks_ddlcontrol].[xp_sprGpOnDoc] @nmode=0, @table_name_result='', @ToTable = 1

	-------------------------------------------------
	-- ������� ���������� ���������� ��������� ��� --2646 - �������������
	-------------------------------------------------
	select
		  a.documentid
		, bn.[������������] as Doc
		, c.AT_1795 as [IspCode]  
		, c.AT_1797 as [Isp]
		, c.id as IdSprVed
		, a.id
		, b.DS_2654 as [id_up]
		, a.CL_2640 as IdSprGp
		, a.at_2837 as [Code]
		, (select count(*) from KS_DDLControl.DS_957_2625 o1 inner join KS_DDLControl.DS_957_2475 a1 on o1.id_up = a1.id where o1.DS_2654 = a.id and a1.documentid in (221,226)) as [CountDwn]
		, (
		select 
			count(distinct b1.CL_2556) 
		from 
			KS_DDLControl.DS_957_2625 a1  --����
			left join KS_DDLControl.DS_957_2625 a2 on a2.DS_2654 = a1.id_up --�����������
			left join KS_DDLControl.DS_957_2625 a3 on a3.DS_2654 = a2.id_up --��������������
			left join KS_DDLControl.DS_957_2625 a4 on a4.DS_2654 = a3.id_up --�����������������
			left join KS_DDLControl.DS_957_2506 b1 on b1.id_up in (a1.id_up, a2.id_up, a3.id_up, a4.id_up)
		where 
			a1.DS_2654 = a.id --��
			or a2.DS_2654 = a.id --����
			or a3.DS_2654 = a.id --�����������
			or a4.DS_2654 = a.id --��������������
			or a4.id_up   = a.id --�����������������
		)           as CountIsp
		, d.[Post] 	-- a.[M_2879] as Post 
		, (case when a.documentid in (221,226) then 0 else a.documentid end) as [EntityObjectID]
	into [#alldoc] 
	from
		[ks_ddlControl].[GetDocGP](@GP, 0, @Ver) o inner join KS_DDLControl.DS_957_2475 a on o.id = a.id
			inner join #tab_sprGpOnDoc bn on a.id = bn.[��_���������]
			left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
			Left Join #sprVed  c on a.CL_2533 = c.id        	     --�����_���������
			Left Join #sprGP  d on a.CL_2640 = d.id   		     --�_������������
	where
		a.documentid in (220,221,226)
--select * from  @Cl
--select * from #sprGp
--select * from #tab_sprGpOnDoc		
--select * from [#alldoc] 
	select
		  a.documentid
		, a.Doc
		, '' as [KBK_Ved]  ---kbk1.AT_1795 as [KBK_Ved]  
		, kbk1.id as [Ved]
		, kbk2.AT_1798 as [KBK_Div]  
		, kbk3.AT_1801 as [KBK_CS]   
		, 'x' as [KBK_VR]   --, (case when object_id('tempdb..#temp_result_Gppril7_svod') is null then 'x' else kbk4.AT_1804 end) as [KBK_VR] 
		
--		, sum(round(isnull(e.M_2928,0.0)/1000,2)) as [���13]
--		, sum(round(isnull(e.M_2929,0.0)/1000,2)) as [���14]
--		, sum(round(isnull(e.M_2930,0.0)/1000,2)) as [���15]  
--		, sum(round(isnull(e.M_2931,0.0)/1000,2)) as [���16]
--		, sum(round(isnull(e.M_2932,0.00)/1000,3)) as [���17]
--		, sum(round(isnull(e.M_2937,0.00)/1000,3)) as [���18]
--	   	, sum(round(isnull(e.M_2938,0.0)/1000,2)) as [���19]
--		, sum(round(isnull(e.M_2939,0.0)/1000,2)) as [���20]
--		, sum(round(isnull(e.M_3073,0.0)/1000,2)) as [���21]
--      , sum(round(isnull(e.M_3344,0.0)/1000,2)) as [���22]
--		, sum(round(isnull(e.M_3345,0.0)/1000,2)) as [���23]
--		, sum(round(isnull(e.M_3346,0.0)/1000,2)) as [���24]
--		, sum(round(isnull(e.M_3347,0.0)/1000,2)) as [���25]

        , sum(isnull(e.M_2928,0.0)/1000) as [���13]
		, sum(isnull(e.M_2929,0.0)/1000) as [���14]
		, sum(isnull(e.M_2930,0.0)/1000) as [���15]  
		, sum(isnull(e.M_2931,0.0)/1000) as [���16]
		, sum(isnull(e.M_2932,0.0)/1000) as [���17]
		, sum(isnull(e.M_2937,0.0)/1000) as [���18]
		, sum(isnull(e.M_2938,0.0)/1000) as [���19]
		--, sum(isnull(e.M_2938,0.0)) as [���19]
		, sum(isnull(e.M_2939,0.0)/1000) as [���20]
		, sum(isnull(e.M_3073,0.0)/1000) as [���21]
		, sum(isnull(e.M_3344,0.0)/1000) as [���22]
		, sum(isnull(e.M_3345,0.0)/1000) as [���23]
		, sum(isnull(e.M_3346,0.0)/1000) as [���24]
		, sum(isnull(e.M_3347,0.0)/1000) as [���25]
		, isnull(kbk1.AT_1797, a.[Isp]) as [Isp]
		, a.id
		, a.[id_up]
		, a.IdSprGp
		, s.at_1999 as [res]
		, s.at_1997 as [resCode]
		, a.[Code]
		, a.[CountDwn]
		, a.[CountIsp]
		, newid() as serial
		, a.[Post]
		
	into [#allSource] 
	from
		[#alldoc] a 
--			left join KS_DDLControl.DS_1279_3189 e1  on e1.DS_3169 = a.id 
--			left join KS_DDLControl.DS_1279_3196 e  on e.id_up = e1.id       --�������_�����2 
			Left Join KS_DDLControl.DS_957_2506 e on e.id_up=a.id
			Left Join #sprVed kbk1 on e.CL_2556 = kbk1.id   --�����_���������
			Left Join KS_DDLControl.CL_865_1714 kbk2 on e.CL_2557 = kbk2.id   --�����_����������
			Left Join KS_DDLControl.CL_866_1718 kbk3 on e.CL_2558 = kbk3.id   --�����_������� ������
			Left Join KS_DDLControl.CL_867_1722 kbk4 on e.CL_2559 = kbk4.id   --�����_���� ��������
			Left Join KS_DDLControl.CL_870_1752 kbk5 on e.CL_2560 = kbk5.id   --�����_��������
			Left Join KS_DDLControl.CL_871_1759 Corr on e.CL_2571 = Corr.id   --�����_��������������
			left Join KS_DDLControl.CL_894_1936 S on e.CL_2572 = S.id         --�����_��������� ��������������
	where
		a.documentid in (220,221,226)
	group by
	
		  a.documentid
		, a.id
		, a.[Doc] 
		, kbk1.id
--		, kbk1.AT_1795
		, kbk2.AT_1798
		, kbk3.AT_1801
		--, kbk4.AT_1804
		, isnull(kbk1.AT_1797, a.[Isp])
		, a.[id_up]
		, a.IdSprGp
		, s.at_1999
		, s.at_1997
		, a.[Code]
		, a.[CountDwn]
		, a.[CountIsp]
		, a.[Post]

        
		
--select * from [#allSource] 	
	-----------------
	--������������ --
	-----------------
	declare @Post varchar(max)
	select @Post = Post from #alldoc where documentid = 220 and IdSprGp =@GP --and IdSprVed=@Ved
	-----------------
	-- ����� �� �� --
	-----------------
	insert into #temp_result_Gppril2  
	(res
	, documentid
	, name
	, Doc
	, [KBK_Ved]
	, [���13]
	, [���14]
	, [���15]
	, [���16]
	, [���17]
	, [���18]
	, [���19]
	, [���20]
	, [���21]
	, [���22]
	, [���23]
	, [���24]
	, [���25]
	, DocId)
	select 
	'�����'
	, a1.documentid
	, ''
	, a1.Doc
	, ''
	, sum(isnull(a2.[���13],0.0))
	, sum(isnull(a2.[���14],0.0))
	, sum(isnull(a2.[���15],0.0))
	, sum(isnull(a2.[���16],0.0))
	, sum(isnull(a2.[���17],0.0))
	, sum(isnull(a2.[���18],0.0))
	, sum(isnull([���19],0.0))
	, sum(isnull([���20],0.0))
	, sum(isnull([���21],0.0))
	, sum(isnull([���22],0.0))
	, sum(isnull([���23],0.0))
	, sum(isnull([���24],0.0))
	, sum(isnull([���25],0.0))
	, a1.Id
	from 
	
		#alldoc a1 cross join #allSource a2
	where a1.documentid = 220 and a1.IdSprGp =@GP and a2.res is not null --and a2.Ved=@Ved
	group by a1.documentid, a1.Doc, a1.ID

	-----------------------------------------------------
	-- ����� �� �� � ������� ���������� �������������� -- '_�����'
	-----------------------------------------------------
	insert into #temp_result_Gppril2  (res, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23],[���24],[���25])
	select 
		a2.resCode + a2.res
		, ''
		, sum(isnull(a2.[���13],0.0))
		, sum(isnull(a2.[���14],0.0))
		, sum(isnull(a2.[���15],0.0))
		, sum(isnull(a2.[���16],0.0))
		, sum(isnull(a2.[���17],0.0))
		, sum(isnull([���18],0.0))
		, sum(isnull([���19],0.0))
		, sum(isnull([���20],0.0))
		, sum(isnull([���21],0.0))
		, sum(isnull([���22],0.0))
		, sum(isnull([���23],0.0))
		, sum(isnull([���24],0.0))
		, sum(isnull([���25],0.0))
		
	from 
		[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id 
	where 
		a2.res is not null --and a2.Ved=@Ved
	group by 
		a2.resCode, a2.res
	having 
		count(distinct isnull(a2.KBK_Ved, a1.IspCode))>1 
	union all
	select 
		a.at_1997 + a.at_1999, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25]
	from
		KS_DDLControl.CL_894_1936 a
	left join 
		(
		select 
			a2.ResCode
			, '' as [KBK_Ved]
			, sum(isnull(a2.[���13],0.0)) as [���13]
			, sum(isnull(a2.[���14],0.0)) as [���14]
			, sum(isnull(a2.[���15],0.0)) as [���15]
			, sum(isnull(a2.[���16],0.0)) as [���16]
			, sum(isnull(a2.[���17],0.0)) as [���17]
			, sum(isnull([���18],0.0)) as [���18]
			, sum(isnull([���19],0.0)) as [���19]
			, sum(isnull([���20],0.0)) as [���20]
			, sum(isnull([���21],0.0)) as [���21]
			, sum(isnull([���22],0.0)) as [���22]
			, sum(isnull([���23],0.0)) as [���23]
			, sum(isnull([���24],0.0)) as [���24]
			, sum(isnull([���25],0.0)) as [���25]
		from 
			[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
		group by
			a2.ResCode, isnull(a2.KBK_Ved, a1.IspCode)
		) t
	on a.at_1997 = t.resCode
	order by 1,2
---isnull(a2.KBK_Ved, a1.IspCode)
	update #temp_result_Gppril2 set 
		doc = (select top 1 doc from #temp_result_Gppril2 where doc is not null) 
	        , documentid = 220
		, name = '��������������� ���������: '
		, DocID = (select top 1 docId from #temp_result_Gppril2 where doc is not null) 
	where doc is null

	Declare @documentidPpGpPrevious int
	Declare @NumOM int
	Declare @documentidPpGp int
	Declare @DocPpGp        varchar(2000)
	Declare @IspPpGp        varchar(2000)
	Declare @IdPpGP         int
	Declare @NumPpGP        int
	Declare @Num            varchar(100)
	Declare @CountDwnPpGP   int
	Declare @CountIspPpGP   int

	Declare @documentidM1 int
	Declare @DocM1        varchar(2000)
	Declare @IspM1        varchar(2000)
	Declare @IdM1         int
	Declare @NumM1        int
	Declare @CountDwnM1   int
	Declare @CountIspM1   int

	Declare @documentidM2 int
	Declare @DocM2        varchar(2000)
	Declare @IspM2        varchar(2000)
	Declare @IdM2         int
	Declare @NumM2        int
	Declare @CountDwnM2   int
	Declare @CountIspM2   int

	Declare @documentidM3 int
	Declare @DocM3        varchar(2000)
	Declare @IspM3        varchar(2000)
	Declare @IdM3         int
	Declare @NumM3        int
	Declare @CountDwnM3   int
	Declare @CountIspM3   int

	-----------------------
	-- ��� ������ �� �� --
	-----------------------
	Create table #t (id int)
	declare @IdOM int
	Declare CurOM cursor for select a.Id from #alldoc a where a.documentid = 226 and isnull(a.[id_up],0) in (select isnull(DocID,0) from #temp_result_Gppril2)
	open CurOM
	fetch next from CurOM into @IdOM
	while @@fetch_status = 0 
	begin
		insert into #t select id from [ks_ddlControl].[GetDocID](@IdOM)
	fetch next from CurOM into @IdOM
	end
	Close CurOM
	deallocate CurOM

	-------------------
	-- ����, ��, ��� --
	-------------------
	Declare Cur1 Cursor for select 
					a.documentid, a.Doc, a.Id, a.CountDwn, a.CountIsp
				from 
					#alldoc a inner join #temp_result_Gppril2 b on a.[id_up] = b.DocID
				where 
					b.id = 1
				order by 
					a.[EntityObjectID], a.Code, a.ID
	Open Cur1
	fetch next from Cur1 into @documentidPpGP, @DocPpGP, @IdPpGP,@CountDwnPpGP,@CountIspPpGP

	while @@fetch_status=0
	begin
		if @documentidPpGP = 226 and isnull(@documentidPpGpPrevious,0) = @documentidPpGP
		begin
			set @NumOM = isnull(@NumOM,0) + 1
			set @Num   = convert(varchar, @NumPpGP) +'.'+ convert(varchar, @NumOM)
		end else if @documentidPpGP = 226 
		begin
			set @NumPpGP = isnull(@NumPpGP,0) + 1
			set @NumOM = isnull(@NumOM,0) + 1
			set @Num   = convert(varchar, @NumPpGP) +'.'+ convert(varchar, @NumOM)

			----------------------
			-- ����� �� ���� �� --
			----------------------
			insert into #temp_result_Gppril2  (
			res
			, documentid
			, name
			, Doc
			, [KBK_Ved]
			, [���13]
			, [���14]
			, [���15]
			, [���16]
			, [���17]
			, [���18]
			, [���19]
			, [���20]
			, [���21]
			, [���22]
			, [���23]
			, [���24]
			, [���25]
			, Num)
			select 
			'�����'
			, a1.documentid
			, '��������� �����������'
			, ': '
			, ''
			, sum(isnull(a2.[���13],0.0))
			, sum(isnull(a2.[���14],0.0))
			, sum(isnull(a2.[���15],0.0))
			, sum(isnull(a2.[���16],0.0))
			, sum(isnull(a2.[���17],0.0))
			, sum(isnull([���18],0.0))
--			, sum(round(isnull(a2.[���17],0.0),2))
--			, sum(round(isnull([���18],0.0),2))
			, sum(isnull([���19],0.0))
			, sum(isnull([���20],0.0))
			, sum(isnull([���21],0.0))
			, sum(isnull([���22],0.0))
			, sum(isnull([���23],0.0))
			, sum(isnull([���24],0.0))
			, sum(isnull([���25],0.0))
			, @NumPpGP
			from 
				[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
				inner join #t o on a1.id = o.id 
			group by a1.documentid
			
			----------------------------------------------------------
			-- ����� �� ���� �� � ������� ���������� �������������� --
			----------------------------------------------------------
			---case when [���13]+[���14]+[���15]+[���16]+[���17]+[���18]+[���19]+[���20]+[���21]>0 then '�����' else '' end
			insert into #temp_result_Gppril2  (
			res
			, documentid
			, name
			, Doc
			, [KBK_Ved]
			, [���13]
			, [���14]
			, [���15]
			, [���16]
			, [���17]
			, [���18]
			, [���19]
			, [���20]
			, [���21]
			, [���22]
			, [���23]
			, [���24]
			, [���25]
			, Num)
			select 
				a.at_1997 + a.at_1999
				, t.documentid
				, '��������� �����������'
				, ': '
				, ''
				, [���13]
				, [���14]
				, [���15]
				, [���16]
				, [���17]
				, [���18]
				, [���19]
				, [���20]
				, [���21]
				, [���22]
				, [���23]
				, [���24]
				, [���25]
				, @NumPpGP
			from
				KS_DDLControl.CL_894_1936 a
			left join 
				(
				select 
					a2.ResCode
					, a1.documentid
					, sum(isnull(a2.[���13],0.0)) as [���13]
					, sum(isnull(a2.[���14],0.0)) as [���14]
					, sum(isnull(a2.[���15],0.0)) as [���15]
					, sum(isnull(a2.[���16],0.0)) as [���16]
--					, sum(round(isnull(a2.[���17],0.0),2))
--			, sum(round(isnull([���18],0.0),2))
					
					, sum(isnull(a2.[���17],0.0)) as [���17]
					, sum(isnull([���18],0.0)) as [���18]
					
					, sum(isnull([���19],0.0)) as [���19]
					, sum(isnull([���20],0.0)) as [���20]
					, sum(isnull([���21],0.0)) as [���21]
					, sum(isnull([���22],0.0)) as [���22]
					, sum(isnull([���23],0.0)) as [���23]
					, sum(isnull([���24],0.0)) as [���24]
					, sum(isnull([���25],0.0)) as [���25] 
				from 
					[#allDoc] a1
					left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
					inner join #t o on a2.id = o.id 
				group by
					a2.ResCode, a1.documentid
				) t
			on a.at_1997 = t.resCode
			union all
			select 
			a2.resCode + a2.res
			, a1.documentid
			, '��������� �����������'
			, ': '
			, isnull(a2.KBK_Ved, a1.IspCode)
			, sum(isnull(a2.[���13],0.0))
			, sum(isnull(a2.[���14],0.0))
			, sum(isnull(a2.[���15],0.0))
			, sum(isnull(a2.[���16],0.0))
--			, sum(round(isnull(a2.[���17],0.0),2))
--			, sum(round(isnull([���18],0.0),2))
			, sum(isnull(a2.[���17],0.0))
			, sum(isnull([���18],0.0))
			, sum(isnull([���19],0.0))
			, sum(isnull([���20],0.0))
			, sum(isnull([���21],0.0))
			, sum(isnull([���22],0.0))
			, sum(isnull([���23],0.0))
			, sum(isnull([���24],0.0))
			, sum(isnull([���25],0.0))
			, @NumPpGP
			from 
				[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id -- and a2.Ved=@Ved
				inner join #t o on a1.id = o.id 
			where a2.res is not null --and a2.Ved=@Ved
			group by a2.resCode, a2.res, a1.documentid, isnull(a2.KBK_Ved, a1.IspCode)
			order by 1

		end else 
		begin
			set @NumOM=0
			set @NumPpGP = isnull(@NumPpGP,0) + 1
			set @Num = convert(varchar, @NumPpGP)
		end

		----------
		-- ���� --
		----------
		insert into #temp_result_Gppril2  (
		res
		, documentid
		, name
		, Doc
		, [KBK_Ved]
		, [���13]
		, [���14]
		, [���15]
		, [���16]
		, [���17]
		, [���18]
		, [���19]
		, [���20]
		, [���21]
		, [���22]
		, [���23]
		, [���24]
		, [���25]
		, DocId
		, Num)
		select '�����', @documentidPpGp
				,(case 
					when @documentidPpGp = 221 then '������������ �'
					when @documentidPpGp = 226 then '��������� ����������� �' 
					else '��������� �� �� �' 
				 end) 
			, @DocPpGP
			, ''
			, sum(isnull(a2.[���13],0.0))
			, sum(isnull(a2.[���14],0.0))
			, sum(isnull(a2.[���15],0.0))
			, sum(isnull(a2.[���16],0.0))
--			, sum(round(isnull(a2.[���17],0.0),2))
--			, sum(round(isnull([���18],0.0),2))
			, sum(isnull(a2.[���17],0.0))
			, sum(isnull([���18],0.0))
			, sum(isnull([���19],0.0))
			, sum(isnull([���20],0.0))
			, sum(isnull([���21],0.0))
			, sum(isnull([���22],0.0))
			, sum(isnull([���23],0.0))
			, sum(isnull([���24],0.0))
			, sum(isnull([���25],0.0))
			, @IdPpGP
			, @Num
			
		from 
			[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
			inner join (select id from [ks_ddlControl].[GetDocID](@IdPpGP) union select @IdPpGP) o on a1.id = o.id 
		
		----------------------------------------------
		-- ���� � ������� ���������� �������������� -- '_�����'
		----------------------------------------------
		insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24],[���25], DocId, Num)
		select a2.resCode + a2.res, @documentidPpGp as documentid
			,(case 
				when @documentidPpGp=221 then '������������ �'
				when @documentidPpGp=226 then '��������� ����������� �' 
				else '��������� �� �� �' 
			 end) 
			, @DocPpGP, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0))
			, sum(isnull(a2.[���16],0.0))
			, sum(isnull(a2.[���17],0.0))
			, sum(isnull([���18],0.0))
--			, sum(round(isnull(a2.[���17],0.0),2))
--			, sum(round(isnull([���18],0.0),2))
			, sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdPpGP, @Num
			
		from 
			[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
			inner join (select id from [ks_ddlControl].[GetDocID](@IdPpGP) union select @IdPpGP) o on a1.id = o.id 
		where a2.res is not null --and a2.Ved=@Ved
		group by a2.resCode, a2.res
		having count(distinct isnull(a2.KBK_Ved, a1.IspCode))>1 
		union all
		select 
			a.at_1997 + a.at_1999, @documentidPpGp as documentid
			,(case 
				when @documentidPpGp=221 then '������������ �'
				when @documentidPpGp=226 then '��������� ����������� �' 
				else '��������� �� �� �' 
			 end) 
			, @DocPpGP, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], @IdPpGP, @Num
		from
			KS_DDLControl.CL_894_1936 a
		left join 
			(
			select 
				a2.ResCode, '' as [KBK_Ved], sum(isnull(a2.[���13],0.0)) as [���13], sum(isnull(a2.[���14],0.0)) as [���14], sum(isnull(a2.[���15],0.0)) as [���15], sum(isnull(a2.[���16],0.0)) as [���16]
				, sum(isnull(a2.[���17],0.0)) as [���17]
				, sum(isnull([���18],0.0)) as [���18]
--				, sum(round(isnull(a2.[���17],0.0),2)) as [���17]
--			    , sum(round(isnull([���18],0.0),2)) as [���18]
				
				
				, sum(isnull([���19],0.0)) as [���19], sum(isnull([���20],0.0)) as [���20], sum(isnull([���21],0.0)) as [���21], sum(isnull([���22],0.0)) as [���22], sum(isnull([���23],0.0)) as [���23], sum(isnull([���24],0.0)) as [���24], sum(isnull([���25],0.0)) as [���25]
			from 
				[#allDoc] a1
				left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
				inner join (select id from [ks_ddlControl].[GetDocID](@IdPpGP) union select @IdPpGP) o on a1.id = o.id 
			group by
				a2.ResCode, isnull(a2.KBK_Ved, a1.IspCode)
			) t
		on a.at_1997 = t.resCode
		order by 1,5
---isnull(a2.KBK_Ved, a1.IspCode) as [KBK_Ved]

		-----------------
		-- ����������� --
		-----------------
		set @NumM1 = null
		Declare Cur2 Cursor for select a.documentid, a.Doc, a.Isp, a.Id, a.CountDwn, a.CountIsp
					from #alldoc a 
					where a.[id_up] = @IdPpGP 
					order by a.code, a.ID

		Open Cur2
		fetch next from Cur2 into @documentidM1, @DocM1, @IspM1, @IdM1,@CountDwnM1,@CountIspM1
		while @@fetch_status=0
		begin
			set @NumM1 = isnull(@NumM1,0) + 1
			----------
			-- ���� --
			----------
			insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23],[���24], [���25], DocId, Num)
			select '�����', @documentidM1
					,'����������� �'
				, @DocM1, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0)), sum(isnull(a2.[���16],0.0)), sum(isnull(a2.[���17],0.0)), sum(isnull([���18],0.0)), sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdM1, @Num +'.'+ convert(varchar, @NumM1)
			from 
				[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
				inner join (select id from [ks_ddlControl].[GetDocID](@IdM1) union select @IdM1) o on a1.id = o.id 
			
			----------------------------------------------
			-- ���� � ������� ���������� �������������� -- '_�����'
			----------------------------------------------
			insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], DocID, Num)
			select a2.resCode + a2.res, @documentidM1 as documentid
				,'����������� �'
				, @DocM1, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0)), sum(isnull(a2.[���16],0.0)), sum(isnull(a2.[���17],0.0)), sum(isnull([���18],0.0)), sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdM1, @Num +'.'+ convert(varchar, @NumM1)
			from 
				[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
				inner join (select id from [ks_ddlControl].[GetDocID](@IdM1) union select @IdM1) o on a1.id = o.id 
			where a2.res is not null --and a2.Ved=@Ved
			group by a2.resCode, a2.res
			having count(distinct isnull(a2.KBK_Ved, a1.IspCode))>1 
			union all
			select 
				a.at_1997 + a.at_1999, @documentidM1 as documentid
				,'����������� �'
				, @DocM1, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], @IdM1, @Num +'.'+ convert(varchar, @NumM1)
			from
				KS_DDLControl.CL_894_1936 a
			left join 
				(
				select 
					a2.ResCode, '' as [KBK_Ved], sum(isnull(a2.[���13],0.0)) as [���13], sum(isnull(a2.[���14],0.0)) as [���14], sum(isnull(a2.[���15],0.0)) as [���15], sum(isnull(a2.[���16],0.0)) as [���16], sum(isnull(a2.[���17],0.0)) as [���17], sum(isnull([���18],0.0)) as [���18], sum(isnull([���19],0.0)) as [���19], sum(isnull([���20],0.0)) as [���20], sum(isnull([���21],0.0)) as [���21], sum(isnull([���22],0.0)) as [���22], sum(isnull([���23],0.0)) as [���23], sum(isnull([���24],0.0)) as [���24], sum(isnull([���25],0.0)) as [���25]
				from 
					[#allDoc] a1
					left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
					inner join (select id from [ks_ddlControl].[GetDocID](@IdM1) union select @IdM1) o on a1.id = o.id 
				group by
					a2.ResCode, isnull(a2.KBK_Ved, a1.IspCode)
				) t
			on a.at_1997 = t.resCode
			order by 1,5
--isnull(a2.KBK_Ved, a1.IspCode)
			--------------------
			-- �������������� --
			--------------------
			set @NumM2 = null
			Declare Cur3 Cursor for select a.documentid, a.Doc, a.Isp, a.Id, a.CountDwn, a.CountIsp
						from #alldoc a 
						where a.[id_up] = @IdM1 
						order by a.code, a.Id
			Open Cur3
			fetch next from Cur3 into @documentidM2, @DocM2, @IspM2, @IdM2, @CountDwnM2, @CountIspM2
			while @@fetch_status=0
			begin
				set @NumM2 = isnull(@NumM2,0) + 1
				----------
				-- ���� --
				----------
				insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], DocId, Num)
				select '�����', @documentidM2
						,'����������� �'
					, @DocM2, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0)), sum(isnull(a2.[���16],0.0)), sum(isnull(a2.[���17],0.0)), sum(isnull([���18],0.0)), sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdM2, @Num +'.'+ convert(varchar, @NumM1) +'.'+ convert(varchar, @NumM2)
				from 
					[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
					inner join (select id from [ks_ddlControl].[GetDocID](@IdM2) union select @IdM2) o on a1.id = o.id 
				
				----------------------------------------------
				-- ���� � ������� ���������� �������������� --'_�����'
				----------------------------------------------
				insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], DocID, Num)
				select a2.resCode + a2.res, @documentidM2 as documentid
					,'����������� �'
					, @DocM2, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0)), sum(isnull(a2.[���16],0.0)), sum(isnull(a2.[���17],0.0)), sum(isnull([���18],0.0)), sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdM2, @Num +'.'+ convert(varchar, @NumM1) +'.'+ convert(varchar, @NumM2)
				from 
					[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
					inner join (select id from [ks_ddlControl].[GetDocID](@IdM2) union select @IdM2) o on a1.id = o.id 
				where a2.res is not null --and a2.Ved=@Ved
				group by a2.resCode, a2.res
				having count(distinct isnull(a2.KBK_Ved, a1.IspCode))>1 
				union all
				select 
					a.at_1997 + a.at_1999, @documentidM2 as documentid
					,'����������� �'
					, @DocM2, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], @IdM2, @Num +'.'+ convert(varchar, @NumM1) +'.'+ convert(varchar, @NumM2)
				from
					KS_DDLControl.CL_894_1936 a
				left join 
					(
					select 
						a2.ResCode, '' as [KBK_Ved], sum(isnull(a2.[���13],0.0)) as [���13], sum(isnull(a2.[���14],0.0)) as [���14], sum(isnull(a2.[���15],0.0)) as [���15], sum(isnull(a2.[���16],0.0)) as [���16], sum(isnull(a2.[���17],0.0)) as [���17], sum(isnull([���18],0.0)) as [���18], sum(isnull([���19],0.0)) as [���19], sum(isnull([���20],0.0)) as [���20], sum(isnull([���21],0.0)) as [���21], sum(isnull([���22],0.0)) as [���22], sum(isnull([���23],0.0)) as [���23], sum(isnull([���24],0.0)) as [���24], sum(isnull([���25],0.0)) as [���25]
					from 
						[#allDoc] a1
						left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
						inner join (select id from [ks_ddlControl].[GetDocID](@IdM2) union select @IdM2) o on a1.id = o.id 
					group by
						a2.ResCode, isnull(a2.KBK_Ved, a1.IspCode)
					) t
				on a.at_1997 = t.resCode
				order by 1,5
---isnull(a2.KBK_Ved, a1.IspCode)
				-----------------------
				-- ����������������� --
				-----------------------
				set @NumM3 = null
				Declare Cur4 Cursor for select a.documentid, a.Doc, a.Isp, a.Id, a.CountDwn, a.CountIsp
							from #alldoc a 
							where a.[id_up] = @IdM2 
							order by a.code, a.Id
				Open Cur4
				fetch next from Cur4 into @documentidM3, @DocM3, @IspM3, @IdM3, @CountDwnM3, @CountIspM3
				while @@fetch_status=0
				begin
					set @NumM3 = isnull(@NumM3,0) + 1
					----------
					-- ���� --
					----------
					insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], DocId, Num)
					select '�����', @documentidM3
							,'����������� �'
						, @DocM3, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0)), sum(isnull(a2.[���16],0.0)), sum(isnull(a2.[���17],0.0)), sum(isnull([���18],0.0)), sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdM3, @Num +'.'+ convert(varchar, @NumM1) +'.'+ convert(varchar, @NumM2) +'.'+ convert(varchar, @NumM3)
					from 
						[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
					where
						a1.id = @IdM3
					
					----------------------------------------------
					-- ���� � ������� ���������� �������������� --'_�����'
					----------------------------------------------
					insert into #temp_result_Gppril2  (res, documentid, name, Doc, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25],  DocID, Num)
					select a2.resCode + a2.res, @documentidM3 as documentid
						,'����������� �'
						, @DocM3, '', sum(isnull(a2.[���13],0.0)), sum(isnull(a2.[���14],0.0)), sum(isnull(a2.[���15],0.0)), sum(isnull(a2.[���16],0.0)), sum(isnull(a2.[���17],0.0)), sum(isnull([���18],0.0)), sum(isnull([���19],0.0)), sum(isnull([���20],0.0)), sum(isnull([���21],0.0)), sum(isnull([���22],0.0)), sum(isnull([���23],0.0)), sum(isnull([���24],0.0)), sum(isnull([���25],0.0)), @IdM2, @Num +'.'+ convert(varchar, @NumM1) +'.'+ convert(varchar, @NumM2) +'.'+ convert(varchar, @NumM3)
					from 
						[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
					where
						a1.id = @IdM3
						and a2.res is not null --and a2.Ved=@Ved
					group by a2.resCode, a2.res
					having count(distinct isnull(a2.KBK_Ved, a1.IspCode))>1 
					union all
					select 
						a.at_1997 + a.at_1999, @documentidM3 as documentid
						,'����������� �'
						, @DocM3, [KBK_Ved], [���13], [���14], [���15], [���16], [���17], [���18], [���19], [���20], [���21], [���22], [���23], [���24], [���25], @IdM3, @Num +'.'+ convert(varchar, @NumM1) +'.'+ convert(varchar, @NumM2) +'.'+ convert(varchar, @NumM3)
					from
						KS_DDLControl.CL_894_1936 a
					left join 
						(
						select 
							a2.ResCode,'' as [KBK_Ved], sum(isnull(a2.[���13],0.0)) as [���13], sum(isnull(a2.[���14],0.0)) as [���14], sum(isnull(a2.[���15],0.0)) as [���15], sum(isnull(a2.[���16],0.0)) as [���16], sum(isnull(a2.[���17],0.0)) as [���17], sum(isnull([���18],0.0)) as [���18], sum(isnull([���19],0.0)) as [���19], sum(isnull([���20],0.0)) as [���20], sum(isnull([���21],0.0)) as [���21], sum(isnull([���22],0.0)) as [���22], sum(isnull([���23],0.0)) as [���23], sum(isnull([���24],0.0)) as [���24], sum(isnull([���25],0.0)) as [���25]
						from 
							[#allDoc] a1 left join [#allSource] a2 on a1.id = a2.id --and a2.Ved=@Ved
						where
							a1.id = @IdM3
						group by
							a2.ResCode, isnull(a2.KBK_Ved, a1.IspCode)
						) t
					on a.at_1997 = t.resCode
					order by 1,5
-- isnull(a2.KBK_Ved, a1.IspCode) 
				fetch next from Cur4 into @documentidM3, @DocM3, @IspM3, @IdM3, @CountDwnM3, @CountIspM3
				end
				Close Cur4
				DealLocate Cur4

			fetch next from Cur3 into @documentidM2, @DocM2, @IspM2, @IdM2, @CountDwnM2, @CountIspM2
			end
			Close Cur3
			DealLocate Cur3

		fetch next from Cur2 into @documentidM1, @DocM1, @IspM1, @IdM1, @CountDwnM1, @CountIspM1
		end
		Close Cur2
		DealLocate Cur2

	set @documentidPpGpPrevious = @documentidPpGP
	fetch next from Cur1 into @documentidPpGP, @DocPpGP, @IdPpGP,@CountDwnPpGP,@CountIspPpGP
	end
	Close Cur1
	DealLocate Cur1


	update #temp_result_Gppril2 set res =right(res, len(res)-3) where res like '00%'
--	update #temp_result_Gppril2 set [KBK_Ved] ='�����' where [KBK_Ved] ='_�����'
	update #temp_result_Gppril2 set [KBK_Ved] ='' 

	update #temp_result_Gppril2 set Doc = '��������������� ���������: ' + Doc where num is null
	update #temp_result_Gppril2 set Doc = name + num +': '+ Doc where num is not null and name != '����������� �'
	update #temp_result_Gppril2 set Doc = name + substring(Num, 1+CharIndex('.', Num),len(Num)-charindex('.',Num)) +': '+ Doc where num is not null and name  = '����������� �'
	update #temp_result_Gppril2 set Post = @Post, DocOriginal = Doc

	--������������ Doc �� 7 �������� 
	declare @Id1 int, @Id2 int, @Doc1 varchar(2000), @Doc2 varchar(2000), @lres int, @ldoc int, @o1 int, @o2 int, @o3 int, @All int
	declare Cur1 Cursor for select id, DocOriginal, len(res), len(DocOriginal) from #temp_result_Gppril2 order by id
	open Cur1 
	fetch next from Cur1 into @Id1, @Doc1, @lres, @ldoc

	set @Doc2= ''
	set @o1 = 0
	set @o2 = 0
	while @@fetch_status = 0
	begin
		if @Doc1 = @Doc2
		begin
			set @o2 = @lres + charindex(' ', substring(@Doc1,@o1 + @lres,2000))
			if @o2=@lres set @o2=2000
		end else 
		begin
			if @Doc2 != '' 
			begin
				update #temp_result_Gppril2 set Doc= Doc + substring(DocOriginal, @o1, 2000) where id = @Id2 --������� ����������� ���������
				set @All = 0
			end
			set @o1 = 1
			set @o2 = Charindex(':',@Doc1)+1
		end

	 if @All = 1 
	 begin
		update #temp_result_Gppril2 set Doc= '' where id = @Id1
	 end else
	 begin
		update #temp_result_Gppril2 set Doc= substring(Doc, @o1, @o2) where id = @Id1
	 end

	 set @Doc2= @Doc1
	 set @Id2 = @Id1
	 if  @lDoc<= @o2 + @o1 set @All=1
	 set @o1 = @o1 + @o2
	 set @o2=0
	 fetch next from Cur1 into @Id1, @Doc1, @lres, @ldoc
	end
	close Cur1
	deallocate Cur1

end


select * from #temp_result_Gppril2 where documentid in (220,221) order by id