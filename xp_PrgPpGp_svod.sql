alter proc [KS_DDLControl].[xp_PrgPpGp_svod_4] (@nmode int=0, @GP int=6574, @PPGP int=7242, @Ver00 varchar(2) = '22', @Ver01 varchar(2) = '21', @OnlyChanges varchar(3) = 'Нет', @ожидаемые_результаты varchar(2)= 'Да')
    as

--exec [KS_DDLControl].[xp_PrgPpGp_svod] @nmode=0, @GP=6566, @PPGP=6773, @Ver00 ='01', @Ver01 = '03', @OnlyChanges = 'Нет'

    create table #temp_resultPPGPChanges
    (
        [idppgp]                              int            null,
        [ид_ппгп]                             int            null,
        [ппгп]                                varchar(max)   null,
        [orderppgp]                           varchar(max)   null,
        [action]                              varchar(max)   null,
        [idcontent]                           int            null,
        [content]                             varchar(max)   null,
        [код_content]                         varchar(max)   null,
        [gpdbegin]                            int            null,
        [gpdend]                              int            null,
        [paramdate]                           int            null,
        [s_gp]                                varchar(max)   null,
        [source]                              varchar(max)   null,
        [код_источника_финансирования]        varchar(max)   null,
        [очг13]                               numeric(20, 4) null,
        [очг14]                               numeric(20, 4) null,
        [очг15]                               numeric(20, 4) null,
        [очг16]                               numeric(20, 4) null,
        [очг17]                               numeric(20, 4) null,
        [очг18]                               numeric(20, 4) null,
        [очг19]                               numeric(20, 4) null,
        [очг20]                               numeric(20, 4) null,
        [очг21]                               numeric(20, 4) null,
        [очг22]                               numeric(20, 4) null,
        [очг23]                               numeric(20, 4) null,
        [очг24]                               numeric(20, 4) null,
        [очг25]                               numeric(20, 4) null,
        [кодппгп]                             varchar(max)   null,
        [порядковыйномерпзппгп]               varchar(max)   null,
        [единицаизмерения]                    varchar(max)   null,
        [показательзадачи]                    varchar(max)   null,
        [методика_расчета_номер_строки]       varchar(max)   null,
        [методика_расчета_метод_расчета]      varchar(max)   null,
        [методика_расчета_формула_описание]   varchar(max)   null,
        [источник_значения]                   varchar(max)   null,

--         [idppgp_1]                            int            null,
        [ид_ппгп_1]                           int            null,
        [ппгп_1]                              varchar(max)   null,
        [idcontent_1]                         int            null,
        [content_1]                           varchar(max)   null,
        [код_content_1]                       varchar(max)   null,
        [gpdbegin_1]                          int            null,
        [gpdend_1]                            int            null,
        [paramdate_1]                         int            null,
        [s_gp_1]                              varchar(max)   null,
        [source_1]                            varchar(max)   null,
        [код_источника_финансирования_1]      varchar(max)   null,
        [очг13_1]                             numeric(20, 4) null,
        [очг14_1]                             numeric(20, 4) null,
        [очг15_1]                             numeric(20, 4) null,
        [очг16_1]                             numeric(20, 4) null,
        [очг17_1]                             numeric(20, 4) null,
        [очг18_1]                             numeric(20, 4) null,
        [очг19_1]                             numeric(20, 4) null,
        [очг20_1]                             numeric(20, 4) null,
        [очг21_1]                             numeric(20, 4) null,
        [очг22_1]                             numeric(20, 4) null,
        [очг23_1]                             numeric(20, 4) null,
        [очг24_1]                             numeric(20, 4) null,
        [очг25_1]                             numeric(20, 4) null,
        [кодппгп_1]                           varchar(max)   null,
        [documentid]                          int            null,
        [post]                                varchar(max)   null,
        [порядковыйномерпзппгп_1]             varchar(max)   null,
        [единицаизмерения_1]                  varchar(max)   null,
        [показательзадачи_1]                  varchar(max)   null,
        [методика_расчета_номер_строки_1]     varchar(max)   null,
        [методика_расчета_метод_расчета_1]    varchar(max)   null,
        [методика_расчета_формула_описание_1] varchar(max)   null,
        [источник_значения_1]                 varchar(max)   null,
        id                                    int identity (1,1)
    )
    if @nmode <> -1
        begin
            set nocount on
            set transaction isolation level read uncommitted


            IF OBJECT_ID('dbo.tmpPPGPver00', 'U') IS NOT NULL
                DROP TABLE dbo.tmpPPGPver00;
            IF OBJECT_ID('dbo.tmpPPGPver01', 'U') IS NOT NULL
                DROP TABLE dbo.tmpPPGPver01;

            declare
                @aDateInt varchar(15)
            select @aDateInt = convert(varchar, dbo.getworkdate(), 112)

            exec [ks_ddlcontrol].[sp_poly_hybrid_query_3486] @nmode=0,
                 @table_name_result='tmpPPGPver00', @cProject=0,
                 @Программа=@GP, @Версия= @Ver00, @Подпрограмма= @PPGP,
                 @Собрать_ожидаемые_результаты= @ожидаемые_результаты,
                 @Дата_версионность= @aDateInt
            delete from dbo.tmpPPGPver00 where idppgp is null

            exec [ks_ddlcontrol].[sp_poly_hybrid_query_3486] @nmode=0, @table_name_result='tmpPPGPver01', @cProject=0,
                 @Программа=@GP, @Версия= @Ver01, @Подпрограмма= @PPGP,
                 @Собрать_ожидаемые_результаты= @ожидаемые_результаты,
                 @Дата_версионность= @aDateInt
            delete from dbo.tmpPPGPver01 where idppgp is null

            --             select * from dbo.tmpPPGPver00
--             select *
--             from dbo.tmpPPGPver01

            --------------------------------
            --Изменение в наименовании ГП --
            --------------------------------
--             declare
--                 @gp_name varchar(2000), @id int
--             declare
--                 @gp_name_1 varchar(2000), @id_1 int
--             select @gp_name = [ГП], @id = idgp from dbo.tmpPPGPver00
--             select @gp_name_1 = [ГП], @id_1 = idgp from  dbo.tmpPPGPver01

            insert into #temp_resultPPGPChanges
            select isnull(a.idppgp, b.idppgp),
--                    a.[idppgp],
                   a.[ид_ппгп],
                   a.[ппгп],
                   isnull(a.orderPPGP, b.orderPPGP),
                   isnull(a.action, b.action),
                   a.[idcontent],
                   a.[content],
                   a.[код_content],
                   a.[gpdbegin],
                   a.[gpdend],
                   a.[paramdate],
                   a.[s_gp],
                   a.[source],
                   a.[код_источника_финансирования],
                   a.[очг13],
                   a.[очг14],
                   a.[очг15],
                   a.[очг16],
                   a.[очг17],
                   a.[очг18],
                   a.[очг19],
                   a.[очг20],
                   a.[очг21],
                   a.[очг22],
                   a.[очг23],
                   a.[очг24],
                   a.[очг25],
                   a.[кодппгп],
                   a.[порядковыйномерпзппгп],
                   a.[единицаизмерения],
                   a.[показательзадачи],
                   a.[методика_расчета_номер_строки],
                   a.[методика_расчета_метод_расчета],
                   a.[методика_расчета_формула_описание],
                   a.[источник_значения],
                   --b.[idppgp],
                   b.[ид_ппгп],
                   b.[ппгп],
                   b.[idcontent],
                   b.[content],
                   b.[код_content],
                   b.[gpdbegin],
                   b.[gpdend],
                   b.[paramdate],
                   b.[s_gp],
                   b.[source],
                   b.[код_источника_финансирования],
                   b.[очг13],
                   b.[очг14],
                   b.[очг15],
                   b.[очг16],
                   b.[очг17],
                   b.[очг18],
                   b.[очг19],
                   b.[очг20],
                   b.[очг21],
                   b.[очг22],
                   b.[очг23],
                   b.[очг24],
                   b.[очг25],
                   b.[кодппгп],
                   isnull(a.documentid, b.documentid),
                   isnull(a.Post, b.Post),
                   b.[порядковыйномерпзппгп],
                   b.[единицаизмерения],
                   b.[показательзадачи],
                   b.[методика_расчета_номер_строки],
                   b.[методика_расчета_метод_расчета],
                   b.[методика_расчета_формула_описание],
                   b.[источник_значения]
            from dbo.tmpPPGPver00 a
                     full join dbo.tmpPPGPver01 b on (isnull(a.[orderppgp], '') = isnull(b.[orderppgp], '') and
                                                      isnull(a.action, '') = isnull(b.action, '') and
                                                      isnull(a.[content], '') = isnull(b.[content], '') and
                                                      isnull(a.[код_content], '') = isnull(b.[код_content], '') and
                                                      isnull(a.[source], '') = isnull(b.[source], ''))

            delete
            from #temp_resultPPGPChanges
            where [код_Content] is null
              and [код_Content_1] is null
              and action in (select distinct action
                             from #temp_resultPPGPChanges
                             where [код_Content] is not null
                                or [код_Content_1] is not null)

            delete
            from #temp_resultPPGPChanges
            where orderPPgp = '09'
              and [ОЧГ13] = 0
              and [ОЧГ14] = 0
              and [ОЧГ15] = 0
              and [ОЧГ16] = 0
              and [ОЧГ17] = 0
              and [ОЧГ13_1] is null

            IF OBJECT_ID('dbo.tmpPPGPver00', 'U') IS NOT NULL
                DROP TABLE dbo.tmpPPGPver00;
            IF OBJECT_ID('dbo.tmpPPGPver01', 'U') IS NOT NULL
                DROP TABLE dbo.tmpPPGPver01;

        end
    if @OnlyChanges = 'Да'
        begin
            select *
            from #temp_resultPPGPChanges
            where isnull([Код_Content], '') != isnull([Код_Content_1], '')
               or isnull([Content], '') != isnull([Content_1], '')
               or isnull([ОЧГ13], 0) != isnull([ОЧГ13_1], 0)
               or isnull([ОЧГ14], 0) != isnull([ОЧГ14_1], 0)
               or isnull([ОЧГ15], 0) != isnull([ОЧГ15_1], 0)
               or isnull([ОЧГ16], 0) != isnull([ОЧГ16_1], 0)
               or isnull([ОЧГ17], 0) != isnull([ОЧГ17_1], 0)
               or isnull([ОЧГ18], 0) != isnull([ОЧГ18_1], 0)
               or isnull([ОЧГ19], 0) != isnull([ОЧГ19_1], 0)
               or isnull([ОЧГ20], 0) != isnull([ОЧГ20_1], 0)
               or isnull([ОЧГ21], 0) != isnull([ОЧГ21_1], 0)
               or isnull([ОЧГ22], 0) != isnull([ОЧГ22_1], 0)
               or isnull([ОЧГ23], 0) != isnull([ОЧГ23_1], 0)
               or isnull([ОЧГ24], 0) != isnull([ОЧГ24_1], 0)
               or isnull([ОЧГ25], 0) != isnull([ОЧГ25_1], 0)
               or isnull([ПорядковыйНомерПЗППГП], 0) != isnull([ПорядковыйНомерПЗППГП_1], 0)


        end
    else
        begin
            select * from #temp_resultPPGPChanges
        end