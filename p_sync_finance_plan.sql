--������������� �������������� �� ����� �����������. ���� ����������
alter procedure dbo.p_sync_finance_plan
   @uRid uniqueidentifier = null -- ��� ������ 
as
begin
	set nocount on
--�������� �� ���������� ��� ������� ���� ��������� ����������
	declare @docid int
	declare @status int --������ ���������

	declare @user_type int
	declare @permission int
	
	select [int] as docid into #docid from dbo.sys_values where rid=@uRid and code  = 'ElementId'

	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()
	
	select @permission = AllowPermission from dbo.ENTITY where ID = 864
	while exists (select * from #docid)
	begin
		--���� �� ������� ���������
		select top 1 @docid = docid from #docid
		delete from #docid where docid=@docid
		--�������� ������ ���������
		select top 1 @status = status.at_2661 from ks_ddlcontrol.ds_1279_3189 doc
			left join ks_ddlcontrol.cl_1008_2633 status on (doc.cl_3164 = status.id)
		where doc.id = @docid
		
		
		
		--���� ���������� ����������� ������ �� ��������� �� ��������� ��� ���������
		IF @status = 1
            CONTINUE;
            
		--�������� ��� ������������, ���� ��������� � ��� ����������� ������� ��������� ������
		select top 1
			gp.at_2524 as gp_code, --��� ������������ at_3190
			doc.at_3190 as date_doc,--���� ���������
			events.at_2524 as event_code, --��� �����������*/
			doc.cl_3234 as [ID_���������]
		into #event
		from ks_ddlcontrol.ds_1279_3189 doc
			left join ks_ddlcontrol.ds_1279_3270 struct_gp on (doc.id = struct_gp.id_up)
			left join ks_ddlcontrol.cl_956_2470 gp on (struct_gp.cl_3236 = gp.id)
			left join ks_ddlcontrol.cl_956_2470 events on (doc.cl_3233 = events.id) --���������� �_������������
	
			where doc.id = @docid
		
		--������� ������ �� ������� ���������� ������ (�� ����������� ������)
		if @user_type != 4
		begin
			delete from #event where [ID_���������] not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
		end 
		
		
		--������� ��������� ������ �� ����� ����������
		delete plan_r from ks_ddlcontrol.ds_1279_3196 plan_r 
			inner join #event event on (plan_r.id_up = event.id)
		where plan_r.id_up = @docid
		
		--�������� ������ � ���� ����������
		insert into ks_ddlcontrol.ds_1279_3196
			(
			id_up, --id ��
			at_3576, -- ������ �� ���
			cl_3176, -- �����_��������� ��������������			
			cl_3178, -- �����_�����
			cl_3177, -- �����_��������������
			cl_3170, -- �����_���������			
			cl_3171, -- �����_����������			
			cl_3172, -- �����_������� ������
			cl_3173, -- �����_���� ��������			
			cl_3175, -- �����_��������
			cl_3174, -- �����_�����
			m_3182 -- 2019 ��� (������)		
			)
		select 
			@docid, --id ��
			'1', -- ������ �� ���
			decryption.cl_3383, -- �����_��������� ��������������
			decryption.cl_3384, -- �����_�����
			decryption.cl_3385, -- �����_��������������
			decryption.cl_3370, -- �����_���������
			decryption.cl_3371, -- �����_����������
			decryption.cl_3372, -- �����_������� ������
			decryption.cl_3373, -- �����_���� ��������
			decryption.cl_3375, -- �����_��������
			decryption.cl_3374, -- �����_�����
			decryption.at_3378 -- ����� ��������� ������������				
		from ks_ddlcontrol.ds_1323_3421 decryption
			left join  ks_ddlcontrol.ds_1323_3416 doc on (doc.id = decryption.id_up) --��������
			left join ks_ddlcontrol.cl_956_2470 gp on (decryption.cl_3465 = gp.id) -- ���������� �_������������ (�����������)
			left join ks_ddlcontrol.cl_1332_3443 code_gp on (doc.cl_3394 = code_gp.id) -- ���������� ��� ������������
			inner join #event event on (gp.at_2524 = event.event_code and doc.at_3380 = event.date_doc)
		drop table #event
		delete from #docid where docid=@docid
	end		
end