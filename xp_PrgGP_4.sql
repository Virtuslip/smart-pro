alter proc [KS_DDLControl].[xp_PrgGP_4](@nmode int=0, @GP int=6559, @Ver varchar(2) = '02',
    @Result varchar(max) = 'Да', @table_name_result varchar(500)='',
    @ToTable int =0)
    as
    create table #temp_resultPrgGp
    (
        idgp                                int,
        [ГП]                                varchar(2000),
        orderGP                             varchar(2),
        action                              varchar(5000),
        idppgp                              int,
        [ППГП]                              varchar(max),
        [ППГП_Код]                          varchar(50),
        GpDBegin                            int,
        GpDEnd                              int,
        ParamDate                           int
        --, [S_gp] varchar(5000)
        ,
        [Source]                            varchar(1000),
        [Код источника финансирования]      varchar(100),
        [ОЧГ13]                             numeric(20, 4),
        [ОЧГ14]                             numeric(20, 4),
        [ОЧГ15]                             numeric(20, 4),
        [ОЧГ16]                             numeric(20, 4),
        [ОЧГ17]                             numeric(20, 4),
        [ОЧГ18]                             numeric(20, 4),
        [ОЧГ19]                             numeric(20, 4),
        [ОЧГ20]                             numeric(20, 4),
        [ОЧГ21]                             numeric(20, 4),
        [ОЧГ22]                             numeric(20, 4),
        [ОЧГ23]                             numeric(20, 4),
        [ОЧГ24]                             numeric(20, 4),
        [ОЧГ25]                             numeric(20, 4),
        documentid                          int,
        Post                                varchar(max),
        id                                  int identity (1,1)
--			, AT_3255 varchar(5000)
--		    , AT_3254 datetime
--		    , AT_3272 datetime
--		    , AT_3273 varchar(5000)
        ,
        [ЕдИзмерения]                       varchar(500),
        [ПорядковыйНомерПЗППГП]             varchar(10),
        [наименование_показателя]           varchar(100),
        [Методика_расчета]                  varchar(5000),
        [Методика_расчета_формула_описание] varchar(max),
        [Источник_значения]                 varchar(8000)

    )
    if @nmode <> -1
        begin
            set nocount on
            set transaction isolation level read uncommitted
-- Пример запуска:  exec [KS_DDLControl].[xp_PrgGP_2] @nmode=0, @GP=6561, @Ver='11',@Result = 'Да'

            declare
                @GpDocID int
            declare
                @aDateInt int
--select  @aDateInt = convert(varchar,dbo.getworkdate(),112)
            select @aDateInt = convert(varchar, GetDate(), 112)


            create table #prg
            (
                gp     varchar(30),
                action varchar(500),
                id     int identity (1,1)
            )
            insert into #prg select 'Госпрограммы', 'Ответственный исполнитель'
            insert into #prg select 'Госпрограммы', 'Соисполнители'
            insert into #prg select 'Госпрограммы', 'Соисполнители '
            insert into #prg select 'Госпрограммы', 'Сроки и этапы реализации государственной программы'
            insert into #prg select 'Госпрограммы', 'Подпрограммы'
            insert into #prg select 'Госпрограммы', 'Цель государственной программы'
            insert into #prg select 'Госпрограммы', 'Индикаторы цели'
            insert into #prg select 'Госпрограммы', 'Задачи государственной программы'
            insert into #prg select 'Госпрограммы', 'Показатели задач'
            insert into #prg
            select 'Госпрограммы',
                   'Параметры финансового обеспечения всего, в том числе по годам реализации государственной программы'
            insert into #prg
            select 'Госпрограммы',
                   'Параметры финансового обеспечения всего, в том числе по годам реализации государственной программы'
            --insert into  #prg select 'Госпрограммы', 'Ожидаемые результаты реализации государственной программы'
            insert into #prg select 'Госпрограммы', 'Ожидаемые результаты реализации государственной программы'

            ------------
            --Доступы --
            ------------
            Declare
                @user_type int
            select @user_type = user_type -- если user_type = 4, то это администратор, для него доступы проверять не надо
            from s_users
            where loginame = suser_sname()

            Declare
                @GmpPermission int
            select @GmpPermission = AllowPermission --Признак назначать права
            from dbo.ENTITY
            where ID = 956 --Справочники ГМП

            Declare
                @VedPermission int
            select @VedPermission = AllowPermission --Признак назначать права
            from dbo.ENTITY
            where ID = 864 --Справочник Ведомства 864

            Declare
                @SourcePermission int
            select @SourcePermission = AllowPermission --Признак назначать права
            from dbo.ENTITY
            where ID = 894
            --Справочник Источники финансирования

            ---------------------------------------------------------
            --Выборка актуальных значений справочника Госпрограммы --
            ---------------------------------------------------------
            declare
                @Cl table
                    (
                        id             int,
                        [Наименование] varchar(2000),
                        [DBegin]       int,
                        [Код]          varchar(50),
                        Post           varchar(max)
                    )
            insert into @Cl
            select a.id
                 --, isnull(AT_2526,'') as [ГП]
                 , AT_2526   as [ГП]
                 , b.at_3398 as [DBegin]
                 , a.AT_2524
                 , a.AT_2924
            from

                --KS_DDLControl.CL_956_2470 a
                --left join (select * from ks_ddlcontrol.cl_956_3452 where Isnull(at_3398, 20160101) <= @aDateInt) b on a.id = b.id_up
                -- left join KS_DDLControl.DS_957_2475 e on e.CL_2640 = a.ID
                --left  join ks_ddlcontrol.cl_956_3452 b on a.id = b.id_up

                --where at_3398 <= e.AT_2529

                KS_DDLControl.CL_956_2470 a
                    left join KS_DDLControl.DS_957_2475 e on a.id = e.cl_2640
                    left join (select * from ks_ddlcontrol.cl_956_3452 where at_3398 <= @aDateInt) b on a.id = b.id_up
            --left join ks_ddlcontrol.cl_956_3452 b on a.id=b.id_up
            --left join  KS_DDLControl.DS_957_2475 e on a.id=e.cl_2640

            --where b.at_3398 = e.AT_2529


            select tab2.id, tab2.[Наименование] as [ГП], tab2.[Код], tab2.Post
            into #sprGp
            from (select id, max(DBegin) as DBegin from @Cl group by id) tab1
                     inner join
                     (select id, DBegin, [Наименование], [Код], Post from @Cl) tab2
                     on tab1.id = tab2.id and isnull(tab1.DBegin, 0) = isnull(tab2.DBegin, 0)

            --Удаление элементов на которые отсутствует доступ
            if isnull(@GmpPermission, 0) > 0 and @user_type != 4
                begin
                    delete
                    from #sprGp
                    where id not in (select CLID
                                     from KS_DDLControl.CLA_956 CLA
                                              inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups]
                                     where GRP.[users] = user_id(suser_sname()))
                end
            -- select * from #sprGp
            -------------------------------------------
            --Выборка значений справочника Ведомства --
            -------------------------------------------
            select a.*,
                   a1.AT_1797,
                   a1.AT_1796,
                   a1.AT_1930
--	(select * from KS_DDLControl.CL_864_3145 a1  where a1.ID_UP=a.ID)
            into #sprVed
            from KS_DDLControl.CL_864_1710 a
                     left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
            /*--Удаление элементов на которые отсутствует доступ
            if isnull(@VedPermission,0) > 0 and @user_type != 4
            begin
                delete from #sprVed where id not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
            end
        */

-------------------------------------------
            --Выборка значений справочника ИсполнителиГМП --
            -------------------------------------------
            select a.*, a1.AT_3200, a1.AT_3201, a1.AT_3202
            into #sprVed1
            from KS_DDLControl.CL_1280_3226 a
                     left join KS_DDLControl.CL_1280_3231 a1 on a.id = a1.id_up

            --select * from    #sprVed1
            ----------------------------------------------------------
            --Выборка значений справочника Источники финансирования --
            ----------------------------------------------------------
            select * into #sprSource from KS_DDLControl.CL_894_1936
            --Удаление элементов на которые отсутствует доступ
            if isnull(@SourcePermission, 0) > 0 and @user_type != 4
                begin
                    delete
                    from #sprSource
                    where id not in (select CLID
                                     from KS_DDLControl.CLA_894 CLA
                                              inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups]
                                     where GRP.[users] = user_id(suser_sname()))
                end

            -------------------------------------------------
            -- Выборка актуальных документов источника ГМП --2646 - ИдНовойВерсии
            -------------------------------------------------
            create table #tab_sprGpOnDoc
            (
                [ИД_Документа] int,
                [Код]          varchar(50),
                [Наименование] varchar(2000),
                [Тип1]         varchar(10),
                [idГП]         int
            )

            --insert into #tab_sprGpOnDoc exec [ks_ddlcontrol].[xp_sprGpOnDoc] @nmode=0, @table_name_result='', @ToTable = 1
            exec [ks_ddlcontrol].[xp_sprGpOnDoc] @nmode=0, @table_name_result='', @ToTable = 1

            select a.id
                 --, a.AT_2529
                 , a.CL_2533
                 , a.CL_3252
                 , a.documentid
                 , a.AT_2538
                 , a.AT_2539
                 , a.CL_2640
                 , a.CL_3253 -- статус
--		, a.AT_3255
--		, a.AT_3254
--		, a.AT_3272
--		, a.AT_3273
                 , b.DS_2654 --Вышестоящий документ
                 , AT_2656                     as [SCode]
                 , AT_2658                     as [SNaim]
                 , AT_2661                     as [SLock]
                 , AT_1998                     as [Source]
                 , a.at_2837                   as [Код]
                 , at_1997                     as [Код источника финансирования]
                 , sum(isnull(e.M_2928, 0.00)) as [ОЧГ13]
                 , sum(isnull(e.M_2929, 0.00)) as [ОЧГ14]
                 , sum(isnull(e.M_2930, 0.00)) as [ОЧГ15]
                 , sum(isnull(e.M_2931, 0.00)) as [ОЧГ16]
                 , sum(isnull(e.M_2932, 0.00)) as [ОЧГ17]
                 , sum(isnull(e.M_2937, 0.00)) as [ОЧГ18]
                 , sum(isnull(e.M_2938, 0.00)) as [ОЧГ19]
                 , sum(isnull(e.M_2939, 0.00)) as [ОЧГ20]
                 , sum(isnull(e.M_3073, 0.00)) as [ОЧГ21]
                 , sum(isnull(e.M_3344, 0.00)) as [ОЧГ22]
                 , sum(isnull(e.M_3345, 0.00)) as [ОЧГ23]
                 , sum(isnull(e.M_3346, 0.00)) as [ОЧГ24]
                 , sum(isnull(e.M_3347, 0.00)) as [ОЧГ25]

                 , M_2879                      as [Zakon]
                 , e.cl_2556
                 , q.AT_2544
                 --,a.AT_3349
                 , p.at_3546
                 , a.at_3275
            into #GMP_old
            from [ks_ddlControl].[GetDocGP](@GP, 0, @Ver) o
                     inner join KS_DDLControl.DS_957_2475 a on o.id = a.id
                     left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
                     Left Join KS_DDLControl.CL_1008_2633 c on a.CL_3253 = c.id --Статус
                     Left Join KS_DDLControl.DS_957_2506 e on e.id_up = a.id
                --			left join KS_DDLControl.DS_1279_3189 e1  on e1.DS_3169 = a.id
--			left join KS_DDLControl.DS_1279_3196 e  on e.id_up = e1.id      --Оценка расходов  KS_DDLControl.DS_957_2506
                     Left Join #sprSource S on e.CL_2572 = S.id --Макет_Источники финансирования
                     left join KS_DDLControl.CL_960_2491 q on q.id = a.CL_2633
                     left join ks_ddlcontrol.ds_957_3639 p on p.id_up = a.id


            group by a.id
                   --, a.AT_2529
                   , a.CL_2533
                   , a.CL_3252 -- для ИсполнителиГМП
                   , a.documentid
                   , a.AT_2538
                   , a.AT_2539
                   , a.CL_2640
                   , a.CL_3253
                   , a.AT_3272
                   , a.AT_3273
                   , b.DS_2654
                   , AT_2656
                   , AT_2658
                   , AT_2661
                   , AT_1998
                   , a.at_2837
                   , M_2879
                   , e.cl_2556
                   , q.AT_2544
                   --,a.AT_3349
                   , p.at_3546
                   , at_1997
                   , a.at_3275
--select * from #GMP_old

            select a.[id]
                 --, a.[AT_2529]
                 , a.[CL_2533]
                 , a.[CL_3252]
                 , a.[cl_2556]
                 , a.[documentid]
                 , a.[AT_2538]
                 , a.[AT_2539]
                 , a.[CL_2640]
                 , a.[CL_3253]
--		, a.[AT_3255]
--		, a.[AT_3254]
--		, a.[AT_3272]
--		, a.[AT_3273]
                 , a.[DS_2654]
                 , a.[SCode]
                 , a.[SNaim]
                 , a.[SLock]
                 , a.[Source]
                 , a.[Код источника финансирования]
                 , a.[Код]
                 , a.[ОЧГ13]
                 , a.[ОЧГ14]
                 , a.[ОЧГ15]
                 , a.[ОЧГ16]
                 , a.[ОЧГ17]
                 , a.[ОЧГ18]
                 , a.[ОЧГ19]
                 , a.[ОЧГ20]
                 , a.[ОЧГ21]
                 , a.[ОЧГ22]
                 , a.[ОЧГ23]
                 , a.[ОЧГ24]
                 , a.[ОЧГ25]
                 , a.[Zakon]
                 , b.[Наименование] as [ГП]
                 , b.[idГП]
                 , a.AT_2544
                 --, a.AT_3349
                 , at_3546
                 , a.at_3275

            into #GMP
            from #GMP_old a
                     inner join #tab_sprGpOnDoc b on a.id = b.[ИД_Документа]

            --select * from #GMP
---------------
            -- Программы --
            ---------------

            select gp
                 , tab.id  as idgp
                 , [ГП]
                 , action
                 , #prg.id as orderGP
                 , tab.DocumentId
                 --,[DateInt]
                 , [DBegin]
                 , [DEnd]
                 , [SCode]
                 , [SNaim]
                 , [SLock]
                 , [Исполнитель.Наименование]
                 , [Код]
                 , [Zakon]
                 , [AT_2656]
--		, [AT_3255]
--		, [AT_3254]
--		, [AT_3272]
--		, [AT_3273]
                 , [Этапы]

            into [#Государственная программа]
            from #prg
                     left join
                 (select a.id
                       --, [ГП] + ' (от '+ right(convert(varchar,AT_2529),2) + '.' + substring(convert(varchar,AT_2529),5,2)  + '.' + left(convert(varchar,AT_2529),4) + (case when a.[SNaim] is null then '' else ' - ' + [SNaim] end) +')' as [ГП]
                       , [ГП]
                       , a.DocumentId
                       --, AT_2529 as [DateInt]
                       , a.AT_2538        as DBegin
                       , a.AT_2539        as DEnd
                       , [SCode]
                       , [SNaim]
                       , [SLock]
                       --, #sprVed.AT_1797 as [Исполнитель.Наименование] -- для справочника ведомства
                       , #sprVed1.AT_3200 as [Исполнитель.Наименование] -- для справочника М_Исполнители_ГМП
                       , a.[Код]
                       , [Zakon]
                       , c.[AT_2656]
--		, a.[AT_3255]
--		, a.[AT_3254]
--		, a.[AT_3272]
--		, a.[AT_3273]
                       -- ,a.AT_3349 as [Этапы]
                       , a.at_3546        as [Этапы]
                  from #GMP a
                           --left join #sprVed on #sprVed.ID = a.CL_2533 - для справочника ведомства
                           left join #sprVed1 on #sprVed1.id = a.CL_3252 -- для справочника М_Исполнители_ГМП
                           left join KS_DDLControl.CL_1008_2633 c on c.id = a.CL_3253
                  where DocumentId = 220
                    and a.idГП = @Gp
                     --and a.id = @Gp                 --Госпрограмма
                 ) tab on 1 = 1
--select * from [#Государственная программа]
            if @GP > 0
                begin

                    ------------------
                    -- Подпрограммы --
                    ------------------
                    select a.id                                              as idppgp
                         , 'Подпрограмма ' + (case when LEFT(a.Код, 1) = '0' then '' ELSE LEFT(a.Код, 1) END) +
                           SUBSTRING(a.Код, 2, 4) + ' ' + '«' + a.[ГП] + '»' as [ППГП]
                         , DS_2654                                           as [Вышестоящий документ]
                         , a.DocumentId
                         , a.AT_2538                                         as DBegin
                         , a.AT_2539                                         as DEnd
                         , a.[SCode]
                         , a.[SNaim]
                         , a.[SLock]
                         --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                         , #sprVed1.AT_3200                                  as [Исполнитель.Наименование]
                         , a.[Код]
                    into [#Подрограмма]
                    from #GMP a
                             inner join [#Государственная программа] b
                                        on a.DS_2654 = b.idgp and b.action = 'Подпрограммы' --Только Подпрограммы привязанные к ГП = @GP
                        --left join #sprVed on #sprVed.ID = a.CL_2533
                             left join #sprVed1 on #sprVed1.id = a.CL_3252
                    where a.DocumentId = 221
                      and a.[ГП] is not null

                    ---------
                    --Цель --
                    ---------
                    select a.id
                         , a.DocumentID
                         , a.[ГП]           as [Цели]
                         --,(case when LEFT(a.Код,1)='0' then '' ELSE LEFT(a.Код,1) END)+SUBSTRING(a.Код,2,4)+'. '+a.[ГП] as [Цели]
                         , DS_2654          as [Вышестоящий документ]
                         , a.AT_2538        as DBegin
                         , a.AT_2539        as DEnd
                         , a.[SCode]
                         , a.[SNaim]
                         , a.[SLock]
                         --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                         , #sprVed1.AT_3200 as [Исполнитель.Наименование]
                         , a.[Код]
                    into [#Цели]
                    from #GMP a
                             inner join [#Государственная программа] a1
                                        on a1.action = 'Цель государственной программы' and a1.idgp = a.DS_2654
                        --left join #sprVed on #sprVed.ID = a.CL_2533
                             left join #sprVed1 on #sprVed1.id = a.CL_3252
                    where a.DocumentId = 222

                    --SELECT * FROM [#Цели]
                    -----------
                    --Задачи --
                    -----------
                    select a.id
                         , a.DocumentID
                         , (case when LEFT(a.Код, 1) = '0' then '' ELSE LEFT(a.Код, 1) END) + SUBSTRING(a.Код, 2, 4) +
                           '. ' + a.[ГП]    as [Задачи]
                         , DS_2654          as [Вышестоящий документ]
                         , a.AT_2538        as DBegin
                         , a.AT_2539        as DEnd
                         , a.[SCode]
                         , a.[SNaim]
                         , a.[SLock]
                         --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                         , #sprVed1.AT_3200 as [Исполнитель.Наименование]
                         , a.[Код]
                    into [#Задачи]
                    from #GMP a
                             inner join [#Государственная программа] a1
                                        on a1.action = 'Задачи государственной программы' and a1.idgp = a.DS_2654
                        --left join #sprVed on #sprVed.ID = a.CL_2533
                             left join #sprVed1 on #sprVed1.id = a.CL_3252
                    where a.DocumentId = 229
                    order by a.[Код]

                    --Выбираем оргиниальное название задачи если она только 1
                    if (select count(*) from #Задачи) = 1
                        update z
                        set z.[Задачи] = a.[ГП]
                        from [#Задачи] as z
                                 left join #GMP a on (z.id = a.id)


                    ---------------------------
                    -- Показатели задач --
                    ---------------------------

                    select a.id
                         , a.DocumentID
                         --, a.[ГП]+','+' '+a.AT_2544+',' as [Мероприятия]
                         , a.[ГП] + ', ' + isnull(a.AT_2544, '') as [Мероприятия]
                         --, a.[ГП] as [Мероприятия]
                         , a1.[Вышестоящий документ]             as [Вышестоящий документ]
                         , a1.Код                                as [Код задачи]
                         , 'Показатели задач'                    as action
                         , 1                                     as orderPPgp
                         , a.AT_2538                             as DBegin
                         , a.AT_2539                             as DEnd
                         , a.[SCode]
                         , a.[SNaim]
                         , a.[SLock]
                         --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                         , #sprVed1.AT_3200                      as [Исполнитель.Наименование]
                         , a.[Source]
                         , a.[ОЧГ13]
                         , a.[ОЧГ14]
                         , a.[ОЧГ15]
                         , a.[ОЧГ16]
                         , a.[ОЧГ17]
                         , a.[ОЧГ18]
                         , a.[ОЧГ19]
                         , a.[ОЧГ20]
                         , a.[ОЧГ21]
                         , a.[ОЧГ22]
                         , a.[ОЧГ23]
                         , a.[ОЧГ24]
                         , a.[ОЧГ25]
                         , a.[Код]
                         , a.AT_2544

                    into [#Показателизадач]
                    from #GMP a
                             inner join [#Задачи] a1 on a1.id = a.DS_2654
                        --left join #sprVed on #sprVed.ID = a.CL_2533 --Исполнитель
                             left join #sprVed1 on #sprVed1.id = a.CL_3252

                    where a.DocumentId = 252
                    order by a1.Код, a.[Код] asc

                    --select * from [#Задачи]
--select * from [#Показателизадач]


                    ---------------
                    --Индикаторы --
                    ---------------

                    select a.id
                         , a.DocumentID
                         , (case when LEFT(a.Код, 1) = '0' then '' ELSE LEFT(a.Код, 1) END) + SUBSTRING(a.Код, 2, 4) +
                           '. ' + a.[ГП]             as [Индикаторы]
                         --, a.[ГП] as [Индикаторы]
                         , a1.[Вышестоящий документ] as [Вышестоящий документ]
                         , a.AT_2538                 as DBegin
                         , a.AT_2539                 as DEnd
                         , a.[SCode]
                         , a.[SNaim]
                         , a.[SLock]
                         --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                         , #sprVed1.AT_3200          as [Исполнитель.Наименование]
                         , a.[Код]
                         , a.AT_2544
                         , a.at_3275
                         , a1.Код                    as [код_цели]

                    into [#Индикаторы]
                    from #GMP a
                             inner join [#Цели] a1 on a1.id = a.DS_2654
                        --left join #sprVed on #sprVed.ID = a.CL_2533
                             left join #sprVed1 on #sprVed1.id = a.CL_3252
                    where a.DocumentId = 252

                    order by a.[Код]
                    --Выбираем оргиниальное название индикатора если он только 1
                    if (select count(*) from [#Индикаторы]) = 1
                        update i
                        set i.[Индикаторы] = a.[ГП]
                        from [#Индикаторы] as i
                                 left join #GMP a on (i.id = a.id)

                    --select * from [#Индикаторы]
                    --------------------------------------------------------
                    --Объем средств краевого бюджета на финансирование ГП --
                    --------------------------------------------------------
                    create table #Volume
                    (
                        [Source]                       varchar(100),
                        [Код источника финансирования] varchar(10),
                        [ОЧГ13]                        numeric(20, 2),
                        [ОЧГ14]                        numeric(20, 2),
                        [ОЧГ15]                        numeric(20, 2),
                        [ОЧГ16]                        numeric(20, 2),
                        [ОЧГ17]                        numeric(20, 2),
                        [ОЧГ18]                        numeric(20, 2),
                        [ОЧГ19]                        numeric(20, 2),
                        [ОЧГ20]                        numeric(20, 2),
                        [ОЧГ21]                        numeric(20, 2),
                        [ОЧГ22]                        numeric(20, 2),
                        [ОЧГ23]                        numeric(20, 2),
                        [ОЧГ24]                        numeric(20, 2),
                        [ОЧГ25]                        numeric(20, 2)
                    )

                    insert into #Volume
                    select [Source]
                         , [Код источника финансирования]
                         , sum([ОЧГ13])
                         , sum([ОЧГ14])
                         , sum([ОЧГ15])
                         , sum([ОЧГ16])
                         , sum([ОЧГ17])
                         --, sum(round(isnull([ОЧГ18],0.0),2))
                         , sum([ОЧГ18])
                         , sum([ОЧГ19])
                         , sum([ОЧГ20])
                         , sum([ОЧГ21])
                         , sum([ОЧГ22])
                         , sum([ОЧГ23])
                         , sum([ОЧГ24])
                         , sum([ОЧГ25])
                    from #GMP
                    where [Source] is not null
                    group by [Source], [Код источника финансирования]

                    delete from #Volume where [Source] is null
                    update #Volume set Source = 'иные' where Source = 'внебюджетные иные' --not in ('краевые','федеральные')
                    update #Volume set Source = ' ' + Source where Source = 'областные'
                    --select * from #GMP

--------------------------------------------------------
                    --Объем средств  бюджета на финансирование ГП Всего --
                    --------------------------------------------------------
                    create table #Volume2
                    (
                        --  [a] varchar(100)
                        [ОЧГ13] numeric(20, 2),
                        [ОЧГ14] numeric(20, 2),
                        [ОЧГ15] numeric(20, 2),
                        [ОЧГ16] numeric(20, 2),
                        [ОЧГ17] numeric(20, 2),
                        [ОЧГ18] numeric(20, 2),
                        [ОЧГ19] numeric(20, 2),
                        [ОЧГ20] numeric(20, 2),
                        [ОЧГ21] numeric(20, 2),
                        [ОЧГ22] numeric(20, 2),
                        [ОЧГ23] numeric(20, 2),
                        [ОЧГ24] numeric(20, 2),
                        [ОЧГ25] numeric(20, 2)
                    )

                    insert into #Volume2
                    select
                         --[a]
                        sum([ОЧГ13])
                         , sum([ОЧГ14])
                         , sum([ОЧГ15])
                         , sum([ОЧГ16])
                         , sum([ОЧГ17])
                         , sum([ОЧГ18])
                         , sum([ОЧГ19])
                         , sum([ОЧГ20])
                         , sum([ОЧГ21])
                         , sum([ОЧГ22])
                         , sum([ОЧГ23])
                         , sum([ОЧГ24])
                         , sum([ОЧГ25])
                    from #GMP
                    --where [Source] is not null group by [Source]

                    --delete from #Volume where [Source] is null
                    --update #Volume set Source = 'иные' where Source ='внебюджетные иные' --not in ('краевые','федеральные')
                    --update #Volume set Source = ' ' + Source where Source ='областные'
--select * from #Volume2

/*
		-------------------
		-- Соисполнители --
		-------------------
		select distinct
			 a.[CL_2533] as Код
			 , #sprVed.AT_1797 as [Исполнитель.Наименование]
		into [#Soisp]
		from
			[#GMP] a
			left join #sprVed on #sprVed.ID = a.CL_2533

*/

                    --select * from 	#GMP
                    -------------------
                    -- Соисполнители --
                    -------------------
                    select distinct
                                  --#sprVed.AT_1797 as [Исполнитель.Наименование]
                        a.CL_3252                       as Код
                                  , #sprVed1.AT_3200    as [Исполнитель.Наименование]
                                  , identity(int, 1, 1) as idcontent
                    into [#Soisp]
                    from [#GMP] a

                             left join #sprVed1 on #sprVed1.id = a.CL_3252 -- м_соисполниели

                    order by AT_3200
                    -------------------
                    -- Соисполнители ГРИД в ОМ-
                    -------------------
                    select distinct
                                  --#sprVed.AT_1797 as [Исполнитель.Наименование]
                        e.AT_3200                       as [Исполнитель.Наименование]
                                  --,c.CL_3205


                                  , identity(int, 1, 1) as idcontent
                    into [#Soisp2]
                    from [#GMP] a

                             left join KS_DDLControl.DS_957_3235 c on a.id = c.id_up
                             left join #sprVed1 e on e.id = c.CL_3205

                    order by AT_3200


                    --select * from [#Soisp]
--select * from [#Soisp2]
                    --delete from #Soisp where [Исполнитель.Наименование] in (select distinct [Исполнитель.Наименование]  from ---[#Подрограмма])

                    delete
                    from #Soisp
                    where [Исполнитель.Наименование] in
                          (select distinct [Исполнитель.Наименование] from [#Государственная программа])
                    delete from #Soisp where rtrim(ltrim(isnull([Исполнитель.Наименование], ''))) = ''


                    delete
                    from #Soisp2
                    where [Исполнитель.Наименование] in
                          (select distinct [Исполнитель.Наименование] from [#Государственная программа])
                    delete from #Soisp2 where rtrim(ltrim(isnull([Исполнитель.Наименование], ''))) = ''

                    declare
                        @Post varchar(max)
                    select @Post = Post from [#sprgp] where id = @GP
                    --select * from [#Государственная программа]

--select * from #temp_resultPrgGp
                    ---------------------------------------------
                    --Передача данных в результирующую таблицу --
                    ---------------------------------------------

                    insert into #temp_resultPrgGp
                    select a.idgp                                                                                 --Гр1.Уровень1
                         , a.[ГП]                                                                                 --Гр1.Уровень1.Заголовок
                         , right('0' + convert(varchar, orderGP), 2)                                              --Гр2.Уровень2.Наименование
                         , a.action                                                                               --Гр2.Уровень2.Наименование.Заголовок
                         , coalesce(b.idppgp, c1.id, t1.id, i1.id, m1.id)                               as idppgp --Гр3.Уровень2.Содержание - Все что привязно к ГП
                         , (case
                                when a.action = 'Ответственный исполнитель' then a.[Исполнитель.Наименование]
                                when a.action = 'Соисполнители' then s.[Исполнитель.Наименование]
                                when a.action = 'Соисполнители ' then z.[Исполнитель.Наименование]

                                when a.action = 'Сроки и этапы реализации государственной программы' and [Этапы] <> ''
                                    then left(convert(varchar, a.DBegin), 4) + '-' + left(convert(varchar, a.DEnd), 4) +
                                         ' годы' + '
				' + [Этапы]

                                when a.action = 'Сроки и этапы реализации государственной программы' then
                                        left(convert(varchar, a.DBegin), 4) + '-' + left(convert(varchar, a.DEnd), 4) +
                                        ' годы'

                                when a.action = 'Ожидаемые результаты реализации государственной программы'
                                    then q.[AT_2532]
                                else coalesce(b.[ППГП], c1.[Цели], t1.[Задачи], i1.[Индикаторы], m1.[Мероприятия])
                        end)                                                                            as [ППГП] --Гр3.Уровень2.Содержание.Заголовок
                         --,coalesce (s.[Код], c1.[Код], t1.[Код],  convert (float, i1.[Код]), m1.[Код задачи]) as [ППГП_Код] 	--Гр3.Уровень2.Содержание.Заголовок
                         --,coalesce ( c1.[Код], t1.[Код],  convert (float, i1.[Код]), m1.[Код задачи], i1.[код_цели]) as [ППГП_Код] 	--Гр3.Уровень2.Содержание.Заголовок
                         , coalesce(c1.[Код], t1.[Код], convert(float, i1.[код_цели]), m1.[Код задачи]) as [ППГП_Код]
                         --, convert(varchar,i1.[Код])

                         --,coalesce (s.[Код], (convert (float,b.[Код]))) as [ППГП_Код]

                         , isnull(a.DBegin, 0)
                         --, isnull(a.DEnd  , 21000101)
                         , (case
                                when a.action = 'Показатели задач' then isnull(m1.DEnd, 20201231)
                                when a.action = 'Индикаторы цели' then isnull(i1.DEnd, 20201231)
                                else isnull(a.DEnd, 20201231)
                        end)


                         , @aDateInt                                                                    as ParamDate
                         --, a.[SNaim]
                         , v.[Source]
                         , v.[Код источника финансирования]
                         --, isnull(v.[Source], v2.[Source])
                         , isnull(v.[ОЧГ13], v2.[ОЧГ13])
                         , isnull(v.[ОЧГ14], v2.[ОЧГ14])
                         , isnull(v.[ОЧГ15], v2.[ОЧГ15])
                         , isnull(v.[ОЧГ16], v2.[ОЧГ16])
                         , isnull(v.[ОЧГ17], v2.[ОЧГ17])
                         , isnull(v.[ОЧГ18], v2.[ОЧГ18])
                         , isnull(v.[ОЧГ19], v2.[ОЧГ19])
                         , isnull(v.[ОЧГ20], v2.[ОЧГ20])
                         , isnull(v.[ОЧГ21], v2.[ОЧГ21])
                         , isnull(v.[ОЧГ22], v2.[ОЧГ22])
                         , isnull(v.[ОЧГ23], v2.[ОЧГ23])
                         , isnull(v.[ОЧГ24], v2.[ОЧГ24])
                         , isnull(v.[ОЧГ25], v2.[ОЧГ25])

                         , coalesce(b.documentid, c1.documentid, t1.documentid, i1.documentid, m1.documentid)
                         , @Post                                                                                  --a.Zakon
                         , (case
                                when a.action = 'Показатели задач' then m1.AT_2544
                                when a.action = 'Индикаторы цели' then i1.AT_2544
                        end)                                                                            as [ЕдИзмерения]

                         , coalesce(m1.[Код], i1.[Код])
                         , ''
                         , ''
                         , ''
                         , ''

                    from [#Государственная программа] a --Уровень1
                             left join [#Soisp] s on a.action = 'Соисполнители'
                             left join [#Soisp2] z on a.action = 'Соисполнители '
                             left join [#Подрограмма] b
                                       on a.idgp = b.[Вышестоящий документ] and a.action = 'Подпрограммы' --Уровень2
                             left join [#Показателизадач] m1
                                       on a.idgp = m1.[Вышестоящий документ] and a.action = 'Показатели задач' --Уровень2
                             left join [#Цели] c1 on a.idgp = c1.[Вышестоящий документ] and
                                                     a.action = 'Цель государственной программы' --Уровень2
                             left join [#Задачи] t1 on a.idgp = t1.[Вышестоящий документ] and
                                                       a.action = 'Задачи государственной программы' --Уровень2
                             left join [#Индикаторы] i1
                                       on a.idgp = i1.[Вышестоящий документ] and a.action = 'Индикаторы цели' --Уровень2
                             Left Join KS_DDLControl.DS_957_2478 q on a.idgp = q.id_up and
                                                                      a.action = 'Ожидаемые результаты реализации государственной программы' -- Область_ввода
                             left join (select [Source]
                                             , [Код источника финансирования]
--					, sum(round(isnull([ОЧГ13],0),2)) as [ОЧГ13]
--					, sum(round(isnull([ОЧГ14],0),2)) as [ОЧГ14]
--					, sum(round(isnull([ОЧГ15],0),2)) as [ОЧГ15]
--					, sum(round(isnull([ОЧГ16],0),2)) as [ОЧГ16]
--					, sum(round(isnull([ОЧГ17],0),2)) as [ОЧГ17]
--					, sum(round(isnull([ОЧГ18],0),2)) as [ОЧГ18]
--					, sum(round(isnull([ОЧГ19],0),2)) as [ОЧГ19]
--					, sum(round(isnull([ОЧГ20],0),2)) as [ОЧГ20]
--					, sum(round(isnull([ОЧГ21],0),2)) as [ОЧГ21]
--					, sum(round(isnull([ОЧГ22],0),2)) as [ОЧГ22]
--					, sum(round(isnull([ОЧГ23],0),2)) as [ОЧГ23]
--					, sum(round(isnull([ОЧГ24],0),2)) as [ОЧГ24]
--					, sum(round(isnull([ОЧГ25],0),2)) as [ОЧГ25]
                                             , sum(isnull([ОЧГ13], 0)) as [ОЧГ13]
                                             , sum(isnull([ОЧГ14], 0)) as [ОЧГ14]
                                             , sum(isnull([ОЧГ15], 0)) as [ОЧГ15]
                                             , sum(isnull([ОЧГ16], 0)) as [ОЧГ16]
                                             , sum(isnull([ОЧГ17], 0)) as [ОЧГ17]
                                             , sum(isnull([ОЧГ18], 0)) as [ОЧГ18]
                                             , sum(isnull([ОЧГ19], 0)) as [ОЧГ19]
                                             , sum(isnull([ОЧГ20], 0)) as [ОЧГ20]
                                             , sum(isnull([ОЧГ21], 0)) as [ОЧГ21]
                                             , sum(isnull([ОЧГ22], 0)) as [ОЧГ22]
                                             , sum(isnull([ОЧГ23], 0)) as [ОЧГ23]
                                             , sum(isnull([ОЧГ24], 0)) as [ОЧГ24]
                                             , sum(isnull([ОЧГ25], 0)) as [ОЧГ25]

                                        from #Volume
                                        group by [Source], [Код источника финансирования]
                    ) v on a.orderGP = 11

                             left join (select
                                             --[Source]
                                            sum(isnull([ОЧГ13], 0))    as [ОЧГ13]
                                             , sum(isnull([ОЧГ14], 0)) as [ОЧГ14]
                                             , sum(isnull([ОЧГ15], 0)) as [ОЧГ15]
                                             , sum(isnull([ОЧГ16], 0)) as [ОЧГ16]
                                             , sum(isnull([ОЧГ17], 0)) as [ОЧГ17]
                                             , sum(isnull([ОЧГ18], 0)) as [ОЧГ18]
                                             , sum(isnull([ОЧГ19], 0)) as [ОЧГ19]
                                             , sum(isnull([ОЧГ20], 0)) as [ОЧГ20]
                                             , sum(isnull([ОЧГ21], 0)) as [ОЧГ21]
                                             , sum(isnull([ОЧГ22], 0)) as [ОЧГ22]
                                             , sum(isnull([ОЧГ23], 0)) as [ОЧГ23]
                                             , sum(isnull([ОЧГ24], 0)) as [ОЧГ24]
                                             , sum(isnull([ОЧГ25], 0)) as [ОЧГ25]

                                        from #Volume2 --group by [Source]

                    ) v2 on a.orderGP = 10


                end
        end

    --select * from #temp_resultPrgGp

--delete from #temp_resultPrgGp where GpDEnd < @aDateInt
    delete
    from #temp_resultPrgGp
    where @GP = '6570'
      and orderGP = 02
    delete
    from #temp_resultPrgGp
    where @GP <> '6570'
      and orderGP = 03

--Собираем текстовку для итоговых результатов
    declare
        @textResult varchar(max)
    select
        --LOWER(LEFT(gmp.AT_2537, 1))+RIGHT(gmp.AT_2537,LEN(gmp.AT_2537)-1) as [наименование],
        LOWER(LEFT(gmp.AT_2537, 1)) + SUBSTRING(gmp.AT_2537, 2, LEN(gmp.AT_2537) - 1) as [наименование],
        --gmp.AT_2537 as [наименование],
        LEFT(gmp.AT_2538, 4)                                                          as [год_начала_реализации],
        LEFT(r.gpdend, 4)                                                             as [год_окончания_реализации],
        [М_Госпрограммы].cl_3586                                                      as [признак_количественности],
        cast(cast(case (LEFT(gmp.AT_2538, 4))
                      when '2012' then p.m_2643
                      when '2013' then p.m_2628
                      when '2014' then p.m_2629
                      when '2015' then p.m_2630
                      when '2016' then p.m_2631
                      when '2017' then p.m_2632
                      when '2018' then p.m_2934
                      when '2019' then p.m_2935
                      when '2020' then p.m_2936
                      when '2021' then p.m_3072
                      when '2022' then p.m_3339
                      when '2023' then p.m_3340
                      when '2024' then p.m_3341
                      when '2025' then p.m_3343
                      ELSE '0'
            end as float) as varchar)                                                 as [показатель_начала_реализации],
        r.[едизмерения]                                                               as [ед_измерения],
        gmp.at_3275                                                                   as [показатель_направленности],
        case gmp.at_3275
            when '1' then 'увеличится'
            when '0' then 'снизится'
            end                                                                       as [направленность_текст],
        cast(cast(case (LEFT(r.gpdend, 4))
                      when '2012' then p.m_2643
                      when '2013' then p.m_2628
                      when '2014' then p.m_2629
                      when '2015' then p.m_2630
                      when '2016' then p.m_2631
                      when '2017' then p.m_2632
                      when '2018' then p.m_2934
                      when '2019' then p.m_2935
                      when '2020' then p.m_2936
                      when '2021' then p.m_3072
                      when '2022' then p.m_3339
                      when '2023' then p.m_3340
                      when '2024' then p.m_3341
                      when '2025' then p.m_3343
                      ELSE '0'
            end as float) as varchar)                                                 as [показатель_окончания_реализации],
        cast(cast(p.m_3577 as float) as varchar)                                      as [показатель_итого],
        gmp.at_2837                                                                   as [порядковый_номер],
        r.ordergp
    into #expectedResult
    from #temp_resultPrgGp r
             left join ks_ddlcontrol.ds_957_2475 gmp on (gmp.id = r.idppgp)
             left join ks_ddlcontrol.cl_956_2470 [М_Госпрограммы] on ([М_Госпрограммы].id = gmp.cl_2640)
             left join ks_ddlcontrol.ds_957_2478 p on (p.id_up = r.idppgp)
    where r.action = 'Индикаторы цели'
       or r.action = 'Показатели задач'

    order by r.ordergp, r.[ППГП_Код], r.[ПорядковыйНомерПЗППГП]
    --order by  r.[ППГП_Код]
--select * from #expectedResult

    alter table #expectedResult
        ADD textResult
            AS
                (
                    case ([признак_количественности])
                        when 356 then
                                '- ' + [наименование] + ' до ' + [год_окончания_реализации] + ' года - ' +
                                [показатель_итого] + ' ' + [ед_измерения] + ';' + char(10)
                        when 357 then
                                '- ' + [наименование] + ' составит ' + [показатель_окончания_реализации] + ' ' +
                                [ед_измерения] + ' в ' + [год_окончания_реализации] + ' году;' + char(10)
                        else
                                '- ' + [наименование] + ' с ' + [показатель_начала_реализации] + ' ' + [ед_измерения] +
                                ' в ' + [год_начала_реализации] + ' году ' + [направленность_текст] + ' до ' +
                                [показатель_окончания_реализации] + ' ' + [ед_измерения] + ' в ' +
                                [год_окончания_реализации] + ' году;' + char(10)
                        end
                    );

    --select * from #expectedResult

--Собираем текстовку в строку
    select @textResult = (select textResult as 'data()'
                          from #expectedResult FOR XML PATH (''))
    set @textResult = (LEFT(@textResult, LEN(@textResult) - 2))

--Обновляем источник - добавляем собранную строку
    UPDATE #temp_resultPrgGp
    SET ппгп = 'В количественном выражении: ' + char(10) + @textResult
    WHERE action = 'Ожидаемые результаты реализации государственной программы'
      and @Result = 'Да';


--Выставляем source для ordergp 10
    UPDATE #temp_resultPrgGp
    SET source                         = '1',
        [Код источника финансирования] = '001'
    WHERE ordergp = '10';

--Получаем количество показателей по каждой задаче
    select count(*) as [количество_показателей],
           ППГП_Код
    into #countInd
    from #temp_resultPrgGp
    where action = 'Показатели задач'
    group by ППГП_Код
    order by ППГП_Код


--Собираем наименования
    UPDATE
        p
    SET p.[наименование_показателя] = case i.[количество_показателей]
                                          when 1 then 'Показатель задачи ' + cast(p.ППГП_Код as varchar(50))
                                          else
                                              --'Показатель ' + cast(cast(p.[ПорядковыйНомерПЗППГП] as int)as varchar(50)) + ' задачи ' + cast(p.ППГП_Код as varchar(50))
                                                  'Показатель ' +
                                                  case
                                                      when left(p.[ПорядковыйНомерПЗППГП], 1) = '0' and
                                                           len(p.[ПорядковыйНомерПЗППГП]) = '5'
                                                          then right(p.[ПорядковыйНомерПЗППГП], 4)
                                                      when left(p.[ПорядковыйНомерПЗППГП], 1) = '0' and
                                                           len(p.[ПорядковыйНомерПЗППГП]) = '4'
                                                          then right(p.[ПорядковыйНомерПЗППГП], 3)
                                                      when left(p.[ПорядковыйНомерПЗППГП], 1) = '0' and
                                                           len(p.[ПорядковыйНомерПЗППГП]) = '2'
                                                          then right(p.[ПорядковыйНомерПЗППГП], 1)
                                                      else p.[ПорядковыйНомерПЗППГП] end +
                                                  ' задачи ' +
                                                  case
                                                      when left(p.ППГП_Код, 1) = '0' and len(p.ППГП_Код) = '5'
                                                          then right(p.ППГП_Код, 4)
                                                      when left(p.ППГП_Код, 1) = '0' and len(p.ППГП_Код) = '4'
                                                          then right(p.ППГП_Код, 3)
                                                      when left(p.ППГП_Код, 1) = '0' and len(p.ППГП_Код) = '2'
                                                          then right(p.ППГП_Код, 1)
                                                      else p.ППГП_Код end
        end


    from #temp_resultPrgGp p
             left join #countInd i on (p.ППГП_Код = i.ППГП_Код)
    where p.action = 'Показатели задач'

    ----------------------------------------------------
    --Текстовка для параметров финансового обеспечения--
    ----------------------------------------------------
    declare
        @yearStartImplementation int --год начала реализации
        , @yearEndImplementation int --год окончания реализации
    select @yearStartImplementation = (select top 1 left(GpDBegin, 4) from #temp_resultPrgGp)
    select @yearEndImplementation = (select top 1 left(GpDEnd, 4) from #temp_resultPrgGp)
    select id
         , p.[Код источника финансирования]
         , case p.[Код источника финансирования]
               when '001' then 'Общий объем финансового обеспечения - '
               when '002' then 'объем ассигнований федерального бюджета - '
               when '003' then 'объем ассигнований областного бюджета - '
               when '004' then 'объем ассигнований местного бюджета - '
               when '005'
                   then 'объем ассигнований государственных внебюджетных фондов и средств государственных корпораций - '
               when '006' then 'объем ассигнований из внебюджетных источников - '
        end               as block_name
         , case
               when (@yearStartImplementation <= 2013 and @yearEndImplementation >= 2013) then p.ОЧГ13
               else 0 end as ОЧГ13
         , case
               when (@yearStartImplementation <= 2014 and @yearEndImplementation >= 2014) then p.ОЧГ14
               else 0 end as ОЧГ14
         , case
               when (@yearStartImplementation <= 2015 and @yearEndImplementation >= 2015) then p.ОЧГ15
               else 0 end as ОЧГ15
         , case
               when (@yearStartImplementation <= 2016 and @yearEndImplementation >= 2016) then p.ОЧГ16
               else 0 end as ОЧГ16
         , case
               when (@yearStartImplementation <= 2017 and @yearEndImplementation >= 2017) then p.ОЧГ17
               else 0 end as ОЧГ17
         , case
               when (@yearStartImplementation <= 2018 and @yearEndImplementation >= 2018) then p.ОЧГ18
               else 0 end as ОЧГ18
         , case
               when (@yearStartImplementation <= 2019 and @yearEndImplementation >= 2019) then p.ОЧГ19
               else 0 end as ОЧГ19
         , case
               when (@yearStartImplementation <= 2020 and @yearEndImplementation >= 2020) then p.ОЧГ20
               else 0 end as ОЧГ20
         , case
               when (@yearStartImplementation <= 2021 and @yearEndImplementation >= 2021) then p.ОЧГ21
               else 0 end as ОЧГ21
         , case
               when (@yearStartImplementation <= 2022 and @yearEndImplementation >= 2022) then p.ОЧГ22
               else 0 end as ОЧГ22
         , case
               when (@yearStartImplementation <= 2023 and @yearEndImplementation >= 2023) then p.ОЧГ23
               else 0 end as ОЧГ23
         , case
               when (@yearStartImplementation <= 2024 and @yearEndImplementation >= 2024) then p.ОЧГ24
               else 0 end as ОЧГ24
         , case
               when (@yearStartImplementation <= 2025 and @yearEndImplementation >= 2025) then p.ОЧГ25
               else 0 end as ОЧГ25
         , case
               when (@yearStartImplementation <= 2013 and @yearEndImplementation >= 2013) then '2013 год - ' +
                                                                                               replace(replace(convert(varchar, cast(p.ОЧГ13 as money), 1), ',', ' '), '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ13_текст
         , case
               when (@yearStartImplementation <= 2014 and @yearEndImplementation >= 2014) then '2014 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ14 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ14_текст
         , case
               when (@yearStartImplementation <= 2015 and @yearEndImplementation >= 2015) then '2015 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ15 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ15_текст
         , case
               when (@yearStartImplementation <= 2016 and @yearEndImplementation >= 2016) then '2016 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ16 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ16_текст
         , case
               when (@yearStartImplementation <= 2017 and @yearEndImplementation >= 2017) then '2017 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ17 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ17_текст
         , case
               when (@yearStartImplementation <= 2018 and @yearEndImplementation >= 2018) then '2018 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ18 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ18_текст
         , case
               when (@yearStartImplementation <= 2019 and @yearEndImplementation >= 2019) then '2019 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ19 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ19_текст
         , case
               when (@yearStartImplementation <= 2020 and @yearEndImplementation >= 2020) then '2020 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ20 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ20_текст
         , case
               when (@yearStartImplementation <= 2021 and @yearEndImplementation >= 2021) then '2021 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ21 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ21_текст
         , case
               when (@yearStartImplementation <= 2022 and @yearEndImplementation >= 2022) then '2022 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ22 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ22_текст
         , case
               when (@yearStartImplementation <= 2023 and @yearEndImplementation >= 2023) then '2023 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ23 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ23_текст
         , case
               when (@yearStartImplementation <= 2024 and @yearEndImplementation >= 2024) then '2024 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ24 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ24_текст
         , case
               when (@yearStartImplementation <= 2025 and @yearEndImplementation >= 2025) then '2025 год - ' +
                                                                                               replace(
                                                                                                       replace(convert(varchar, cast(p.ОЧГ25 as money), 1), ',', ' '),
                                                                                                       '.', ',') +
                                                                                               ' руб.;' + char(10)
               else
                   '' end as ОЧГ25_текст
    into #paramFin
    from #temp_resultPrgGp p
    where action = 'Параметры финансового обеспечения всего, в том числе по годам реализации государственной программы';

    --select * from #paramFin

    alter table #paramFin
        ADD textResult varchar(8000)
    update p
    set p.textResult = p.ОЧГ13_текст + p.ОЧГ14_текст + p.ОЧГ15_текст +
                       p.ОЧГ16_текст + p.ОЧГ17_текст + p.ОЧГ18_текст +
                       p.ОЧГ19_текст + p.ОЧГ20_текст + p.ОЧГ21_текст +
                       p.ОЧГ22_текст + p.ОЧГ23_текст + p.ОЧГ24_текст + p.ОЧГ25_текст
    from #paramFin p
    update #paramFin
    set textResult = LEFT(textResult, LEN(textResult) - 2)
    where id = (select top (1) id from #paramFin order by id desc)
    update t
    set t.ППГП = p.block_name +
                 replace(replace(convert(varchar, cast((p.ОЧГ13 + p.ОЧГ14 + p.ОЧГ15 + p.ОЧГ16 + p.ОЧГ17 +
                                                        p.ОЧГ18 + p.ОЧГ19 + p.ОЧГ20 + p.ОЧГ21 + p.ОЧГ22 +
                                                        p.ОЧГ23 + p.ОЧГ24 + p.ОЧГ25) as money), 1), ',', ' '), '.', ',') +
                 ' руб., в том числе по годам:' + char(10) + p.textResult


    from #temp_resultPrgGp as t
             inner join #paramFin p on p.id = t.id


    ----------------------------------------------------
    --                    END_BLOCK                   --
    ----------------------------------------------------

-- Возвращаем результат или схему
    if @table_name_result <> ''
        begin
            exec ('select * into ' + @table_name_result + ' from [#temp_resultPrgGp]')
        end
    else
        begin
            if @ToTable = 1
                insert into #t_PrgGP select * from [#temp_resultPrgGp]
            else
                select * from [#temp_resultPrgGp]
        end