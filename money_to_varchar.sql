alter function dbo.money_to_varchar (@value decimal(35,2))
    returns varchar(200)
as
begin
            replace(
                                                                                                           replace(convert(varchar, cast(p.ОЧГ23 as money), 1), ',', ' '),
                                                                                                           '.', ',')
    if @value is null set @value=0.00
    if @value>0.00
        begin

            declare @str varchar(200)
                ,@str1 varchar(200)
            select @str= convert(varchar(200), cast(@value as bigint))
                 ,@str1=''

            declare @i int
            set @i=1
            while @i<=len(@str)
            begin
                if len(right(@str,@i)) % 3 =0   select @str1=left(right(@str,@i),3)+' '+ @str1--+1,@i)

                select @i=@i+1
            end

            select @str1=stuff(reverse(@str1),1,1,'')
            select @str1= left(@str,len(@str)-len(replace(@str1,' ','')))+' ' +@str1
            select @str1=@str1+stuff(cast((@value - floor(@value)) as varchar(30)),1,1,'')
            if(left(@str1, 2) = '00')
                begin
                    select @str1 = substring(@str1, 3, len(@str1)-2)
                end
            if(left(@str1, 1) = '0')
                begin
                    select @str1 = substring(@str1, 2, len(@str1)-1)
                end

            select @str1 = replace(@str1,'.',',')
        end
    else set @str1='0,00'
    return @str1

end