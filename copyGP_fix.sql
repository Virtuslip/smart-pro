CREATE proc [KS_DDLControl].[xp_CopyGP](@GP int=1000, @Ver varchar(10)='01', @dateDocEvent varchar(20) = '01.09.2019')
as
    set nocount on
    set transaction isolation level read uncommitted

declare @aDateInt int
declare @dateEvent varchar(8)

--Преобразовываем дату контрольного события в формат базы данных
    SET @dateEvent = SUBSTRING(@dateDocEvent, 7, 4) + SUBSTRING(@dateDocEvent, 4, 2) + SUBSTRING(@dateDocEvent, 1, 2)

select @aDateInt = convert(varchar, getdate(), 112)
declare @Unit uniqueidentifier
    set @Unit = newid()
    Create Table #tmpDS_957_2475
    (
        id   int,
        Guid uniqueidentifier
    )
declare @ClRow_Id int
select @ClRow_Id = CL_2640
from KS_DDLControl.DS_957_2475
where id = @GP
    if exists(select *
              from KS_DDLControl.DS_957_2475
              where id = @GP) and isnull(@ClRow_Id, 0) > 0
        begin try
            begin transaction
                select a.*
                     , b.DS_2654 --Вышестоящий документ
                into #GMP
                from KS_DDLControl.DS_957_2475 a
                         left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
                where a.AT_2845 = @Ver

                --Удаляем контрольные события дата которых не равна переданной дате из параметра
                DELETE FROM #GMP WHERE at_2529 <> @dateEvent AND documentid = 337;

                --Обновляем дату документа расчетной датой для госпрограммы
                --update #GMP set at_2529 = @aDateIn where documentid = 220

                insert into #tmpDS_957_2475
                select id, newid()
                from #GMP
                where id = @GP

                while @@ROWCOUNT > 0
                begin
                    insert into #tmpDS_957_2475
                    select a.id
                         , newid()
                    from #GMP a
                             inner join #tmpDS_957_2475 b on a.DS_2654 = b.id
                    where not exists(select * from #tmpDS_957_2475 where id = a.id)
                end
                --select * from #tmpDS_957_2475
                Declare
                    @VerTo varchar(2)
                --select @VerTo= right('0' + convert(varchar,convert(int,@Ver)+1),2)
                select @VerTo = right('0' + convert(varchar, convert(int, max(AT_2845)) + 1), 2)
                from KS_DDLControl.DS_957_2475 a
                where documentid = 220
                  and CL_2640 in (select top 1 CL_2640 from KS_DDLControl.DS_957_2475 where id = @GP)


                insert into KS_DDLControl.DS_957_2475 ( AT_2529
                                                      , CL_2533
                                                      , Unit
                                                      , DOCUMENTID
                                                      , EODGuid
                                                      , AT_2537
                                                      , AT_2538
                                                      , AT_2539
                                                      , DS_2599
                                                      , CL_2633
                                                      , CL_2640
                                                      , AT_2646
                                                      , CL_3253
--		,AT_3255
--		,AT_3254
--		,AT_3256
                    --,CL_2754
                                                      , GUID
                                                      , AT_2837
                                                      , AT_2845
                    --,CL_2849
                    --,CL_2853
                                                      , M_2879
                    --,CL_2925
                                                      , CL_3252
                                                      , AT_3275
                                                      , CL_3258
                                                      , AT_3303
                                                      , AT_3338
                                                      , AT_3349
                                                      , AT_3388
                                                      , cl_3459)
                select case a.DOCUMENTID
                           when 220 then @aDateInt --Госпрограмма
                           else a.AT_2529
                    end [Дата документа]
                     , a.CL_2533
                     , @Unit
                     , a.DOCUMENTID
                     , a.EODGuid
                     , a.AT_2537
                     , a.AT_2538
                     , a.AT_2539
                     , a.DS_2599
                     , a.CL_2633
                     , a.CL_2640
                     , a.AT_2646
                     , case a.DOCUMENTID
                           when 337 then 10 --КС
                           else 8
                    end [Статус]
                     --a.CL_3253
--		,a.AT_3255
--		,a.AT_3254
--		,a.AT_3256
                     --,a.CL_2754
                     , b.GUID
                     , a.AT_2837
                     , @VerTo
                     --,CL_2849
                     --,CL_2853
                     , M_2879
                     --,CL_2925
                     , CL_3252
                     , AT_3275
                     , a.CL_3258
                     , case a.DOCUMENTID
                           when 220 then 'Проект изменений' --Госпрограмма
                           else a.AT_3303
                    end [Примечание]
                     , a.AT_3338
                     , a.AT_3349
                     , a.AT_3388
                     , cl_3459
                from KS_DDLControl.DS_957_2475 a
                         inner join #tmpDS_957_2475 b on a.id = b.id

                --Область_ввода
                insert into KS_DDLControl.DS_957_2478 ( ID_UP, AT_2532, UNIT, EODGuid, M_2628, M_2629, M_2630, M_2631
                                                      , M_2632, M_2643, M_2731, M_2732, M_2733, GUID, M_2870, M_2871
                                                      , M_2872, M_2933, M_2934, M_2935, M_2936
                                                      , M_3072
                                                      , M_3339
                                                      , M_3340
                                                      , M_3341
                                                      , M_3343
                                                      , M_3096
                                                      , M_3097
                                                      , M_3098
                                                      , M_3099
                                                      , M_3106
                                                      , M_3115
                                                      , M_3116)
                select a1.ID
                     , b.AT_2532
                     , @Unit
                     , b.EODGuid
                     , b.M_2628
                     , b.M_2629
                     , b.M_2630
                     , b.M_2631
                     , b.M_2632
                     , b.M_2643
                     , b.M_2731
                     , b.M_2732
                     , b.M_2733
                     , newid()
                     , b.M_2870
                     , b.M_2871
                     , b.M_2872
                     , b.M_2933
                     , b.M_2934
                     , b.M_2935
                     , b.M_2936
                     , b.M_3072
                     , b.M_3339
                     , b.M_3340
                     , b.M_3341
                     , b.M_3343
                     , b.M_3096
                     , b.M_3097
                     , b.M_3098
                     , b.M_3099
                     , b.M_3106
                     , b.M_3115
                     , b.M_3116

                from KS_DDLControl.DS_957_2475 a
                         inner join KS_DDLControl.DS_957_2478 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid

                --Оценка расходов
                insert into KS_DDLControl.DS_957_2506
                ( ID_UP
                , UNIT
                , EODGuid
                , CL_2556
                , CL_2557
                , CL_2558
                , CL_2559
                , CL_2560
                , CL_2571
                , CL_2572
                , M_2761
                , M_2767
                , M_2768
                , M_2769
                , M_2770
                , M_2771
                , M_2772
                , M_2773
                , CL_2834
                , GUID
                , M_2880
                , M_2928
                , M_2929
                , M_2930
                , M_2931
                , M_2932
                , M_2937
                , M_2938
                , M_2939
                , M_3073
                , M_3344
                , M_3345
                , M_3346
                , M_3347
                , M_3100
                , M_3101
                , M_3102
                , M_3103
                , M_3104
                , M_3105
                , M_3108
                , CL_3162
                , CL_3163)

                select a1.ID
                     , @Unit
                     , b.EODGuid
                     , b.CL_2556
                     , b.CL_2557
                     , b.CL_2558
                     , b.CL_2559
                     , b.CL_2560
                     , b.CL_2571
                     , b.CL_2572
                     , b.M_2761
                     , b.M_2767
                     , b.M_2768
                     , b.M_2769
                     , b.M_2770
                     , b.M_2771
                     , b.M_2772
                     , b.M_2773
                     , b.CL_2834
                     , newid()
                     , b.M_2880
                     , b.M_2928
                     , b.M_2929
                     , b.M_2930
                     , b.M_2931
                     , b.M_2932
                     , b.M_2937
                     , b.M_2938
                     , b.M_2939
                     , b.M_3073
                     , b.M_3344
                     , b.M_3345
                     , b.M_3346
                     , b.M_3347
                     , b.M_3100
                     , b.M_3101
                     , b.M_3102
                     , b.M_3103
                     , b.M_3104
                     , b.M_3105
                     , b.M_3108
                     , b.CL_3162
                     , b.CL_3163

                from KS_DDLControl.DS_957_2475 a
                         inner join KS_DDLControl.DS_957_2506 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid

                --Область_ввода3
                insert into KS_DDLControl.DS_957_2521
                ( ID_UP
                , UNIT
                , EODGuid
                , AT_2755
                    --,AT_2755_XML
                , AT_2756
                , CL_2758
                , AT_2759
                , AT_2760
                , GUID
                , CL_2860
                , CL_2926
                , AT_3118
                , AT_3119
                    --,AT_3120
                , CL_3260
                , AT_3288
                , AT_3386
                , AT_3407
                , AT_3411
                , AT_3412)
                select a1.ID
                     , @Unit
                     , b.EODGuid
                     , b.AT_2755
                     --,b.AT_2755_XML
                     , b.AT_2756
                     , b.CL_2758
                     , b.AT_2759
                     , b.AT_2760
                     , newid()
                     , b.CL_2860
                     , b.CL_2926
                     , b.AT_3118
                     , b.AT_3119
                     --,b.AT_3120
                     , b.CL_3260
                     , b.AT_3288
                     , b.AT_3386
                     , b.AT_3407
                     , b.AT_3411
                     , b.AT_3412

                from KS_DDLControl.DS_957_2475 a
                         inner join KS_DDLControl.DS_957_2521 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid

                --Область_ввода4
                insert into KS_DDLControl.DS_957_2619
                ( ID_UP
                , UNIT
                , EODGuid
                , AT_2644
                , AT_2645
                , CL_2752
                , GUID
                , M_2865
                , AT_2866
                , AT_2867
                , AT_3109
                , AT_3110
                , AT_3112
                , AT_3113
                , M_3114)
                select a1.ID
                     , @Unit
                     , b.EODGuid
                     , b.AT_2644
                     , b.AT_2645
                     , b.CL_2752
                     , newid()
                     , b.M_2865
                     , b.AT_2866
                     , b.AT_2867
                     , b.AT_3109
                     , b.AT_3110
                     , b.AT_3112
                     , b.AT_3113
                     , b.M_3114
                from KS_DDLControl.DS_957_2475 a
                         inner join KS_DDLControl.DS_957_2619 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid

                /*--Приложения
                    insert into KS_DDLControl.DS_957_2890
                    (
                         ID_UP
                        ,GUID
                        ,UNIT
                        ,EODGuid
                        ,M_2889
                        ,M_2890
                        ,M_2891
                        ,M_2892
                        ,M_2893
                        ,M_2894
                        ,M_2895
                        ,M_2896
                        ,M_2897
                        ,M_2898
                        ,M_2899
                        ,M_2900
                        ,M_2901
                        ,M_2902
                        ,M_2903
                        ,CL_2904
                        ,CL_2905
                        ,CL_2906
                        ,M_2907
                        ,M_2908
                        ,M_2909
                        ,M_2910
                        ,M_2911
                        ,M_2912
                        ,M_2913
                        ,M_2914
                        ,M_2915
                        ,M_2916
                        ,M_2917
                        ,M_2918
                        ,M_2920
                        ,M_2921
                        ,M_2922
                        ,M_2923
                        ,M_3056
                        ,M_3057
                        ,M_3058
                        ,M_3059
                        ,M_3060
                        ,M_3062
                        ,M_3063
                        ,M_3064
                        ,M_3065
                        ,M_3066
                        ,M_3067
                        ,M_3068
                        ,M_3069
                        ,M_3070
                        ,M_3071
                        ,M_3074
                        ,M_3075
                        ,M_3076
                        ,M_3077
                        ,M_3078
                        ,M_3107
                    )
                    select
                         a1.ID
                        ,newid()
                        ,@Unit
                        ,b.EODGuid
                        ,b.M_2889
                        ,b.M_2890
                        ,b.M_2891
                        ,b.M_2892
                        ,b.M_2893
                        ,b.M_2894
                        ,b.M_2895
                        ,b.M_2896
                        ,b.M_2897
                        ,b.M_2898
                        ,b.M_2899
                        ,b.M_2900
                        ,b.M_2901
                        ,b.M_2902
                        ,b.M_2903
                        ,b.CL_2904
                        ,b.CL_2905
                        ,b.CL_2906
                        ,b.M_2907
                        ,b.M_2908
                        ,b.M_2909
                        ,b.M_2910
                        ,b.M_2911
                        ,b.M_2912
                        ,b.M_2913
                        ,b.M_2914
                        ,b.M_2915
                        ,b.M_2916
                        ,b.M_2917
                        ,b.M_2918
                        ,b.M_2920
                        ,b.M_2921
                        ,b.M_2922
                        ,b.M_2923
                        ,b.M_3056
                        ,b.M_3057
                        ,b.M_3058
                        ,b.M_3059
                        ,b.M_3060
                        ,b.M_3062
                        ,b.M_3063
                        ,b.M_3064
                        ,b.M_3065
                        ,b.M_3066
                        ,b.M_3067
                        ,b.M_3068
                        ,b.M_3069
                        ,b.M_3070
                        ,b.M_3071
                        ,b.M_3074
                        ,b.M_3075
                        ,b.M_3076
                        ,b.M_3077
                        ,b.M_3078
                        ,b.M_3107
                    from
                        KS_DDLControl.DS_957_2475 a inner join KS_DDLControl.DS_957_2890 b on a.id = b.id_up
                                inner join #tmpDS_957_2475 t on a.id=t.id
                            inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                            */

---------------------------Этапы реализации----------------------
                insert into ks_ddlcontrol.ds_957_3639
                ( ID_UP
                , GUID
                , UNIT
                , EODGuid
                , at_3546
                , at_3547)
                select a1.ID
                     , newid()
                     , @Unit
                     , b.EODGuid
                     , b.at_3546
                     , b.at_3547

                from KS_DDLControl.DS_957_2475 a
                         inner join ks_ddlcontrol.ds_957_3639 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                ---------------------------------------------------------------

---------------------------Методы госрегулировани----------------------
                insert into ks_ddlcontrol.ds_957_3252
                ( ID_UP
                , GUID
                , UNIT
                , EODGuid
                , cl_3220
                , at_3439
                , at_3221
                , at_3222
                , at_3223
                , at_3224
                , at_3225
                , at_3226
                , at_3227
                , at_3434
                , at_3435
                , at_3436
                , at_3437
                , at_3438
                , at_3228
                    --,at_3229
                )
                select a1.ID
                     , newid()
                     , @Unit
                     , b.EODGuid
                     , b.cl_3220
                     , b.at_3439
                     , b.at_3221
                     , b.at_3222
                     , b.at_3223
                     , b.at_3224
                     , b.at_3225
                     , b.at_3226
                     , b.at_3227
                     , b.at_3434
                     , b.at_3435
                     , b.at_3436
                     , b.at_3437
                     , b.at_3438
                     , b.at_3228
                     --,b.at_3229
                from KS_DDLControl.DS_957_2475 a
                         inner join ks_ddlcontrol.ds_957_3252 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                ---------------------------------------------------------------

---------------------------Показатели методов госрегулирования----------------------
                insert into ks_ddlcontrol.ds_957_3476
                ( ID_UP
                , GUID
                , UNIT
                , EODGuid
                , at_3440
                , cl_3417
                , cl_3441
                , m_3423
                , m_3418
                , m_3419
                , m_3420
                , m_3421
                , m_3422
                , m_3424
                , m_3425
                , m_3426
                , m_3427
                , m_3428
                , m_3429
                , m_3430
                , m_3431
                , at_3229
                , at_3588)
                select b.ID
                     , newid()
                     , @Unit
                     , p.EODGuid
                     , p.at_3440
                     , p.cl_3417
                     , p.cl_3441
                     , p.m_3423
                     , p.m_3418
                     , p.m_3419
                     , p.m_3420
                     , p.m_3421
                     , p.m_3422
                     , p.m_3424
                     , p.m_3425
                     , p.m_3426
                     , p.m_3427
                     , p.m_3428
                     , p.m_3429
                     , p.m_3430
                     , p.m_3431
                     , p.at_3229
                     , p.at_3588
                from KS_DDLControl.DS_957_2475 a
                         inner join ks_ddlcontrol.ds_957_3252 b on a.id = b.id_up
                         inner join ks_ddlcontrol.ds_957_3476 p on b.id = p.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                ---------------------------------------------------------------

                ---------------------------Справочники-------------------------
                insert into ks_ddlcontrol.ds_957_3629
                ( ID_UP
                , GUID
                , UNIT
                , EODGuid
                , cl_3540
                , at_3580)
                select a1.ID
                     , newid()
                     , @Unit
                     , b.EODGuid
                     , b.cl_3540
                     , b.at_3580

                from KS_DDLControl.DS_957_2475 a
                         inner join ks_ddlcontrol.ds_957_3629 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                ---------------------------------------------------------------

                ---------------------------Методики расчета----------------------
                insert into ks_ddlcontrol.ds_957_3631
                ( ID_UP
                , GUID
                , UNIT
                , EODGuid
                , at_3542
                , at_3541
                , cl_3561
                , at_3587)
                select a1.ID
                     , newid()
                     , @Unit
                     , b.EODGuid
                     , b.at_3542
                     , b.at_3541
                     , b.cl_3561
                     , b.at_3587
                from KS_DDLControl.DS_957_2475 a
                         inner join ks_ddlcontrol.ds_957_3631 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid

                ---------------------------------------------------------------

                ---------------------------НПА-------------------------------
                insert into ks_ddlcontrol.ds_957_3626
                ( ID_UP
                , GUID
                , UNIT
                , EODGuid
                , cl_3538
                , AT_3579)
                select a1.ID
                     , newid()
                     , @Unit
                     , b.EODGuid
                     , b.cl_3538
                     , b.AT_3579

                from KS_DDLControl.DS_957_2475 a
                         inner join ks_ddlcontrol.ds_957_3626 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                ---------------------------------------------------------------


                ----------------Исполнители
                insert into KS_DDLControl.DS_957_3235(id_up, CL_3205, unit, eodguid, guid)
                select a1.ID
                     , b.CL_3205
                     , @Unit
                     , b.EODGuid
                     , newid()
                from KS_DDLControl.DS_957_2475 a
                         inner join KS_DDLControl.DS_957_3235 b on a.id = b.id_up
                         inner join #tmpDS_957_2475 t on a.id = t.id
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid
                -- новые

                --Вышестоящий документ
                insert into KS_DDLControl.DS_957_2625
                ( ID_UP
                , UNIT
                , EODGuid
                , DS_2649
                , AT_2650
                , DS_2654
                , GUID)
                select a1.ID
                     , @Unit
                     , b.EODGuid
                     , b.DS_2649

                     , b.AT_2650
                     , a3.id
                     , newid()
                from KS_DDLControl.DS_957_2475 a
                         inner join KS_DDLControl.DS_957_2625 b on a.id = b.id_up --старые
                         inner join #tmpDS_957_2475 t1 on a.id = t1.id --связка
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t1.Guid --новые

                         inner join KS_DDLControl.DS_957_2475 a2 on a2.id = b.DS_2654 --вышестоящий документ старые
                         inner join #tmpDS_957_2475 t2 on a2.id = t2.id --вышестоящий документ связка
                         inner join KS_DDLControl.DS_957_2475 a3 on a3.Guid = t2.Guid --вышестоящий документ новые


                if not exists(select *
                              from sys.objects
                              WHERE object_id = object_id(N'[KS_DDLControl].[VersionCloneHistory]')
                                and type = N'U') create table [KS_DDLControl].[VersionCloneHistory]
                                                 (
                                                     unit        uniqueidentifier,
                                                     VersionFrom varchar(2),
                                                     VersionTo   varchar(2),
                                                     Row_idFrom  int,
                                                     Row_idTo    int,
                                                     EntityClID  int,
                                                     ClRow_Id    int,
                                                     Uid         int,
                                                     EventDate   datetime
                                                 )

                declare
                    @Row_idTo int
                select @Row_idTo = a1.id
                from KS_DDLControl.DS_957_2475 a
                         inner join #tmpDS_957_2475 t on a.id = t.id and a.id = @GP
                         inner join KS_DDLControl.DS_957_2475 a1 on a1.Guid = t.Guid

                insert into [KS_DDLControl].[VersionCloneHistory] (unit, VersionFrom, VersionTo, Row_idFrom, Row_idTo,
                                                                   EntityClID, ClRow_Id, Uid, EventDate)
                select @Unit,
                       @Ver,
                       @VerTo,
                       @GP,
                       @Row_idTo,
                       956,
                       @ClRow_Id,
                       user_id(suser_sname()),
                       getdate()
            commit transaction
        end try
        begin catch
            declare
                @error_message varchar(4000), @error_severity int, @error_state int, @error_line int
            select @error_message = ERROR_MESSAGE(),
                   @error_severity = ERROR_SEVERITY(),
                   @error_state = ERROR_STATE(),
                   @error_line = ERROR_LINE()
            set @error_message = @error_message + '%s' + '%d'
            raiserror (@error_message, @error_severity, @error_state, N' Ошибка в строке: ', @error_line)
            rollback transaction

        end catch