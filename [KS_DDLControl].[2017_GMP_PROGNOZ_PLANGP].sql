CREATE proc [KS_DDLControl].[2017_GMP_PROGNOZ_PLANGP]
as
begin
set nocount on
set transaction isolation level read uncommitted

select v.id
		,v.docid
		,v.depid
		,dep.AT_1795   	depCode
		,v.divid
		,div.AT_1798	divCode
		,v.targetid
		,target.AT_1801	tgtCode
		,v.expid
		,exps.AT_1804	expCode
		,v.klassid		itemId
		,item.code	itemCode
		,item.name	itemName
		,v.addklassid
		,add_cl.AT_1831	addclassCode
		,v.sourceid
		,src.AT_1997	sourceCode
		,src.AT_1999	sourceName
		,v.accid
		,acc.AT_2376	accCode
		,[ochg-4]
		,[ochg-3]
		,[ochg-2]
		,[ochg-1]
		,ochg2017
		,ochg2018
		,ochg2019
		,ochg2020
		,ochg2021
		
into #temp_result
from v_gmp_plan_expends v
	left join KS_DDLControl.CL_864_1710 dep on v.depid=dep.id
	left join KS_DDLControl.CL_865_1714 div	on v.divid=div.id
	left join KS_DDLControl.CL_866_1718 target on v.targetid=target.id
	left join KS_DDLControl.CL_867_1722 exps  on v.expid=exps.id
	--left join KS_DDLControl.CL_869_1745 item on v.klassid=item.id -- ����� ������� � ��������������, ��� �� ���� ������������
	left join ks_ddlcontrol.get_maket_kosgu_0 item on v.klassid=item.id
	
	--left join KS_DDLControl.CL_869_3149 itemv on item.id=itemv.id_up
	left join KS_DDLControl.CL_870_1752 add_cl on v.addklassid=add_cl.id
	left join KS_DDLControl.CL_894_1936 src	on v.sourceid=src.id
	left join KS_DDLControl.CL_917_2326 acc on v.accid=acc.id
	
	
select * from #temp_result

SELECT id
from #temp_result
group by id 
having count(*)>1 

end