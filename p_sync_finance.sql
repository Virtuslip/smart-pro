--������������� �������������� �� ����� �����������
alter procedure dbo.p_sync_finance
   @uRid uniqueidentifier = null -- ��� ������ 
as
begin
	set nocount on
	--�������� �� ���������� ��� ������� ���� ��������� ����������
	declare @docid int
	declare @user_type int
	declare @permission int
	
	select [int] as docid into #docid from dbo.sys_values where rid=@uRid and code  = 'ElementId'

	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()
	
	select @permission = AllowPermission from dbo.ENTITY where ID = 864
	while exists (select * from #docid)
	begin
		--���� �� ������� ���������
		select top 1 @docid = docid from #docid
		--�������� ��� ������������, ������ � ��� �����������
		select 
			code_gp.at_3391 as gp_code, --��� ������������
			gmp.at_2845 as gp_version, --������ ������������
			gp.at_2524 as event_code, --��� �����������
			[�����_�����������_���].cl_3232 as [ID_���������]
		into #event
		from ks_ddlcontrol.ds_957_2475 gmp
			left join ks_ddlcontrol.cl_956_2470 gp on (gmp.cl_2640 = gp.id) --���������� �_������������ (�����������)
			left join ks_ddlcontrol.cl_1332_3443 code_gp on (gp.cl_3395 = code_gp.id) -- ���������� ��� ������������
			left join ks_ddlcontrol.cl_1280_3226 [�����_�����������_���] on (gmp.cl_3252 = [�����_�����������_���].id)
			--left join ks_ddlcontrol.cl_864_1710 [�����_���������] on ([�����_�����������_���].cl_3232 = [�����_���������].id)
			where gmp.id = @docid
		
		--������� ������ �� ������� ���������� ������ (�� ����������� ������)
		if @user_type != 4
		begin
			delete from #event where [ID_���������] not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
		end 
		
		--������� ����� ����������� ������ �� ��� -> ������ ��������
		delete expense from ks_ddlcontrol.ds_957_2506 expense
			left join ks_ddlcontrol.ds_957_2475 gmp on (gmp.id = expense.id_up)
			left join ks_ddlcontrol.cl_956_2470 gp on (gmp.cl_2640 = gp.id)
			left join ks_ddlcontrol.cl_1332_3443 code_gp on (gp.cl_3395 = code_gp.id)
			inner join #event event on (gp.at_2524 = event.event_code and code_gp.at_3391 = event.gp_code and gmp.at_2845 = event.gp_version) --�������� ������ �� ������ ������� ���� � ������� #event
			where expense.at_3575 = 1
		
			
			--���������� ������������ ����� 
		update expense
		set
		  expense.m_2939=0,
		  expense.m_3073=0,
		  expense.m_3344=0
		from ks_ddlcontrol.ds_957_2506 expense
			left join ks_ddlcontrol.ds_957_2475 gmp on (gmp.id = expense.id_up)
			left join ks_ddlcontrol.cl_956_2470 gp on (gmp.cl_2640 = gp.id)
			left join ks_ddlcontrol.cl_1332_3443 code_gp on (gp.cl_3395 = code_gp.id)
			inner join #event event on (gp.at_2524 = event.event_code and code_gp.at_3391 = event.gp_code and gmp.at_2845 = event.gp_version) --�������� ������ �� ������ ������� ���� � ������� #event


		--�������� ������ � ��� -> ������ ��������
		insert into ks_ddlcontrol.ds_957_2506 
			(
			id_up, --id ��
			at_3575, -- ������� �������������
			cl_2572, -- �����_��������� ��������������
			cl_3163, -- �����_�����
			cl_2571, -- �����_��������������
			cl_2556, -- �����_���������
			cl_2557, -- �����_����������
			cl_2558, -- �����_������� ������
			cl_2559, -- �����_���� ��������
			cl_2560, -- �����_��������
			cl_3162, -- �����_�����
			m_2939, -- 2020 ��� (������)
			m_3073, -- 2021 ��� (������)
			m_3344  -- 2022 ��� (������)
			)
		select 
			@docid, --id ��
			'1', -- ������� �������������
			decryption.cl_3383, -- �����_��������� ��������������
			cl_3384, -- �����_�����
			cl_3385, -- �����_��������������
			cl_3370, -- �����_���������
			cl_3371, -- �����_����������
			cl_3372, -- �����_������� ������
			cl_3373, -- �����_���� ��������
			cl_3375, -- �����_��������
			cl_3374, -- �����_�����
			at_3564, -- �� ���
			at_3566, -- �� ���+1
			at_3566  -- �� ���+2	
		from ks_ddlcontrol.ds_1323_3421 decryption -- �����������
			left join  ks_ddlcontrol.ds_1323_3416 logic_structure on (logic_structure.id = decryption.id_up) --���������� ��������
			left join ks_ddlcontrol.cl_956_2470 gp on (decryption.cl_3465 = gp.id) -- ���������� �_������������ (�����������)
			left join ks_ddlcontrol.cl_1332_3443 code_gp on (logic_structure.cl_3394 = code_gp.id) -- ���������� ��� ������������
			inner join #event event on (gp.at_2524 = event.event_code and code_gp.at_3391 = event.gp_code and logic_structure.at_3381 = event.gp_version) --�������� ������ �� ������ ������� ���� � ������� #event
		
		drop table #event
		delete from #docid where docid=@docid
	end		
end