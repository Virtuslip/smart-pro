alter proc [KS_DDLControl].[xp_PrgPpGp] (@nmode int=0, @GP int=6561, @PPGP varchar(max)='6777@#$6821@#$6854', @Ver varchar(2)='10', @ToTable int=0)
as

create table #temp_resultPPGP 
			( 
			  idppgp int
			, [����] varchar(2000)
			, orderPPGP varchar(2)
			, action varchar(8000)
			, idContent int
			, [Content] varchar(8000)
			, [���_Content] varchar(50)
			, GpDBegin int
			, GpDEnd int
			, ParamDate int
			, [S_gp] varchar(500)
			, [Source] varchar(100)
			, [���13] numeric (20,4)
			, [���14] numeric (20,4)
			, [���15] numeric (20,4)
			, [���16] numeric (20,4)
			, [���17] numeric (20,4)
			, [���18] numeric (20,4)
			, [���19] numeric (20,4)
			, [���20] numeric (20,4)
			, [���21] numeric (20,4)
			, [���22] numeric (20,4)
			, [���23] numeric (20,4)
			, [���24] numeric (20,4)
			, [���25] numeric (20,4)
			, [�������] varchar(20)
			, documentid int
			, Post varchar(max)
			, id int identity (1,1)
			, [���������������������] varchar(10)
		--	, [id����] varchar(20)
			, [����������������] varchar(200)
--			, [����������2020] numeric (20,2)
--			, [����������������] varchar(2000)
			)  
			
			--�������� ID ������������
			set @PPGP = replace(@PPGP, '@#$', '|') + '|'
			declare @ppgpID table (id int)
			declare @pos int = charindex('|',@PPGP)
			declare @id int
			while (@pos != 0)
			begin			  
			    set @id = SUBSTRING(@PPGP, 1, @pos-1)			    
			    insert into @ppgpID (id) values(cast(@id as int))			   
			    set @PPGP = SUBSTRING(@PPGP, @pos+1, LEN(@PPGP))			
			    set @pos = CHARINDEX('|',@PPGP)
			end
				
			
if @nmode <> -1 
begin
set nocount on
set transaction isolation level read uncommitted
-- ������ �������:  exec [KS_DDLControl].[xp_PrgPpGp] @nmode=0, @GP=6574, @PPGP=7355, @Ver='01'

declare @PPGPDocID int
declare @aDateInt int
select  @aDateInt = convert(varchar,GetDate(),112)

--select  @aDateInt = convert(varchar,dbo.getworkdate(),112)

	create table #prg (gp varchar(30), action varchar(8000), id int identity (1,1))
	insert into  #prg select '������������', '������������� ����������� ������������'
	insert into  #prg select '������������', '������������� ������������'
	insert into  #prg select '������������', '�������������  ������������' 
	insert into  #prg select '������������', '���� ������������'
	insert into  #prg select '������������', '������� ���������� ������������'
	insert into  #prg select '������������', '������ ������������'
	insert into  #prg select '������������', '���������� ����� ������������'
	insert into  #prg select '������������', '����� � ����� ���������� ������������'
	insert into  #prg select '������������', '������ �������������� �� ���� ������� ���������� ������� �����, � ��� ����� �� ����� ���������� ������������'
	insert into  #prg select '������������', '��������� ���������� ���������� ������������'
	--    insert into  #prg select '������������', '��������� ���������� ����������  ��������a���'

	------------
	--������� --
	------------
	Declare @user_type int
	select 
		@user_type = user_type -- ���� user_type = 4, �� ��� �������������, ��� ���� ������� ��������� �� ����
	from s_users where loginame = suser_sname()

	Declare @GmpPermission int
	select 
		@GmpPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 956  --����������� ���

	Declare @VedPermission int
	select 
		@VedPermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 864  --���������� ��������� 

	Declare @SourcePermission int
	select 
		@SourcePermission = AllowPermission --������� ��������� �����
	from dbo.ENTITY
	where ID = 894  --���������� ��������� ��������������

	---------------------------------------------------------
	--������� ���������� �������� ����������� ������������ --
	---------------------------------------------------------
	declare @Cl table (id int, [������������] varchar(2000), [DBegin] int, [���] varchar(20), Post varchar(max))
	insert into @Cl
	select
		  a.id
		--, isnull(AT_2641, AT_2526) as [��]
		, AT_2526 as [��]
		, b.at_3398 as [DBegin]
		, a.AT_2524
		, a.AT_2924
	from
        	KS_DDLControl.CL_956_2470 a
		left join (select * from ks_ddlcontrol.cl_956_3452 where at_3398 <= @aDateInt) b on a.id = b.id_up

	select tab2.id, tab2.[������������] as [��], tab2.[���], tab2.Post into #sprGp from
	(select id, max(DBegin) as DBegin from @Cl group by id) tab1
	inner join 
	(select id, DBegin, [������������], [���], Post from @Cl) tab2
	on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)

	--�������� ��������� �� ������� ����������� ������
	if isnull(@GmpPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprGp where id not in (select CLID from KS_DDLControl.CLA_956 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

-------------------------------------------
	--������� �������� ����������� �������������� --
	-------------------------------------------
	select a.*, a1.AT_3200, a1.AT_3201, a1.AT_3202
	into #sprVed1
    from KS_DDLControl.CL_1280_3226 a 
    left join KS_DDLControl.CL_1280_3231 a1 on a.id = a1.id_up
      
     
--select * from    #sprVed1
     
	-------------------------------------------
	--������� �������� ����������� ��������� --
	-------------------------------------------
	select a.*, a1.AT_1797, a1.AT_1796, a1.AT_1930
	into #sprVed 
    from KS_DDLControl.CL_864_1710 a 
	left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
	 --from KS_DDLControl.CL_1280_3226 a 
     --left join KS_DDLControl.CL_864_3145 a1 on a.cl_3232 = a1.id_up
     
--select * from #sprVed
		
/*
	--�������� ��������� �� ������� ����������� ������
	if isnull(@VedPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprVed where id not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 
*/
	----------------------------------------------------------
	--������� �������� ����������� ��������� �������������� --
	----------------------------------------------------------
	select * into #sprSource from KS_DDLControl.CL_894_1936
	--�������� ��������� �� ������� ����������� ������
	if isnull(@SourcePermission,0) > 0 and @user_type != 4
	begin
		delete from #sprSource where id not in (select CLID from KS_DDLControl.CLA_894 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end 

	-------------------------------------------------
	-- ������� ���������� ���������� ��������� ��� --2646 - �������������
	-------------------------------------------------
	create table #GpName 
			( 
			  [��_���������] int
			, [���] varchar(50)
			, [������������] varchar(2000)
			, [���] varchar(10)
			, [id��] int
			)  

	insert into #GpName exec [ks_ddlcontrol].[xp_sprGpOnDoc_2] @nmode=0

	declare @idPPGP int
	declare cur cursor for select id from @ppgpID
	open cur
	FETCH NEXT FROM cur INTO @idPPGP
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    select
		a.id
		, a.AT_2529
		, a.CL_2533
		, a.CL_3252
		, a.documentid
		, a.AT_2538
		, a.AT_2539
		, a.CL_2640
		, a.CL_3253
		, b.DS_2654   --����������� ��������
		, AT_2656 as [SCode]
		, AT_2658 as [SNaim]
		, AT_2661 as [SLock]
		, AT_1998 as [Source]
		, a.at_2837 as [���]
		, sum(isnull(e.M_2928,0.0)) as [���13]
		, sum(isnull(e.M_2929,0.0)) as [���14]
		, sum(isnull(e.M_2930,0.0)) as [���15]  
		, sum(isnull(e.M_2931,0.0)) as [���16]
		, sum(isnull(e.M_2932,0.0)) as [���17]
		, sum(isnull(e.M_2937,0.0)) as [���18]
		, sum(isnull(e.M_2938,0.0)) as [���19]
		, sum(isnull(e.M_2939,0.0)) as [���20]
		, sum(isnull(e.M_3073,0.0)) as [���21]
		, sum(isnull(e.M_3344,0.0)) as [���22]
		, sum(isnull(e.M_3345,0.0)) as [���23]
		, sum(isnull(e.M_3346,0.0)) as [���24]
		, sum(isnull(e.M_3347,0.0)) as [���25]	
		, M_2879 as [Zakon]
		,e.cl_2556
		,q.AT_2544
		, a.AT_3349	
		
	into #GMP_old
	from
							
			[ks_ddlControl].[GetDocGP](@GP, @idPPGP , @Ver) o inner join KS_DDLControl.DS_957_2475 a on o.id = a.id
			left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
			Left Join KS_DDLControl.CL_1008_2633 c on a.CL_3253 = c.id   --������
			Left Join KS_DDLControl.DS_957_2506 e on e.id_up=a.id -- ������ �������� (���������)
--			left join KS_DDLControl.DS_1279_3189 e1  on e1.DS_3169 = a.id 
--			left join KS_DDLControl.DS_1279_3196 e  on e.id_up = e1.id 
			Left Join #sprSource S on e.CL_2572 = S.id         --�����_��������� ��������������
			left join KS_DDLControl.CL_960_2491 q on q.id=a.CL_2633
			
	
	group by
		a.id
		, a.AT_2529
		, a.CL_2533
		, a.CL_3252 -- ��� ��������������
		, e.cl_2556
		, a.documentid
		, a.AT_2538
		, a.AT_2539
		, a.CL_2640
		, a.CL_3253
		, b.DS_2654
		, AT_2656 
		, AT_2658 
		, AT_2661 
		, AT_1998 
		, a.at_2837
		, M_2879
		, q.AT_2544
		, a.AT_3349

	select
		  a.[id]
		, a.[AT_2529]
		, a.[CL_2533]
		, a.[CL_3252]
		, a.[cl_2556]
		, a.[documentid]
		, a.[AT_2538]
		, a.[AT_2539]
		, a.[CL_2640]
		, a.[CL_3253]
		, a.[DS_2654]
		, a.[SCode]
		, a.[SNaim]
		, a.[SLock]
		, a.[Source]
		, a.[���]
		, a.[���13]
		, a.[���14]
		, a.[���15]  
		, a.[���16]
		, a.[���17]
		, a.[���18]
		, a.[���19]
		, a.[���20]
		, a.[���21]
		, a.[���22]
		, a.[���23]
		, a.[���24]
		, a.[���25]
		, a.[Zakon]
		, b.[������������] as [��]
		, b.[id��]
		, a.AT_2544 
		, a.AT_3349
		
	into #GMP
	from
		#GMP_old a inner join #GpName b on a.id = b.[��_���������]
		
--select * from #GpName
--select * from #sprVed
--select * from #GMP

	---------------
	-- ������������ --
	---------------
	select
		gp
		,tab.id as idppgp
		,[����]
		,action
		,#prg.id as orderPPGP
		,tab.DocumentId
		,[DateInt]
		,[DBegin]
		,[DEnd]
		,[SCode]
		,[SNaim]
		,[SLock]
		,[�����������.������������]
		,[���]
		,[id��]
--		,DS_2654 as [����������� ��������]
--		,M_2936 as [����������2020]
--		,AT_2544
        ,[�����]
		
	into [#�����������]
	from
		#prg left join
		(select 
			  a.id
			, a.[��] as [����]
			, a.DocumentId
			, a.AT_2529 as [DateInt]
			, a.AT_2538 as DBegin
			, a.AT_2539 as DEnd
			, a.[SCode]
			, a.[SNaim]
			, a.[SLock]
			--,#sprVed.AT_1797 as [�����������.������������]
			,#sprVed1.AT_3200 as [�����������.������������]
			,a.[���]
			,a.[id��]
--			,a.ds_2654
--			,b.M_2936
--			,a.AT_2544�
            ,a.AT_3349 as [�����]

		from 
			#GMP a
			--left join #sprVed on #sprVed.ID = a.CL_2533
			left join #sprVed1 on #sprVed1.id = a.CL_3252
--			left join KS_DDLControl.DS_957_2478 b on b.id_up=a.id
		where
			a.[id��] = @idPPGP  --������������ 

		) tab on 1=1

		declare @Post varchar(max)
		select @Post = Post from [#sprgp] where id =@GP
		--select @Post = M_2879 from KS_DDLControl.DS_957_2475 where CL_2640 = @GP and AT_2845 = @Ver

		---------
		--���� --
		---------
		select
			  a.id
			, a.DocumentID
			, a.[��] as [����]
			, DS_2654 as [����������� ��������]
			, a.AT_2538 as DBegin
			, a.AT_2539 as DEnd
			, a.[SCode]
			, a.[SNaim]
			, a.[SLock]
			--, #sprVed.AT_1797 as [�����������.������������]
			,#sprVed1.AT_3200 as [�����������.������������]
			, a.[���]
		into [#����]
		from
			#GMP a inner join [#�����������] b on a.DS_2654 = b.idppgp and b.action ='���� ������������'
			--left join #sprVed on #sprVed.ID = a.CL_2533
			left join #sprVed1 on #sprVed1.id = a.CL_3252
		where
			a.DocumentId = 222
        
		-----------
		--������ --
		-----------
		
		--if (select count(*) from  [#������] �  )=1  begin �.���=''  end	as [���]
 
		select
			  a.id
			, a.DocumentID
			--, a.[��] as [������]
			, (case when LEFT(a.���,1)='0' then '' ELSE LEFT(a.���,1) END)+SUBSTRING(a.���,2,4)+'. '+a.[��] as [������]
			, DS_2654 as [����������� ��������]
			, a.AT_2538 as DBegin
			, a.AT_2539 as DEnd
			, a.[SCode]
			, a.[SNaim]
			, a.[SLock]
			--, #sprVed.AT_1797 as [�����������.������������]
			,#sprVed1.AT_3200 as [�����������.������������]
		    , a.[���]
	
		into [#������]
		from
			#GMP a inner join [#�����������] b on a.DS_2654 = b.idppgp and b.action ='������ ������������'
			--left join #sprVed on #sprVed.ID = a.CL_2533
			left join #sprVed1 on #sprVed1.id = a.CL_3252
		where
			a.DocumentId = 229
	-- select * from #������
	
 
		---------------------------
		-- ���������� ����� --
		---------------------------
		select
			  a.id
			, a.DocumentID
			, a.[��] as [����������������]
			, a1.[����������� ��������] as [����������� ��������]
			, a1.��� as [��� ������]
			, '���������� ����� ������������' as action
			, 1 as orderPPgp
			, a.AT_2538 as DBegin
			, a.AT_2539 as DEnd
			,a.[SCode]
			,a.[SNaim]
			,a.[SLock]
			--,#sprVed.AT_1797 as [�����������.������������]
            ,#sprVed1.AT_3200 as [�����������.������������]			
			, a.[Source]
			, a.[���13]
			, a.[���14]
			, a.[���15]  
			, a.[���16] 
			, a.[���17]  
			, a.[���18]  
			, a.[���19]  
			, a.[���20]  
			, a.[���21]  
			, a.[���22]
		    , a.[���23]
		    , a.[���24]
		    , a.[���25]
			, a.[���]
			--, a.AT_2837
			,b.M_2936 as [����������2020]
			,a.AT_2544
		into [#���������������]
		from
			#GMP a
			inner join [#������] a1 on a1.id = a.DS_2654   
		--left join #sprVed on #sprVed.ID = a.CL_2533 --�����������
		  left join #sprVed1 on #sprVed1.id = a.CL_3252
		  left join KS_DDLControl.DS_957_2478 b on b.id_up=a.id
		where
			a.DocumentId = 252	
--select * from [#���������������]
		
		---------------
		--���������� --
		---------------
		select
			  a.id
			, a.DocumentID
			, (case when LEFT(a.���,1)='0' then '' ELSE LEFT(a.���,1) END)+SUBSTRING(a.���,2,4)+'. '+a.[��] as [����������]
			, a1.[����������� ��������] as [����������� ��������]
			, a.AT_2538 as DBegin
			, a.AT_2539 as DEnd
			,a.[SCode]
			,a.[SNaim]
			,a.[SLock]
			,#sprVed.AT_1797 as [�����������.������������]
			--,#sprVed1.AT_3200 as [�����������.������������]
			, a.[���]
			,a.AT_2544
		into [#����������]
		from
			#GMP a
			inner join  [#����] a1 on a1.id = a.DS_2654
			left join #sprVed on #sprVed.ID = a.CL_2533
			--left join #sprVed1 on #sprVed1.id = a.CL_3254
		where
			a.DocumentId = 252			
			
--select * from #GMP
--select * from [#�����������]
--select * from [#����������]  
--select * from [#���������������]  

		--------------------------------------------------------
		--����� ������� �������� ������� �� �������������� ���� --
		--------------------------------------------------------
		create table #Volume 
			(  id���� int
			, [Source] varchar(100)
			, [���13] numeric (20,2)
			, [���14] numeric (20,2)
			, [���15] numeric (20,2)
			, [���16] numeric (20,2)
			, [���17] numeric (20,2)
			, [���18] numeric (20,2)
			, [���19] numeric (20,2)
			, [���20] numeric (20,2)
			, [���21] numeric (20,2)
			, [���22] numeric (20,2)
		    , [���23] numeric (20,2)
		    , [���24] numeric (20,2)
		    , [���25] numeric (20,2))
		--insert into #Volume select [Source], sum([���13]), sum([���14]), sum([���15]), sum([���16]), sum([���17]) from #GMP where [Source] is not null group by [Source]
		insert into #Volume 
		select  DS_2654 as  id����
			,[Source]	
			, sum(isnull([���13],0.00))
			, sum(isnull([���14],0.00))
			, sum(isnull([���15],0.00))
			, sum(isnull([���16],0.00))
			, sum(isnull([���17],0.00)) 
			, sum(isnull([���18],0.00)) 
			, sum(isnull([���19],0.00)) 
			, sum(isnull([���20],0.00)) 
			, sum(isnull([���21],0.00)) 
			, sum(isnull([���22],0.00)) 
			, sum(isnull([���23],0.00)) 
			, sum(isnull([���24],0.00)) 
			, sum(isnull([���25],0.00)) 
		from 
			#GMP 
		group by 
			 DS_2654
			,[Source]

		delete from #Volume where [Source] is null
		update #Volume set Source = '����' where Source ='������������ ����' --not in ('�������','�����������')
		update #Volume set Source = ' ' + Source where Source != '����'
		
		
--select * from #GMP
--select * from [#�����������]
--select * from #Volume

		-------------------
		-- ������������� �_�����������-
		-------------------
		select distinct
			--#sprVed.AT_1797 as [�����������.������������]
			e.AT_3200 as [�����������.������������]
							
			
			, identity(int, 1,1) as idcontent
		into [#Soisp] 
		from 
			[#GMP] a
			--left join #sprVed on #sprVed.id = a.CL_2556 -- ����� ���������
			left join #sprVed1 e on e.id = a.CL_3252 -- �_������������
			--left join #sprVed on #sprVed.id = a.CL_2533 -- ����� ���������
			
		
			-------------------
		-- ������������� ���� � ��-
		-------------------
		select distinct
			--#sprVed.AT_1797 as [�����������.������������]
			e.AT_3200 as [�����������.������������]
			,c.CL_3205
						
			
			, identity(int, 1,1) as idcontent
		into [#Soisp2] 
		from 
			[#GMP] a
			
			left join KS_DDLControl.DS_957_3235 c on a.id =c.id_up
			left join #sprVed1 e on e.id = c.CL_3205 
			
			
--select * from [#GMP]	
--select * from #sprVed
--select * from #sprVed1
--select * from [#Soisp]
--select * from [#Soisp2]
--select * from [#�����������]
--if(select COUNT(*) FROM #Soisp) > 1
--begin

	delete from #Soisp where [�����������.������������] in (select distinct [�����������.������������]  from [#�����������])
       
    delete from #Soisp2 where [�����������.������������] in (select distinct [�����������.������������]  from [#�����������])   
--end	
	---------------------------------------------
		--�������� ������ � �������������� ������� --
		---------------------------------------------
		
		insert into #temp_resultPPGP 
		select 
			  a.idppgp	--��1.�������1
			, a.[����]	--��1.�������1.���������
			, right('0' + convert(varchar,a.orderPPGP),2)	--��2.�������2.������������
			, a.action	--��2.�������2.������������.���������
			, coalesce(c1.id,     t1.id,       i1.id, s.idcontent,           m1.id, z.idcontent )           as idContent		--��3.�������2.���������� - ��� ��� �������� � ��
			, (case 
			    when a.action = '������������� ����������� ������������' then a.[�����������.������������] 
			    when a.action = '������������� ������������' then s.[�����������.������������] 
--			    when @GP=6570 and a.action = '�������������  ������������' then z.[�����������.������������]  
                when  a.action = '�������������  ������������' then z.[�����������.������������]  
				
				when a.action = '����� � ����� ���������� ������������'  and [�����]<>'' then left(convert(varchar,a.DBegin),4) + '-' + left(convert(varchar,a.DEnd),4) + ' ����' +'
				'+ [�����]
				when a.action = '����� � ����� ���������� ������������'   then left(convert(varchar,a.DBegin),4) + '-' + left(convert(varchar,a.DEnd),4) + ' ����' 
				
				
				when a.action = '��������� ���������� ���������� ������������' then  q.[AT_2644] else coalesce(c1.[����], t1.[������], i1.[����������], m1.[����������������])
			  end) as [Content] 	--��3.�������2.����������.���������
			, coalesce(c1.[���], t1.[���], i1.[���],m1.[��� ������]) as [���_Content]
			, isnull(a.DBegin, 0)
			--, isnull(a.DEnd  , 20201231)
			
			,(case 
			    when a.action = '���������� ����� ������������' then isnull (m1.DEnd  , 20201231)
				when a.action = '������� ���������� ������������' then isnull (i1.DEnd  , 20201231)
				else isnull(a.DEnd  , 20201231)
				 end)
			
			
			, @aDateInt as ParamDate
			, a.[SNaim]

			, v.[Source]
			, isnull(v.[���13]  , 0.00)
			, isnull(v.[���14]  , 0.00)
			, isnull(v.[���15]  , 0.00)
			, isnull(v.[���16]  , 0.00)
			, isnull(v.[���17]  , 0.00) 
			, isnull(v.[���18]  , 0.00) 
			, isnull(v.[���19]  , 0.00) 
			, isnull(v.[���20]  , 0.00) 
			, isnull(v.[���21]  , 0.00)
			, isnull(v.[���22]  , 0.00)
			, isnull(v.[���23]  , 0.00)
			, isnull(v.[���24]  , 0.00)
			, isnull(v.[���25]  , 0.00)
			, a.[���]
			, coalesce(c1.documentid, t1.documentid, i1.documentid)	
			, @Post
	        , m1. [���]
	       -- , a.[id��]
	     	,(case 
			    when a.action = '���������� ����� ������������' then m1.AT_2544
				when a.action = '������� ���������� ������������' then i1. AT_2544
				 end) as [�����������] 
	     	
	     	
--	     	,m1.AT_2544
--	        ,m1.����������2020
--	        ,m1.����������������
		from 
			[#�����������] a --�������1
			left join [#Soisp] s  on a.action = '������������� ������������' 
			left join [#Soisp2] z  on a.action = '�������������  ������������' 
			left join [#����] c1 on       a.idppgp = c1.[����������� ��������] and a.action = '���� ������������'           --�������2
			left join [#������] t1 on     a.idppgp = t1.[����������� ��������] and a.action = '������ ������������'         --�������2
		left join [#���������������] m1 on a.idppgp  = m1.[����������� ��������] and a.action = '���������� ����� ������������'  
		--or  a.action = '��������� ���������� ����������  ������������ '   --�������2
			left join [#����������] i1 on a.idppgp = i1.[����������� ��������] and a.action = '������� ���������� ������������'     --�������2
			Left Join KS_DDLControl.DS_957_2619 q on a.idppgp = q.id_up and a.action='��������� ���������� ���������� ������������' -- �������_�����
			left join (select 
			--id����
			 [Source]
--		    , sum(round(isnull([���13],0.00)/1000,2)) as [���13]
--			, sum(round(isnull([���14],0.00)/1000,2)) as [���14]
--			, sum(round(isnull([���15],0.00)/1000,2)) as [���15]
--			, sum(round(isnull([���16],0.00)/1000,2)) as [���16]
--			, sum(round(isnull([���17],0.00)/1000,2)) as [���17]
--			, sum(round(isnull([���18],0.00)/1000,2)) as [���18]
--			, sum(round(isnull([���19],0.00)/1000,2)) as [���19]
--			, sum(round(isnull([���20],0.00)/1000,2)) as [���20]
--			, sum(round(isnull([���21],0.00)/1000,2)) as [���21]
--			, sum(round(isnull([���22],0.00)/1000,2)) as [���22]
--			, sum(round(isnull([���23],0.00)/1000,2)) as [���23]
--			, sum(round(isnull([���24],0.00)/1000,2)) as [���24]
--			, sum(round(isnull([���25],0.00)/1000,2)) as [���25]

            , sum(isnull([���13],0.00)/1000) as [���13]
			, sum(isnull([���14],0.00)/1000) as [���14]
			, sum(isnull([���15],0.00)/1000) as [���15]
			, sum(isnull([���16],0.00)/1000) as [���16]
			, sum(isnull([���17],0.00)/1000) as [���17]
			, sum(isnull([���18],0.00)/1000) as [���18]
			, sum(isnull([���19],0.00)/1000) as [���19]
			, sum(isnull([���20],0.00)/1000) as [���20]
			, sum(isnull([���21],0.00)/1000) as [���21]
			, sum(isnull([���22],0.00)/1000) as [���22]
			, sum(isnull([���23],0.00)/1000) as [���23]
			, sum(isnull([���24],0.00)/1000) as [���24]
			, sum(isnull([���25],0.00)/1000) as [���25]
	
			
			from #Volume group by [Source]) v on a.orderPPGP = 09 --and v.id����=a.idppgp
			
--delete from #temp_resultPPGP where GpDEnd < @aDateInt

			delete from #temp_resultPPGP  where @GP='6570' and orderPPGP = 02 

			delete from #temp_resultPPGP where @GP<>'6570' and orderPPGP = 03 
			
			drop table #gmp_old
			drop table #gmp
			drop table #�����������
			drop table #������
			drop table #���������������
			drop table #����
			drop table #����������
			drop table #Volume
			drop table #soisp
			drop table #soisp2
			
	    FETCH NEXT FROM cur INTO @idPPGP
	END
	
	
	--��������� ������
	CLOSE cur
   	DEALLOCATE cur
	
	
	
	


end

if @ToTable=1
begin
	insert into #ToTable select * from #temp_resultPPGP  
end else
begin
	select * from #temp_resultPPGP order by id
end