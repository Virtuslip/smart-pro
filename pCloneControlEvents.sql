alter proc [KS_DDLControl].[p_CloneControlEvents](@GP int=6577, @Ver varchar(10)='07', @VerTo varchar(10)='11', @dateDocEvent varchar(20) = '21.03.2019', @dateTarget varchar(20) = '22.11.2019')
    as
    begin
        set nocount on
        set transaction isolation level read uncommitted
        declare
            @docIdCe int --Код документа контрольные события
        set @docIdCe = 337
        declare
            @UnitGMP uniqueidentifier
        set @UnitGMP = newid()

        declare
            @aDateInt varchar(8)
        SET @aDateInt =
                    SUBSTRING(@dateTarget, 7, 4) + SUBSTRING(@dateTarget, 4, 2) + SUBSTRING(@dateTarget, 1, 2)
        --select @aDateInt = convert(varchar,dbo.at_3255getworkdate(),112)

        declare
            @dateEvent varchar(8)
        SET @dateEvent =
                    SUBSTRING(@dateDocEvent, 7, 4) + SUBSTRING(@dateDocEvent, 4, 2) + SUBSTRING(@dateDocEvent, 1, 2)
        begin try
            begin transaction
                --Контрольные события (источник)
                select gmp.*, newid() as new_id
                into #source_control_events
                from KS_DDLControl.DS_957_2475 gmp
                where gmp.documentid = @docIdCe
                  and gmp.cl_3258 = @GP
                  and gmp.at_2529 = @dateEvent
                  and gmp.at_2845 = @Ver;


                --Вставка в документ Контрольные события
                insert into ks_ddlcontrol.ds_957_2475( documentid
                                                     , unit
                                                     , guid
                                                     , at_3272
                                                     , at_3273
                                                     , at_3274
                                                     , cl_3253
                                                     , at_3256
                                                     , at_2529
                                                     , at_2537
                                                     , at_2538
                                                     , at_2539
                                                     , at_2646
                                                     , at_2837
                                                     , at_2845
                                                     , at_3130
                                                     , at_3275
                                                     , at_3303
                                                     , at_3304
                                                     , at_3338
                                                     , at_3349
                                                     , at_3388
                                                     , m_2879
                                                     , cl_2633
                                                     , cl_2533
                                                     , cl_3252
                                                     , cl_2640
                                                     , cl_3258
                                                     , cl_3257
                                                     , cl_3445
                                                     , cl_3459)
                select s.documentid
                     , @UnitGMP
                     , s.new_id
                     , s.at_3272
                     , s.at_3273
                     , s.at_3274
                     , 8
                     , s.at_3256
                     , @aDateInt
                     , s.at_2537
                     , s.at_2538
                     , s.at_2539
                     , s.at_2646
                     , s.at_2837
                     , @VerTo
                     , s.at_3130
                     , s.at_3275
                     , s.at_3303
                     , s.at_3304
                     , s.at_3338
                     , s.at_3349
                     , s.at_3388
                     , s.m_2879
                     , s.cl_2633
                     , s.cl_2533
                     , s.cl_3252
                     , s.cl_2640
                     , s.cl_3258
                     , s.cl_3257
                     , s.cl_3445
                     , s.cl_3459
                from #source_control_events s

                --Получаем вставленные записи
                select gmp.*
                into #inserted_control_events
                from KS_DDLControl.DS_957_2475 gmp
                         inner join #source_control_events s on s.new_id = gmp.guid

                --Вставка в перечень контрольных событий
                insert into ks_ddlcontrol.ds_957_2521( id_up
                                                     , unit
                                                     , guid
                                                     , at_3407
                                                     , at_3411
                                                     , at_3412
                                                     , cl_2758
                                                     , at_2759
                                                     , at_3386
                                                     , cl_3260
                                                     , at_3288
                                                     , at_2756
                                                     , at_2755
                                                     , at_2760
                                                     , cl_2860
                                                     , cl_2926
                                                     , at_3118
                                                     , at_3119)
                select t.id
                     , @UnitGMP
                     , newid()
                     , ce.at_3407
                     , ce.at_3411
                     , ce.at_3412
                     , ce.cl_2758
                     , ce.at_2759
                     , ce.at_3386
                     , ce.cl_3260
                     , ce.at_3288
                     , ce.at_2756
                     , ce.at_2755
                     , ce.at_2760
                     , ce.cl_2860
                     , ce.cl_2926
                     , ce.at_3118
                     , ce.at_3119
                from ks_ddlcontrol.ds_957_2521 ce
                         inner join #source_control_events sce on sce.id = ce.id_up
                         inner join #inserted_control_events t on t.guid = sce.new_id

                --                 select * from #source_control_events

                --Получаем все мероприятия госпрограммы источника
                select a.ds_2654 as old_gmp, targetGmp.id as new_gmp, e.CL_2640 as eventCode
                into #targetEvents
                from ks_ddlcontrol.ds_957_2625 a
                         inner join #source_control_events sce on sce.id = a.id_up
                         left join ks_ddlcontrol.ds_957_2475 e on (a.ds_2654 = e.id)
                         left join (select max(id) as id, CL_2640
                                    from ks_ddlcontrol.ds_957_2475
                                    where at_2845 = @VerTo
                                    group by CL_2640) targetGmp
                                   on (e.CL_2640 = targetGmp.CL_2640)


                --select * from #targetEvents
                --Вставляем вышестоящие документы
                insert into ks_ddlcontrol.ds_957_2625( id_up
                                                     , unit
                                                     , guid
                                                     , at_2650
                                                     , ds_2654)
                select t.id
                     , @UnitGMP
                     , newid()
                     , a.at_2650
                     , targetGmp.new_gmp
                from ks_ddlcontrol.ds_957_2625 a
                         inner join #source_control_events gmp on (a.id_up = gmp.id)
                         inner join #inserted_control_events t on t.guid = gmp.new_id
                         inner join #targetEvents targetGmp on (targetGmp.old_gmp = a.ds_2654)


                ---Удаляем контрольные события по которым отсуствуют вышестоящие документы
                select ice.id
                into #delete_ks_id
                from #inserted_control_events ice
                         left join ks_ddlcontrol.ds_957_2625 a on a.id_up = ice.id
                where a.ds_2654 is null

                delete from ks_ddlcontrol.ds_957_2625 where id_up in (select * from #delete_ks_id)
                delete from ks_ddlcontrol.ds_957_2521 where id_up in (select * from #delete_ks_id)
                delete from ks_ddlcontrol.ds_957_2475 where id in (select * from #delete_ks_id)

                select * from ks_ddlcontrol.ds_957_2625 where unit = @UnitGMP


                --select * from #target_events
            commit transaction
            --rollback transaction
        end try
        begin catch
            declare
                @error_message varchar(4000), @error_severity int, @error_state int, @error_line int
            select @error_message = ERROR_MESSAGE(),
                   @error_severity = ERROR_SEVERITY(),
                   @error_state = ERROR_STATE(),
                   @error_line = ERROR_LINE()
            set @error_message = @error_message + '%s' + '%d'
            raiserror (@error_message, @error_severity, @error_state, N' Ошибка в строке: ', @error_line)
            rollback transaction
        end catch
    end