CREATE proc [KS_DDLControl].[xp_PrgPpGp_3] (@nmode int=0, @GP int=6560, @PPGP int=6718, @Ver varchar(2)='14', @Result varchar(max) = 'Да', @ToTable int=0)
as

    create table #temp_resultPPGP
    (
        idppgp int
        , [ППГП] varchar(2000)
        , orderPPGP varchar(2)
        , action varchar(8000)
        , idContent int
        , [Content] varchar(max)
        , [код_Content] varchar(50)
        , GpDBegin int
        , GpDEnd int
        , ParamDate int
        , [S_gp] varchar(500)
        , [Source] varchar(100)
        , [Код источника финансирования] varchar(100)
        , [ОЧГ13] numeric (20,4)
        , [ОЧГ14] numeric (20,4)
        , [ОЧГ15] numeric (20,4)
        , [ОЧГ16] numeric (20,4)
        , [ОЧГ17] numeric (20,4)
        , [ОЧГ18] numeric (20,4)
        , [ОЧГ19] numeric (20,4)
        , [ОЧГ20] numeric (20,4)
        , [ОЧГ21] numeric (20,4)
        , [ОЧГ22] numeric (20,4)
        , [ОЧГ23] numeric (20,4)
        , [ОЧГ24] numeric (20,4)
        , [ОЧГ25] numeric (20,4)
        , [КодППГП] varchar(20)
        , documentid int
        , Post varchar(max)
        , id int identity (1,1)
        , [ПорядковыйНомерПЗППГП] varchar(10)
        --	, [idППГП] varchar(20)
        , [ЕдиницаИзмерения] varchar(200)
--			, [Показатель2020] numeric (20,2)
        , [ПоказательЗадачи] varchar(2000)

    )


    if @nmode <> -1
        begin
            set nocount on
            set transaction isolation level read uncommitted
-- Пример запуска:  exec [KS_DDLControl].[xp_PrgPpGp_3] @nmode=0, @GP=6566, @PPGP=7319, @Ver='12', @Result = 'Да'

            declare @PPGPDocID int
            declare @aDateInt int
            select  @aDateInt = convert(varchar,GetDate(),112)

--select  @aDateInt = convert(varchar,dbo.getworkdate(),112)

            create table #prg (gp varchar(30), action varchar(8000), id int identity (1,1))
            insert into  #prg select 'Подпрограммы', 'Ответственный исполнитель и (или) соисполнители'
            insert into  #prg select 'Подпрограммы', 'Соисполнители подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Соисполнители  подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Цели подпрограммы'

            insert into  #prg select 'Подпрограммы', 'Задачи подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Целевые индикаторы подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Показатели задач подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Этапы и сроки реализации подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Параметры финансового обеспечения всего, в том числе по годам реализации подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Параметры финансового обеспечения всего, в том числе по годам реализации подпрограммы'
            insert into  #prg select 'Подпрограммы', 'Ожидаемые результаты реализации подпрограммы'

            ------------
            --Доступы --
            ------------
            Declare @user_type int
            select
                    @user_type = user_type -- если user_type = 4, то это администратор, для него доступы проверять не надо
            from s_users where loginame = suser_sname()

            Declare @GmpPermission int
            select
                    @GmpPermission = AllowPermission --Признак назначать права
            from dbo.ENTITY
            where ID = 956  --Справочники ГМП

            Declare @VedPermission int
            select
                    @VedPermission = AllowPermission --Признак назначать права
            from dbo.ENTITY
            where ID = 864  --Справочник Ведомства

            Declare @SourcePermission int
            select
                    @SourcePermission = AllowPermission --Признак назначать права
            from dbo.ENTITY
            where ID = 894  --Справочник Источники финансирования

            ---------------------------------------------------------
            --Выборка актуальных значений справочника Госпрограммы --
            ---------------------------------------------------------
            declare @Cl table (id int, [Наименование] varchar(2000), [DBegin] int, [Код] varchar(20), Post varchar(max))
            insert into @Cl
            select
                a.id
                 --, isnull(AT_2641, AT_2526) as [ГП]
                 , AT_2526 as [ГП]
                 , b.at_3398 as [DBegin]
                 , a.AT_2524
                 , a.AT_2924
            from
                KS_DDLControl.CL_956_2470 a
                    left join (select * from ks_ddlcontrol.cl_956_3452 where at_3398 <= @aDateInt) b on a.id = b.id_up

            select tab2.id, tab2.[Наименование] as [ГП], tab2.[Код], tab2.Post into #sprGp from
                (select id, max(DBegin) as DBegin from @Cl group by id) tab1
                    inner join
                (select id, DBegin, [Наименование], [Код], Post from @Cl) tab2
                on tab1.id =tab2.id and isnull(tab1.DBegin,0) = isnull(tab2.DBegin,0)
            --select * from #sprGp

            --Удаление элементов на которые отсутствует доступ
            if isnull(@GmpPermission,0) > 0 and @user_type != 4
                begin
                    delete from #sprGp where id not in (select CLID from KS_DDLControl.CLA_956 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
                end

            -------------------------------------------
            --Выборка значений справочника ИсполнителиГМП --
            -------------------------------------------
            select a.*, a1.AT_3200, a1.AT_3201, a1.AT_3202
            into #sprVed1
            from KS_DDLControl.CL_1280_3226 a
                     left join KS_DDLControl.CL_1280_3231 a1 on a.id = a1.id_up


            --select * from    #sprVed1

            -------------------------------------------
            --Выборка значений справочника Ведомства --
            -------------------------------------------
            select a.*, a1.AT_1797, a1.AT_1796, a1.AT_1930
            into #sprVed
            from KS_DDLControl.CL_864_1710 a
                     left join KS_DDLControl.CL_864_3145 a1 on a.id = a1.id_up
            --from KS_DDLControl.CL_1280_3226 a
            --left join KS_DDLControl.CL_864_3145 a1 on a.cl_3232 = a1.id_up

--select * from #sprVed

/*
	--Удаление элементов на которые отсутствует доступ
	if isnull(@VedPermission,0) > 0 and @user_type != 4
	begin
		delete from #sprVed where id not in (select CLID from KS_DDLControl.CLA_864 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
	end
*/
            ----------------------------------------------------------
            --Выборка значений справочника Источники финансирования --
            ----------------------------------------------------------
            select * into #sprSource from KS_DDLControl.CL_894_1936
            --Удаление элементов на которые отсутствует доступ
            if isnull(@SourcePermission,0) > 0 and @user_type != 4
                begin
                    delete from #sprSource where id not in (select CLID from KS_DDLControl.CLA_894 CLA inner join dbo.USERS_GROUPS GRP on CLA.usergroupid = GRP.[groups] where GRP.[users] = user_id(suser_sname()))
                end

            -------------------------------------------------
            -- Выборка актуальных документов источника ГМП --2646 - ИдНовойВерсии
            -------------------------------------------------
            create table #GpName
            (
                [ИД_Документа] int
                , [Код] varchar(50)
                , [Наименование] varchar(2000)
                , [Тип] varchar(10)
                , [idГП] int
            )

            insert into #GpName exec [ks_ddlcontrol].[xp_sprGpOnDoc] @nmode=0

            select
                a.id
                 , a.AT_2529
                 , a.CL_2533
                 , a.CL_3252
                 , a.documentid
                 , a.AT_2538
                 , a.AT_2539
                 , a.CL_2640
                 , a.CL_3253
                 , b.DS_2654   --Вышестоящий документ
                 , AT_2656 as [SCode]
                 , AT_2658 as [SNaim]
                 , AT_2661 as [SLock]
                 , AT_1998 as [Source]
                 , a.at_2837 as [Код]
                 , at_1997 as [Код источника финансирования]
                 , sum(isnull(e.M_2928,0.0)) as [ОЧГ13]
                 , sum(isnull(e.M_2929,0.0)) as [ОЧГ14]
                 , sum(isnull(e.M_2930,0.0)) as [ОЧГ15]
                 , sum(isnull(e.M_2931,0.0)) as [ОЧГ16]
                 , sum(isnull(e.M_2932,0.0)) as [ОЧГ17]
                 , sum(isnull(e.M_2937,0.0)) as [ОЧГ18]
                 , sum(isnull(e.M_2938,0.0)) as [ОЧГ19]
                 , sum(isnull(e.M_2939,0.0)) as [ОЧГ20]
                 , sum(isnull(e.M_3073,0.0)) as [ОЧГ21]
                 , sum(isnull(e.M_3344,0.0)) as [ОЧГ22]
                 , sum(isnull(e.M_3345,0.0)) as [ОЧГ23]
                 , sum(isnull(e.M_3346,0.0)) as [ОЧГ24]
                 , sum(isnull(e.M_3347,0.0)) as [ОЧГ25]
                 , M_2879 as [Zakon]
                 , e.cl_2556
                 , q.AT_2544
                 , a.AT_3349
                 , p.at_3546

            into #GMP_old
            from

                [ks_ddlControl].[GetDocGP](@GP, @PPGP , @Ver) o inner join KS_DDLControl.DS_957_2475 a on o.id = a.id
                                                                left join KS_DDLControl.DS_957_2625 b on a.id = b.id_up
                                                                Left Join KS_DDLControl.CL_1008_2633 c on a.CL_3253 = c.id   --Статус
                                                                Left Join KS_DDLControl.DS_957_2506 e on e.id_up=a.id -- оценка расходов (ведомства)
--			left join KS_DDLControl.DS_1279_3189 e1  on e1.DS_3169 = a.id
--			left join KS_DDLControl.DS_1279_3196 e  on e.id_up = e1.id
                                                                Left Join #sprSource S on e.CL_2572 = S.id         --Макет_Источники финансирования
                                                                left join KS_DDLControl.CL_960_2491 q on q.id=a.CL_2633
                                                                left join ks_ddlcontrol.ds_957_3639 p on p.id_up=a.id

            group by
                a.id
                   , a.AT_2529
                   , a.CL_2533
                   , a.CL_3252 -- для ИсполнителиГМП
                   , e.cl_2556
                   , a.documentid
                   , a.AT_2538
                   , a.AT_2539
                   , a.CL_2640
                   , a.CL_3253
                   , b.DS_2654
                   , AT_2656
                   , AT_2658
                   , AT_2661
                   , AT_1998
                   , a.at_2837
                   , M_2879
                   , q.AT_2544
                   , a.AT_3349
                   , at_1997
                   , at_3546

            select
                a.[id]
                 , a.[AT_2529]
                 , a.[CL_2533]
                 , a.[CL_3252]
                 , a.[cl_2556]
                 , a.[documentid]
                 , a.[AT_2538]
                 , a.[AT_2539]
                 , a.[CL_2640]
                 , a.[CL_3253]
                 , a.[DS_2654]
                 , a.[SCode]
                 , a.[SNaim]
                 , a.[SLock]
                 , a.[Source]
                 , a.[Код источника финансирования]
                 , a.[Код]
                 , a.[ОЧГ13]
                 , a.[ОЧГ14]
                 , a.[ОЧГ15]
                 , a.[ОЧГ16]
                 , a.[ОЧГ17]
                 , a.[ОЧГ18]
                 , a.[ОЧГ19]
                 , a.[ОЧГ20]
                 , a.[ОЧГ21]
                 , a.[ОЧГ22]
                 , a.[ОЧГ23]
                 , a.[ОЧГ24]
                 , a.[ОЧГ25]
                 , a.[Zakon]
                 , b.[Наименование] as [ГП]
                 , b.[idГП]
                 , a.AT_2544
                 , a.AT_3349
                 , at_3546

            into #GMP
            from
                #GMP_old a inner join #GpName b on a.id = b.[ИД_Документа]

            --select * from #GpName
--select * from #sprVed
--select * from #GMP

            ---------------
            -- Подпрограммы --
            ---------------
            select
                gp
                 ,tab.id as idppgp
                 ,[ППГП]
                 ,action
                 ,#prg.id as orderPPGP
                 ,tab.DocumentId
                 ,[DateInt]
                 ,[DBegin]
                 ,[DEnd]
                 ,[SCode]
                 ,[SNaim]
                 ,[SLock]
                 ,[Исполнитель.Наименование]
                 ,[Код]
                 ,[idГП]
--		,DS_2654 as [Вышестоящий документ]
--		,M_2936 as [Показатель2020]
--		,AT_2544
                 ,[Этапы]

            into [#Подрограмма]
            from
                #prg left join
                (select
                     a.id
                      , a.[ГП] as [ППГП]
                      , a.DocumentId
                      , a.AT_2529 as [DateInt]
                      , a.AT_2538 as DBegin
                      , a.AT_2539 as DEnd
                      , a.[SCode]
                      , a.[SNaim]
                      , a.[SLock]
                      --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                      ,#sprVed1.AT_3200 as [Исполнитель.Наименование]
                      ,a.[Код]
                      ,a.[idГП]
--			,a.ds_2654
--			,b.M_2936
--			,a.AT_2544б
                      --,a.AT_3349 as [Этапы]
                      ,a.at_3546 as  [Этапы]

                 from
                     #GMP a
                         --left join #sprVed on #sprVed.ID = a.CL_2533
                         left join #sprVed1 on #sprVed1.id = a.CL_3252
--			left join KS_DDLControl.DS_957_2478 b on b.id_up=a.id
                 where
                         a.[idГП] = @PPGP  --Подпрограмма

                    /*case when @PPGP ='___Все___' then 1
                    else charindex('@#$'+cast(a.[idГП]  as varchar(20))+'@#$','@#$'+@PPGP +'@#$')
                    end>0--@kod_vr
            */
                ) tab on 1=1

            declare @Post varchar(max)
            select @Post = Post from [#sprgp] where id =@GP
            --select @Post = M_2879 from KS_DDLControl.DS_957_2475 where CL_2640 = @GP and AT_2845 = @Ver

            ---------
            --Цель --
            ---------
            select
                a.id
                 , a.DocumentID
                 , a.[ГП] as [Цели]
                 , DS_2654 as [Вышестоящий документ]
                 , a.AT_2538 as DBegin
                 , a.AT_2539 as DEnd
                 , a.[SCode]
                 , a.[SNaim]
                 , a.[SLock]
                 --, #sprVed.AT_1797 as [Исполнитель.Наименование]
                 ,#sprVed1.AT_3200 as [Исполнитель.Наименование]
                 , a.[Код]
            into [#Цели]
            from
                #GMP a inner join [#Подрограмма] b on a.DS_2654 = b.idppgp and b.action ='Цели подпрограммы'
                    --left join #sprVed on #sprVed.ID = a.CL_2533
                       left join #sprVed1 on #sprVed1.id = a.CL_3252
            where
                    a.DocumentId = 222

            -----------
            --Задачи --
            -----------

            --if (select count(*) from  [#Задачи] с  )=1  begin с.Код=''  end	as [КОД]

            select
                a.id
                 , a.DocumentID
                 --, a.[ГП] as [Задачи]
                 , (case when LEFT(a.Код,1)='0' then '' ELSE LEFT(a.Код,1) END)+SUBSTRING(a.Код,2,4)+'. '+a.[ГП] as [Задачи]
                 , DS_2654 as [Вышестоящий документ]
                 , a.AT_2538 as DBegin
                 , a.AT_2539 as DEnd
                 , a.[SCode]
                 , a.[SNaim]
                 , a.[SLock]
                 --, #sprVed.AT_1797 as [Исполнитель.Наименование]
                 ,#sprVed1.AT_3200 as [Исполнитель.Наименование]
                 , a.[Код]

            into [#Задачи]
            from
                #GMP a inner join [#Подрограмма] b on a.DS_2654 = b.idppgp and b.action ='Задачи подпрограммы'
                    --left join #sprVed on #sprVed.ID = a.CL_2533
                       left join #sprVed1 on #sprVed1.id = a.CL_3252
            where
                    a.DocumentId = 229
            --elect * from #Задачи
            --Выбираем оргиниальное название задачи если она только 1
            if(select count(*) from #Задачи) = 1
                update z set z.[Задачи] = a.[ГП] from [#Задачи] as z left join #GMP a on (z.id = a.id)


            ---------------------------
            -- Показатели задач --
            ---------------------------
            select
                a.id
                 , a.DocumentID
                 , a.[ГП]+', '+a.AT_2544 as [ПоказательЗадачи]
                 , a1.[Вышестоящий документ] as [Вышестоящий документ]
                 , a1.Код as [Код задачи]
                 , 'Показатели задач подпрограммы' as action
                 , 1 as orderPPgp
                 , a.AT_2538 as DBegin
                 , a.AT_2539 as DEnd
                 ,a.[SCode]
                 ,a.[SNaim]
                 ,a.[SLock]
                 --,#sprVed.AT_1797 as [Исполнитель.Наименование]
                 ,#sprVed1.AT_3200 as [Исполнитель.Наименование]
                 , a.[Source]
                 , a.[ОЧГ13]
                 , a.[ОЧГ14]
                 , a.[ОЧГ15]
                 , a.[ОЧГ16]
                 , a.[ОЧГ17]
                 , a.[ОЧГ18]
                 , a.[ОЧГ19]
                 , a.[ОЧГ20]
                 , a.[ОЧГ21]
                 , a.[ОЧГ22]
                 , a.[ОЧГ23]
                 , a.[ОЧГ24]
                 , a.[ОЧГ25]
                 , a.[Код]
                 --, a.AT_2837
                 ,b.M_2936 as [Показатель2020]
                 ,a.AT_2544
            into [#Показателизадач]
            from
                #GMP a
                    inner join [#Задачи] a1 on a1.id = a.DS_2654
                    --left join #sprVed on #sprVed.ID = a.CL_2533 --Исполнитель
                    left join #sprVed1 on #sprVed1.id = a.CL_3252
                    left join KS_DDLControl.DS_957_2478 b on b.id_up=a.id
            where
                    a.DocumentId = 252

            --select * from [#Показателизадач]

            ---------------
            --Индикаторы --
            ---------------
            select
                a.id
                 , a.DocumentID
                 , (case when LEFT(a.Код,1)='0' then '' ELSE LEFT(a.Код,1) END)+SUBSTRING(a.Код,2,4)+'. '+a.[ГП] as [Индикаторы]
                 , a1.[Вышестоящий документ] as [Вышестоящий документ]
                 , a.AT_2538 as DBegin
                 , a.AT_2539 as DEnd
                 ,a.[SCode]
                 ,a.[SNaim]
                 ,a.[SLock]
                 ,#sprVed.AT_1797 as [Исполнитель.Наименование]
                 --,#sprVed1.AT_3200 as [Исполнитель.Наименование]
                 , a.[Код]
                 ,a.AT_2544
            into [#Индикаторы]
            from
                #GMP a
                    inner join  [#Цели] a1 on a1.id = a.DS_2654
                    left join #sprVed on #sprVed.ID = a.CL_2533
                --left join #sprVed1 on #sprVed1.id = a.CL_3254
            where
                    a.DocumentId = 252

            --Выбираем оргиниальное название индикатора если он только 1
            if(select count(*) from [#Индикаторы]) = 1
                update i set i.[Индикаторы] = a.[ГП] from [#Индикаторы] as i left join #GMP a on (i.id = a.id)


            --select * from #GMP
--select * from [#Подрограмма]
--select * from [#Индикаторы]
--select * from [#Показателизадач]

            --------------------------------------------------------
            --Объем средств краевого бюджета на финансирование ППГП --
            --------------------------------------------------------
            create table #Volume
            (  idППГП int
                , [Source] varchar(100)
                , [Код источника финансирования] varchar(100)
                , [ОЧГ13] numeric (20,2)
                , [ОЧГ14] numeric (20,2)
                , [ОЧГ15] numeric (20,2)
                , [ОЧГ16] numeric (20,2)
                , [ОЧГ17] numeric (20,2)
                , [ОЧГ18] numeric (20,2)
                , [ОЧГ19] numeric (20,2)
                , [ОЧГ20] numeric (20,2)
                , [ОЧГ21] numeric (20,2)
                , [ОЧГ22] numeric (20,2)
                , [ОЧГ23] numeric (20,2)
                , [ОЧГ24] numeric (20,2)
                , [ОЧГ25] numeric (20,2))
            --insert into #Volume select [Source], sum([ОЧГ13]), sum([ОЧГ14]), sum([ОЧГ15]), sum([ОЧГ16]), sum([ОЧГ17]) from #GMP where [Source] is not null group by [Source]
            insert into #Volume
            select  DS_2654 as  idППГП
                 ,[Source]
                 , [Код источника финансирования]
                 , sum(isnull([ОЧГ13],0.00))
                 , sum(isnull([ОЧГ14],0.00))
                 , sum(isnull([ОЧГ15],0.00))
                 , sum(isnull([ОЧГ16],0.00))
                 , sum(isnull([ОЧГ17],0.00))
                 , sum(isnull([ОЧГ18],0.00))
                 , sum(isnull([ОЧГ19],0.00))
                 , sum(isnull([ОЧГ20],0.00))
                 , sum(isnull([ОЧГ21],0.00))
                 , sum(isnull([ОЧГ22],0.00))
                 , sum(isnull([ОЧГ23],0.00))
                 , sum(isnull([ОЧГ24],0.00))
                 , sum(isnull([ОЧГ25],0.00))
            from
                #GMP
            group by
                DS_2654
                   ,[Source]
                   , [Код источника финансирования]
            delete from #Volume where [Source] is null
            update #Volume set Source = 'иные' where Source ='внебюджетные иные' --not in ('краевые','федеральные')
            update #Volume set Source = ' ' + Source where Source != 'иные'


            --select * from #GMP
--select * from [#Подрограмма]
--select * from #Volume


            -------------------------------------------------------
            --Объем средств  бюджета на финансирование ГП Всего --
            --------------------------------------------------------
            create table #Volume2
            (
                [ОЧГ13] numeric (20,2)
                , [ОЧГ14] numeric (20,2)
                , [ОЧГ15] numeric (20,2)
                , [ОЧГ16] numeric (20,2)
                , [ОЧГ17] numeric (20,2)
                , [ОЧГ18] numeric (20,2)
                , [ОЧГ19] numeric (20,2)
                , [ОЧГ20] numeric (20,2)
                , [ОЧГ21] numeric (20,2)
                , [ОЧГ22] numeric (20,2)
                , [ОЧГ23] numeric (20,2)
                , [ОЧГ24] numeric (20,2)
                , [ОЧГ25] numeric (20,2)
            )

            insert into #Volume2
            select
                sum([ОЧГ13])
                 , sum([ОЧГ14])
                 , sum([ОЧГ15])
                 , sum([ОЧГ16])
                 , sum([ОЧГ17])
                 , sum([ОЧГ18])
                 , sum([ОЧГ19])
                 , sum([ОЧГ20])
                 , sum([ОЧГ21])
                 , sum([ОЧГ22])
                 , sum([ОЧГ23])
                 , sum([ОЧГ24])
                 , sum([ОЧГ25])
            from #GMP  --where [Source] is not null group by [Source]


            -------------------
            -- Соисполнители М_ИСПОЛНИТЕЛИ-
            -------------------
            select distinct
                          --#sprVed.AT_1797 as [Исполнитель.Наименование]
                e.AT_3200 as [Исполнитель.Наименование]


                          , identity(int, 1,1) as idcontent
            into [#Soisp]
            from
                [#GMP] a
                    --left join #sprVed on #sprVed.id = a.CL_2556 -- макет ведомства
                    left join #sprVed1 e on e.id = a.CL_3252 -- м_соисполниели
            --left join #sprVed on #sprVed.id = a.CL_2533 -- макет ведомства


            -------------------
            -- Соисполнители ГРИД в ОМ-
            -------------------
            select distinct
                          --#sprVed.AT_1797 as [Исполнитель.Наименование]
                e.AT_3200 as [Исполнитель.Наименование]
                          ,c.CL_3205


                          , identity(int, 1,1) as idcontent
            into [#Soisp2]
            from
                [#GMP] a

                    left join KS_DDLControl.DS_957_3235 c on a.id =c.id_up
                    left join #sprVed1 e on e.id = c.CL_3205


            --select * from [#GMP]
--select * from #sprVed
--select * from #sprVed1
--select * from [#Soisp]
--select * from [#Soisp2]
--select * from [#Подрограмма]
--if(select COUNT(*) FROM #Soisp) > 1
--begin

            delete from #Soisp where [Исполнитель.Наименование] in (select distinct [Исполнитель.Наименование]  from [#Подрограмма])

            delete from #Soisp2 where [Исполнитель.Наименование] in (select distinct [Исполнитель.Наименование]  from [#Подрограмма])
            --end
            ---------------------------------------------
            --Передача данных в результирующую таблицу --
            ---------------------------------------------

            insert into #temp_resultPPGP
            select
                a.idppgp	--Гр1.Уровень1
                 , a.[ППГП]	--Гр1.Уровень1.Заголовок
                 , right('0' + convert(varchar,a.orderPPGP),2)	--Гр2.Уровень2.Наименование
                 , a.action	--Гр2.Уровень2.Наименование.Заголовок
                 , coalesce(c1.id,     t1.id,       i1.id, s.idcontent,           m1.id, z.idcontent )           as idContent		--Гр3.Уровень2.Содержание - Все что привязно к ГП

                 , (case
                        when a.action = 'Ответственный исполнитель и (или) соисполнители' then a.[Исполнитель.Наименование]
                        when a.action = 'Соисполнители подпрограммы' then s.[Исполнитель.Наименование]
--			    when @GP=6570 and a.action = 'Соисполнители  подпрограммы' then z.[Исполнитель.Наименование]
                        when  a.action = 'Соисполнители  подпрограммы' then z.[Исполнитель.Наименование]

                        when a.action = 'Этапы и сроки реализации подпрограммы'  and [Этапы]<>'' then left(convert(varchar,a.DBegin),4) + '-' + left(convert(varchar,a.DEnd),4) + ' годы' +'
				'+ [Этапы]
                        when a.action = 'Этапы и сроки реализации подпрограммы'   then left(convert(varchar,a.DBegin),4) + '-' + left(convert(varchar,a.DEnd),4) + ' годы'


                        when a.action = 'Ожидаемые результаты реализации подпрограммы' then  q.[AT_2644] else coalesce(c1.[Цели], t1.[Задачи], i1.[Индикаторы], m1.[ПоказательЗадачи])
                end) as [Content] 	--Гр3.Уровень2.Содержание.Заголовок
                 , coalesce(c1.[Код], t1.[Код], i1.[Код],m1.[Код задачи]) as [Код_Content]

                 , isnull(a.DBegin, 0)
                 --, isnull(a.DEnd  , 20201231)

                 ,(case
                       when a.action = 'Показатели задач подпрограммы' then isnull (m1.DEnd  , 20201231)
                       when a.action = 'Целевые индикаторы подпрограммы' then isnull (i1.DEnd  , 20201231)
                       else isnull(a.DEnd  , 20201231)
                end)


                 , @aDateInt as ParamDate
                 , a.[SNaim]

                 , v.[Source]
                 , v.[Код источника финансирования]
                 , isnull(v.[ОЧГ13], v2.[ОЧГ13])
                 , isnull(v.[ОЧГ14], v2.[ОЧГ14])
                 , isnull(v.[ОЧГ15], v2.[ОЧГ15])
                 , isnull(v.[ОЧГ16], v2.[ОЧГ16])
                 , isnull(v.[ОЧГ17], v2.[ОЧГ17])
                 , isnull(v.[ОЧГ18], v2.[ОЧГ18])
                 , isnull(v.[ОЧГ19], v2.[ОЧГ19])
                 , isnull(v.[ОЧГ20], v2.[ОЧГ20])
                 , isnull(v.[ОЧГ21], v2.[ОЧГ21])
                 , isnull(v.[ОЧГ22], v2.[ОЧГ22])
                 , isnull(v.[ОЧГ23], v2.[ОЧГ23])
                 , isnull(v.[ОЧГ24], v2.[ОЧГ24])
                 , isnull(v.[ОЧГ25], v2.[ОЧГ25])
                 , a.[Код]
                 , coalesce(c1.documentid, t1.documentid, i1.documentid)
                 , @Post
                 , m1. [Код]
                 ,(case
                       when a.action = 'Показатели задач подпрограммы' then m1.AT_2544
                       when a.action = 'Целевые индикаторы подпрограммы' then i1. AT_2544
                end) as [ЕдИзмерения]
                 ,''


            from
                [#Подрограмма] a --Уровень1
                    left join [#Soisp] s  on a.action = 'Соисполнители подпрограммы'
                    left join [#Soisp2] z  on a.action = 'Соисполнители  подпрограммы'
                    left join [#Цели] c1 on       a.idppgp = c1.[Вышестоящий документ] and a.action = 'Цели подпрограммы'           --Уровень2
                    left join [#Задачи] t1 on     a.idppgp = t1.[Вышестоящий документ] and a.action = 'Задачи подпрограммы'         --Уровень2
                    left join [#Показателизадач] m1 on a.idppgp  = m1.[Вышестоящий документ] and a.action = 'Показатели задач подпрограммы'
                    --or  a.action = 'Ожидаемые результаты реализации  подпрограммы '   --Уровень2
                    left join [#Индикаторы] i1 on a.idppgp = i1.[Вышестоящий документ] and a.action = 'Целевые индикаторы подпрограммы'     --Уровень2
                    Left Join KS_DDLControl.DS_957_2619 q on a.idppgp = q.id_up and a.action='Ожидаемые результаты реализации подпрограммы' -- Область_ввода
                    left join (select [Source]
                                    , [Код источника финансирования]
--					, sum(round(isnull([ОЧГ13],0),2)) as [ОЧГ13]
--					, sum(round(isnull([ОЧГ14],0),2)) as [ОЧГ14]
--					, sum(round(isnull([ОЧГ15],0),2)) as [ОЧГ15]
--					, sum(round(isnull([ОЧГ16],0),2)) as [ОЧГ16]
--					, sum(round(isnull([ОЧГ17],0),2)) as [ОЧГ17]
--					, sum(round(isnull([ОЧГ18],0),2)) as [ОЧГ18]
--					, sum(round(isnull([ОЧГ19],0),2)) as [ОЧГ19]
--					, sum(round(isnull([ОЧГ20],0),2)) as [ОЧГ20]
--					, sum(round(isnull([ОЧГ21],0),2)) as [ОЧГ21]
--					, sum(round(isnull([ОЧГ22],0),2)) as [ОЧГ22]
--					, sum(round(isnull([ОЧГ23],0),2)) as [ОЧГ23]
--					, sum(round(isnull([ОЧГ24],0),2)) as [ОЧГ24]
--					, sum(round(isnull([ОЧГ25],0),2)) as [ОЧГ25]
                                    , sum(isnull([ОЧГ13],0)) as [ОЧГ13]
                                    , sum(isnull([ОЧГ14],0)) as [ОЧГ14]
                                    , sum(isnull([ОЧГ15],0)) as [ОЧГ15]
                                    , sum(isnull([ОЧГ16],0)) as [ОЧГ16]
                                    , sum(isnull([ОЧГ17],0)) as [ОЧГ17]
                                    , sum(isnull([ОЧГ18],0)) as [ОЧГ18]
                                    , sum(isnull([ОЧГ19],0)) as [ОЧГ19]
                                    , sum(isnull([ОЧГ20],0)) as [ОЧГ20]
                                    , sum(isnull([ОЧГ21],0)) as [ОЧГ21]
                                    , sum(isnull([ОЧГ22],0)) as [ОЧГ22]
                                    , sum(isnull([ОЧГ23],0)) as [ОЧГ23]
                                    , sum(isnull([ОЧГ24],0)) as [ОЧГ24]
                                    , sum(isnull([ОЧГ25],0)) as [ОЧГ25]

                               from #Volume
                               group by [Source], [Код источника финансирования]

                ) v on a.orderPPGP = 10

                    left join (select
                                    --[Source]
                                   sum(isnull([ОЧГ13],0)) as [ОЧГ13]
                                    , sum(isnull([ОЧГ14],0)) as [ОЧГ14]
                                    , sum(isnull([ОЧГ15],0)) as [ОЧГ15]
                                    , sum(isnull([ОЧГ16],0)) as [ОЧГ16]
                                    , sum(isnull([ОЧГ17],0)) as [ОЧГ17]
                                    , sum(isnull([ОЧГ18],0)) as [ОЧГ18]
                                    , sum(isnull([ОЧГ19],0)) as [ОЧГ19]
                                    , sum(isnull([ОЧГ20],0)) as [ОЧГ20]
                                    , sum(isnull([ОЧГ21],0)) as [ОЧГ21]
                                    , sum(isnull([ОЧГ22],0)) as [ОЧГ22]
                                    , sum(isnull([ОЧГ23],0)) as [ОЧГ23]
                                    , sum(isnull([ОЧГ24],0)) as [ОЧГ24]
                                    , sum(isnull([ОЧГ25],0)) as [ОЧГ25]


                               from #Volume2 ) v2 on a.orderPPGP = 09 --and v.idППГП=a.idppgp


--delete from #temp_resultPPGP where GpDEnd < @aDateInt

            delete from #temp_resultPPGP  where @GP='6570' and orderPPGP = 02

            delete from #temp_resultPPGP where @GP<>'6570' and orderPPGP = 03

--Собираем текстовку для итоговых результатов
            declare @textResult varchar(max)
--select * from #temp_resultPPGP
            select
                p.*,
                --LOWER(LEFT(gmp.AT_2537, 1))+RIGHT(gmp.AT_2537,LEN(gmp.AT_2537)-1) as [наименование],
                LOWER(LEFT(gmp.AT_2537, 1))+SUBSTRING(gmp.AT_2537,2, LEN(gmp.AT_2537)-1) as [наименование],
                LEFT(gmp.AT_2538, 4) 												as [год_начала_реализации],
                LEFT(r.gpdend, 4) 						as [год_окончания_реализации],
                [М_Госпрограммы].cl_3586 				as [признак_количественности],
                cast(cast( case (LEFT(gmp.AT_2538, 4))
                               when '2012' then p.m_2643
                               when '2013' then p.m_2628
                               when '2014' then p.m_2629
                               when '2015' then p.m_2630
                               when '2016' then p.m_2631
                               when '2017' then p.m_2632
                               when '2018' then p.m_2934
                               when '2019' then p.m_2935
                               when '2020' then p.m_2936
                               when '2021' then p.m_3072
                               when '2022' then p.m_3339
                               when '2023' then p.m_3340
                               when '2024' then p.m_3341
                               when '2025' then p.m_3343
                               ELSE '0'
                    end as float) as varchar) 				as [показатель_начала_реализации],
                r.[ЕдиницаИзмерения] 						as [ед_измерения],
                gmp.at_3275 							as [показатель_направленности],
                case gmp.at_3275
                    when '1' then 'увеличится'
                    when '0' then 'снизится'
                    end 									as [направленность_текст],
                cast(cast(case (LEFT(r.gpdend, 4))
                              when '2012' then p.m_2643
                              when '2013' then p.m_2628
                              when '2014' then p.m_2629
                              when '2015' then p.m_2630
                              when '2016' then p.m_2631
                              when '2017' then p.m_2632
                              when '2018' then p.m_2934
                              when '2019' then p.m_2935
                              when '2020' then p.m_2936
                              when '2021' then p.m_3072
                              when '2022' then p.m_3339
                              when '2023' then p.m_3340
                              when '2024' then p.m_3341
                              when '2025' then p.m_3343
                              ELSE '0'
                    end as float) as varchar) 			as [показатель_окончания_реализации],
                cast(cast(p.m_3577 as float) as varchar) as [показатель_итого],

                gmp.at_2837 							as [порядковый_номер]
                --,r.[ПорядковыйНомерПЗППГП],
                --r.[код_Content]
            into #expectedResult
            from #temp_resultPPGP  r
                     left join ks_ddlcontrol.ds_957_2475 gmp on (gmp.id = r.idcontent)
                     left join ks_ddlcontrol.cl_956_2470 [М_Госпрограммы] on ([М_Госпрограммы].id = gmp.cl_2640)
                     left join ks_ddlcontrol.ds_957_2478 p on (p.id_up = r.idcontent)
            where (r.action = 'Целевые индикаторы подпрограммы' or r.action = 'Показатели задач подпрограммы') and r.idcontent is not null
            order by r.[код_Content], r.[ПорядковыйНомерПЗППГП]
--select * from  #expectedResult

            alter table #expectedResult
                ADD textResult
                    AS
                        (
                            case([признак_количественности])
                                when 356 then
                                        '- ' + [наименование] + ' до '+ [год_окончания_реализации] + ' года '+ ' - ' + [показатель_итого] + ' ' + [ед_измерения] + ';'+ char(10)
                                when 357 then
                                        '- ' + [наименование] + ' составит ' + [показатель_окончания_реализации] + ' ' + [ед_измерения] + ' в ' + [год_окончания_реализации] + ' году;'+ char(10)
                                else
                                        '- ' + [наименование] + ' с ' + [показатель_начала_реализации] + ' ' + [ед_измерения] + ' в ' + [год_начала_реализации] + ' году ' + [направленность_текст] + ' до '+ [показатель_окончания_реализации] + ' ' + [ед_измерения] + ' в ' + [год_окончания_реализации] + ' году;' + char(10)
                                end
                            );



            --select * from #expectedResult

--Собираем текстовку в строку
            select @textResult = (select  textResult as 'data()' from #expectedResult FOR XML PATH(''))
            if LEN(@textResult) > 2
                begin
                    set @textResult = (LEFT(@textResult, LEN(@textResult)-2))
                end

--Обновляем источник - добавляем собранную строку
            UPDATE #temp_resultPPGP SET content = 'В количественном выражении: ' + char(10)+ @textResult WHERE action = 'Ожидаемые результаты реализации подпрограммы' and @Result = 'Да';


--Выставляем source для ordergp 09
            UPDATE #temp_resultPPGP   SET source = '1', [Код источника финансирования] = '001'   WHERE orderppgp = '09';


        end

--Получаем количество показателей по каждой задаче
select
    count (distinct content)  	as [количество_показателей],
    код_content

into #countInd
from #temp_resultPPGP
where action = 'Показатели задач подпрограммы'
group by код_content

    --select * from #countInd

--Собираем наименования
UPDATE
    p
SET p.[ПоказательЗадачи] = 	case i.[количество_показателей]
                                  when 1 then 'Показатель задачи ' + cast(cast(p.код_content as int) as varchar(4))
                                  else
                                      --'Показатель ' + cast(cast(p.[ПорядковыйНомерПЗППГП] as int)as varchar(4)) + ' задачи ' + cast(cast(p.код_content as int)as varchar(4))
                                          'Показатель ' +
                                          case when left(p.[ПорядковыйНомерПЗППГП],1)='0' and len(p.[ПорядковыйНомерПЗППГП])='5' then right(p.[ПорядковыйНомерПЗППГП],4)
                                               when left(p.[ПорядковыйНомерПЗППГП],1)='0' and len(p.[ПорядковыйНомерПЗППГП])='4' then right(p.[ПорядковыйНомерПЗППГП],3)
                                               when left(p.[ПорядковыйНомерПЗППГП],1)='0' and len(p.[ПорядковыйНомерПЗППГП])='2' then right(p.[ПорядковыйНомерПЗППГП],1)
                                               else p.[ПорядковыйНомерПЗППГП] end  + ' задачи ' +
                                          case when left(p.код_content,1)='0' and len(p.код_content)='5' then right(p.код_content,4)
                                               when left(p.код_content,1)='0' and len(p.код_content)='4' then right(p.код_content,3)
                                               when left(p.код_content,1)='0' and len(p.код_content)='2' then right(p.код_content,1)
                                               else p.код_content end
    end


from #temp_resultPPGP p
         left join #countInd i on (p.код_content = i.код_content)
where p.action = 'Показатели задач подпрограммы'


update #temp_resultPPGP set action = 'Ответственный исполнитель и (или) соисполнители' where  action = 'Соисполнители подпрограммы'
    if @ToTable=1
        begin
            insert into #ToTable select * from #temp_resultPPGP
        end else
        begin
            select * from #temp_resultPPGP order by id
        end