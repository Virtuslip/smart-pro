alter PROC [KS_DDLControl].[xp_GPpril7_svod_new] (
    @nmode INT = 0
    ,@GP INT = 6575
    ,@Ver01 VARCHAR(2) = '01'
    ,@Ver02 VARCHAR(2) = '02'
    ,@Data_ver1 VARCHAR(10) = '20170101'
    ,@Data_ver_ks_1 VARCHAR(10) = '20170101'
    ,@Data_ver2 VARCHAR(10) = '20170606'
    ,@Data_ver_ks_2 VARCHAR(10) = '20170101'
    ,@OnlyChanges VARCHAR(3) = 'Да'
    )
    --exec  [KS_DDLControl].[xp_GPpril7_svod_new] @nmode = 0, @GP  = 6577, @Ver01  = '03', @Ver02  = '03',@Data_ver1  = '20180514' ,@Data_ver_ks_1  = '20180514' ,@Data_ver2  = '20180801' ,@Data_ver_ks_2  = '20180801' , @OnlyChanges  = 'Нет'
    AS
    CREATE TABLE #temp_result (
                                  NAME VARCHAR(50)
        ,Doc VARCHAR(2000)
        ,ДатаНачала VARCHAR(50) --datetime
        ,ДатаОконч VARCHAR(50)
        ,ДатаНаступления VARCHAR(50) --1
        ,[KBK_Ved] VARCHAR(3)
        ,[KBK_Div] VARCHAR(4)
        ,[KBK_CS] VARCHAR(10)
        ,[KBK_VR] VARCHAR(3)
        ,[KBK_ITEM] VARCHAR(3)
        ,[KBK_ADD_BK] VARCHAR(10)
        --,[ОЧГ13] NUMERIC(20, 2)
        --,[ОЧГ14] NUMERIC(20, 2)
        --,[ОЧГ15] NUMERIC(20, 2)
        --,[ОЧГ16] NUMERIC(20, 2)
        --,[ОЧГ17] NUMERIC(20, 2)
        --,[ОЧГ18] NUMERIC(20, 2)
        --,[ОЧГ19] NUMERIC(20, 2)
        --,[ОЧГ20] NUMERIC(20, 2)
        --,[ОЧГ21] NUMERIC(20, 2)
        ,[Isp] VARCHAR(2000)
        ,DocId INT
        ,Num VARCHAR(50) --Номер в разрезе групп 01.01.01.02
        ,[ОЧГ17фед] NUMERIC(20, 2)
        ,[ОЧГ17обл] NUMERIC(20, 2)
        ,[ОЧГ17итог] NUMERIC(20, 2)
        ,[КС_показатель] NUMERIC(20, 2)
        ,ДатаДокумента VARCHAR(50)
        ,[КС_ЕдИзмерения] VARCHAR(50) --11.07
        ,[Порядковый номер] VARCHAR(50)
        ,serial INT
        ,ПорядковыйНомерКС VARCHAR(10)
        --,[БР] NUMERIC(20, 2)
        ,NAME_1 VARCHAR(50)
        ,Doc_1 VARCHAR(2000)
        ,ДатаНачала_1 VARCHAR(50)
        ,ДатаОконч_1 VARCHAR(50)
        ,ДатаНаступления_1 VARCHAR(50)
        ,[KBK_Ved_1] VARCHAR(3)
        ,[KBK_Div_1] VARCHAR(4)
        ,[KBK_CS_1] VARCHAR(10)
        ,[KBK_VR_1] VARCHAR(3)
        ,[KBK_ITEM_1] VARCHAR(3)
        ,[KBK_ADD_BK_1] VARCHAR(10)
        --,[ОЧГ13_1] NUMERIC(20, 2)
        --,[ОЧГ14_1] NUMERIC(20, 2)
        --,[ОЧГ15_1] NUMERIC(20, 2)
        --,[ОЧГ16_1] NUMERIC(20, 2)
        --,[ОЧГ17_1] NUMERIC(20, 2)
        --,[ОЧГ18_1] NUMERIC(20, 2)
        --,[ОЧГ19_1] NUMERIC(20, 2)
        --,[ОЧГ20_1] NUMERIC(20, 2)
        --,[ОЧГ21_1] NUMERIC(20, 2)
        ,[Isp_1] VARCHAR(2000)
        ,DocId_1 INT
        ,Num_1 VARCHAR(50)
        ,[ОЧГ17фед_1] NUMERIC(20, 2)
        ,[ОЧГ17обл_1] NUMERIC(20, 2)
        ,[ОЧГ17итог_1] NUMERIC(20, 2)
        ,[КС_показатель_1] NUMERIC(20, 2)
        ,ДатаДокумента_1 VARCHAR(50)
        ,[КС_ЕдИзмерения_1] VARCHAR(50) --11.07
        ,[Порядковый номер1] VARCHAR(50)
        ,serial1 INT
        ,ПорядковыйНомерКС_1 VARCHAR(10)
        ,documentid INT
        ,Post VARCHAR(max)
        ,id INT identity(1, 1)
        ,Примечание VARCHAR(2000)
        ,Файл VARCHAR(2000)
        ,ИмяФайла VARCHAR(2000)
    )

    IF @nmode <> - 1
        BEGIN
            SET NOCOUNT ON
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

            CREATE TABLE #t_GPpril (
                                       documentid INT
                ,NAME VARCHAR(50)
                ,Doc VARCHAR(2000)
                ,ДатаНачала VARCHAR(50)
                ,ДатаОконч VARCHAR(50)
                ,ДатаНаступления VARCHAR(50)
                ,[KBK_Ved] VARCHAR(3)
                ,[KBK_Div] VARCHAR(4)
                ,[KBK_CS] VARCHAR(10)
                ,[KBK_VR] VARCHAR(3)
                ,[KBK_ITEM] VARCHAR(3)
                ,[KBK_ADD_BK] VARCHAR(10)
                --,[ОЧГ13] NUMERIC(20, 2)
                --,[ОЧГ14] NUMERIC(20, 2)
                --,[ОЧГ15] NUMERIC(20, 2)
                --,[ОЧГ16] NUMERIC(20, 2)
                --,[ОЧГ17] NUMERIC(20, 2)
                --,[ОЧГ18] NUMERIC(20, 2)
                --,[ОЧГ19] NUMERIC(20, 2)
                --,[ОЧГ20] NUMERIC(20, 2)
                --,[ОЧГ21] NUMERIC(20, 2)
                ,[Isp] VARCHAR(2000)
                ,DocId INT
                ,Num VARCHAR(50)
                ,Post VARCHAR(max)
                ,id INT
                ,[ОЧГ17фед] NUMERIC(20, 2)
                ,[ОЧГ17обл] NUMERIC(20, 2)
                ,[ОЧГ17итог] NUMERIC(20, 2)
                ,[КС_показатель] NUMERIC(20, 2)
                ,ДатаДокумента VARCHAR(50)
                ,[КС_ЕдИзмерения] VARCHAR(50)
                ,[Порядковый номер] VARCHAR(50)
                ,serial INT
                ,ПорядковыйНомерКС VARCHAR(10)
                ,Примечание VARCHAR(2000)
                ,Файл VARCHAR(2000)
                ,ИмяФайла VARCHAR(2000)
            )

            --,[БР] NUMERIC(20, 2)
            CREATE TABLE #Ver01 (
                                    documentid INT
                ,NAME VARCHAR(50)
                ,Doc VARCHAR(2000)
                ,ДатаНачала VARCHAR(50)
                ,ДатаОконч VARCHAR(50)
                ,ДатаНаступления VARCHAR(50)
                ,[KBK_Ved] VARCHAR(3)
                ,[KBK_Div] VARCHAR(4)
                ,[KBK_CS] VARCHAR(10)
                ,[KBK_VR] VARCHAR(3)
                ,[KBK_ITEM] VARCHAR(3)
                ,[KBK_ADD_BK] VARCHAR(10)
                --,[ОЧГ13] NUMERIC(20, 2)
                --,[ОЧГ14] NUMERIC(20, 2)
                --,[ОЧГ15] NUMERIC(20, 2)
                --,[ОЧГ16] NUMERIC(20, 2)
                --,[ОЧГ17] NUMERIC(20, 2)
                --,[ОЧГ18] NUMERIC(20, 2)
                --,[ОЧГ19] NUMERIC(20, 2)
                --,[ОЧГ20] NUMERIC(20, 2)
                --,[ОЧГ21] NUMERIC(20, 2)
                ,[Isp] VARCHAR(2000)
                ,DocId INT
                ,Num VARCHAR(50)
                ,Post VARCHAR(max)
                ,id INT
                ,[ОЧГ17фед] NUMERIC(20, 2)
                ,[ОЧГ17обл] NUMERIC(20, 2)
                ,[ОЧГ17итог] NUMERIC(20, 2)
                ,[КС_показатель] NUMERIC(20, 2)
                ,ДатаДокумента VARCHAR(50)
                ,[КС_ЕдИзмерения] VARCHAR(50)
                ,[Порядковый номер] VARCHAR(50)
                ,serial INT
                ,ПорядковыйНомерКС VARCHAR(10)
                ,Примечание VARCHAR(2000)
                ,Файл VARCHAR(2000)
                ,ИмяФайла VARCHAR(2000)
            )

            --,[БР] NUMERIC(20, 2)
            INSERT INTO #Ver01
                EXEC [KS_DDLControl].[xp_GPpril7_work_new] @nmode = 0
                    ,@GP = @GP
                    ,@Ver = @Ver01
                    ,@Data_n = @Data_ver1
                    ,@Data_ks = @Data_ver_ks_1 --, @ToTable=1
            --insert into #Ver01 select * from #t_GPpril --where ДатаДокумента= @Data_ver1

            --	delete from #t_GPpril
            --select * from #Ver01
            CREATE TABLE #Ver02 (
                                    documentid INT
                ,NAME VARCHAR(50)
                ,Doc VARCHAR(2000)
                ,ДатаНачала VARCHAR(50)
                ,ДатаОконч VARCHAR(50)
                ,ДатаНаступления VARCHAR(50)
                ,[KBK_Ved] VARCHAR(3)
                ,[KBK_Div] VARCHAR(4)
                ,[KBK_CS] VARCHAR(10)
                ,[KBK_VR] VARCHAR(3)
                ,[KBK_ITEM] VARCHAR(3)
                ,[KBK_ADD_BK] VARCHAR(10)
                --,[ОЧГ13] NUMERIC(20, 2)
                --,[ОЧГ14] NUMERIC(20, 2)
                --,[ОЧГ15] NUMERIC(20, 2)
                --,[ОЧГ16] NUMERIC(20, 2)
                --,[ОЧГ17] NUMERIC(20, 2)
                --,[ОЧГ18] NUMERIC(20, 2)
                --,[ОЧГ19] NUMERIC(20, 2)
                --,[ОЧГ20] NUMERIC(20, 2)
                --,[ОЧГ21] NUMERIC(20, 2)
                ,[Isp] VARCHAR(2000)
                ,DocId INT
                ,Num VARCHAR(50)
                ,Post VARCHAR(max)
                ,id INT
                ,[ОЧГ17фед] NUMERIC(20, 2)
                ,[ОЧГ17обл] NUMERIC(20, 2)
                ,[ОЧГ17итог] NUMERIC(20, 2)
                ,[КС_показатель] NUMERIC(20, 2)
                ,ДатаДокумента VARCHAR(50)
                ,[КС_ЕдИзмерения] VARCHAR(50)
                ,[Порядковый номер] VARCHAR(50)
                ,serial INT
                ,ПорядковыйНомерКС VARCHAR(10)
                ,Примечание VARCHAR(2000)
                ,Файл VARCHAR(2000)
                ,ИмяФайла VARCHAR(2000)
            )

            --,[БР] NUMERIC(20, 2)
            INSERT INTO #Ver02
                EXEC [KS_DDLControl].[xp_GPpril7_work_new] @nmode = 0
                    ,@GP = @GP
                    ,@Ver = @Ver02
                    ,@Data_n = @Data_ver2
                    ,@Data_ks = @Data_ver_ks_2 --, @ToTable=1
            --	insert into #Ver02 select * from #t_GPpril

            --select * from #t_GPpril
            --select * from #Ver01
            --select * from #Ver02
            INSERT INTO #temp_result
            SELECT a.NAME
                 ,a.Doc
                 ,a.ДатаНачала
                 ,a.ДатаОконч
                 ,a.ДатаНаступления
                 ,a.[KBK_Ved]
                 ,a.[KBK_Div]
                 ,a.[KBK_CS]
                 ,a.[KBK_VR]
                 ,a.[KBK_ITEM]
                 ,a.[KBK_ADD_BK]
                 --,a.[ОЧГ13]
                 --,a.[ОЧГ14]
                 --,a.[ОЧГ15]
                 --,a.[ОЧГ16]
                 --,a.[ОЧГ17]
                 --,a.[ОЧГ18]
                 --,a.[ОЧГ19]
                 --,a.[ОЧГ20]
                 --,a.[ОЧГ21]
                 ,a.[Isp]
                 ,a.DocId
                 ,a.Num
                 ,a.[ОЧГ17фед]
                 ,a.[ОЧГ17обл]
                 ,a.[ОЧГ17итог]
                 ,a.[КС_показатель]
                 ,a.ДатаДокумента
                 ,a.[КС_ЕдИзмерения]
                 ,a.[Порядковый номер]
                 ,a.serial
                 ,a.ПорядковыйНомерКС
                 ,b.NAME
                 ,b.Doc
                 ,b.ДатаНачала
                 ,b.ДатаОконч
                 ,b.ДатаНаступления
                 ,b.[KBK_Ved]
                 ,b.[KBK_Div]
                 ,b.[KBK_CS]
                 ,b.[KBK_VR]
                 ,b.[KBK_ITEM]
                 ,b.[KBK_ADD_BK]
                 --,b.[ОЧГ13]
                 --,b.[ОЧГ14]
                 --,b.[ОЧГ15]
                 --,b.[ОЧГ16]
                 --,b.[ОЧГ17]
                 --,b.[ОЧГ18]
                 --,b.[ОЧГ19]
                 --,b.[ОЧГ20]
                 --,b.[ОЧГ21]
                 ,b.[Isp]
                 ,b.DocId
                 ,b.Num
                 ,b.[ОЧГ17фед]
                 ,b.[ОЧГ17обл]
                 ,b.[ОЧГ17итог]
                 ,b.[КС_показатель]
                 ,b.ДатаДокумента
                 ,b.[КС_ЕдИзмерения]
                 ,b.[Порядковый номер]
                 ,b.serial
                 ,b.ПорядковыйНомерКС
                 ,isnull(a.documentid, b.documentid)
                 ,isnull(a.Post, b.Post)
                 ,isnull(a.Примечание, b.Примечание)
                 ,isnull(a.Файл, b.Файл)
                 ,isnull(a.ИмяФайла, b.ИмяФайла)
                 -- , isnull(a.id,b.id)
            FROM #Ver01 a
                     FULL JOIN #Ver02 b ON
                            isnull(a.doc, '') + isnull(a.ПорядковыйНомерКС, 0) = isnull(b.doc, '')+isnull(b.ПорядковыйНомерКС, 0)
                    --AND isnull(a.Isp, '') = isnull(b.Isp, '')
                    --and isnull(a.[Name]      ,'') = isnull(b.[Name]     ,'')
                    AND isnull(a.[KBK_Ved], '') = isnull(b.[KBK_Ved], '')
                    AND isnull(a.Isp, '') = isnull(b.Isp, '')
                    AND isnull(a.[KBK_Div], '') = isnull(b.[KBK_Div], '')
                    AND isnull(a.[KBK_CS], '') = isnull(b.[KBK_CS], '')
                 --AND isnull(a.[KBK_Div], '') = isnull(b.[KBK_Div], '')
                 --and isnull(a.[ОЧГ17фед]  , 0) = isnull(b.[ОЧГ17фед] , 0)
                 --and isnull(a.[ОЧГ17обл]    , 0) = isnull(b.[ОЧГ17обл]   , 0)
                 --and isnull(a.[ОЧГ17итог]    , 0) = isnull(b.[ОЧГ17итог]   , 0)
                 --and isnull(a.[ОЧГ17]    , 0) = isnull(b.[ОЧГ17]   , 0)
                 --AND isnull(a.[КС_Показатель], 0) = isnull(b.[КС_Показатель], 0)
                 --AND isnull(a.[ДатаНаступления], 0) = isnull(b.[ДатаНаступления], 0) -- ошибка при корректировке дат
                 --and isnull(a.[Порядковый номер], 0) = isnull(b.[Порядковый номер], 0)
                 --and isnull(a.ПорядковыйНомерКС, 0) = isnull(b.ПорядковыйНомерКС, 0)

                 --	ORDER BY isnull(a.Num, b.Num)

                 --ORDER BY b.[Порядковый номер], a.[Порядковый номер]
            ORDER BY isnull(b.serial, a.serial), a.serial
            --ORDER BY a.serial
            --ORDER BY b.serial
        END

    IF @OnlyChanges = 'Да'
        BEGIN
            SELECT *
            FROM #temp_result
            WHERE isnull(Doc, '') != isnull(Doc_1, '')
               OR isnull(Isp, '') != isnull(Isp_1, '')
               OR isnull([KBK_CS], 0) != isnull([KBK_CS_1], 0)
               OR isnull([KBK_Ved], '') != isnull([KBK_Ved_1], '')
               OR isnull([KBK_Div], '') != isnull([KBK_Div_1], '')
               OR isnull(ОЧГ17фед, 0) != isnull(ОЧГ17фед_1, 0)
               OR isnull(ОЧГ17обл, 0) != isnull(ОЧГ17обл_1, 0)
               OR isnull(ОЧГ17итог, 0) != isnull(ОЧГ17итог_1, 0)
               OR isnull([КС_Показатель], 0) != isnull([КС_Показатель_1], 0)
               OR isnull([ДатаОконч], 0) != isnull([ДатаОконч_1], 0)
               OR isnull([ДатаНаступления], 0) != isnull([ДатаНаступления_1], 0) -- ошибка при корректировке дат
        END
    ELSE
        BEGIN
            SELECT *
            FROM #temp_result --order by serial order by id
        END